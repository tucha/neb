#include "commentsmanager.h"

CommentsManager::CommentsManager(QObject* parent, RequestPriority priority):
    BookmarksManagerBase( parent, priority )
{

}

CommentsManager::~CommentsManager()
{
    Clear();
}

QString CommentsManager::Get(int pageIndex, QString resPath)
{
    return m_bookmarks.Get( pageIndex, resPath ).Text();
}


void CommentsManager::SendRequest(int pageIndex, QString resPath, QString text)
{
    DebugAssert( pageIndex >= 0 );

    DebugAssert( IsActual( pageIndex, resPath ) );
    if( !IsActual( pageIndex, resPath ) ) return;

    if( Get( pageIndex, resPath ) == text  ) return;

    if( Contains( pageIndex, resPath ))
    {
        if( text.isEmpty() ) RequestRemoval( pageIndex, resPath );
        else RequestModification( pageIndex, resPath, text );
    }
    else
    {
        if( text.isEmpty() );
        else RequestAddition( pageIndex, resPath, text );
    }
}

Bookmarks CommentsManager::Validate(Bookmarks bookmarks)
{
    QHash<int,Bookmark> resultIndex;
    QHash<QString,Bookmark> resultResPath;

    foreach( Bookmark b, bookmarks )
    {
        int page = b.PageIndex();
        if(b.ResPath() == "")
        {
            if( resultIndex.contains( page ) )
            {
                if( b.IsOlderThan( resultIndex.value( page ) ) ) resultIndex.insert( page, b );
            }
            else
            {
                resultIndex.insert( page, b );
            }
        }
        else
        {
            QString resPath = b.ResPath();
            if( resultResPath.contains( resPath ) )
            {
                if( b.IsOlderThan( resultResPath.value( resPath ) ) ) resultResPath.insert( resPath, b );
            }
            else
            {
                resultResPath.insert( resPath, b );
            }
        }
    }

    return resultIndex.values() + resultResPath.values();
}

Bookmark CommentsManager::Validate(Bookmark bookmark)
{
    return bookmark;
}

void CommentsManager::EmitUpdate()
{
    emit SignalUpdate( this );
}

void CommentsManager::EmitLoaded()
{
    emit SignalLoaded( this );
}

void CommentsManager::SlotFinishedModificationStep1(ErrorCode error, Bookmark result)
{
    if( IsEmpty() ) return;

    BookmarksAddRequest* s = static_cast<BookmarksAddRequest*>( sender() );
    if( m_addRequests.contains( s ) == false ) return;

    if( error != API_NO_ERROR )
    {
//        s->Send();
        return;
    }

    m_addRequests.remove( s );
    if(s->ResPath() == "")
    {
        m_addPages.remove( s->PageIndex() );
        Bookmark oldBookmark = m_bookmarks.Get( result.PageIndex(), "" );
        m_bookmarks.Remove( oldBookmark );
        m_bookmarks.Add( Validate( result ) );

        BookmarksDeleteRequest* r = BaseRequest()->NewCommentsDelete( oldBookmark, Priority() );
        connect(r,SIGNAL(SignalFinished(ErrorCode)),this,SLOT(SlotFinishedModificationStep2(ErrorCode)));
        m_removeRequests.insert( r );
        m_removePages.insert( r->GetBookmark().PageIndex() );
        r->Send();
    }
    else
    {
        m_addResPaths.remove(s->ResPath());

        Bookmark oldBookmark = m_bookmarks.Get( result.PageIndex(), result.ResPath() );
        m_bookmarks.Remove( oldBookmark );
        m_bookmarks.Add( Validate( result ) );

        BookmarksDeleteRequest* r = BaseRequest()->NewCommentsDelete( oldBookmark, Priority() );
        connect(r,SIGNAL(SignalFinished(ErrorCode)),this,SLOT(SlotFinishedModificationStep2(ErrorCode)));
        m_removeRequests.insert( r );
        m_removeResPaths.insert( r->GetBookmark().ResPath() );
        r->Send();
    }

    DeleteLaterAndNull( s );
}

void CommentsManager::SlotFinishedModificationStep2(ErrorCode error)
{
    if( IsEmpty() ) return;

    BookmarksDeleteRequest* s = static_cast<BookmarksDeleteRequest*>( sender() );
    if( m_removeRequests.contains( s ) == false ) return;

    if( error != API_NO_ERROR )
    {
//        s->Send();
        return;
    }

    m_removeRequests.remove( s );

    if(s->GetBookmark().ResPath() == "")
        m_removePages.remove( s->GetBookmark().PageIndex() );
    else
        m_removeResPaths.remove(s->GetBookmark().ResPath());
    DeleteLaterAndNull( s );

    EmitUpdate();
}

void CommentsManager::RequestModification(int pageIndex, QString resPath, QString text)
{
    DebugAssert( Contains( pageIndex, resPath ) );
    if(! Contains( pageIndex, resPath ) ) return;

    BookmarksAddRequest* r = BaseRequest()->NewCommentsAdd( GetDocument(), pageIndex, resPath, text, Priority() );
    connect(r,SIGNAL(SignalFinished(ErrorCode,Bookmark)),this,SLOT(SlotFinishedModificationStep1(ErrorCode,Bookmark)));
    connect(r,SIGNAL(SignalError()),this,SLOT(SlotErrorAddRequest()));
    m_addRequests.insert( r );
    m_addPages.insert( pageIndex );
    r->Send();
    EmitUpdate();
}

BookmarksGetRequest*CommentsManager::CreateGetRequest()
{
    return BaseRequest()->NewCommentsGetForAllPages( GetDocument(), Priority() );
}

BookmarksAddRequest*CommentsManager::CreateAddRequest(int pageIndex, QString resPath, QString text)
{
    DebugAssert( text.isEmpty() == false );
    return BaseRequest()->NewCommentsAdd( GetDocument(), pageIndex, resPath, text, Priority() );
}

BookmarksDeleteRequest*CommentsManager::CreateRemoveRequest(int pageIndex, QString resPath)
{

    return BaseRequest()->NewCommentsDelete( m_bookmarks.Get( pageIndex, resPath ), Priority() );
}
