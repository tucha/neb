#ifndef BOOKMARKSGETREQUEST_H
#define BOOKMARKSGETREQUEST_H

#include "accountbaserequest.h"
#include "document.h"
#include "bookmark.h"

#define ALL_PAGES -1

class BookmarksGetRequest : public AccountBaseRequest
{
	Q_OBJECT
public:
	explicit BookmarksGetRequest(QObject *parent, QNetworkAccessManager* manager, QString baseUrl,
								 User user, Document document, int pageIndex, bool comment, RequestPriority priority = PRIORITY_NORMAL );

	Document GetDocument(void)const;
	int PageIndex(void)const;
	bool AllPages(void)const;
	bool IsComment(void)const;

	void Send(void);

signals:
	void SignalFinished( ErrorCode error, Bookmarks result );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

protected:
	virtual QNetworkReply* SendInternal( QString requestString );

private:
	Bookmarks ParseResult(QByteArray data );

	const Document m_document;
	const int m_pageIndex;
	const bool m_comment;
};

#endif // BOOKMARKSGETREQUEST_H
