#include "requestsearch.h"

#include "defines.h"
#include <QJsonDocument>
#include <QJsonParseError>
RequestDocumentSearch::RequestDocumentSearch(QObject* parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority, QStringList phraseList):
    Request( parent, manager, identifier, document, priority )
{
    m_phraseList = phraseList;
    connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

void RequestDocumentSearch::Send()
{
    QString requestString = QString( "%1/%2/%3" )
                            .arg( "document" )
                            .arg( "search" )
                            .arg(QString::fromUtf8(QUrl::toPercentEncoding(m_phraseList.join("|") + " ")));
//    qDebug() << QString::fromUtf8(QUrl::toPercentEncoding(m_phraseList.join("|") + " "));
    ApiBase::Send( requestString );
}

void RequestDocumentSearch::SlotFinished(ErrorCode error, QByteArray data)
{
    QJsonParseError js_error;
    auto jd = QJsonDocument::fromJson(data, &js_error);
    if(js_error.error != QJsonParseError::NoError)
    {
        qDebug() << js_error.errorString();
        emit SignalFinished( error, QVariantMap(), true );
        return;

    }
    auto l = jd.toVariant().toList();
    if(l.isEmpty())
    {
        emit SignalFinished( error, QVariantMap(), true );
        return;
    }
    if(l[0].type() != QVariant::Map)
    {
        emit SignalFinished( error, QVariantMap(), true );
        return;
    }

    emit SignalFinished( error, l[0].toMap(), false );
    qDebug() << QString::fromUtf8(data);
}
