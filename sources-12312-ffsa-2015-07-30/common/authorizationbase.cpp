#include "authorizationbase.h"

#include <QAuthenticator>

#include "authorizationrequest.h"
#include "authorizationcheck.h"
#include "AnonymToken.h"

AuthorizationBase::AuthorizationBase(QObject* parent, QNetworkAccessManager* manager, QString baseUrl, RequestPriority priority):
	ApiBase( parent, manager, baseUrl, priority )
{
#ifdef WINDOWS_PHONE
	m_workaround = true;
#endif
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

AuthorizationRequest*AuthorizationBase::NewRequest(QString email, QString password, RequestPriority priority ) const
{
	return new AuthorizationRequest( parent(), Manager(), BaseUrl(), priority, email, password );
}


AnonymToken* AuthorizationBase::NewAnonym(RequestPriority priority ) const
{
    return new AnonymToken( parent(), Manager(), BaseUrl(), priority);
}


AuthorizationCheck*AuthorizationBase::NewCheck(QString token, RequestPriority priority) const
{
	return new AuthorizationCheck( parent(), Manager(), BaseUrl(), priority, token );
}

void AuthorizationBase::SlotFinished(ErrorCode error, QByteArray data)
{
	if( error != API_NO_ERROR ) emit SignalFinished( error, AuthorizationResult() );
	else emit SignalFinished( error, AuthorizationResult( data ) );
}

QNetworkReply* AuthorizationBase::SendInternal(QString requestString)
{
//    qDebug() << "############SSO: "<< BaseUrl();
	QUrl url = QUrl( QString( "%1/%2/%3/" )
				  .arg( BaseUrl() )
				  .arg( "sso" )
				  .arg( requestString ) );

	QNetworkRequest request;
	request.setUrl( url );
	request.setPriority( Priority() );
	request.setHeader( QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded" );

#if QT_VERSION >= 0x050000
	QByteArray data = GetParameters().query().toUtf8();
#else
	QByteArray data;
	QList<QPair<QString, QString> > prms = GetParameters();
	for( QList<QPair<QString, QString> >::iterator iter = prms.begin(); iter != prms.end(); ++iter )
	{
		data.append( iter->first + url.queryValueDelimiter() + iter->second + url.queryPairDelimiter() );
	}
	if( data.endsWith( url.queryPairDelimiter() ) ) data.remove( data.length() - 1, 1 );
#endif
	return Manager()->post( request, data );
}
