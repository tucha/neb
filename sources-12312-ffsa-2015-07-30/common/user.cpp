#include "user.h"

#include "defines.h"

#define KEY_ID				"ID"
#define KEY_NAME			"NAME"
#define KEY_LAST_NAME		"LAST_NAME"
#define KEY_EMAIL			"EMAIL"
#define KEY_DATE_REGISTER	"DATE_REGISTER"
#define KEY_SECOND_NAME		"SECOND_NAME"
#define KEY_TOKEN			"TOKEN"
#define KEY_STATUS			"STATUS"
#define KEY_IP_IS_ALLOWED	"IP_IS_ALLOWED"
#define KEY_ROLEGROUPS	     "GROUPS"

#define DATE_FORMAT	"dd.MM.yyyy hh:mm:ss"

User::User(QVariantMap map):
	m_map( map ),
	m_session()
{
	if( Check() == false )
	{
		m_map.clear();
		DebugAssert( IsEmpty() );
	}
}

bool User::IsEmpty() const
{
	return m_map.isEmpty();
}

uint User::GetId() const
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return 0;

	return m_map.value( KEY_ID ).toUInt();
}

QString User::GetName() const
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return QString();

	return m_map.value( KEY_NAME ).toString();
}

QString User::GetLastName() const
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return QString();

	return m_map.value( KEY_LAST_NAME ).toString();
}

QString User::GetEmail() const
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return QString();

	return m_map.value( KEY_EMAIL ).toString();
}

QDateTime User::GetDateRegister() const
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return QDateTime();

	return QDateTime::fromString( m_map.value( KEY_DATE_REGISTER ).toString(), DATE_FORMAT );
}

QString User::GetSecondName() const
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return QString();

	return m_map.value( KEY_SECOND_NAME ).toString();
}

QString User::GetToken() const
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return QString();

	return m_map.value( KEY_TOKEN ).toString();
}

void User::SetStatus(UserStatus status)
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return;

	DebugAssert( HasStatus() == false );
	if( HasStatus() ) return;

	m_map.insert( KEY_STATUS, status );
}

void User::SetIpIsAllowed(bool value)
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return;

	DebugAssert( HasIpIsAllowedValue() == false );
	if( HasIpIsAllowedValue() ) return;

	m_map.insert( KEY_IP_IS_ALLOWED, value );
}

void User::SetSession(SessionPointer session)
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return;

	DebugAssert( HasSession() == false );
	if( HasSession() ) return;

	DebugAssert( session.isNull() == false );
	if( session.isNull() ) return;

	m_session = session;
}

User::UserStatus User::GetStatus() const
{
	DebugAssert( HasStatus() );
	if( HasStatus() == false ) return Undefined;

	bool ok;
	UserStatus result = static_cast<UserStatus>( m_map.value( KEY_STATUS ).toInt( &ok ) );

	DebugAssert( ok );
	if( ok == false ) return Undefined;

	return result;
}

bool User::IpIsAllowed() const
{
	DebugAssert( HasIpIsAllowedValue() );
	if( HasIpIsAllowedValue() == false ) return false;

	return m_map.value( KEY_IP_IS_ALLOWED ).toBool();
}

SessionPointer User::GetSession() const
{
	DebugAssert( HasSession() );
	if( HasSession() == false ) return SessionPointer();

	return m_session;
}

bool User::HasStatus() const
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return false;

	return m_map.contains( KEY_STATUS );
}

bool User::HasIpIsAllowedValue() const
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return false;

	return m_map.contains( KEY_IP_IS_ALLOWED );
}

bool User::HasSession() const
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return false;

    return m_session.isNull() == false;
}

bool User::IsAdmin()
{
    if(!m_map.contains( KEY_ROLEGROUPS )) return false;
    if(m_map.value(KEY_ROLEGROUPS).type() != QVariant::List) return false;
    return m_map.value(KEY_ROLEGROUPS).toList().contains("admin")
            || m_map.value(KEY_ROLEGROUPS).toList().contains("operator")
            || m_map.value(KEY_ROLEGROUPS).toList().contains("library_admin")
            || m_map.value(KEY_ROLEGROUPS).toList().contains("operator_wchz");
}

bool User::Check() const
{
	if( IsEmpty() ) return true;

//	DebugAssert( m_map.count() == 10 );

	if( m_map.contains( KEY_ID ) == false ) return false;
	if( m_map.contains( KEY_NAME ) == false ) return false;
	if( m_map.contains( KEY_LAST_NAME ) == false ) return false;
	if( m_map.contains( KEY_EMAIL ) == false ) return false;
	if( m_map.contains( KEY_DATE_REGISTER ) == false ) return false;
	if( m_map.contains( KEY_SECOND_NAME ) == false ) return false;
	if( m_map.contains( KEY_TOKEN ) == false ) return false;

	bool checkId;
	m_map.value( KEY_ID ).toUInt( &checkId );
	DebugAssert( checkId );
	if( checkId == false ) return false;

	QDateTime dateTime = QDateTime::fromString( m_map.value( KEY_DATE_REGISTER ).toString(), DATE_FORMAT );
	DebugAssert( dateTime.isValid() );
	if( dateTime.isValid() == false ) return false;

	QString token = m_map.value( KEY_TOKEN ).toString();
	DebugAssert( token.isEmpty() == false );
	if( token.isEmpty() ) return false;

	return true;
}
