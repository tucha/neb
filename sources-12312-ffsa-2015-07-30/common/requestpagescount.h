#ifndef REQUESTPAGESCOUNT_H
#define REQUESTPAGESCOUNT_H

#include "request.h"

class RequestPagesCount : public Request
{
	Q_OBJECT
public:
	RequestPagesCount( QObject *parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority );
	void Send(void);

signals:
	void SignalFinished( ErrorCode error, int pagesCount );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

private:
};

#endif // REQUESTPAGESCOUNT_H
