#include "bookmark.h"

#include "defines.h"

#define UNDEFINED_PAGE_INDEX -1

Bookmark::Bookmark():
	m_user(),
	m_document(),
	m_pageIndex( UNDEFINED_PAGE_INDEX ),
    m_resPath(""),
	m_id(),
	m_text()
{
	DebugAssert( IsEmpty() );
}

Bookmark::Bookmark(User user, Document document, int pageIndex, QString resPath, QString id, QString text):
	m_user( user ),
	m_document( document ),
	m_pageIndex( pageIndex ),
    m_resPath(resPath),
	m_id( id ),
	m_text( text )
{
	DebugAssert( IsEmpty() == false );
}

bool Bookmark::IsEmpty() const
{
	bool result = m_id.isEmpty();
	if( result )
	{
		DebugAssert( m_user.IsEmpty() );
		DebugAssert( m_document.IsEmpty() );
//		DebugAssert( m_pageIndex == UNDEFINED_PAGE_INDEX );
		DebugAssert( m_id.isEmpty() );
		DebugAssert( m_text.isEmpty() );
	}
	else
	{
		DebugAssert( m_user.IsEmpty() == false );
		DebugAssert( m_document.IsEmpty() == false );
//		DebugAssert( m_pageIndex >= 0 );
		DebugAssert( m_id.isEmpty() == false );
	}
	return result;
}

User Bookmark::GetUser() const
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return User();

	return m_user;
}

Document Bookmark::GetDocument() const

{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return Document();

	return m_document;
}

int Bookmark::PageIndex() const
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return UNDEFINED_PAGE_INDEX;

    return m_pageIndex;
}

QString Bookmark::ResPath() const
{
    if( IsEmpty() ) return "";
    return m_resPath;
}

QString Bookmark::Id() const
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return QString();

	return m_id;
}

QString Bookmark::Text() const
{
	return m_text;
}

bool Bookmark::IsComment() const
{
	return Text().isEmpty() == false;
}

bool Bookmark::IsOlderThan(Bookmark another) const
{
	return this->Id().toInt() > another.Id().toInt();
}
