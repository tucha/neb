#ifndef BlockFingerprintRequest_H
#define BlockFingerprintRequest_H

#include "apibase.h"
#include "viewersettings.h"
#include "user.h"

class BlockFingerprintRequest : public ApiBase
{
	Q_OBJECT

public:
    explicit BlockFingerprintRequest(QObject *parent, QNetworkAccessManager* manager, QString baseUrl,
            User user,
                                     RequestPriority priority = PRIORITY_NORMAL );

	void Send();

signals:
    void SignalFinished( ErrorCode error, bool );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

protected:
	virtual QNetworkReply* SendInternal( QString requestString );

private:

};

#endif // BlockFingerprintRequest
