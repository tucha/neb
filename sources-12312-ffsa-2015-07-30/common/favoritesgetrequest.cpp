#include "favoritesgetrequest.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include "platform_id/platform_id.h"
#include "session.h"


FavoritesGetRequest::FavoritesGetRequest(QObject* parent, QNetworkAccessManager* manager, QString baseUrl,
										 User user, RequestPriority priority):
	AccountBaseRequest( parent, manager, baseUrl, user, priority )
{
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));

	AddGetParameter( "token", UserToken() );
    AddGetParameter(  "fingerprint", PlatformID::hash());
    AddGetParameter(  "os", REQUEST_OS_ID);
    if(GetUser().HasSession())AddGetParameter("session", GetUser().GetSession()->GetID());

}

void FavoritesGetRequest::Send()
{
	AccountBaseRequest::Send( "favorites" );
}

void FavoritesGetRequest::SlotFinished(ErrorCode error, QByteArray data)
{
	if( error != API_NO_ERROR )
	{
		emit SignalFinished( error, FavoriteBooks() );
		return;
	}

	emit SignalFinished( error, ParseResult( data ) );
}

QNetworkReply*FavoritesGetRequest::SendInternal(QString requestString)
{
	QUrl url( requestString );
#if QT_VERSION >= 0x050000
	url.setQuery( GetParameters() );
#else
	QList<QPair<QString,QString> > prms = GetParameters();
	for( QList<QPair<QString,QString> >::iterator iter = prms.begin(); iter != prms.end(); ++iter )
	{
		url.addQueryItem( iter->first, iter->second );
	}
#endif

	QNetworkRequest request;
	request.setUrl( url );
	request.setPriority( Priority() );

	return Manager()->get( request );
}

FavoriteBooks FavoritesGetRequest::ParseResult(QByteArray data)
{
	QJsonDocument document = QJsonDocument::fromJson( data );

	DebugAssert( document.isObject() );
	if( document.isObject() == false ) return FavoriteBooks();

	FavoriteBooks result;

	QSet<int> ids;
	QVariantMap map = document.object().toVariantMap();
	foreach( QString key, map.keys() )
	{
		DebugAssert( map.value( key ).isValid() );
		if( map.value( key ).isValid() == false ) return FavoriteBooks();

		QString idString = map.value( key ).toString();

		bool ok;
		int id = idString.toInt( &ok );

		DebugAssert( ok );
		if( ok == false ) return FavoriteBooks();

		DebugAssert( ids.contains( id ) == false );
		if( ids.contains( id ) ) return FavoriteBooks();
		ids.insert( id );

		QString documentId = key;
		if( Document::CheckDocumentId( documentId ) == false ) continue;

		result.append( FavoriteBook( GetUser(), Document::FromId( documentId ), idString ) );
	}

	return result;
}
