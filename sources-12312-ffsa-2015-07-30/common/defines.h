#ifndef DEFINES_H
#define DEFINES_H

#include "qglobal.h"

//#define BUILD_DEBUG
//#define PRINT_SAVE

#define UNUSED( T ) Q_UNUSED( T )
#define DISABLE_COPY( T ) Q_DISABLE_COPY( T )
#define DeleteAndNull( T ) delete T; T = NULL
#define DeleteLaterAndNull( T ) if( T != NULL ) T->deleteLater(); T = NULL

#ifdef BUILD_DEBUG
#define DebugAssert( T ) if( !( T ) ) throw
#define NotImplemented() throw
#define DebugOutput( T ) Debug::Instance()->Output( T )
#else
#define DebugAssert( T )
#define NotImplemented() {}
#define DebugOutput( T )
#endif // BUILD_DEBUG

#endif // DEFINES_H
