#include "requestwordpages.h"

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonObject>

#define KEY_WORD	"word"
#define KEY_PAGES	"pages"

RequestWordPages::RequestWordPages(QObject* parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority,
								   QStringList words):
	Request( parent, manager, identifier, document, priority ),
	m_words( words )
{
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

void RequestWordPages::Send()
{
	QString requestString = QString( "%1/%2/%3" )
							.arg( "document" )
							.arg( "search" )
							.arg( WordsString( Words() ) );
	ApiBase::Send( requestString );
}

QStringList RequestWordPages::Words() const
{
	return m_words;
}

void RequestWordPages::SlotFinished(ErrorCode error, QByteArray data)
{
	if( error == API_NO_ERROR ) emit SignalFinished( error, ParseWordPages( Words(), data ) );
	else emit SignalFinished( error, WordPages() );
}

WordPages RequestWordPages::ParseWordPages(QStringList words, QByteArray data)
{
	QJsonDocument json = QJsonDocument::fromJson( data );
	DebugAssert( json.isNull() == false );

	DebugAssert( json.isArray() );
	QJsonArray array = json.array();

	DebugAssert( words.count() == array.count() );
	if( words.count() != array.count() ) return WordPages();

	WordPages result;
	for( int i = 0; i < array.count(); ++i )
	{
		QString word = words.at( i );

		QJsonValue value = array.at( i );
		DebugAssert( value.isObject() );

		QJsonObject object = value.toObject();
		DebugAssert( object.count() == 2 );
		DebugAssert( object.contains( KEY_WORD ) );
		DebugAssert( object.contains( KEY_PAGES ) );

		DebugAssert( object.value( KEY_WORD ).isString() );
		DebugAssert( object.value( KEY_WORD ).toString() == word );
		if( object.value( KEY_WORD ).toString() != word ) continue;

		DebugAssert( object.value( KEY_PAGES ).isArray() );
		QJsonArray pages = object.value( KEY_PAGES ).toArray();

		QList<int> resultPages;
		for( int i = 0; i < pages.count(); ++i )
		{
			DebugAssert( pages.at( i ).isDouble() );
			int page = pages.at( i ).toInt();

			DebugAssert( page > 0 );
			resultPages.append( page );
		}
		result.insert( word, resultPages );
	}
	return result;

}
