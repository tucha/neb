#include "bookmarkscontainer.h"

#include "defines.h"

BookmarksContainer::BookmarksContainer( Bookmarks bookmarks ):
	m_user(),
	m_document(),
	m_comments( false ),
    m_pagesByIndex(),
    m_pagesByResPath(),
	m_ids()
{
	DebugAssert( IsEmpty() );
	foreach( Bookmark b, bookmarks ) Add( b );

	DebugAssert( Count() == bookmarks.count() );
	if( Count() != bookmarks.count() ) Clear();
}

bool BookmarksContainer::IsEmpty() const
{
	bool result = Count() == 0;
	if( result )
	{
		DebugAssert( m_user.IsEmpty() );
		DebugAssert( m_document.IsEmpty() );
		DebugAssert( m_comments == false );
//		DebugAssert( m_pagesByIndex.isEmpty() );
		DebugAssert( m_ids.isEmpty() );
	}
	else
	{
		DebugAssert( m_user.IsEmpty() == false );
		DebugAssert( m_document.IsEmpty() == false );
//		DebugAssert( m_pagesByIndex.isEmpty() == false );
//        DebugAssert( m_ids.count() == m_pagesByIndex.count() +  );
	}
    return result;
}

bool BookmarksContainer::Contains(int pageIndex, QString resPath) const
{
    return (m_pagesByIndex.contains( pageIndex ) && pageIndex !=-1) || m_pagesByResPath.contains(resPath);
}

bool BookmarksContainer::Add(Bookmark bookmark)
{
	DebugAssert( bookmark.IsEmpty() == false );
	if( bookmark.IsEmpty() ) return false;

	if( IsEmpty() == false )
	{
		DebugAssert( Check( bookmark ) );
		if( Check( bookmark ) == false ) return false;

        DebugAssert( Contains( bookmark.PageIndex(), bookmark.ResPath() ) == false );
        if( Contains( bookmark.PageIndex(), bookmark.ResPath() ) ) return false;
	}
	else
	{
		m_user = bookmark.GetUser();
		m_document = bookmark.GetDocument();
		m_comments = bookmark.IsComment();
	}

    if(bookmark.ResPath() == "")m_pagesByIndex.insert( bookmark.PageIndex(), bookmark );
    else m_pagesByResPath.insert(bookmark.ResPath(), bookmark);
	m_ids.insert( bookmark.Id() );
	return true;
}

Bookmark BookmarksContainer::Get(int pageIndex, QString resPath) const
{
    if(resPath != "" && m_pagesByResPath.contains(resPath))
        return m_pagesByResPath.value( resPath, Bookmark() );
    else
        return m_pagesByIndex.value( pageIndex, Bookmark() );
}

bool BookmarksContainer::Remove(Bookmark bookmark)
{
	DebugAssert( bookmark.IsEmpty() == false );
	if( bookmark.IsEmpty() ) return false;

    if( Contains( bookmark.PageIndex(), bookmark.ResPath() ) == false) return false;

    m_pagesByIndex.remove( bookmark.PageIndex() );
    m_pagesByResPath.remove( bookmark.ResPath() );

	m_ids.remove( bookmark.Id() );

    if( m_pagesByIndex.isEmpty() &&m_pagesByResPath.isEmpty() )
	{
		m_user = User();
		m_document = Document();
		m_comments = false;
		DebugAssert( IsEmpty() );
	}



	return true;
}

int BookmarksContainer::Count() const
{
    return m_pagesByIndex.count() + m_pagesByResPath.count();
}

void BookmarksContainer::Clear()
{
	*this = BookmarksContainer();
	DebugAssert( IsEmpty() );
}

QList<int> BookmarksContainer::PagesWithIndex() const
{
    return m_pagesByIndex.keys();
}

QList<QString> BookmarksContainer::PagesWithResPath() const
{
    return m_pagesByResPath.keys();
}

bool BookmarksContainer::Check(Bookmark bookmark) const
{
	if( IsEmpty() ) return true;

	if( m_user.GetToken() != bookmark.GetUser().GetToken() ) return false;
	if( m_document != bookmark.GetDocument() ) return false;
	if( m_comments != bookmark.IsComment() ) return false;
	if( m_ids.contains( bookmark.Id() ) ) return false;
	return true;
}
