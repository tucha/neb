#ifndef BOOKMARK_H
#define BOOKMARK_H

#include <QString>
#include <QList>

#include "user.h"
#include "document.h"

class Bookmark;
typedef QList<Bookmark> Bookmarks;

class Bookmark
{
public:
	Bookmark();
    Bookmark( User user, Document document, int pageIndex, QString resPath, QString id, QString text = QString() );

	bool IsEmpty( void )const;

	User		GetUser(void)const;
	Document	GetDocument(void)const;
	int			PageIndex(void)const;
    QString     ResPath()const;
	QString		Id(void)const;
	QString		Text(void)const;
	bool		IsComment(void)const;

	bool		IsOlderThan( Bookmark another )const;

private:
	User		m_user;
	Document	m_document;
	int			m_pageIndex;
    QString     m_resPath;
	QString		m_id;
	QString		m_text;
};

#endif // BOOKMARK_H
