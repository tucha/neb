#include "bookmarksmanagerbase.h"

#include "defines.h"

BookmarksManagerBase::BookmarksManagerBase(QObject* parent, RequestPriority priority) :
    QObject(parent),
    m_baseRequest( NULL ),
    m_document(),
    m_isLoaded( false ),
    m_isNotFound(false),
    m_bookmarks(),
    m_addRequests(),
    m_removeRequests(),
    m_getRequest( NULL ),
    m_addPages(),
    m_removePages(),
    m_addResPaths(),
    m_removeResPaths(),
    m_priority( priority )
{
    DebugAssert( IsEmpty() );
}

BookmarksManagerBase::~BookmarksManagerBase()
{
    DebugAssert( IsEmpty() );
}

void BookmarksManagerBase::Clear()
{
    if( IsEmpty() ) return;

    m_baseRequest = NULL;
    m_document = Document();
    m_isLoaded = false;
    m_bookmarks.Clear();
    m_isNotFound = false;
    DeleteLaterAndNull( m_getRequest );

    foreach( BookmarksAddRequest* r, m_addRequests ) r->deleteLater();
    m_addRequests.clear();
    m_addPages.clear();
    m_addResPaths.clear();

    foreach( BookmarksDeleteRequest* r, m_removeRequests ) r->deleteLater();
    m_removeRequests.clear();
    m_removePages.clear();
    m_removeResPaths.clear();

    DebugAssert( IsEmpty() );
    EmitUpdate();
}

void BookmarksManagerBase::Load(AccountBaseRequest* baseRequest, Document document)
{
    DebugAssert( baseRequest != NULL );
    if( baseRequest == NULL ) return;

    DebugAssert( document.IsEmpty() == false );
    if( document.IsEmpty() ) return;

    Clear();

    m_baseRequest = baseRequest;
    m_document = document;

    m_getRequest = CreateGetRequest();
    connect(m_getRequest,SIGNAL(SignalFinished(ErrorCode,Bookmarks)),this,SLOT(SlotFinishedGetRequest(ErrorCode,Bookmarks)));
    m_getRequest->Send();
}


bool BookmarksManagerBase::IsActual(int pageIndex, QString resPath)const
{
    if( m_isLoaded == false ) return false;
    if( WasRequestedAddition( pageIndex, resPath ) ) return false;
    if( WasRequestedRemoval( pageIndex, resPath ) ) return false;
    return true;
}


bool BookmarksManagerBase::Contains(int pageIndex, QString resPath ) const
{
    return m_bookmarks.Contains( pageIndex, resPath );
}


QList<QPair<int, QString> > BookmarksManagerBase::ActualPages() const
{
    QList<QPair<int, QString> > result;

    foreach( int pageIndex, m_bookmarks.PagesWithIndex() )
    {
        if( IsActual( pageIndex, "" ) ) result.append( QPair<int, QString> (pageIndex, "") );
    }

    foreach( QString resPath, m_bookmarks.PagesWithResPath() )
    {
        if( IsActual( -1, resPath ) ) result.append( QPair<int, QString> (-1, resPath) );
    }

    return result;
}

void BookmarksManagerBase::SlotFinishedGetRequest(ErrorCode error, Bookmarks result)
{
    if( IsEmpty() ) {
        m_isNotFound = true;
        return;
    }

    BookmarksGetRequest* s = static_cast<BookmarksGetRequest*>( sender() );
    if( m_getRequest != s ) return;

    if( error != API_NO_ERROR )
    {
        m_isNotFound = true;
//        s->Send();
        return;
    }

    DebugAssert( m_isLoaded == false );
    if( m_isLoaded )
    {
        DebugOutput( "Something went wrong with bookmarks... turning off the feature." );
        Clear();
        return;
    }

    DebugAssert( m_bookmarks.IsEmpty() );
    if( m_bookmarks.IsEmpty() == false )
    {
        DebugOutput( "Something went wrong with bookmarks... turning off the feature." );
        Clear();
        return;
    }

    m_isLoaded = true;
    m_bookmarks = BookmarksContainer( Validate( result ) );
    EmitUpdate();
    EmitLoaded();
}

void BookmarksManagerBase::SlotFinishedAddRequest(ErrorCode error, Bookmark result)
{
    if( IsEmpty() ) return;

    BookmarksAddRequest* s = static_cast<BookmarksAddRequest*>( sender() );
    if( m_addRequests.contains( s ) == false ) return;

    if( error != API_NO_ERROR )
    {
//        s->Send();
        return;
    }

    bool additionSucceed = m_bookmarks.Add( Validate( result ) );

    DebugAssert( additionSucceed );
    if( additionSucceed == false )
    {
        DebugOutput( "Uncorrect bookmarks where received from AccountService. Bookmarks feature turned off." );
        Clear();
        return;
    }

    m_addRequests.remove( s );
    m_addPages.remove( s->PageIndex() );
    m_addResPaths.remove(s->ResPath());
    DeleteLaterAndNull( s );

    EmitUpdate();
}

void BookmarksManagerBase::SlotFinishedRemoveRequest(ErrorCode error)
{
    if( IsEmpty() ) return;

    BookmarksDeleteRequest* s = static_cast<BookmarksDeleteRequest*>( sender() );
    if( m_removeRequests.contains( s ) == false ) return;

    if( error != API_NO_ERROR )
    {
//        s->Send();
        return;
    }

    bool removalSucceed = m_bookmarks.Remove( s->GetBookmark() );

    DebugAssert( removalSucceed );
    if( removalSucceed == false )
    {
        DebugOutput( "Something went wrong with bookmarks... turning off the feature." );
        Clear();
        return;
    }

    m_removeRequests.remove( s );
    m_removeResPaths.remove(s->GetBookmark().ResPath());
    m_removePages.remove( s->GetBookmark().PageIndex() );
    DeleteLaterAndNull( s );

    EmitUpdate();
}

void BookmarksManagerBase::SlotErrorAddRequest()
{
    if( IsEmpty() ) return;

    BookmarksAddRequest* s = static_cast<BookmarksAddRequest*>( sender() );
    if( m_addRequests.contains( s ) == false ) return;

    DebugOutput( "Something went wrong with bookmarks... turning off the feature." );
    Clear();
}

void BookmarksManagerBase::RequestAddition(int pageIndex, QString resPath, QString text)
{
    DebugAssert( Contains( pageIndex, resPath ) == false );
    if( Contains( pageIndex, resPath ) ) return;

    BookmarksAddRequest* r = CreateAddRequest( pageIndex, resPath, text );
    connect(r,SIGNAL(SignalFinished(ErrorCode,Bookmark)),this,SLOT(SlotFinishedAddRequest(ErrorCode,Bookmark)));
    connect(r,SIGNAL(SignalError()),this,SLOT(SlotErrorAddRequest()));
    m_addRequests.insert( r );
    if( resPath == "")
        m_addPages.insert( pageIndex );
    else
        m_addResPaths.insert(resPath);
    r->Send();
    EmitUpdate();
}

void BookmarksManagerBase::RequestRemoval(int pageIndex, QString resPath)
{
    DebugAssert( Contains( pageIndex, resPath ) );
    if( Contains( pageIndex, resPath ) == false ) return;

    BookmarksDeleteRequest* r = CreateRemoveRequest( pageIndex, resPath );
    connect(r,SIGNAL(SignalFinished(ErrorCode)),this,SLOT(SlotFinishedRemoveRequest(ErrorCode)));
    m_removeRequests.insert( r );
    if( resPath == "")
        m_removePages.insert( pageIndex );
    else
        m_removeResPaths.insert(resPath);
    r->Send();
    EmitUpdate();
}

AccountBaseRequest*BookmarksManagerBase::BaseRequest() const
{
    return m_baseRequest;
}

Document BookmarksManagerBase::GetDocument() const
{
    return m_document;
}

RequestPriority BookmarksManagerBase::Priority() const
{
    return m_priority;
}

bool BookmarksManagerBase::IsEmpty()const
{
    bool result = m_baseRequest == NULL;

    if( result )
    {
        DebugAssert( m_baseRequest == NULL );
        DebugAssert( m_document.IsEmpty() );
        DebugAssert( m_isLoaded == false );
        DebugAssert( m_bookmarks.IsEmpty() );
        DebugAssert( m_addRequests.isEmpty() );
        DebugAssert( m_removeRequests.isEmpty() );
        DebugAssert( m_addPages.isEmpty() );
        DebugAssert( m_removePages.isEmpty() );
        DebugAssert( m_addResPaths.isEmpty() );
        DebugAssert( m_removeResPaths.isEmpty() );

        DebugAssert( m_getRequest == NULL );
    }
    else
    {
        DebugAssert( m_baseRequest != NULL );
        DebugAssert( m_document.IsEmpty() == false );
        DebugAssert( CheckRequests() );
    }

    return result;
}

bool BookmarksManagerBase::CheckRequests()const
{
    if( m_getRequest != NULL )
    {
        if( m_getRequest->AllPages() == false ) return false;

        if( m_getRequest->UserToken() != m_baseRequest->UserToken() ) return false;
        if( m_getRequest->GetDocument() != m_document ) return false;
    }

    foreach( BookmarksAddRequest* r, m_addRequests )
    {
        if( r->UserToken() != m_baseRequest->UserToken() ) return false;
        if( r->GetDocument() != m_document ) return false;

        if( !m_addPages.contains( r->PageIndex() )
                && !m_addResPaths.contains( r->ResPath())) return false;
        if( m_removePages.contains( r->PageIndex() )
                || m_removeResPaths.contains( r->ResPath())) return false;
    }

    foreach( BookmarksDeleteRequest* r, m_removeRequests )
    {
        if( r->UserToken() != m_baseRequest->UserToken() ) return false;
        if( r->GetBookmark().GetDocument() != m_document ) return false;

        if(! m_removePages.contains( r->GetBookmark().PageIndex() )
                && !m_removeResPaths.contains( r->GetBookmark().ResPath())) return false;
        if( m_addPages.contains( r->GetBookmark().PageIndex() )
                || m_addResPaths.contains( r->GetBookmark().ResPath())) return false;

    }

    if( m_addPages.count() + m_addResPaths.count() != m_addRequests.count() ) return false;
    if( m_removePages.count() + m_removeResPaths.count() != m_removeRequests.count() ) return false;

    return true;
}

bool BookmarksManagerBase::WasRequestedAddition(int pageIndex, QString resPath)const
{
    if(resPath=="")
        return m_addPages.contains( pageIndex );
    else
        return m_addResPaths.contains( resPath );
}

bool BookmarksManagerBase::WasRequestedRemoval(int pageIndex, QString resPath)const
{
    if(resPath=="")
        return m_removePages.contains( pageIndex );
    else
        return m_removeResPaths.contains( resPath );
}

bool BookmarksManagerBase::IsNotFound()
{
    return m_isNotFound;
}
