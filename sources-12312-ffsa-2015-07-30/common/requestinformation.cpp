#include "requestinformation.h"

#include <QJsonDocument>
#include <QJsonObject>

#include "defines.h"

RequestInformation::RequestInformation(QObject* parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority):
	Request( parent, manager, identifier, document, priority )
{
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

void RequestInformation::Send()
{
	QString requestString = QString( "%1/%2" )
							.arg( "document" )
							.arg( "info" );
	ApiBase::Send( requestString );
}

void RequestInformation::SlotFinished(ErrorCode error, QByteArray data)
{
	if( error == API_NO_ERROR ) emit SignalFinished( error, ParseInformation( data ) );
	else emit SignalFinished( error, QVariantMap() );
}

QVariantMap RequestInformation::ParseInformation(QByteArray data)
{
	QJsonDocument json = QJsonDocument::fromJson( data );
	DebugAssert( json.isNull() == false );

	DebugAssert( json.isObject() );
	QJsonObject object = json.object();

	return object.toVariantMap();
}
