#include "requestimage.h"

#include "getparameterslice.h"
#include "getparameterhighlight.h"

RequestImage::RequestImage(QObject* parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority,
						   int pageIndex, ImageType type, int ppi, ImageFormat format ):
    RequestPage( parent, manager, identifier, document, priority, pageIndex ),
	m_type( type ), m_ppi( ppi ), m_format( format )
{
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

void RequestImage::Send()
{
	DebugAssert( TestPpi( Type(), Ppi() ) );

	QString requestString = QString( "%1/%2%3%4%5" )
							.arg( TypeString( Type() ) )
							.arg( "ppi" )
							.arg( Ppi() )
							.arg( "." )
							.arg( FormatString( Format() ) );
	RequestPage::Send( requestString );
}

RequestPage::ImageType RequestImage::Type() const
{
	return m_type;
}

int RequestImage::Ppi() const
{
	return m_ppi;
}

RequestPage::ImageFormat RequestImage::Format() const
{
	return m_format;
}

void RequestImage::Slice(QRectF rectangle)
{
	AddGetParameter( new GetParameterSlice( rectangle ) );
}

void RequestImage::Hightlight(QRectF rectangle)
{
	AddGetParameter( new GetParameterHighlight( rectangle ) );
}

void RequestImage::SlotFinished(ErrorCode error, QByteArray data)
{
	if( error == API_NO_ERROR ) emit SignalFinished( error, ParseImage( data, Format() ) );
	else emit SignalFinished( error, QImage() );
}
