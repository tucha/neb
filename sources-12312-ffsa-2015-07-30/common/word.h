#ifndef WORD_H
#define WORD_H

#include <QRectF>
#include <QVariantMap>
#include <QString>

class Word
{
public:
	Word();
	Word( QRectF rectangle, QString text );
	Word( QVariantMap map );
	Word( QString text, QVariantMap map );
	QRectF Rectangle(void)const;
	QString Text(void)const;
private:
	QRectF GetRectangle( QVariantMap map );
	QString GetText( QVariantMap map );
	QRectF m_rectangle;
	QString m_text;
};

#endif // WORD_H
