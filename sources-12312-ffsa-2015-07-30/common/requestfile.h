#ifndef REQUESTFILE_H
#define REQUESTFILE_H

#include "request.h"

class RequestFile : public Request
{
	Q_OBJECT
public:
	RequestFile( QObject *parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority,
				 QString fileExtension );
	void Send(void);
	QString FileExtension(void)const;

private:
	const QString m_fileExtension;
};

#endif // REQUESTFILE_H
