#include "requesttype.h"
#include <QJsonDocument>
RequestDocumentFormat::RequestDocumentFormat(QObject* parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority):
    Request( parent, manager, identifier, document, priority )
{
    connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

void RequestDocumentFormat::Send()
{
        QString requestString = QString( "%1" )
                                                        .arg( "resources" )
                                                       ;
        ApiBase::Send( requestString );
}

void RequestDocumentFormat::SlotFinished(ErrorCode error, QByteArray data)
{
    auto typeInfo = QJsonDocument::fromJson(data).toVariant();

    if(typeInfo.type() != QVariant::List)
        emit SignalFinished( error, Document::UNKNOWN );
    else
    {
        auto s = typeInfo.toStringList();
        if(s.contains("epub"))
        {
            emit SignalFinished( error, Document::EPUB );
        } else
        {
            emit SignalFinished( error, Document::PDF );
        }
    }
}
