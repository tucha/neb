#include "authorizationrequest.h"

AuthorizationRequest::AuthorizationRequest(QObject* parent, QNetworkAccessManager* manager, QString baseUrl, RequestPriority priority, QString email, QString password ):
	AuthorizationBase( parent, manager, baseUrl, priority ),
	m_email( email ),
	m_password( password )
{
	AddGetParameter( "email", PercentsEncoded( email.toUtf8() ) );
	AddGetParameter( "password", PercentsEncoded( password.toUtf8() ) );
}

void AuthorizationRequest::Send()
{
	ApiBase::Send( "auth" );
}

QString AuthorizationRequest::Email() const
{
	return m_email;
}

QString AuthorizationRequest::Password() const
{
	return m_password;
}

QString AuthorizationRequest::PercentsEncoded(QByteArray bytes)
{
	QString result;
	foreach( uchar byte, bytes )
	{
		QString byteString = QString::number( byte, 16 ).toUpper();
		if( byteString.length() == 1 ) byteString = "0" + byteString;
		result += "%" + byteString;
	}
	return result;
}
