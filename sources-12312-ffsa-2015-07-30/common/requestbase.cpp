#include "requestbase.h"

#include "request.h"
#include "requestcollections.h"
#include "requestmarks.h"
#include "requesttexts.h"
#include "platform_id/platform_id.h"
#include "session.h"


RequestBase::RequestBase(QObject *parent, QNetworkAccessManager *manager, Identifier identifier, QString baseUrl, RequestPriority priority):
	ApiBase( parent, manager, baseUrl, priority ),
	m_identifier( identifier )
{
	ApplyIdentifier();
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

RequestBase::~RequestBase()
{

}

Request*RequestBase::NewRequest(Document document, RequestPriority priority) const
{
	DebugAssert( document.BaseUrl() == BaseUrl() );
	return new Request( parent(), Manager(), GetIdentifier(), document, priority );
}

RequestCollections*RequestBase::NewCollections( RequestPriority priority ) const
{
	return new RequestCollections( parent(), Manager(), GetIdentifier(), BaseUrl(), priority );
}

RequestMarks*RequestBase::NewMarks(QString collection, RequestPriority priority) const
{
	return new RequestMarks( parent(), Manager(), GetIdentifier(), BaseUrl(), collection, priority );
}

RequestTexts*RequestBase::NewTexts(QString collection, RequestPriority priority) const
{
	return new RequestTexts( parent(), Manager(), GetIdentifier(), BaseUrl(), collection, priority );
}

Identifier RequestBase::GetIdentifier() const
{
	return m_identifier;
}

void RequestBase::SlotFinished(ErrorCode error, QByteArray data)
{
	UNUSED( data );
	if( error == API_NO_ERROR ) return;
	GetIdentifier().CheckSession();
}

QNetworkReply*RequestBase::SendInternal(QString requestString)
{
	QUrl url = QUrl( QString( "%1/%2" )
				  .arg( BaseUrl() )
				  .arg( requestString ) );
#if QT_VERSION >= 0x050000
	url.setQuery( GetParameters() );
#else
	QList<QPair<QString,QString> > prms = GetParameters();
	for( QList<QPair<QString,QString> >::iterator iter = prms.begin(); iter != prms.end(); ++iter )
	{
		url.addQueryItem( iter->first, iter->second );
	}
#endif

	QNetworkRequest request;
	request.setUrl( url );
//    request.setRawHeader( "DLIB-KEY", GetIdentifier().GetClientId().toUtf8() );
    request.setRawHeader( "DLIB-KEY", BUILD_TOKEN);

    request.setPriority( Priority() );

	return Manager()->get( request );
}

void RequestBase::ApplyIdentifier()
{
	Identifier id = GetIdentifier();
	DebugAssert( id.GetClientId().isEmpty() == false );

    AddGetParameter(  "fingerprint", PlatformID::hash());
    AddGetParameter(  "os", REQUEST_OS_ID);
    AddGetParameter("session", id.GetSession()->GetID());

	if( id.ContainsUserToken() )
	{
		DebugAssert( id.GetUserToken().isEmpty() == false );
		AddGetParameter( "token", id.GetUserToken() );
	}

	if( id.ContainsAppToken() )
	{
		DebugAssert( id.GetAppToken().isEmpty() == false );
		AddGetParameter( "app", id.GetAppToken() );
	}

	if( id.ContainsIp() )
	{
		DebugAssert( id.GetIp().isEmpty() == false );
		AddGetParameter( "ip", id.GetIp() );
	}
}
