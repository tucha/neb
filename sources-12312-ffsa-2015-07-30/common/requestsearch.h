#ifndef REQUESTSEARCH_H
#define REQUESTSEARCH_H
#include "request.h"



class RequestDocumentSearch : public Request
{
    Q_OBJECT
    QStringList m_phraseList;
public:
    RequestDocumentSearch(QObject *parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority, QStringList phraseList );
    void Send(void);

signals:
    void SignalFinished( ErrorCode error, QVariantMap results, bool js_error);

private slots:
    void SlotFinished( ErrorCode error, QByteArray data );

private:
};


#endif // REQUESTSEARCH_H
