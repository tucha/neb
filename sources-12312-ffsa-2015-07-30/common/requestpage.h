#ifndef REQUESTPAGE_H
#define REQUESTPAGE_H

#include "request.h"

#include <QStringList>

class RequestPageSize;
class RequestImage;
class RequestImageFixed;
class RequestWords;
class RequestWordCoordinates;
class RequestImageWithWords;

#define TYPE_IMAGES		RequestPage::Images
#define TYPE_PRINT		RequestPage::Print
#define TYPE_THUMBNAILS	RequestPage::Thumbnails

#define FORMAT_JPEG		RequestPage::Jpeg
#define FORMAT_TIFF		RequestPage::Tiff

#define GEOMETRY_WIDTH	RequestPage::Width
#define GEOMETRY_HEIGHT	RequestPage::Height
#define GEOMETRY_MAX	RequestPage::Max

class RequestPage : public Request
{
	Q_OBJECT
public:
	enum ImageType
	{
		Images,
		Print,
		Thumbnails,
	};

	enum ImageFormat
	{
		Jpeg,
		Tiff,
	};

	enum Geometry
	{
		Width,
		Height,
		Max,
	};

	RequestPage( QObject *parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority,
					int pageIndex );
	void Send( QString requestString );
	int PageIndex(void)const;

	RequestPageSize*	NewSize( RequestPriority priority = PRIORITY_NORMAL )const;
	RequestImage*		NewImage( ImageType type, int ppi, ImageFormat format, RequestPriority priority = PRIORITY_NORMAL )const;
	RequestImageFixed*	NewImageFixed( ImageType type, Geometry geometry, int restriction, ImageFormat format, RequestPriority priority = PRIORITY_NORMAL )const;
	RequestWords*		NewWords( RequestPriority priority = PRIORITY_NORMAL )const;
	RequestWordCoordinates* NewWordCoordinates( QStringList words, RequestPriority priority = PRIORITY_NORMAL )const;
	RequestImageWithWords*	NewImageWithWords( QStringList words, int ppi, ImageFormat format, RequestPriority priority = PRIORITY_NORMAL )const;

	static QString TypeString( ImageType type );
	static QString FormatString( ImageFormat format );
	static QString GeometryString( Geometry geometry );
	static bool TestPpi( ImageType type, int ppi );
	static bool TestRestriction( ImageType type, int restriction );
	static QImage ParseImage( QByteArray data, ImageFormat format );

private:
	int m_pageIndex;
};

#endif // REQUESTPAGE_H
