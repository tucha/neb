#ifndef AUTHORIZATIONBASE_H
#define AUTHORIZATIONBASE_H

#include "apibase.h"

#include "authorizationresult.h"

class AuthorizationRequest;
class AuthorizationCheck;
class AnonymToken;

class AuthorizationBase: public ApiBase
{
    Q_OBJECT
public:
	AuthorizationBase( QObject *parent, QNetworkAccessManager* manager, QString baseUrl,
					   RequestPriority priority = PRIORITY_NORMAL );

	AuthorizationRequest*	NewRequest(QString email, QString password, RequestPriority priority = PRIORITY_NORMAL)const;
	AuthorizationCheck*		NewCheck(QString token, RequestPriority priority = PRIORITY_NORMAL)const;
    AnonymToken *NewAnonym(RequestPriority priority = PRIORITY_NORMAL) const;

signals:
	void SignalFinished( ErrorCode error, AuthorizationResult result );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

protected:
	virtual QNetworkReply* SendInternal( QString requestString );
};

#endif // AUTHORIZATIONBASE_H
