#ifndef REQUESTIMAGEFIXED_H
#define REQUESTIMAGEFIXED_H

#include "requestpage.h"

#include <QImage>

class RequestImageFixed : public RequestPage
{
	Q_OBJECT
public:
	RequestImageFixed( QObject *parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority,
					int pageIndex, ImageType type, Geometry geometry, int restriction, ImageFormat format );
	void Send(void);

	ImageType Type(void)const;
	Geometry GetGeometry(void)const;
	int Restriction(void)const;
	ImageFormat Format(void)const;

	void SetRestriction( int restriction );

	void Slice( QRectF rectangle );
	void Hightlight( QRectF rectangle );

signals:
	void SignalFinished( ErrorCode error, QImage image );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

private:
	const ImageType m_type;
	const Geometry m_geometry;
	int m_restriction;
	const ImageFormat m_format;
};


#endif // REQUESTIMAGEFIXED_H
