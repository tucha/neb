#include "blockfingerprintrequest.h"
#include <QJsonDocument>
#include <QJsonObject>
#include "platform_id/platform_id.h"

BlockFingerprintRequest::BlockFingerprintRequest(QObject* parent, QNetworkAccessManager* manager, QString baseUrl, User user, RequestPriority priority):
	ApiBase( parent, manager, baseUrl, priority )
{
    AddGetParameter("token", user.GetToken());
    AddGetParameter("fingerprint", PlatformID::hash());
    AddGetParameter("os", REQUEST_OS_ID);
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

void BlockFingerprintRequest::Send()
{
	ApiBase::Send( QString( "%1/%2/%3/%4/%5" )
				   .arg( BaseUrl() )
				   .arg( "local" )
				   .arg( "tools" )
				   .arg( "viewer" )
				   .arg( "get_viewer_settings.php" ) );
}

void BlockFingerprintRequest::SlotFinished(ErrorCode error, QByteArray data)
{
    Q_UNUSED(data)
	if( error != API_NO_ERROR )
	{
        emit SignalFinished( error, false );
		return;
	}

    emit SignalFinished( error, false );
}

QNetworkReply*BlockFingerprintRequest::SendInternal(QString requestString)
{
	QUrl url = QUrl( requestString );
    url.setQuery( GetParameters() );
	QNetworkRequest request;
	request.setUrl( url );
	request.setPriority( Priority() );
	return Manager()->get( request );
}
