#ifndef USER_H
#define USER_H

#include <QVariantMap>
#include <QDateTime>

#include "sessionpointer.h"

class User
{
public:
	enum UserStatus
	{
		Undefined,
		Anonymous,
		Verified,
		Registered,
		VIP
	};

	explicit User( QVariantMap map = QVariantMap() );

	bool IsEmpty(void)const;

	uint		GetId(void)const;
	QString		GetName(void)const;
	QString		GetLastName(void)const;
	QString		GetEmail(void)const;
	QDateTime	GetDateRegister(void)const;
	QString		GetSecondName(void)const;
	QString		GetToken(void)const;

	void SetStatus( UserStatus status );
	void SetIpIsAllowed( bool value );
	void SetSession( SessionPointer session );

	UserStatus GetStatus(void)const;
	bool IpIsAllowed(void)const;
	SessionPointer GetSession(void)const;

	bool HasStatus(void)const;
	bool HasIpIsAllowedValue(void)const;
	bool HasSession(void)const;

    bool IsAdmin();
private:
	bool Check(void)const;

	QVariantMap m_map;
	SessionPointer m_session;
};

#endif // USER_H
