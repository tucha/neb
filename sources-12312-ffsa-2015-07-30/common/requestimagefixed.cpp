#include "requestimagefixed.h"

#include "getparameterslice.h"
#include "getparameterhighlight.h"

RequestImageFixed::RequestImageFixed(QObject* parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority,
						   int pageIndex, ImageType type, Geometry geometry, int restriction, ImageFormat format ):
    RequestPage( parent, manager, identifier, document, priority, pageIndex ),
	m_type( type ), m_geometry( geometry ), m_restriction( restriction ), m_format( format )
{
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

void RequestImageFixed::Send()
{
	DebugAssert( TestRestriction( Type(), Restriction() ) );

	QString requestString = QString( "%1/%2%3%4%5" )
							.arg( TypeString( Type() ) )
							.arg( GeometryString( GetGeometry() ) )
							.arg( Restriction() )
							.arg( "." )
							.arg( FormatString( Format() ) );
	RequestPage::Send( requestString );
}

RequestPage::ImageType RequestImageFixed::Type() const
{
	return m_type;
}

RequestImageFixed::Geometry RequestImageFixed::GetGeometry() const
{
	return m_geometry;
}

int RequestImageFixed::Restriction() const
{
	return m_restriction;
}

RequestPage::ImageFormat RequestImageFixed::Format() const
{
    return m_format;
}

void RequestImageFixed::SetRestriction(int restriction)
{
	Cancel();
	m_restriction = restriction;
}

void RequestImageFixed::Slice(QRectF rectangle)
{
	AddGetParameter( new GetParameterSlice( rectangle ) );
}

void RequestImageFixed::Hightlight(QRectF rectangle)
{
	AddGetParameter( new GetParameterHighlight( rectangle ) );
}

void RequestImageFixed::SlotFinished(ErrorCode error, QByteArray data)
{
	if( error == API_NO_ERROR ) emit SignalFinished( error, ParseImage( data, Format() ) );
	else emit SignalFinished( error, QImage() );
}
