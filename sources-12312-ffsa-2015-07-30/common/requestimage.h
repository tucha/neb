#ifndef REQUESTIMAGE_H
#define REQUESTIMAGE_H

#include "requestpage.h"

#include <QImage>

class RequestImage : public RequestPage
{
	Q_OBJECT
public:
	RequestImage( QObject *parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority,
					int pageIndex, ImageType type, int ppi, ImageFormat format );
	void Send(void);

	ImageType Type(void)const;
	int Ppi(void)const;
	ImageFormat Format(void)const;

	void Slice( QRectF rectangle );
	void Hightlight( QRectF rectangle );

signals:
	void SignalFinished( ErrorCode error, QImage image );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

private:
	const ImageType m_type;
	const int m_ppi;
	const ImageFormat m_format;
};


#endif // REQUESTIMAGE_H
