#include "checktrustedhostrequest.h"
#include <QJsonDocument>
#include <QJsonObject>
#include "platform_id/platform_id.h"
CheckTrustedHostRequest::CheckTrustedHostRequest(QObject* parent, QNetworkAccessManager* manager, QString baseUrl, User user, RequestPriority priority):
	ApiBase( parent, manager, baseUrl, priority )
{
    AddGetParameter("token", user.GetToken());
    AddGetParameter("fingerprint", PlatformID::hash());
    AddGetParameter("os", REQUEST_OS_ID);
    AddGetParameter("action", "check");

	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

void CheckTrustedHostRequest::Send()
{
    ApiBase::Send( QString( "%1/%2/%3/" )
                      .arg( BaseUrl() )
                      .arg( "rest_api" )
                      .arg( "hosts" )
                       );
}

void CheckTrustedHostRequest::SlotFinished(ErrorCode error, QByteArray data)
{
    emit SignalFinished( error, QJsonDocument::fromJson(data).toVariant().toMap() );
}

QNetworkReply*CheckTrustedHostRequest::SendInternal(QString requestString)
{
	QUrl url = QUrl( requestString );
    url.setQuery( GetParameters() );
	QNetworkRequest request;
	request.setUrl( url );
	request.setPriority( Priority() );
	return Manager()->get( request );
}
