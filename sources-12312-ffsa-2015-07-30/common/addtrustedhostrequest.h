#ifndef ADDTRUSTEDHOSTREQUEST_H
#define ADDTRUSTEDHOSTREQUEST_H

#include "apibase.h"
#include "viewersettings.h"
#include "user.h"

class AddTrustedHostRequest : public ApiBase
{
	Q_OBJECT

public:
    explicit AddTrustedHostRequest(QObject *parent, QNetworkAccessManager* manager, QString baseUrl,
                                   User user,
            RequestPriority priority = PRIORITY_NORMAL );

	void Send();

signals:
    void SignalFinished( ErrorCode error, bool ok );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

protected:
	virtual QNetworkReply* SendInternal( QString requestString );

private:
};

#endif // ADDTRUSTEDHOSTREQUEST_H
