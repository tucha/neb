#ifndef REQUESTWORDPAGES_H
#define REQUESTWORDPAGES_H

#include "request.h"

#include <QHash>
#include <QList>

typedef QHash<QString,QList<int> > WordPages;

class RequestWordPages : public Request
{
	Q_OBJECT
public:
	RequestWordPages( QObject *parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority,
				 QStringList words );
	void Send(void);
	QStringList Words(void)const;

signals:
	void SignalFinished( ErrorCode error, WordPages wordPages );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

private:
	WordPages ParseWordPages(QStringList words, QByteArray data);

	const QStringList m_words;
};


#endif // REQUESTWORDPAGES_H
