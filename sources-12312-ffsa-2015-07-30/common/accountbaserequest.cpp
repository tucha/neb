#include "accountbaserequest.h"
#include "bookmarksaddrequest.h"
#include "bookmarksgetrequest.h"
#include "bookmarksdeleterequest.h"
#include "favoritesaddrequest.h"
#include "favoritesgetrequest.h"
#include "favoritesdeleterequest.h"
#include <addtrustedhostrequest.h>
#include <blockfingerprintrequest.h>
#include <checktrustedhostrequest.h>

#include "session.h"

AccountBaseRequest::AccountBaseRequest(QObject *parent, QNetworkAccessManager* manager, QString baseUrl, User user, RequestPriority priority) :
	ApiBase( parent, manager, baseUrl, priority ),
	m_user( user )
{
	if( GetUser().HasSession() )
	{
		connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
	}
}

User AccountBaseRequest::GetUser() const
{
	return m_user;
}

QString AccountBaseRequest::UserToken() const
{
	return m_user.GetToken();
}

void AccountBaseRequest::Send(QString request)
{
	ApiBase::Send( QString( "%1/%2/%3/" )
				   .arg( BaseUrl() )
				   .arg( "rest_api" )
				   .arg( request ) );
}

AccountBaseRequest*AccountBaseRequest::New( RequestPriority priority ) const
{
	return new AccountBaseRequest( parent(), Manager(), BaseUrl(), GetUser(), priority );
}

BookmarksAddRequest*AccountBaseRequest::NewBookmarksAdd(Document document, int pageIndex, QString resPath, RequestPriority priority) const
{
    return new BookmarksAddRequest( parent(), Manager(), BaseUrl(), GetUser(), document, pageIndex, resPath, priority );
}

BookmarksGetRequest*AccountBaseRequest::NewBookmarksGetForAllPages(Document document, RequestPriority priority) const
{
	return new BookmarksGetRequest( parent(), Manager(), BaseUrl(), GetUser(), document, ALL_PAGES, false, priority );
}

BookmarksGetRequest*AccountBaseRequest::NewBookmarksGetForOnePage(Document document, int pageIndex, RequestPriority priority) const
{
	return new BookmarksGetRequest( parent(), Manager(), BaseUrl(), GetUser(), document, pageIndex, false, priority );
}

BookmarksDeleteRequest*AccountBaseRequest::NewBookmarksDelete(Bookmark bookmark, RequestPriority priority) const
{
	DebugAssert( bookmark.IsComment() == false );
	DebugAssert( GetUser().GetToken() == bookmark.GetUser().GetToken() );
	return new BookmarksDeleteRequest( parent(), Manager(), BaseUrl(), bookmark, priority );
}

BookmarksAddRequest*AccountBaseRequest::NewCommentsAdd(Document document, int pageIndex, QString resPath, QString text, RequestPriority priority) const
{
    return new BookmarksAddRequest( parent(), Manager(), BaseUrl(), GetUser(), document, pageIndex, resPath, text, priority );
}

BookmarksGetRequest*AccountBaseRequest::NewCommentsGetForAllPages(Document document, RequestPriority priority) const
{
	return new BookmarksGetRequest( parent(), Manager(), BaseUrl(), GetUser(), document, ALL_PAGES, true, priority );
}

BookmarksGetRequest*AccountBaseRequest::NewCommentsGetForOnePage(Document document, int pageIndex, RequestPriority priority) const
{
	return new BookmarksGetRequest( parent(), Manager(), BaseUrl(), GetUser(), document, pageIndex, true, priority );
}

BookmarksDeleteRequest*AccountBaseRequest::NewCommentsDelete(Bookmark bookmark, RequestPriority priority) const
{
	DebugAssert( bookmark.IsComment() );
	DebugAssert( GetUser().GetToken() == bookmark.GetUser().GetToken() );
	return new BookmarksDeleteRequest( parent(), Manager(), BaseUrl(), bookmark, priority );
}

FavoritesAddRequest*AccountBaseRequest::NewFavoritesAdd(Document document, RequestPriority priority) const
{
	DebugAssert( document.IsEmpty() == false );
	return new FavoritesAddRequest( parent(), Manager(), BaseUrl(), GetUser(), document, priority );
}

FavoritesGetRequest*AccountBaseRequest::NewFavoritesGet(RequestPriority priority) const
{
	return new FavoritesGetRequest( parent(), Manager(), BaseUrl(), GetUser(), priority );
}

FavoritesDeleteRequest*AccountBaseRequest::NewFavoritesDelete(FavoriteBook favoriteBook, RequestPriority priority) const
{
	DebugAssert( GetUser().GetToken() == favoriteBook.GetUser().GetToken() );
    return new FavoritesDeleteRequest( parent(), Manager(), BaseUrl(), favoriteBook, priority );
}

CheckTrustedHostRequest *AccountBaseRequest::NewCheckTrustedHostRequest(RequestPriority priority) const
{
    return new CheckTrustedHostRequest(parent(), Manager(), BaseUrl(), GetUser(), priority );
}

AddTrustedHostRequest *AccountBaseRequest::NewAddTrustedHostRequest(RequestPriority priority) const
{
    return new AddTrustedHostRequest(parent(), Manager(), BaseUrl(), GetUser(), priority );

}

BlockFingerprintRequest *AccountBaseRequest::NewBlockFingerprintRequest(RequestPriority priority) const
{
    return new BlockFingerprintRequest(parent(), Manager(), BaseUrl(), GetUser(), priority );
}

void AccountBaseRequest::SlotFinished(ErrorCode error, QByteArray data)
{
	DebugAssert( GetUser().IsEmpty() == false );
	if( GetUser().IsEmpty() ) return;

	DebugAssert( GetUser().HasSession() );
	if( GetUser().HasSession() == false ) return;

	UNUSED( data );
	if( error != API_NO_ERROR ) GetUser().GetSession()->CheckToken();
}

QNetworkReply*AccountBaseRequest::SendInternal(QString requestString)
{
	UNUSED( requestString );
	NotImplemented();
	return NULL;
}
