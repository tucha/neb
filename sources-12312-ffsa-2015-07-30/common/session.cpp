#include "session.h"

#include "defines.h"
#include "debug.h"

SessionPointer Session::New(AuthorizationBase* authorizationBase, User user)
{
	DebugAssert( authorizationBase != NULL );
	if( authorizationBase == NULL ) return SessionPointer();

	DebugAssert( user.IsEmpty() == false );
	if( user.IsEmpty() ) return SessionPointer();

	return SessionPointer( new Session( NULL, authorizationBase, user ) );
}

Session::Session(QObject *parent, AuthorizationBase* authorizationBase, User user):
	QObject(parent),
	m_authorizationCheck( authorizationBase->NewCheck( user.GetToken() ) ),
	m_tokenExpired( false )
{
    m_sessionId = qrand();
	connect(m_authorizationCheck,SIGNAL(SignalFinished(ErrorCode,AuthorizationResult)),this,SLOT(SlotAuthorizationCheckFinished(ErrorCode,AuthorizationResult)));
}

Session::~Session()
{
	DeleteLaterAndNull( m_authorizationCheck );
	m_tokenExpired = false;
}

QString Session::GetToken() const
{
	return m_authorizationCheck->Token();
}

void Session::CheckToken()
{
	if( TokenExpired() ) return;
	if( m_authorizationCheck->IsRunning() ) return;
	m_authorizationCheck->Send();
}

bool Session::TokenExpired() const
{
    return m_tokenExpired;
}

QString Session::GetID()
{
    return QString::number(m_sessionId);
}

void Session::SlotAuthorizationCheckFinished(ErrorCode error, AuthorizationResult result)
{
	if( m_authorizationCheck == NULL ) return;

	DebugAssert( sender() == m_authorizationCheck );
	if( sender() != m_authorizationCheck ) return;

	if( error != API_NO_ERROR ) return;

	DebugAssert( result.IsEmpty() == false );
	if( result.IsEmpty() ) return;

	if( result.GetError().isEmpty() == false )
	{
		DebugOutput( "TokenExpired" );
		m_tokenExpired = true;
		emit SignalTokenExpired();
		return;
	}

	User user = result.GetUser();

	DebugAssert( user.IsEmpty() == false );
	if( user.IsEmpty() ) return;

	DebugAssert( user.GetToken() == GetToken() );
	if( user.GetToken() != GetToken() ) return;
}
