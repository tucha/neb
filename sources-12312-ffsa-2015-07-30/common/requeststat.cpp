#include "requeststat.h"

#define DATE_FORMAT "yyyy-MM-dd"

RequestStat::RequestStat(QObject* parent, QNetworkAccessManager* manager, QString baseUrl, QDate from, QDate to, RequestPriority priority):
	ApiBase( parent, manager, baseUrl, priority ),
	m_from( from ),
	m_to( to )
{
	AddGetParameter( "login", "elar" );
	AddGetParameter( "password", "sT5df2" );
	AddGetParameter( "from", From().toString( DATE_FORMAT ) );
	AddGetParameter( "to", To().toString( DATE_FORMAT ) );

	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

void RequestStat::Send()
{
	ApiBase::Send( "stat" );
}

QDate RequestStat::From() const
{
	return m_from;
}

QDate RequestStat::To() const
{
	return m_to;
}

QNetworkReply*RequestStat::SendInternal(QString requestString)
{
	QUrl url = QUrl( QString( "%1/%2" )
				  .arg( BaseUrl() )
				  .arg( requestString ) );

	QNetworkRequest request;
	request.setUrl( url );
	request.setPriority( Priority() );

#if QT_VERSION >= 0x050000
	QByteArray data = GetParameters().query().toUtf8();
#else
	QList<QPair<QString, QString> > prms = GetParameters();

	QByteArray data;
	for( QList<QPair<QString, QString> >::iterator iter = prms.begin(); iter != prms.end(); ++iter )
	{
		data.append( iter->first + url.queryValueDelimiter() + iter->second + url.queryPairDelimiter() );
	}
	if( data.endsWith( url.queryPairDelimiter() ) ) data.remove( data.length() - 1, 1 );
#endif
	return Manager()->post( request, data );
}

void RequestStat::SlotFinished(ErrorCode error, QByteArray data)
{
	if( error != API_NO_ERROR ) emit SignalFinished( error, QString() );
	else emit SignalFinished( error, data );
}
