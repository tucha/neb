#include "favoritebook.h"

#include "defines.h"

FavoriteBook::FavoriteBook():
	m_user(),
	m_document(),
	m_id()
{
	DebugAssert( IsEmpty() );
}

FavoriteBook::FavoriteBook(User user, Document document, QString id):
	m_user( user ),
	m_document( document ),
	m_id( id )
{
	DebugAssert( IsEmpty() == false );
}

bool FavoriteBook::IsEmpty() const
{
	bool result = m_id.isEmpty();

	if( result )
	{
		DebugAssert( m_user.IsEmpty() );
		DebugAssert( m_document.IsEmpty() );
		DebugAssert( m_id.isEmpty() );
	}
	else
	{
		DebugAssert( m_user.IsEmpty() == false );
		DebugAssert( m_document.IsEmpty() == false );
		DebugAssert( m_id.isEmpty() == false );
	}

	return result;
}

User FavoriteBook::GetUser() const
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return User();

	return m_user;
}

Document FavoriteBook::GetDocument() const
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return Document();

	return m_document;
}

QString FavoriteBook::Id() const
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return QString();

	return m_id;
}
