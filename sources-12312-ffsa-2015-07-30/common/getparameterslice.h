#ifndef GETPARAMETERSLICE_H
#define GETPARAMETERSLICE_H

#include "getparameter.h"

class GetParameterSlice : public GetParameter
{
public:
	explicit GetParameterSlice( const QRectF& rectangle );
	virtual ~GetParameterSlice();
	virtual QString Key(void)const;
	virtual QString Value(void)const;

	QRectF Rectangle(void)const;

private:
	const QRectF m_rectangle;
};

#endif // GETPARAMETERSLICE_H
