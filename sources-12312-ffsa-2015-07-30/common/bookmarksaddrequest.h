#ifndef BOOKMARKSADDREQUEST_H
#define BOOKMARKSADDREQUEST_H

#include "accountbaserequest.h"
#include "document.h"
#include "bookmark.h"

class BookmarksAddRequest : public AccountBaseRequest
{
	Q_OBJECT
public:
	explicit BookmarksAddRequest(QObject *parent, QNetworkAccessManager* manager, QString baseUrl,
                                 User user, Document document, int pageIndex, QString resPath,RequestPriority priority = PRIORITY_NORMAL );
	explicit BookmarksAddRequest(QObject *parent, QNetworkAccessManager* manager, QString baseUrl,
                                 User user, Document document, int pageIndex, QString resPath,QString text, RequestPriority priority = PRIORITY_NORMAL );
	Document GetDocument(void)const;
	int PageIndex(void)const;
	QString Text(void)const;
	bool IsComment(void)const;

	void Send(void);

    QString ResPath() const;
signals:
	void SignalFinished( ErrorCode error, Bookmark result );
	void SignalError();

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

protected:
	virtual QNetworkReply* SendInternal( QString requestString );

private:
	void Construct(void);
	Bookmark ParseResult( QByteArray data );

	const Document m_document;
	const int m_pageIndex;
	const QString m_text;
    const QString m_resPath;
};

#endif // BOOKMARKSADDREQUEST_H
