#include "example.h"

#include <QScrollArea>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTextEdit>
#include <QMap>
#include <QVariantMap>
#include <QLabel>
#include <QPixmap>
#include <QAuthenticator>
#include <QNetworkProxy>
#include <QInputDialog>

#define CLIENT_ID	"99Apaf8YwtAskq5sdfyNMnbD3SDU7qUH"
#define DOCUMENT	"https://relar.rsl.ru?id=01003489481"
#define BASE_URL	"https://relar.rsl.ru"
#define TEXT_LIMIT	300
#define PAGES_LIMIT 10

#include "defines.h"

Example::Example(QWidget *parent) :
	QMainWindow(parent),
	m_manager( new QNetworkAccessManager( this ) ),
	m_user(),
	m_bookmarks( NULL, PRIORITY_HIGHT ),
	m_comments( NULL, PRIORITY_HIGHT )
{
	connect(m_manager,SIGNAL(proxyAuthenticationRequired(QNetworkProxy,QAuthenticator*)),this,SLOT(SlotProxyAuthenticationRequired(QNetworkProxy,QAuthenticator*)));

	CreateGui();
	StartAuthentication();
	this->resize( 800, 600 );
}

void Example::CreateGui()
{
	m_scrollArea = new QScrollArea();
	m_horizontalBoxLayout = new QHBoxLayout();
	m_verticalBoxLayout1 = new QVBoxLayout();
	m_verticalBoxLayout2 = new QVBoxLayout();
	m_verticalBoxLayout3 = new QVBoxLayout();
	m_widget = new QWidget();
	m_widget1 = new QWidget();
	m_widget2 = new QWidget();
	m_widget3 = new QWidget();

	m_widget1->setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed );
	m_widget2->setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed );
	m_widget3->setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed );

	m_widget1->setLayout( m_verticalBoxLayout1 );
	m_widget2->setLayout( m_verticalBoxLayout2 );
	m_widget3->setLayout( m_verticalBoxLayout3 );

	m_horizontalBoxLayout->addWidget( m_widget3, 0, Qt::AlignTop );
	m_horizontalBoxLayout->addWidget( m_widget1, 0, Qt::AlignTop );
	m_horizontalBoxLayout->addWidget( m_widget2, 0, Qt::AlignTop );

	m_widget->setLayout( m_horizontalBoxLayout );
	m_scrollArea->setWidget( m_widget );
	this->setCentralWidget( m_scrollArea );
}

void Example::StartAuthentication()
{
	QString domain = "http://xn--90ax2c.xn--p1ai";
	AuthorizationBase* base = new AuthorizationBase( this, m_manager, domain );

	QString email = "pushkin@elar.ru";
	QString password = "111111";

	AuthorizationRequest* request = base->NewRequest( email, password );
	connect(request,SIGNAL(SignalFinished(ErrorCode,AuthorizationResult)),this,SLOT(SlotAuthorizationRequestFinished(ErrorCode,AuthorizationResult)));
	connect(request,SIGNAL(SignalAuthenticationRequired(QAuthenticator*)),this,SLOT(SlotAuthenticationRequired(QAuthenticator*)));
	request->Send();
}

void Example::StartDownloading()
{
	RequestBase* base = new RequestBase( this, m_manager, CreateId(), BASE_URL );

	RequestCollections* collections = base->NewCollections();
	connect(collections,SIGNAL(SignalFinished(ErrorCode,QStringList)),this,SLOT(SlotFinishedCollections(ErrorCode,QStringList)));
	collections->Send();

	Request* request = base->NewRequest( Document( DOCUMENT ) );

	RequestFileExtensions* r = request->NewFileExtensions();
	connect(r,SIGNAL(SignalFinished(ErrorCode,QStringList)),this,SLOT(SlotFinishedFileExtensions(ErrorCode,QStringList)));
	r->Send();

	RequestDescription* d = request->NewDescription( "300", "a" );
	connect(d,SIGNAL(SignalFinished(ErrorCode,QString)),this,SLOT(SlotFinishedDescription(ErrorCode,QString)));
	d->Send();

	RequestInformation* i = request->NewInformation();
	connect(i,SIGNAL(SignalFinished(ErrorCode,QVariantMap)),this,SLOT(SlotFinishedInformation(ErrorCode,QVariantMap)));
	i->Send();

	RequestDocumentType* t = request->NewDocumentType();
	connect(t,SIGNAL(SignalFinished(ErrorCode,QString)),this,SLOT(SlotFinishedDocumentType(ErrorCode,QString)));
	t->Send();

	RequestCollection* c = request->NewCollection();
	connect(c,SIGNAL(SignalFinished(ErrorCode,QString)),this,SLOT(SlotFinishedCollection(ErrorCode,QString)));
	c->Send();

	RequestPagesCount* p = request->NewPagesCount();
	connect(p,SIGNAL(SignalFinished(ErrorCode,int)),this,SLOT(SlotFinishedPagesCount(ErrorCode,int)));
	p->Send();

	RequestPageSet* s = request->NewPageSet( PageSet::FromString( "1,3-5,7,9" ) );
	connect(s,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinishedPageSet(ErrorCode,QByteArray)));
	s->Send();

	RequestWordPages* w = request->NewWordPages( QStringList() << "a" << "11" << "12" );
	connect(w,SIGNAL(SignalFinished(ErrorCode,WordPages)),this,SLOT(SlotFinishedWordPages(ErrorCode,WordPages)));
	w->Send();
}

void Example::StartAccountTest()
{
	AccountBaseRequest* base = new AccountBaseRequest( this, m_manager, "http://xn--90ax2c.xn--p1ai", m_user );
	Document document( DOCUMENT );

	connect(&m_bookmarks,SIGNAL(SignalUpdate(BookmarksManager*)),this,SLOT(BookmarksUpdate(BookmarksManager*)));
	connect(&m_comments,SIGNAL(SignalUpdate(CommentsManager*)),this,SLOT(CommentsUpdate(CommentsManager*)));

	m_bookmarks.Load( base, document );
	m_comments.Load( base, document );
}

void Example::Add1(QString text, int maxHeight)
{
	QTextEdit* textEdit = new QTextEdit();
	textEdit->setText( text );
	textEdit->setMaximumHeight( maxHeight );

	m_verticalBoxLayout1->addWidget( textEdit );
	m_widget->resize( m_widget->sizeHint() );
}

void Example::Add2(QImage image)
{
	QLabel* label = new QLabel();
	label->setPixmap( QPixmap::fromImage( image ) );
	m_verticalBoxLayout2->addWidget( label );
	m_widget->resize( m_widget->sizeHint() );
}

void Example::Add3(QString text, int maxHeight)
{
	QTextEdit* textEdit = new QTextEdit();
	textEdit->setText( text );
	textEdit->setMaximumHeight( maxHeight );

	m_verticalBoxLayout3->addWidget( textEdit );
	m_widget->resize( m_widget->sizeHint() );
}

Identifier Example::CreateId() const
{
	Identifier result;
	result.SetClientId( CLIENT_ID );
	result.SetUserToken( m_user.GetToken() );
	return result;
}

void Example::SlotFinishedCollections(ErrorCode error, QStringList collections)
{
	if( error != API_NO_ERROR )
	{
		Add3( QString( "RequestCollections failed: %1" ).arg( error ) );
		return;
	}

	QString text = "RequestCollections\n";
	foreach (QString collection, collections)
	{
		text.append( collection );
		text.append( "\n" );

		RequestMarks* marks = SENDER->NewMarks( collection );
		connect(marks,SIGNAL(SignalFinished(ErrorCode,QString)),this,SLOT(SlotFinishedMarks(ErrorCode,QString)));
		marks->Send();

		RequestTexts* texts = SENDER->NewTexts( collection );
		connect(texts,SIGNAL(SignalFinished(ErrorCode,QString)),this,SLOT(SlotFinishedTexts(ErrorCode,QString)));
		texts->Send();

	}
	Add3( text );

	SENDER->deleteLater();
}

void Example::SlotFinishedMarks(ErrorCode error, QString marks)
{
	RequestMarks* s = static_cast<RequestMarks*>( sender() );

	if( error != API_NO_ERROR )
	{
		Add3( QString( "RequestMarks %1 failed: %2" )
			  .arg( s->Collection() )
			  .arg( error ) );
		return;
	}

	QString text = "RequestMarks " + s->Collection() + "\n";
	text.append( marks.left( TEXT_LIMIT ) );
	Add3( text );
	s->deleteLater();
}

void Example::SlotFinishedTexts(ErrorCode error, QString texts)
{
	RequestTexts* s = static_cast<RequestTexts*>( sender() );

	if( error != API_NO_ERROR )
	{
		Add3( QString( "RequestTexts %1 failed: %2" )
			  .arg( s->Collection() )
			  .arg( error ) );
		return;
	}

	QString text = "RequestTexts " + s->Collection() + "\n";
	text.append( texts.left( TEXT_LIMIT ) );
	Add3( text );
	s->deleteLater();
}

void Example::SlotProxyAuthenticationRequired(const QNetworkProxy& proxy, QAuthenticator* authenticator)
{
	QString user = QInputDialog::getText( this, proxy.hostName(), "Username:" );
	QString password = QInputDialog::getText( this, proxy.hostName(), "Password" );

	authenticator->setUser( user );
	authenticator->setPassword( password );
}

void Example::SlotAuthorizationRequestFinished(ErrorCode error, AuthorizationResult result)
{
	if( error != API_NO_ERROR )
	{
		Add3( QString( "AuthorizationRequest failed: %1" ).arg( error ) );
		return;
	}

	DebugAssert( result.IsEmpty() == false );
	if( result.IsEmpty() ) return;

	if( result.GetError().isEmpty() == false )
	{
		Add3( QString( "AuthorizationRequest failed: %1" ).arg( result.GetError() ) );
		return;
	}

	User user = result.GetUser();

	DebugAssert( user.IsEmpty() == false );
	if( user.IsEmpty() ) return;

	QString text = "AuthorizationRequest succeed:\n";
	text += QString( "%1 = %2\n" ).arg( "ID" ).arg( user.GetId() );
	text += QString( "%1 = %2\n" ).arg( "NAME" ).arg( user.GetName() );
	text += QString( "%1 = %2\n" ).arg( "LAST_NAME" ).arg( user.GetLastName() );
	text += QString( "%1 = %2\n" ).arg( "EMAIL" ).arg( user.GetEmail() );
	text += QString( "%1 = %2\n" ).arg( "DATE_REGISTER" ).arg( user.GetDateRegister().toString() );
	text += QString( "%1 = %2\n" ).arg( "SECOND_NAME" ).arg( user.GetSecondName() );
	text += QString( "%1 = %2\n" ).arg( "TOKEN" ).arg( user.GetToken() );
	Add3( text, 200 );

	AuthorizationCheck* check = SENDER_AUTH->NewCheck( user.GetToken() );
	connect(check,SIGNAL(SignalFinished(ErrorCode,AuthorizationResult)),this,SLOT(SlotAuthorizationCheckFinished(ErrorCode,AuthorizationResult)));
	check->Send();
}

void Example::SlotAuthorizationCheckFinished(ErrorCode error, AuthorizationResult result)
{
	if( error != API_NO_ERROR )
	{
		Add3( QString( "AuthorizationCheck failed: %1" ).arg( error ) );
		return;
	}

	DebugAssert( result.IsEmpty() == false );
	if( result.IsEmpty() ) return;

	if( result.GetError().isEmpty() == false )
	{
		Add3( QString( "AuthorizationCheck failed: %1" ).arg( result.GetError() ) );
		return;
	}

	User user = result.GetUser();

	DebugAssert( user.IsEmpty() == false );
	if( user.IsEmpty() ) return;

	AuthorizationCheck* check = static_cast<AuthorizationCheck*>( sender() );
	DebugAssert( check->Token() == user.GetToken() );
	UNUSED( check );

	DebugAssert( m_user.IsEmpty() );
	if( m_user.IsEmpty() == false ) return;

	m_user = user;
	Add3( "AuthorizationCheck succeed", 30 );
	StartDownloading();
	StartAccountTest();
}

void Example::SlotAuthenticationRequired(QAuthenticator* authenticator)
{
	authenticator->setUser( "user" );
	authenticator->setPassword( "kEAVyhx9tn" );
}

void Example::SlotFinishedFileExtensions(ErrorCode error, QStringList fileExtensions)
{
	UNUSED( error );

	QString text = "RequestFileExtensions\n";
	foreach( QString fileExtension, fileExtensions )
	{
		text += fileExtension;
		text += "\n";

		RequestFile* r = SENDER->NewFile( fileExtension );
		connect(r,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinishedFile(ErrorCode,QByteArray)));
		r->Send();

		RequestFileSize* f = SENDER->NewFileSize( fileExtension );
		connect(f,SIGNAL(SignalFinished(ErrorCode,int)),this,SLOT(SlotFinishedFileSize(ErrorCode,int)));
		f->Send();
	}

	Add1( text );

	sender()->deleteLater();
}

void Example::SlotFinishedFile(ErrorCode error, QByteArray fileData)
{
	UNUSED( error );

	RequestFile* s = static_cast<RequestFile*>( sender() );

	QString text = "RequestFile " + s->FileExtension() + "\n";
	text += QString( fileData.left( TEXT_LIMIT ) );

	Add1( text );

	sender()->deleteLater();
}

void Example::SlotFinishedFileSize(ErrorCode error, int fileSize)
{
	UNUSED( error );

	RequestFileSize* s = static_cast<RequestFileSize*>( sender() );

	QString text = "RequestFileSize " + s->FileExtension() + "\n";
	text += QString::number( fileSize );

	Add1( text, 50 );

	sender()->deleteLater();
}

void Example::SlotFinishedDescription(ErrorCode error, QString description)
{
	UNUSED( error );

	RequestDescription* s = static_cast<RequestDescription*>( sender() );

	QString text = "RequestDescription " + s->Field() + " " + s->SubField() + "\n";
	text += description;

	Add1( text, 50 );

	sender()->deleteLater();
}

void Example::SlotFinishedInformation(ErrorCode error, QVariantMap information)
{
	UNUSED( error );

	QString text = "RequestInformation\n";

	foreach( QString key, information.keys() )
	{
		text += key + ": " + information.value( key ).toString() + "\n";
	}

	Add1( text );

	sender()->deleteLater();
}

void Example::SlotFinishedDocumentType(ErrorCode error, QString documentType)
{
	UNUSED( error );

	QString text = "RequestDocumentType\n";
	text += documentType;
	Add1( text, 50 );

	sender()->deleteLater();
}

void Example::SlotFinishedCollection(ErrorCode error, QString collection)
{
	UNUSED( error );

	QString text = "RequestCollection\n";
	text += collection;
	Add1( text, 50 );

	sender()->deleteLater();
}

void Example::SlotFinishedPagesCount(ErrorCode error, int pagesCount)
{
	UNUSED( error );

	QString text = "RequestPagesCount\n";
	text += QString::number( pagesCount );
	Add1( text, 50 );

	RequestPagesCount* s = static_cast<RequestPagesCount*>( sender() );
	for( int i = 1; i <= pagesCount; ++i )
	{
		if( i == PAGES_LIMIT ) break;

		RequestPage* base = s->NewPage( i );

		RequestPageSize* pageSize = base->NewSize();
		connect(pageSize,SIGNAL(SignalFinished(ErrorCode,QSize)),this,SLOT(SlotFinishedPageSize(ErrorCode,QSize)));
		pageSize->Send();

		RequestPage::ImageType type = TYPE_IMAGES;
		RequestPage::ImageFormat format = FORMAT_JPEG;

		RequestImage* pageImage = base->NewImage( type, 100, format );
		connect(pageImage,SIGNAL(SignalFinished(ErrorCode,QImage)),this,SLOT(SlotFinishedImage(ErrorCode,QImage)));
		pageImage->Slice( QRectF( 0, 0.666, 1, 0.333 ) );
		pageImage->Send();

		RequestImageFixed* pageImageFixed = base->NewImageFixed( type, GEOMETRY_MAX, 301, format );
		connect(pageImageFixed,SIGNAL(SignalFinished(ErrorCode,QImage)),this,SLOT(SlotFinishedImageFixed(ErrorCode,QImage)));
		pageImageFixed->Hightlight( QRectF( 0, 0.666, 1, 0.333 ) );
		pageImageFixed->Send();

		RequestWords* w = base->NewWords();
		connect(w,SIGNAL(SignalFinished(ErrorCode,QList<Word>)),this,SLOT(SlotFinishedWords(ErrorCode,QList<Word>)));
		w->Send();

		base->deleteLater();
	}


	sender()->deleteLater();
}

void Example::SlotFinishedPageSet(ErrorCode error, QByteArray pageSet)
{
	UNUSED( error );

	RequestPageSet* s = static_cast<RequestPageSet*>( sender() );
	QString text = "RequestPageSet " + s->GetPageSet().ToString() + "\n";
	text += QString( pageSet.left( TEXT_LIMIT ) );
	Add1( text );

	sender()->deleteLater();
}

void Example::SlotFinishedWordPages(ErrorCode error, WordPages wordPages)
{
	UNUSED( error );

	RequestWordPages* s = static_cast<RequestWordPages*>( sender() );
	QString text = "RequestWordPages \n";

	const QString separator = ", ";
	QList<QString> words = wordPages.keys();
	foreach( QString word, words )
	{
		text += word + ": ";

		QList<int> pages = wordPages.value( word );
		foreach( int page, pages )
		{
			text += QString::number( page ) + separator;
		}
		if( pages.count() == 0 ) text += "NotFound";
		else text = text.left( text.length() - separator.length() );
		text += "\n";
	}

	Add1( text );

	DeleteLaterAndNull( s );
}

void Example::SlotFinishedPageSize(ErrorCode error, QSize pageSize)
{
	UNUSED( error );

	RequestPageSize* s = static_cast<RequestPageSize*>( sender() );
	QString text = "RequestPageSize " + QString::number( s->PageIndex() ) +  "\n";
	text += QString( "%1,%2" )
			.arg( pageSize.width() )
			.arg( pageSize.height() );
	Add1( text, 50 );

	sender()->deleteLater();
}

void Example::SlotFinishedImage(ErrorCode error, QImage image)
{
	UNUSED( error );

	Add2( image );

	sender()->deleteLater();
}

void Example::SlotFinishedImageFixed(ErrorCode error, QImage image)
{
	UNUSED( error );

	Add2( image );

	sender()->deleteLater();
}

void Example::SlotFinishedWords(ErrorCode error, QList<Word> words)
{
	UNUSED( error );

	RequestWords* s = static_cast<RequestWords*>( sender() );
	QString text = "RequestWords " + QString::number( s->PageIndex() ) + "\n";
	foreach( Word word, words )
	{
		text += word.Text() + "\n";
	}
	Add1( text.left( TEXT_LIMIT ) );

	sender()->deleteLater();
}

void Example::SlotFinishedWordCoordinates(ErrorCode error, WordCoordinates wordCoordinates)
{
	UNUSED( error );
	UNUSED( wordCoordinates );

	sender()->deleteLater();
}

void Example::SlotFinishedImageWithWords(ErrorCode error, QImage image)
{
	UNUSED( error );
	UNUSED( image );

	sender()->deleteLater();
}

void Example::BookmarksUpdate(BookmarksManager* manager)
{
	int pageIndex = 5;

    bool clickable = manager->IsActual( pageIndex, "" );
    bool bookmark = manager->Contains( pageIndex, "" );

	Add1( QString( "Bookmark = %1" ).arg( bookmark ), 50 );

	if( clickable && ( bookmark == false ) )
	{
        manager->SendRequest( pageIndex, "", !bookmark );
		// ... and set bookmarks button to not clickable state until request is completed
	}
}

void Example::CommentsUpdate(CommentsManager* manager)
{
	int pageIndex = 5;

    bool clickable = manager->IsActual( pageIndex, "" );
    QString comment = manager->Get( pageIndex, "" );

	Add1( QString( "Comment = %1" ).arg( comment ), 50 );

	if( clickable && comment.isEmpty() )
	{
        manager->SendRequest( pageIndex, "", "New Comment" );
		// ... and set comments button to not clickable state until request is completed
	}
}
