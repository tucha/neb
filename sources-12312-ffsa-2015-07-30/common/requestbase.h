#ifndef REQUESTBASE_H
#define REQUESTBASE_H

#include "apibase.h"
#include "identifier.h"
#include "document.h"

class Request;
class RequestCollections;
class RequestMarks;
class RequestTexts;

class RequestBase : public ApiBase
{
	Q_OBJECT

public:
	// Constructor / Destructor
	RequestBase(
			QObject *parent,
			QNetworkAccessManager* manager,
			Identifier identifier,
			QString baseUrl,
			RequestPriority priority = PRIORITY_NORMAL );
	virtual ~RequestBase();

	Request*			NewRequest( Document document, RequestPriority priority = PRIORITY_NORMAL )const;
	RequestCollections*	NewCollections( RequestPriority priority = PRIORITY_NORMAL )const;
	RequestMarks*		NewMarks( QString collection, RequestPriority priority = PRIORITY_NORMAL )const;
	RequestTexts*		NewTexts( QString collection, RequestPriority priority = PRIORITY_NORMAL )const;

	// Public properties
    Identifier GetIdentifier(void)const;

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

protected:
	// Protected methods
	virtual QNetworkReply* SendInternal(QString requestString);

private:
	// Private methods
	void ApplyIdentifier(void);

	// Private members
    const Identifier m_identifier;
};

#endif // REQUESTBASE_H
