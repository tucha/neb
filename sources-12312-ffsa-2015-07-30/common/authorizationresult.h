#ifndef AUTHORIZATIONRESULT_H
#define AUTHORIZATIONRESULT_H

#include <QString>
#include <QByteArray>

#include "user.h"

class AuthorizationResult
{
public:
	AuthorizationResult();
	AuthorizationResult( QByteArray data );

	bool	IsEmpty(void)const;
	QString	GetError(void)const;
	User	GetUser(void)const;

private:
	bool	m_isEmpty;
	QString	m_error;
	User	m_user;
};

#endif // AUTHORIZATIONRESULT_H
