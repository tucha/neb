#include "requestcontent.h"
#include <QJsonDocument>

RequestContent::RequestContent(QObject* parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority):
    Request( parent, manager, identifier, document, priority )
{
    connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

void RequestContent::Send()
{
        QString requestString = QString( "%1" )
                                                        .arg( "arts" )
                                                       ;
        ApiBase::Send( requestString );
}

void RequestContent::SlotFinished(ErrorCode error, QByteArray data)
{
    emit SignalFinished(error, QJsonDocument::fromJson(data).toVariant().toList());
}

