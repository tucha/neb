#ifndef GETPARAMETERSIMPLE_H
#define GETPARAMETERSIMPLE_H

#include "getparameter.h"

class GetParameterSimple : public GetParameter
{
public:
	explicit GetParameterSimple( QString key, QString value );
	virtual ~GetParameterSimple();
	virtual QString Key(void)const;
	virtual QString Value(void)const;

private:
	const QString m_key;
	const QString m_value;
};

#endif // GETPARAMETERSIMPLE_H
