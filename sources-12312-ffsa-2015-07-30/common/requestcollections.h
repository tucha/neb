#ifndef REQUESTCOLLECTIONS_H
#define REQUESTCOLLECTIONS_H

#include "requestbase.h"

#include <QStringList>

class RequestCollections : public RequestBase
{
	Q_OBJECT
public:
	RequestCollections(
			QObject *parent,
			QNetworkAccessManager* manager,
			Identifier identifier,
			QString baseUrl,
			RequestPriority priority = PRIORITY_NORMAL );

	void Send(void);

signals:
	void SignalFinished( ErrorCode error, QStringList collections );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );
};

#endif // REQUESTCOLLECTIONS_H
