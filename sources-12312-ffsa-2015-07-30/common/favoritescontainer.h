#ifndef FAVORITESCONTAINER_H
#define FAVORITESCONTAINER_H

#include <QMap>

#include "favoritebook.h"

class FavoritesContainer
{
public:
	FavoritesContainer();
	FavoritesContainer( FavoriteBooks favoriteBooks );

	bool IsEmpty(void)const;
	bool Add( FavoriteBook favoriteBook );
	bool Remove( Document document );
	bool Contains( Document document )const;
	DocumentList Documents(void)const;
	void Clear();
	FavoriteBook Get( Document document );

private:
	bool Check( FavoriteBook favoriteBook );

	QHash<Document,FavoriteBook> m_favoriteBooks;
	User m_user;
	QMap<QString,Document> m_ids;
};

#endif // FAVORITESCONTAINER_H
