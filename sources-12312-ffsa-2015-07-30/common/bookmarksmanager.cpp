#include "bookmarksmanager.h"

#include "defines.h"

BookmarksManager::BookmarksManager(QObject* parent, RequestPriority priority) :
	BookmarksManagerBase( parent, priority )
{
}

BookmarksManager::~BookmarksManager()
{
    Clear();
}

void BookmarksManager::SendRequest(int pageIndex, QString resPath, bool bookmark)
{
    DebugAssert( pageIndex >= 0 || resPath != "" );

    DebugAssert( IsActual( pageIndex, resPath ) );
    if( !IsActual( pageIndex, resPath ) ) return;

    if( bookmark ) RequestAddition( pageIndex, resPath );
    else RequestRemoval( pageIndex, resPath );
}

BookmarksGetRequest*BookmarksManager::CreateGetRequest()
{
	return BaseRequest()->NewBookmarksGetForAllPages( GetDocument(), Priority() );
}

BookmarksAddRequest*BookmarksManager::CreateAddRequest(int pageIndex, QString resPath, QString text)
{
	UNUSED( text );
	DebugAssert( text.isEmpty() );
    return BaseRequest()->NewBookmarksAdd( GetDocument(), pageIndex, resPath, Priority() );
}

BookmarksDeleteRequest*BookmarksManager::CreateRemoveRequest(int pageIndex, QString resPath)
{
    return BaseRequest()->NewBookmarksDelete( m_bookmarks.Get( pageIndex, resPath ), Priority() );
}

Bookmarks BookmarksManager::Validate(Bookmarks bookmarks)
{
	return bookmarks;
}

Bookmark BookmarksManager::Validate(Bookmark bookmark)
{
	return bookmark;
}

void BookmarksManager::EmitUpdate()
{
	emit SignalUpdate( this );
}

void BookmarksManager::EmitLoaded()
{
    emit SignalLoaded( this );
}
