#ifndef FAVORITESGETREQUEST_H
#define FAVORITESGETREQUEST_H

#include "accountbaserequest.h"
#include "favoritebook.h"

class FavoritesGetRequest : public AccountBaseRequest
{
	Q_OBJECT
public:
	explicit FavoritesGetRequest(QObject *parent, QNetworkAccessManager* manager, QString baseUrl,
								 User user, RequestPriority priority = PRIORITY_NORMAL );

	void Send(void);

signals:
	void SignalFinished( ErrorCode error, FavoriteBooks result );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

protected:
	virtual QNetworkReply* SendInternal( QString requestString );

private:
	FavoriteBooks ParseResult( QByteArray data );
};

#endif // FAVORITESGETREQUEST_H
