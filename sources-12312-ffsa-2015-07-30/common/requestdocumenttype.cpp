#include "requestdocumenttype.h"

#include "defines.h"

RequestDocumentType::RequestDocumentType(QObject* parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority):
	Request( parent, manager, identifier, document, priority )
{
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

void RequestDocumentType::Send()
{
	QString requestString = QString( "%1/%2" )
							.arg( "document" )
							.arg( "type" );
	ApiBase::Send( requestString );
}

void RequestDocumentType::SlotFinished(ErrorCode error, QByteArray data)
{
	emit SignalFinished( error, data );
}
