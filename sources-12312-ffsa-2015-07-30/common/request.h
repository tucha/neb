#ifndef REQUEST_H
#define REQUEST_H

#include "requestbase.h"

#include <QTimer>

#include "pageset.h"

#define ERROR_CODE	"404"
#define NOT_FOUND	"Not Found"
#define INTERNAL_ERROR "Traceback"

#define SENDER			static_cast<Request*>( sender() )
#define SENDER_PAGE		static_cast<RequestPage*>( sender() )
#define SENDER_AUTH		static_cast<AuthorizationBase*>( sender() )

class RequestFileExtensions;
class RequestFile;
class RequestFileSize;
class RequestDescription;
class RequestInformation;
class RequestDocumentFormat;
class RequestCollection;
class RequestDocumentType;
class RequestContent;
class RequestPagesCount;
class RequestPageSet;
class RequestPage;
class RequestWordPages;
class RequestDownloadEpub;
class RequestDocumentSearch;


class Request : public RequestBase
{
    Q_OBJECT

public:
    // Constructor / Destructor
    Request(
            QObject *parent,
            QNetworkAccessManager* manager,
            Identifier identifier,
            Document document,
            RequestPriority priority = PRIORITY_NORMAL );
    virtual ~Request();

    Request*				New( Document document, RequestPriority priority = PRIORITY_NORMAL );
    Request*				CreateCopy( RequestPriority priority = PRIORITY_NORMAL );
    RequestFileExtensions*	NewFileExtensions( RequestPriority priority = PRIORITY_NORMAL );
    RequestFile*			NewFile( QString fileExtension, RequestPriority priority = PRIORITY_NORMAL );
    RequestFileSize*		NewFileSize( QString fileExtension, RequestPriority priority = PRIORITY_NORMAL );
    RequestDescription*		NewDescription( QString field, QString subField = "", RequestPriority priority = PRIORITY_NORMAL );
    RequestInformation*		NewInformation( RequestPriority priority = PRIORITY_NORMAL );
    RequestDocumentFormat*	NewDocumentFormat( RequestPriority priority = PRIORITY_NORMAL );
    RequestCollection*		NewCollection( RequestPriority priority = PRIORITY_NORMAL );
    RequestDocumentType*	NewDocumentType( RequestPriority priority = PRIORITY_NORMAL );
    RequestDownloadEpub*    NewDownloadEpub( RequestPriority priority = PRIORITY_NORMAL);
    RequestPagesCount*		NewPagesCount( RequestPriority priority = PRIORITY_NORMAL );
    RequestPageSet*			NewPageSet( PageSet pageSet, RequestPriority priority = PRIORITY_NORMAL );
    RequestPage*			NewPage( int pageIndex, RequestPriority priority = PRIORITY_NORMAL );
    RequestWordPages*		NewWordPages( QStringList words, RequestPriority priority = PRIORITY_NORMAL );
    RequestContent*         NewContent( RequestPriority priority = PRIORITY_NORMAL );
    RequestDocumentSearch *NewSearch(QStringList phrase,  RequestPriority priority = PRIORITY_NORMAL );
    static QString WordsString(QStringList words);

    // Public properties
    Document GetDocument(void)const;

protected:
    virtual QNetworkReply* SendInternal( QString requestString );

private:
    // Private members
    const Document m_document;
};

#endif // REQUEST_H
