#ifndef REQUESTTYPE_H
#define REQUESTTYPE_H

#include <QObject>
#include "request.h"

class RequestDocumentFormat : public Request
{
    Q_OBJECT
public:
    RequestDocumentFormat(QObject *parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority);
    void Send(void);
signals:
    void SignalFinished( ErrorCode error, Document::Type type );

private slots:
    void SlotFinished( ErrorCode error, QByteArray data );
};

#endif // REQUESTTYPE_H
