#ifndef CheckTrustedHostRequest_H
#define CheckTrustedHostRequest_H

#include "apibase.h"
#include "user.h"

class CheckTrustedHostRequest : public ApiBase
{
	Q_OBJECT

public:
    explicit CheckTrustedHostRequest(QObject *parent, QNetworkAccessManager* manager, QString baseUrl,
            User user,
                                     RequestPriority priority = PRIORITY_NORMAL );

	void Send();

signals:
    void SignalFinished( ErrorCode error, QVariantMap result );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

protected:
	virtual QNetworkReply* SendInternal( QString requestString );


};

#endif // CheckTrustedHostRequest_H
