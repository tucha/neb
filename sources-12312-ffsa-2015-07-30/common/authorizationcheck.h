#ifndef AUTHORIZATIONCHECK_H
#define AUTHORIZATIONCHECK_H

#include "authorizationbase.h"

class AuthorizationCheck: public AuthorizationBase
{
	Q_OBJECT
public:
	AuthorizationCheck( QObject *parent, QNetworkAccessManager* manager, QString baseUrl, RequestPriority priority,
						QString token );

	void Send(void);

	QString Token(void)const;

private:
	const QString m_token;
};

#endif // AUTHORIZATIONCHECK_H
