#include "authorizationresult.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QJsonValue>

#include "jsonfinder.h"
#include "defines.h"
#include <QDebug>
AuthorizationResult::AuthorizationResult():
	m_isEmpty( true ),
	m_error(),
	m_user()
{
}

AuthorizationResult::AuthorizationResult(QByteArray data):
	m_isEmpty( true ),
	m_error(),
	m_user()
{
//    qDebug() << data;
	JsonFinder finder( data );

	DebugAssert( finder.Count() == 1 );
	if( finder.Count() != 1 )
	{
		m_isEmpty = false;
		m_error = QObject::tr( "Unknown server reply" );
		return;
	}

	QJsonDocument json = finder.Get( 0 );
	DebugAssert( json.isNull() == false );

	QJsonObject object = json.object();
	DebugAssert( object.isEmpty() == false );

	QJsonValue error = object.value( "error" );
	DebugAssert( error.isUndefined() == false );

	QJsonValue user = object.value( "result" );
	DebugAssert( user.isUndefined() == false );

	if( error.isUndefined() || user.isUndefined() ) return;

	DebugAssert( error.isString() );
	if( error.isString() == false ) return;

	m_isEmpty = false;
	m_error = error.toString();
	if( m_error.isEmpty() == false ) return;

	DebugAssert( user.isObject() );
	if( user.isObject() == false ) return;

	m_user = User( user.toObject().toVariantMap() );
}

bool AuthorizationResult::IsEmpty() const
{
	return m_isEmpty;
}

QString AuthorizationResult::GetError() const
{
	DebugAssert( IsEmpty() == false );
	return m_error;
}

User AuthorizationResult::GetUser() const
{
	DebugAssert( IsEmpty() == false );
	return m_user;
}
