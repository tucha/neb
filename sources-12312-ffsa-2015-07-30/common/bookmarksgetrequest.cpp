#include "bookmarksgetrequest.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include "platform_id/platform_id.h"
#define KEY_ID			"ID"
#define KEY_BOOK_ID		"BOOK_ID"
#define KEY_NUM_PAGE	"NUM_PAGE"
#define KEY_NUM_WORD	"NUM_WORD"
#define KEY_TEXT		"TEXT"
#include "session.h"

BookmarksGetRequest::BookmarksGetRequest(QObject *parent, QNetworkAccessManager* manager, QString baseUrl,
										 User user, Document document, int pageIndex, bool comment, RequestPriority priority ) :
	AccountBaseRequest(parent, manager, baseUrl, user, priority),
	m_document( document ),
	m_pageIndex( pageIndex ),
	m_comment( comment )
{
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));

	AddGetParameter( "token", UserToken() );
    AddGetParameter(  "fingerprint", PlatformID::hash());
    AddGetParameter(  "os", REQUEST_OS_ID);
    if(GetUser().HasSession())AddGetParameter("session", GetUser().GetSession()->GetID());

	AddGetParameter( "book_id", GetDocument().DocumentId() );
	if( AllPages() == false ) AddGetParameter( "num_page", QString::number( PageIndex() ) );
}

Document BookmarksGetRequest::GetDocument() const
{
	return m_document;
}

int BookmarksGetRequest::PageIndex() const
{
	DebugAssert( m_pageIndex >= 0 );
	return m_pageIndex;
}

bool BookmarksGetRequest::AllPages() const
{
	return m_pageIndex == ALL_PAGES;
}

bool BookmarksGetRequest::IsComment() const
{
	return m_comment;
}

void BookmarksGetRequest::Send()
{
	if( IsComment() ) AccountBaseRequest::Send( "notes" );
	else AccountBaseRequest::Send( "bookmark" );
}

void BookmarksGetRequest::SlotFinished(ErrorCode error, QByteArray data)
{
	if( error != API_NO_ERROR ) emit SignalFinished( error, Bookmarks() );
	else emit SignalFinished( error, ParseResult( data ) );
}

QNetworkReply*BookmarksGetRequest::SendInternal(QString requestString)
{
	QUrl url( requestString );
#if QT_VERSION >= 0x050000
	url.setQuery( GetParameters() );
#else
	QList<QPair<QString, QString> > prms = GetParameters();
	for( QList<QPair<QString,QString> >::iterator iter = prms.begin(); iter != prms.end(); ++iter )
	{
		url.addQueryItem( iter->first, iter->second );
	}
#endif

	QNetworkRequest request;
	request.setUrl( url );
	request.setPriority( Priority() );

	return Manager()->get( request );
}

Bookmarks BookmarksGetRequest::ParseResult(QByteArray data)
{
	QJsonDocument document = QJsonDocument::fromJson( data );

	DebugAssert( document.isObject() );
	if( document.isObject() == false ) return Bookmarks();

	Bookmarks result;

	QJsonObject object = document.object();
	foreach( QString key, object.keys() )
	{
		QJsonValue value = object.value( key );

		DebugAssert( value.isObject() );
		if( value.isObject() == false ) continue;

		QJsonObject subObject = value.toObject();



		DebugAssert( subObject.contains( KEY_ID ) );
		if( subObject.contains( KEY_ID ) == false ) return Bookmarks();
		QJsonValue valueId = subObject.value( KEY_ID );

		DebugAssert( subObject.contains( KEY_BOOK_ID ) );
		if( subObject.contains( KEY_BOOK_ID ) == false ) return Bookmarks();
		QJsonValue valueBookId = subObject.value( KEY_BOOK_ID );

		DebugAssert( subObject.contains( KEY_NUM_PAGE ) );
		if( subObject.contains( KEY_NUM_PAGE ) == false ) return Bookmarks();
		QJsonValue valueNumPage = subObject.value( KEY_NUM_PAGE );




		DebugAssert( valueId.isString() );
		if( valueId.isString() == false ) return Bookmarks();
		QString id = valueId.toString();

		DebugAssert( valueBookId.isString() );
		if( valueBookId.isString() == false ) return Bookmarks();
		QString bookId = valueBookId.toString();

//        DebugAssert( valueNumPage.isString() );
//        if( valueNumPage.isString() == false ) return Bookmarks();


        QJsonValue valueResPath = subObject.value( KEY_NUM_WORD );
        QString resPath = valueResPath.toString();


        int pageIndex = 0;
        if(valueNumPage.isString())
            pageIndex    = valueNumPage.toString().toInt();
        if(valueNumPage.isDouble())
            pageIndex = valueNumPage.toInt(0);
//        DebugAssert( pageIndex != -1 );
//        if( pageIndex != -1 ) return Bookmarks();

//		DebugAssert( pageIndex >= 0 );
//		if( pageIndex < 0 ) return Bookmarks();
		if( pageIndex < 0 ) continue;


		if( AllPages() == false )
		{
			DebugAssert( PageIndex() == pageIndex );
			if( PageIndex() != pageIndex ) return Bookmarks();
		}

		DebugAssert( GetDocument().DocumentId() == bookId );
		if( GetDocument().DocumentId() != bookId ) return Bookmarks();

		DebugAssert( key == id );
		if( key != id ) return Bookmarks();


		if( IsComment() )
		{
			DebugAssert( subObject.contains( KEY_TEXT ) );
			if( subObject.contains( KEY_TEXT ) == false ) return Bookmarks();
			QJsonValue valueText = subObject.value( KEY_TEXT );

			DebugAssert( valueText.isString() );
			if( valueText.isString() == false ) return Bookmarks();
			QString text = valueText.toString();

			DebugAssert( text.isEmpty() == false );
			if( text.isEmpty() ) return Bookmarks();

            result.append( Bookmark( GetUser(), GetDocument(), pageIndex, resPath, id, text ) );
		}
		else
		{
            result.append( Bookmark( GetUser(), GetDocument(), pageIndex, resPath,  id ) );
		}
	}

	return result;
}
