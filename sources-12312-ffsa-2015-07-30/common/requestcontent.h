#ifndef REQUESTCONTENT_H
#define REQUESTCONTENT_H

#include "request.h"
class RequestContent : public Request
{
    Q_OBJECT
public:
    RequestContent(QObject *parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority);
    void Send(void);
signals:
    void SignalFinished( ErrorCode error, QVariantList contents );

private slots:
    void SlotFinished( ErrorCode error, QByteArray data );
};

#endif // REQUESTCONTENT_H
