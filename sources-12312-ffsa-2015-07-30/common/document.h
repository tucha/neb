#ifndef DOCUMENT_H
#define DOCUMENT_H

#include <QString>
#include <QList>
#include <QHash>
#include <QVariant>
class Document;
class Request;

typedef QList<Document> DocumentList;

class Document
{
public:
	explicit Document( const QString& spd = "" );
	QString ToString( bool exceptToken = true )const;

	static bool CheckString( const QString& spd );
	static bool CheckDocumentId( const QString& id );
	static Document Create( QString baseUrl, QString id );
	static Document FromId( QString id );
	static QString FormatBaseUrl( const QString& baseUrl );

	bool IsEmpty(void)const;
	QString BaseUrl(void)const;
	QString DocumentId(void)const;
	QString Token(void)const;
    QVariant Page(void)const;
	bool ContainsToken(void)const;
	bool ContainsPage(void)const;
    enum Type {
        EPUB,
        PDF,
        UNKNOWN
    };


    bool ContainsSearchQuery() const;
    QString SearchQuery();
private:
	QString ParametersString( bool exceptToken )const;

	static QHash<QString,QString> ExtractParameters( const QString& parametersString );
	static bool CheckParameter( QString key, QString value );
	static bool CheckParameters( const QString& parametersString );
	static bool CheckDocumentIdChar( const QChar& ch );

	QString m_baseUrl;
	QHash<QString,QString> m_parameters;
};

inline bool operator==(const Document& d1, const Document& d2)
{
	if( d1.IsEmpty() && d2.IsEmpty() ) return true;
	if( d1.IsEmpty() != d2.IsEmpty() ) return false;
	if( d1.DocumentId() != d2.DocumentId() ) return false;
	return true;
}

inline bool operator!=(const Document& d1, const Document& d2)
{
	if( d1.IsEmpty() && d2.IsEmpty() ) return false;
	if( d1.IsEmpty() != d2.IsEmpty() ) return true;
	if( d1.DocumentId() != d2.DocumentId() ) return true;
	return false;
}

#if QT_VERSION >= 0x050000
inline uint qHash(const Document& d, uint seed)
{
	if( d.IsEmpty() ) return seed;
	return qHash( d.DocumentId() );
}
#else
inline uint qHash(const Document& d)
{
	if( d.IsEmpty() ) return 0;
	return qHash( d.DocumentId() );
}
#endif

#endif // DOCUMENT_H
