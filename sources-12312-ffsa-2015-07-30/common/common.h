#ifndef COMMON_H
#define COMMON_H

#include "debug.h"

#include "request.h"
#include "requestfileextensions.h"
#include "requestfile.h"
#include "requestfilesize.h"
#include "requestdescription.h"
#include "requestinformation.h"
#include "requestdocumenttype.h"
#include "requestcollection.h"
#include "requestpagescount.h"
#include "requesttype.h"
#include "requestdownloadepub.h"
#include "requestcontent.h"
#include "requestsearch.h"

#include "requestpageset.h"
#include "requestwordpages.h"

#include "requestpage.h"
#include "requestpagesize.h"
#include "requestimage.h"
#include "requestimagefixed.h"
#include "requestwords.h"
#include "requestwordcoordinates.h"
#include "requestimagewithwords.h"

#include "getparameter.h"

#include "authorizationrequest.h"
#include "authorizationcheck.h"
#include "authorizationresult.h"

#include "requestcollections.h"
#include "requestmarks.h"
#include "requesttexts.h"

#include "accountbaserequest.h"
#include "bookmarksaddrequest.h"
#include "bookmarksdeleterequest.h"
#include "bookmarksgetrequest.h"

#include "favoritesaddrequest.h"
#include "favoritesdeleterequest.h"
#include "favoritesgetrequest.h"

#endif // COMMON_H
