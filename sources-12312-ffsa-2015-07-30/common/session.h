#ifndef SESSION_H
#define SESSION_H

#include <QObject>

#include "authorizationcheck.h"
#include "user.h"
#include "sessionpointer.h"

class Session : public QObject
{
	Q_OBJECT
public:
	static SessionPointer New( AuthorizationBase* authorizationBase, User user );
	virtual ~Session();

	QString GetToken(void)const;
	void CheckToken(void);
	bool TokenExpired(void)const;
    QString GetID();
signals:
	void SignalTokenExpired();

private slots:
	void SlotAuthorizationCheckFinished( ErrorCode error, AuthorizationResult result );

private:
	explicit Session(QObject *parent, AuthorizationBase* authorizationBase, User user);

	AuthorizationCheck* m_authorizationCheck;
	bool m_tokenExpired;

    int m_sessionId;
};

#endif // SESSION_H
