#ifndef REQUESTFILEEXTENSIONS_H
#define REQUESTFILEEXTENSIONS_H

#include "request.h"

class RequestFileExtensions : public Request
{
	Q_OBJECT
public:
	RequestFileExtensions( QObject *parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority );
	void Send(void);

signals:
	void SignalFinished( ErrorCode error, QStringList fileExtensions );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

private:
	QStringList ParseData(QByteArray data);
};

#endif // REQUESTFILEEXTENSIONS_H
