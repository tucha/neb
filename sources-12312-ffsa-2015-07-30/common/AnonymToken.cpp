#include "AnonymToken.h"
#include <QJsonDocument>

AnonymToken::AnonymToken(QObject *parent, QNetworkAccessManager *manager, QString baseUrl, RequestPriority priority)
    :ApiBase( parent, manager, baseUrl, priority )
{
    AddGetParameter("action", "isanonymousaccess");
    connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

void AnonymToken::SlotFinished(ErrorCode error, QByteArray data)
{
    if( error != API_NO_ERROR )
    {
        emit SignalFinished( error, "" );
        return;
    }

    auto res = QJsonDocument::fromJson(data).toVariant().toMap();
    if(res.contains("token"))
        emit SignalFinished( error, res["token"].toString() );
    else
        emit SignalFinished( error, "" );

}

QNetworkReply*AnonymToken::SendInternal(QString requestString)
{
    QUrl url = QUrl( requestString );
    url.setQuery( GetParameters() );
    QNetworkRequest request;
    request.setUrl( url );
    request.setPriority( Priority() );
    return Manager()->get( request );
}


void AnonymToken::Send()
{
    ApiBase::Send( QString( "%1/%2/%3/" )
                      .arg( BaseUrl() )
                      .arg( "rest_api" )
                      .arg( "reader" )
                       );
}
