#ifndef BOOKMARKSMANAGERBASE_H
#define BOOKMARKSMANAGERBASE_H

#include <QObject>
#include <QSet>

#include "common.h"
#include "bookmarkscontainer.h"

class BookmarksManagerBase : public QObject
{
	Q_OBJECT

private:
	DISABLE_COPY( BookmarksManagerBase )

public:
	// Constructor
	explicit BookmarksManagerBase(QObject *parent, RequestPriority priority = PRIORITY_NORMAL);
	virtual ~BookmarksManagerBase();

	// Public methods
	void Clear(void);
	void Load( AccountBaseRequest* baseRequest, Document document );

    bool Contains( int pageIndex, QString resPath ) const;

    QList<QPair<int, QString> > ActualPages(void)const;
    bool IsActual(int pageIndex, QString resPath) const;

    bool IsNotFound();
private slots:
	// Private slots
	void SlotFinishedGetRequest( ErrorCode error, Bookmarks result );
	void SlotFinishedAddRequest( ErrorCode error, Bookmark result );
	void SlotFinishedRemoveRequest( ErrorCode error );
	void SlotErrorAddRequest();

protected:
	// Protected methods
    void RequestAddition(int pageIndex, QString resPath, QString text = QString() );
    void RequestRemoval(int pageIndex , QString resPath);

	// Protected properties
	bool IsEmpty(void)const;
	AccountBaseRequest* BaseRequest(void)const;
	Document GetDocument(void)const;
	RequestPriority Priority(void)const;

	// Protected virtual methods
	virtual BookmarksGetRequest* CreateGetRequest(void) = 0;
    virtual BookmarksAddRequest* CreateAddRequest( int pageIndex, QString resPath,QString text ) = 0;
    virtual BookmarksDeleteRequest* CreateRemoveRequest( int pageIndex, QString resPath ) = 0;

	virtual Bookmarks Validate( Bookmarks bookmarks ) = 0;
	virtual Bookmark Validate( Bookmark bookmark ) = 0;

	virtual void EmitUpdate(void) = 0;
    virtual void EmitLoaded(void) = 0;
private:
	// Private methods
	bool CheckRequests(void)const;

    bool WasRequestedAddition( int pageIndex, QString resPath ) const;
    bool WasRequestedRemoval( int pageIndex, QString resPath ) const;


	// Private members
	AccountBaseRequest* m_baseRequest;
	Document m_document;
	bool m_isLoaded;
    bool m_isNotFound;
protected:
	BookmarksContainer m_bookmarks;
	QSet<BookmarksAddRequest*> m_addRequests;
	QSet<BookmarksDeleteRequest*> m_removeRequests;
	BookmarksGetRequest* m_getRequest;

	QSet<int> m_addPages;
	QSet<int> m_removePages;

    QSet<QString> m_addResPaths;
    QSet<QString> m_removeResPaths;

private:
	RequestPriority m_priority;
};

#endif // BOOKMARKSMANAGERBASE_H
