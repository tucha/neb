#include "requestpagesize.h"

RequestPageSize::RequestPageSize(QObject* parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority, int pageIndex):
	RequestPage( parent, manager, identifier, document, priority, pageIndex )
{
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

void RequestPageSize::Send()
{
	RequestPage::Send( "geometry" );
}

void RequestPageSize::SlotFinished(ErrorCode error, QByteArray data)
{
	if( error == API_NO_ERROR ) emit SignalFinished( error, ParsePageSize( data ) );
	else emit SignalFinished( error, QSize() );
}

QSize RequestPageSize::ParsePageSize(QString data)
{
	if( data == ERROR_CODE ) return QSize();

	int separator = data.indexOf( "x" );
	QString leftSide = data.left( separator );
	QString rightSide = data.right( data.length() - separator - 1 );

	float width = leftSide.toFloat();
	float height = rightSide.toFloat();

	return QSize( width, height );
}
