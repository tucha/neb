#include "requestdescription.h"

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>

RequestDescription::RequestDescription(QObject* parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority, QString field, QString subField):
	Request( parent, manager, identifier, document, priority ),
	m_field( field ),
	m_subField( subField )
{
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

void RequestDescription::Send()
{
	QString requestString = QString( "%1/%2/%3" )
							.arg( "document" )
							.arg( "marc" )
							.arg( Field() );
	if( SubField().isEmpty() == false )
	{
		requestString += QString( "/%1" )
						 .arg( SubField() );
	}
	ApiBase::Send( requestString );
}

QString RequestDescription::Field() const
{
	return m_field;
}

QString RequestDescription::SubField() const
{
	return m_subField;
}

void RequestDescription::SlotFinished(ErrorCode error, QByteArray data)
{
	if( error != API_NO_ERROR ) emit SignalFinished( error, "" );
	else emit SignalFinished( error, ParseData( data ) );
}

QString RequestDescription::ParseData(QByteArray data)
{
	if( data.startsWith( NOT_FOUND ) ) return QString();
	if( data.startsWith( INTERNAL_ERROR ) ) return QString();

	QJsonDocument json = QJsonDocument::fromJson( data );

//	DebugAssert( json.isNull() == false );
	if( json.isNull() ) return QString();

//	DebugAssert( json.isArray() );
	if( json.isArray() == false ) return QString();

	QJsonArray array = json.array();
	if( array.isEmpty() ) return "";

	DebugAssert( array.count() >= 1 );
	QJsonValue value = array.at( 0 );

	DebugAssert( value.isString() );
	return value.toString();
}

#include "requestdescription.h"
