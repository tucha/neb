#include "common.h"

#include "defines.h"

RequestPage::RequestPage(QObject* parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority,
							   int pageIndex ):
    Request( parent, manager, identifier.WithEncryptedToken(document.DocumentId(), pageIndex), document, priority ),
	m_pageIndex( pageIndex )
{
}

void RequestPage::Send(QString requestString)
{
	QString request = QString( "%1/%2/%3/%4" )
					  .arg( "document" )
					  .arg( "pages" )
					  .arg( PageIndex() )
					  .arg( requestString );
	ApiBase::Send( request );
}

int RequestPage::PageIndex() const
{
	return m_pageIndex;
}

RequestPageSize*RequestPage::NewSize( RequestPriority priority ) const
{
	return new RequestPageSize( parent(), Manager(), GetIdentifier(), GetDocument(), priority, PageIndex() );
}

RequestImage*RequestPage::NewImage(ImageType type, int ppi, ImageFormat format, RequestPriority priority) const
{
	return new RequestImage( parent(), Manager(), GetIdentifier(), GetDocument(), priority, PageIndex(), type, ppi, format );
}

RequestImageFixed*RequestPage::NewImageFixed(RequestPage::ImageType type, RequestPage::Geometry geometry, int restriction, RequestPage::ImageFormat format, RequestPriority priority) const
{
	return new RequestImageFixed( parent(), Manager(), GetIdentifier(), GetDocument(), priority, PageIndex(), type, geometry, restriction, format );
}

RequestWords*RequestPage::NewWords(RequestPriority priority) const
{
	return new RequestWords( parent(), Manager(), GetIdentifier(), GetDocument(), priority, PageIndex() );
}

RequestWordCoordinates*RequestPage::NewWordCoordinates(QStringList words, RequestPriority priority) const
{
	return new RequestWordCoordinates( parent(), Manager(), GetIdentifier(), GetDocument(), priority, PageIndex(), words );
}

RequestImageWithWords*RequestPage::NewImageWithWords(QStringList words, int ppi, RequestPage::ImageFormat format, RequestPriority priority) const
{
	return new RequestImageWithWords( parent(), Manager(), GetIdentifier(), GetDocument(), priority, PageIndex(), words, ppi, format );
}

QString RequestPage::TypeString(RequestPage::ImageType type)
{
	switch( type )
	{
	case Images:		return "images";
	case Print:			return "print";
	case Thumbnails:	return "thumbnails";
	}
	NotImplemented();
	return "";
}

QString RequestPage::FormatString(RequestPage::ImageFormat format)
{
	switch( format )
	{
	case Jpeg:	return "jpeg";
	case Tiff:	return "tiff";
	}
	NotImplemented();
	return "";
}

QString RequestPage::GeometryString(RequestPage::Geometry geometry)
{
	switch( geometry )
	{
	case Width:		return "width";
	case Height:	return "height";
	case Max:		return "max";
	}
	NotImplemented();
	return "";
}

bool RequestPage::TestPpi(RequestPage::ImageType type, int ppi)
{
	switch( type )
	{
	case Images:
	{
		if( ppi < 72 ) return false;
		if( ppi > 300 ) return false;
		return true;
	}
	case Print:
	{
		if( ppi < 72 ) return false;
		if( ppi > 300 ) return false;
		return true;
	}
	case Thumbnails:
	{
		if( ppi < 16 ) return false;
		if( ppi > 71 ) return false;
		return true;
	}
	}
	NotImplemented();
	return false;
}

bool RequestPage::TestRestriction(ImageType type, int restriction)
{
	switch( type )
	{
	case Images:
	{
		if( restriction <= 300 ) return false;
		if( restriction > 2000 ) return false;
		return true;
	}
	case Thumbnails:
	{
		if( restriction < 50 ) return false;
		if( restriction > 300 ) return false;
		return true;
	}
	default:
	{
		NotImplemented();
	}
	}
	return false;
}

QImage RequestPage::ParseImage(QByteArray data, RequestPage::ImageFormat format)
{
	if( data.startsWith( ERROR_CODE ) ) return QImage();

	return QImage::fromData( data, FormatString( format ).toUpper().toUtf8().data() );
}
