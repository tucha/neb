#ifndef IDENTIFIER_H
#define IDENTIFIER_H

#include <QVariantMap>
#include <QString>
#include <session.h>

class Identifier
{
public:
	Identifier( SessionPointer session = SessionPointer() );

	bool SetClientId( QString clientId );
	bool SetUserToken( QString userToken );
	bool SetAppToken( QString appToken );
	bool SetIp( QString ip );

	QString GetClientId(void)const;
	QString GetUserToken(void)const;
	QString GetAppToken(void)const;
	QString GetIp(void)const;

	bool ContainsClientId(void)const;
	bool ContainsUserToken(void)const;
	bool ContainsAppToken(void)const;
	bool ContainsIp(void)const;

	bool ContainsSession(void)const;
	SessionPointer GetSession(void)const;
	void CheckSession(void);

    Identifier WithEncryptedToken(QString docId, int pageNumber);

private:
	QVariantMap m_map;
	SessionPointer m_session;
};

inline bool operator==(const Identifier& i1, const Identifier& i2)
{
	if( i1.GetClientId() != i2.GetClientId() ) return false;
	if( i1.GetUserToken() != i2.GetUserToken() ) return false;
//	if( i1.GetAppToken() != i2.GetAppToken() ) return false;
//	if( i1.GetIp() != i2.GetIp() ) return false;
	return true;
}

inline bool operator!=(const Identifier& i1, const Identifier& i2)
{
	if( i1.GetClientId() != i2.GetClientId() ) return true;
	if( i1.GetUserToken() != i2.GetUserToken() ) return true;
//	if( i1.GetAppToken() != i2.GetAppToken() ) return true;
//	if( i1.GetIp() != i2.GetIp() ) return true;
	return false;
}

#endif // IDENTIFIER_H
