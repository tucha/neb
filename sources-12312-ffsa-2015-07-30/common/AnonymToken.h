#ifndef ANONYMTOKEN_H
#define ANONYMTOKEN_H

#include "apibase.h"
#include "viewersettings.h"
#include "user.h"

class AnonymToken : public ApiBase
{
    Q_OBJECT
public:
    explicit AnonymToken(QObject *parent, QNetworkAccessManager* manager, QString baseUrl,
                         RequestPriority priority = PRIORITY_NORMAL );

    void Send();

signals:
    void SignalFinished( ErrorCode error, QString token );

private slots:
    void SlotFinished( ErrorCode error, QByteArray data );

protected:
    virtual QNetworkReply* SendInternal( QString requestString );

private:

};

#endif // ANONYMTOKEN_H
