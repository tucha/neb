#include "authorizationcheck.h"
#include "platform_id/platform_id.h"
AuthorizationCheck::AuthorizationCheck(QObject* parent, QNetworkAccessManager* manager, QString baseUrl, RequestPriority priority, QString token):
	AuthorizationBase( parent, manager, baseUrl, priority ),
	m_token( token )
{
	AddGetParameter( "token", token );
    AddGetParameter(  "fingerprint", PlatformID::hash());
}

void AuthorizationCheck::Send()
{
	AuthorizationBase::Send( "check_token" );
}

QString AuthorizationCheck::Token() const
{
	return m_token;
}
