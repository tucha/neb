#include "pageset.h"

#include <QStringList>

PageSet::PageSet():
	m_ranges()
{
	DebugAssert( Test() );
}

PageSet::PageSet(const PageSet& another):
	m_ranges( another.m_ranges )
{
	DebugAssert( Test() );
}

PageSet&PageSet::operator=(const PageSet& another)
{
	m_ranges = another.m_ranges;

	DebugAssert( Test() );
	return *this;
}

PageSet::PageSet(int startPage, int endPage):
	m_ranges()
{
	Add( Range( startPage, endPage ) );
}

void PageSet::Add(int page)
{
	DebugAssert( page >= 0 );
	if( page < 0 ) return;

	Add( Range( page, page ) );
	DebugAssert( Test() );
}

void PageSet::Add(const Range& newRange)
{
	DebugAssert( newRange.IsDefined() );
	if( newRange.IsDefined() == false ) return;

	QVector<Range> newRanges;

	// Collect ranges before new range
	foreach( Range range, m_ranges )
	{
		if( range.End() < newRange.Start() ) newRanges.append( range );
	}

	// Collect new range unioned with all ranges that intersects with it
	Range middle = newRange;
	foreach( Range range, m_ranges )
	{
		if( range.IntersectsWith( middle ) ) middle = middle.UnionWith( range );
	}
	newRanges.append( middle );

	// Collect ranges after new range
	foreach( Range range, m_ranges )
	{
		if( range.Start() > newRange.End() ) newRanges.append( range );
	}

	m_ranges = MergeRanges( newRanges );
	DebugAssert( Test() );
}

#define SEPARATOR ","

PageSet PageSet::FromString(const QString& string)
{
	PageSet result;
	QStringList subStrings = string.split( SEPARATOR );
	foreach( QString subString, subStrings )
	{
		Range range = Range::FromString( subString );

		DebugAssert( range.IsDefined() );
		if( range.IsDefined() == false ) continue;

		result.Add( range );
	}

	DebugAssert( result.Test() );
	if( result.Test() == false ) return PageSet();

	return result;
}

bool PageSet::CheckString(const QString& string)
{
	if( string.isEmpty() ) return false;

	QStringList subStrings = string.split( SEPARATOR );
	foreach( QString subString, subStrings )
	{
		if( Range::CheckString( subString ) == false ) return false;
	}
	return true;
}

QString PageSet::ToString() const
{
	QString result;
	QString separator( SEPARATOR );
	foreach( Range range, m_ranges )
	{
		result.append( range.ToString() );
		result.append( separator );
	}
	if( result.isEmpty() == false ) result = result.left( result.length() - separator.length() );
	return result;
}

bool PageSet::IsEmpty() const
{
	DebugAssert( Test() );
	return m_ranges.isEmpty();
}

QVector<int> PageSet::ToList() const
{
	QVector<int> result;

	foreach( Range range, m_ranges )
	{
		result << range.ToList();
	}

	return result;
}

bool PageSet::Test() const
{
	Range previous;
	foreach( Range current, m_ranges )
	{
		DebugAssert( current.IsDefined() );
		if( current.IsDefined() == false ) return false;

		if( previous.IsDefined() == false )
		{
			previous = current;
			continue;
		}

		DebugAssert( previous.IntersectsWith( current ) == false );
		if( previous.IntersectsWith( current ) ) return false;

		DebugAssert( previous.Start() < current.Start() );
		if( ( previous.Start() < current.Start() ) == false ) return false;

		previous = current;
	}
	return true;
}

QVector<Range> PageSet::MergeRanges(QVector<Range> ranges)
{
	QVector<Range> result;
	foreach( Range range, ranges )
	{
		if( result.isEmpty() )
		{
			result.append( range );
			continue;
		}

		Range last = result.last();
		if( last.End() + 1 == range.Start() )
		{
			result.replace( result.count() - 1, Range( last.Start(), range.End() ) );
		}
		else
		{
			result.append( range );
		}
	}
	return result;
}
