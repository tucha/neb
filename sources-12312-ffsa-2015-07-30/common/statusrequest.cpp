#include "statusrequest.h"
#include "platform_id/platform_id.h"
#include "session.h"

StatusRequest::StatusRequest(QObject* parent, QNetworkAccessManager* manager, QString baseUrl, User user, RequestType type, RequestPriority priority):
	ApiBase( parent, manager, baseUrl, priority ),
	m_user( user ),
	m_type( type )
{
    AddGetParameter(  "fingerprint", PlatformID::hash());
    AddGetParameter(  "os", REQUEST_OS_ID);
    if(GetUser().HasSession()) AddGetParameter("session", GetUser().GetSession()->GetID());

	AddGetParameter( "token", GetUser().GetToken() );
	AddGetParameter( "action", GetAction() );
	AddGetParameter( "q", GetQuestion() );

	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

User StatusRequest::GetUser() const
{
	return m_user;
}

StatusRequest::RequestType StatusRequest::GetType() const
{
	return m_type;
}

void StatusRequest::Send()
{
	ApiBase::Send( QString( "%1/%2/%3/" )
				   .arg( BaseUrl() )
				   .arg( "rest_api" )
				   .arg( "reader" ) );
}

void StatusRequest::SlotFinished(ErrorCode error, QByteArray data)
{
	if( GetType() == CheckIp ) FinishedIpCheck( error, data );
	else if( GetType() == CheckUser ) FinishedUserStatus( error, data );
	else NotImplemented();
}

void StatusRequest::FinishedUserStatus(ErrorCode error, QByteArray data)
{
	if( error != API_NO_ERROR )
	{
		emit SignalFinishedUserStatus( error, User::Undefined );
		return;
	}

	User::UserStatus result = User::Undefined;
	if( data == "\"null\"" ) result = User::Anonymous;
	else if( data == "\"user_status_verified\"" ) result = User::Verified;
	else if( data == "\"user_status_registered\"" ) result = User::Registered;
	else if( data == "\"user_status_vip\"" ) result = User::VIP;
	else NotImplemented();

	emit SignalFinishedUserStatus( error, result );
}

void StatusRequest::FinishedIpCheck(ErrorCode error, QByteArray data)
{
	if( error != API_NO_ERROR )
	{
		emit SignalFinished( error, false );
		return;
	}

	bool result = false;
	if( data == "true" ) result = true;
	else if( data == "false" ) result = false;
	else NotImplemented();

	emit SignalFinished( error, result );
}

QString StatusRequest::GetAction() const
{
	if( GetType() == StatusRequest::CheckUser ) return "getUserStatus";
	if( GetType() == StatusRequest::CheckIp ) return "getIPStatus";
	NotImplemented();
	return QString();
}

QString StatusRequest::GetQuestion() const
{
	if( GetType() == StatusRequest::CheckUser ) return GetUser().GetEmail();
	if( GetType() == StatusRequest::CheckIp ) return "XXX.XXX.XXX.XXX";
	NotImplemented();
	return QString();
}

QNetworkReply*StatusRequest::SendInternal(QString requestString)
{
	QUrl url = QUrl( requestString );
#if QT_VERSION >= 0x050000
	url.setQuery( GetParameters() );
#else
	QList<QPair<QString,QString> > prms = GetParameters();
	for( QList<QPair<QString,QString> >::iterator iter = prms.begin(); iter != prms.end(); ++iter )
	{
		url.addQueryItem( iter->first, iter->second );
	}
#endif

	QNetworkRequest request;
	request.setUrl( url );
	request.setPriority( Priority() );

	return Manager()->get( request );
}
