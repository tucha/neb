#ifndef SESSIONPOINTER_H
#define SESSIONPOINTER_H

#include <QSharedPointer>
#include <QSet>

class Session;
class SessionPointer: private QSharedPointer<Session>
{
public:
	SessionPointer();
	explicit SessionPointer( Session* session );

	SessionPointer( const SessionPointer& another );
	SessionPointer& operator=( const SessionPointer& another );

	virtual ~SessionPointer();

	bool isNull(void)const;
	Session* operator->(void)const;
	Session* data(void)const;

#ifdef BUILD_DEBUG
	static QSet<int> Ids();
#endif

private:
	void InitializeId(void);
	void FreeId(void);

#ifdef BUILD_DEBUG
	static QSet<int> g_set;
	static int g_lastId;
	int m_id;
#endif
};

#endif // SESSIONPOINTER_H
