#ifndef ACCOUNTBASEREQUEST_H
#define ACCOUNTBASEREQUEST_H

#include "apibase.h"
#include "document.h"
#include "bookmark.h"
#include "favoritebook.h"

class BookmarksAddRequest;
class BookmarksGetRequest;
class BookmarksDeleteRequest;

class FavoritesAddRequest;
class FavoritesGetRequest;
class FavoritesDeleteRequest;

class CheckTrustedHostRequest;
class AddTrustedHostRequest;
class BlockFingerprintRequest;

class AccountBaseRequest : public ApiBase
{
	Q_OBJECT
public:
	explicit AccountBaseRequest(QObject *parent, QNetworkAccessManager* manager, QString baseUrl,
								User user, RequestPriority priority = PRIORITY_NORMAL );

	User GetUser(void)const;
	QString UserToken(void)const;

	void Send( QString request );

	AccountBaseRequest* New( RequestPriority priority = PRIORITY_NORMAL )const;

    BookmarksAddRequest* NewBookmarksAdd(Document document, int pageIndex, QString resPath, RequestPriority priority = PRIORITY_NORMAL )const;
	BookmarksGetRequest* NewBookmarksGetForAllPages( Document document, RequestPriority priority = PRIORITY_NORMAL )const;
	BookmarksGetRequest* NewBookmarksGetForOnePage( Document document, int pageIndex, RequestPriority priority = PRIORITY_NORMAL )const;
	BookmarksDeleteRequest* NewBookmarksDelete( Bookmark bookmark, RequestPriority priority = PRIORITY_NORMAL )const;

    BookmarksAddRequest* NewCommentsAdd( Document document, int pageIndex, QString resPath, QString text, RequestPriority priority = PRIORITY_NORMAL )const;
	BookmarksGetRequest* NewCommentsGetForAllPages( Document document, RequestPriority priority = PRIORITY_NORMAL )const;
	BookmarksGetRequest* NewCommentsGetForOnePage( Document document, int pageIndex, RequestPriority priority = PRIORITY_NORMAL )const;
	BookmarksDeleteRequest* NewCommentsDelete( Bookmark bookmark, RequestPriority priority = PRIORITY_NORMAL )const;

	FavoritesAddRequest* NewFavoritesAdd( Document document, RequestPriority priority = PRIORITY_NORMAL )const;
	FavoritesGetRequest* NewFavoritesGet( RequestPriority priority = PRIORITY_NORMAL )const;
	FavoritesDeleteRequest* NewFavoritesDelete( FavoriteBook favoriteBook, RequestPriority priority = PRIORITY_NORMAL )const;

    CheckTrustedHostRequest *NewCheckTrustedHostRequest(RequestPriority priority = PRIORITY_NORMAL )const;
    AddTrustedHostRequest   *NewAddTrustedHostRequest(RequestPriority priority = PRIORITY_NORMAL )const;
    BlockFingerprintRequest *NewBlockFingerprintRequest(RequestPriority priority = PRIORITY_NORMAL )const;
private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

private:
	const User m_user;

protected:
	virtual QNetworkReply* SendInternal( QString requestString );
};

#endif // ACCOUNTBASEREQUEST_H
