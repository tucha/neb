#include "requestfile.h"

RequestFile::RequestFile(QObject* parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority, QString fileExtension):
	Request( parent, manager, identifier, document, priority ),
	m_fileExtension( fileExtension )
{

}

void RequestFile::Send()
{
	QString requestString = QString( "%1/%2" )
							.arg( "resources" )
							.arg( FileExtension() );
	ApiBase::Send( requestString );
}

QString RequestFile::FileExtension() const
{
	return m_fileExtension;
}
