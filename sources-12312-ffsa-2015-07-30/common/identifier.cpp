#include "identifier.h"

#include "defines.h"

#define KEY_CLIENT_ID	"ClientId"
#define KEY_USER_TOKEN	"UserToken"
#define KEY_APP_TOKEN	"AppToken"
#define KEY_IP			"Ip"

Identifier::Identifier(SessionPointer session):
	m_map(),
	m_session( session )
{
}

bool Identifier::SetClientId(QString clientId)
{
	if( ContainsClientId() ) return false;
	m_map.insert( KEY_CLIENT_ID, clientId );
	return true;
}

bool Identifier::SetUserToken(QString userToken)
{
	if( ContainsUserToken() ) return false;
	m_map.insert( KEY_USER_TOKEN, userToken );
	return true;
}

bool Identifier::SetAppToken(QString appToken)
{
	if( ContainsAppToken() ) return false;
	m_map.insert( KEY_APP_TOKEN, appToken );
	return true;
}

bool Identifier::SetIp(QString ip)
{
	if( ContainsIp() ) return false;
	m_map.insert( KEY_IP, ip );
	return true;
}

QString Identifier::GetClientId() const
{
	DebugAssert( ContainsClientId() );
	return m_map.value( KEY_CLIENT_ID ).toString();
}

QString Identifier::GetUserToken() const
{
	DebugAssert( ContainsUserToken() );
	return m_map.value( KEY_USER_TOKEN ).toString();
}

QString Identifier::GetAppToken() const
{
	DebugAssert( ContainsAppToken() );
	return m_map.value( KEY_APP_TOKEN ).toString();
}

QString Identifier::GetIp() const
{
	DebugAssert( ContainsIp() );
	return m_map.value( KEY_IP ).toString();
}

bool Identifier::ContainsClientId() const
{
	return m_map.contains( KEY_CLIENT_ID );
}

bool Identifier::ContainsUserToken() const
{
	return m_map.contains( KEY_USER_TOKEN );
}

bool Identifier::ContainsAppToken() const
{
	return m_map.contains( KEY_APP_TOKEN );
}

bool Identifier::ContainsIp() const
{
	return m_map.contains( KEY_IP );
}

bool Identifier::ContainsSession() const
{
	return GetSession().isNull() == false;
}

SessionPointer Identifier::GetSession() const
{
	return m_session;
}

void Identifier::CheckSession()
{
	if( ContainsSession() == false ) return;
	if( ContainsUserToken() == false ) return;

	DebugAssert( GetSession()->GetToken() == GetUserToken() );
	if( GetSession()->GetToken() != GetUserToken() ) return;

    GetSession()->CheckToken();
}

Identifier Identifier::WithEncryptedToken(QString docId, int pageNumber)
{
    Identifier ret = *this;
    if(!m_map.contains(KEY_USER_TOKEN)) return ret;
    QByteArray tokenBytes = m_map[KEY_USER_TOKEN].toString().toUtf8();
    auto md5 = QCryptographicHash::hash((QString::number(pageNumber) + docId).toUtf8(),QCryptographicHash::Md5);
    for(int i = 0; i<md5.length(); i ++)
    {
        md5[i] = md5[i]^tokenBytes[i%tokenBytes.length()];
    }
    ret.SetUserToken(QString::fromUtf8(md5.toBase64()));
    return ret;
}
