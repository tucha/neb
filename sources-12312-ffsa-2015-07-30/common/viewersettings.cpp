#include "viewersettings.h"

#include "defines.h"

#include <QUrl>
#include <QTextCodec>
#include <QStringList>

//#define MAP_COUNT						13
#define MAP_COUNT						24

#define KEY_VIEWER_VERSION				"viewer_version"
#define KEY_VIEWER_UPDATE_LINK			"viewer_link"

#define KEY_FIRST_WARNING_SHOW			"viewer_ex_warning_show"
#define KEY_FIRST_WARNING_TEXT			"viewer_ex_warning_text"

#define KEY_SECOND_WARNING_BOOK_PERCENT	"viewer_ex_book_percent"
#define KEY_SECOND_WARNING_SHOW			"viewer_ex_library_window_show"
#define KEY_SECOND_WARNING_TEXT			"viewer_ex_library_window_text"
#define KEY_SECOND_WARNING_PERIOD		"viewer_ex_library_window_period"
#define KEY_SECOND_WARNING_INTERVAL		"viewer_ex_library_window_interval"
#define KEY_SECOND_WARNING_GROW			"viewer_ex_library_window_grow"
#define KEY_SECOND_WARNING_GROW_SPEED	"viewer_ex_library_window_grow_speed"

#define KEY_HIDE_PAGES					"viewer_ex_books_show"
#define KEY_HIDE_PAGES_TEXT				"viewer_ex_message"

#define VERSION_SEPARATOR				";"

ViewerSettings::ViewerSettings(QVariantMap map) :
	m_map( map )
{
	if( m_map.isEmpty() == false )
	{
		DebugAssert( IsCorrect() );
		if( IsCorrect() == false ) m_map = QVariantMap();
	}
}

ViewerSettings::~ViewerSettings()
{

}

bool ViewerSettings::IsCorrect() const
{
	if( m_map.isEmpty() ) return false;
//	if( m_map.count() != MAP_COUNT ) return false;

	// 1
	if( Contains( KEY_VIEWER_VERSION ) == false ) return false;
	if( IsCorrectString( KEY_VIEWER_VERSION ) == false ) return false;

	// 2
	if( Contains( KEY_VIEWER_UPDATE_LINK ) == false ) return false;
	if( IsCorrectString( KEY_VIEWER_UPDATE_LINK ) == false ) return false;
	if( QUrl( UpdateLink() ).isValid() == false ) return false;

	// 3
	if( Contains( KEY_FIRST_WARNING_SHOW ) == false ) return false;
	if( IsCorrectBoolean( KEY_FIRST_WARNING_SHOW ) == false ) return false;

	// 4
	if( Contains( KEY_FIRST_WARNING_TEXT ) == false ) return false;
	if( IsCorrectString( KEY_FIRST_WARNING_TEXT ) == false ) return false;

	// 5
	if( Contains( KEY_SECOND_WARNING_BOOK_PERCENT ) == false ) return false;
	if( IsCorrectInteger( KEY_SECOND_WARNING_BOOK_PERCENT ) == false ) return false;
	if( SecondWarningBookPercent() < 0 ) return false;
	if( SecondWarningBookPercent() > 100 ) return false;

	// 6
	if( Contains( KEY_SECOND_WARNING_SHOW ) == false ) return false;
	if( IsCorrectBoolean( KEY_SECOND_WARNING_SHOW ) == false ) return false;

	// 7
	if( Contains( KEY_SECOND_WARNING_TEXT ) == false ) return false;
	if( IsCorrectString( KEY_SECOND_WARNING_TEXT ) == false ) return false;

	// 8
	if( Contains( KEY_SECOND_WARNING_PERIOD ) == false ) return false;
	if( IsCorrectInteger( KEY_SECOND_WARNING_PERIOD ) == false ) return false;
	if( SecondWarningPeriod() == ViewerSettings::Undefined ) return false;

	// 9
	if( Contains( KEY_SECOND_WARNING_INTERVAL ) == false ) return false;
	if( IsCorrectInteger( KEY_SECOND_WARNING_INTERVAL ) == false ) return false;
	if( SecondWarningPeriod() == ViewerSettings::ByPage )
	{
		if( SecondWarningInterval() < SECOND_WARNING_INTERVAL_PAGE_LIMIT ) return false;
	}
	else if( SecondWarningPeriod() == ViewerSettings::ByTime )
	{
		if( SecondWarningInterval() < SECOND_WARNING_INTERVAL_TIME_LIMIT ) return false;
	}
	else NotImplemented();

	// 10
	if( Contains( KEY_SECOND_WARNING_GROW ) == false ) return false;
	if( IsCorrectBoolean( KEY_SECOND_WARNING_GROW ) == false ) return false;

	// 11
	if( SecondWarningGrow() )
	{
		if( Contains( KEY_SECOND_WARNING_GROW_SPEED ) == false ) return false;
		if( IsCorrectInteger( KEY_SECOND_WARNING_GROW_SPEED ) == false ) return false;
		if( SecondWarningGrowSpeed() < 0 ) return false;
	}

	// 12
	if( Contains( KEY_HIDE_PAGES ) == false ) return false;
	if( IsCorrectBoolean( KEY_HIDE_PAGES ) == false ) return false;

	// 13
	if( Contains( KEY_HIDE_PAGES_TEXT ) == false ) return false;
	if( IsCorrectString( KEY_HIDE_PAGES_TEXT ) == false ) return false;

	return true;
}

QStringList ViewerSettings::ViewerVersions() const
{
	return String( KEY_VIEWER_VERSION ).split( VERSION_SEPARATOR, QString::SkipEmptyParts );
}

QString ViewerSettings::UpdateLink() const
{
	return String( KEY_VIEWER_UPDATE_LINK );
}

bool ViewerSettings::FirstWarningShow() const
{
	return Boolean( KEY_FIRST_WARNING_SHOW );
}

QString ViewerSettings::FirstWarningText() const
{
	return String( KEY_FIRST_WARNING_TEXT );
}

int ViewerSettings::SecondWarningBookPercent() const
{
	return Integer( KEY_SECOND_WARNING_BOOK_PERCENT );
}

bool ViewerSettings::SecondWarningShow() const
{
	return Boolean( KEY_SECOND_WARNING_SHOW );
}

QString ViewerSettings::SecondWarningText() const
{
	return String( KEY_SECOND_WARNING_TEXT );
}

ViewerSettings::Period ViewerSettings::SecondWarningPeriod() const
{
	if( Integer( KEY_SECOND_WARNING_PERIOD ) == 1 ) return ViewerSettings::ByTime;
	if( Integer( KEY_SECOND_WARNING_PERIOD ) == 2 ) return ViewerSettings::ByPage;
	return ViewerSettings::Undefined;
}

int ViewerSettings::SecondWarningInterval() const
{
	return Integer( KEY_SECOND_WARNING_INTERVAL );
}

bool ViewerSettings::SecondWarningGrow() const
{
	return Boolean( KEY_SECOND_WARNING_GROW );
}

int ViewerSettings::SecondWarningGrowSpeed() const
{
	return Integer( KEY_SECOND_WARNING_GROW_SPEED );
}

bool ViewerSettings::HidePages() const
{
	return Boolean( KEY_HIDE_PAGES );
}

QString ViewerSettings::HidePagesText() const
{
	return String( KEY_HIDE_PAGES_TEXT );
}

bool ViewerSettings::Contains(QString key) const
{
	return m_map.contains( key );
}

QVariant ViewerSettings::Value(QString key) const
{
	DebugAssert( Contains( key ) );
	return m_map.value( key );
}

bool ViewerSettings::IsCorrectString(QString key) const
{
	if( Value( key ).type() != QVariant::String ) return false;
	if( Value( key ).isNull() ) return false;
	return true;
}

QString ViewerSettings::String(QString key) const
{
	DebugAssert( IsCorrectString( key ) );
	return Value( key ).toByteArray();
}

bool ViewerSettings::IsCorrectBoolean(QString key) const
{
	if( String( key ) == "Y" ) return true;
	if( String( key ) == "N" ) return true;
	return false;
}

bool ViewerSettings::Boolean(QString key) const
{
	if( String( key ) == "Y" ) return true;
	if( String( key ) == "N" ) return false;
	NotImplemented();
	return false;
}

bool ViewerSettings::IsCorrectInteger(QString key) const
{
	bool ok;
	String( key ).toInt( &ok );
	return ok;
}

int ViewerSettings::Integer(QString key) const
{
	bool ok;
	int result = String( key ).toInt( &ok );
	DebugAssert( ok );
	return result;
}
