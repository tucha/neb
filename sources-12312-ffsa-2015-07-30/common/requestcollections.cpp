#include "requestcollections.h"

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>

RequestCollections::RequestCollections(QObject* parent, QNetworkAccessManager* manager, Identifier identifier, QString baseUrl, RequestPriority priority) :
	RequestBase( parent, manager, identifier, baseUrl, priority )
{
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

void RequestCollections::Send()
{
	ApiBase::Send( "collections" );
}

void RequestCollections::SlotFinished(ErrorCode error, QByteArray data)
{
	if( error != API_NO_ERROR )
	{
		emit SignalFinished( error, QStringList() );
		return;
	}

	QJsonDocument document = QJsonDocument::fromJson( data );

	DebugAssert( document.isArray() );
	if( document.isArray() == false ) return;

	QStringList collections;

	QJsonArray array = document.array();
	for( int i = 0; i < array.count(); ++i )
	{
		DebugAssert( array.at( i ).isString() );
		collections.append( array.at( i ).toString() );
	}

	emit SignalFinished( error, collections );
}
