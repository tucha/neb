#include "requestdownloadepub.h"

RequestDownloadEpub::RequestDownloadEpub(QObject* parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority):
    Request( parent, manager, identifier.WithEncryptedToken(document.DocumentId(), 1), document, priority )
{
    connect(this,&Request::SignalFinished,this,&RequestDownloadEpub::SlotFinished);
}

void RequestDownloadEpub::Send()
{
        QString requestString = QString( "%1/%2" )
                                                        .arg( "resources" )
                                                        .arg( "epub" )
                                                       ;
        ApiBase::Send( requestString );
}

void RequestDownloadEpub::SlotFinished(ErrorCode error, QByteArray data)
{
    emit SignalFinished(error, data);

}
