#ifndef REQUESTPAGESIZE_H
#define REQUESTPAGESIZE_H

#include "requestpage.h"

#include <QSize>

class RequestPageSize : public RequestPage
{
	Q_OBJECT
public:
	RequestPageSize( QObject *parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority,
					int pageIndex );
	void Send(void);

signals:
	void SignalFinished( ErrorCode error, QSize pageSize );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

private:
	QSize ParsePageSize(QString data);
};

#endif // REQUESTPAGESIZE_H
