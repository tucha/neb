#ifndef REQUESTCOLLECTION_H
#define REQUESTCOLLECTION_H

#include "request.h"

class RequestCollection : public Request
{
	Q_OBJECT
public:
	RequestCollection( QObject *parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority );
	void Send(void);

signals:
	void SignalFinished( ErrorCode error, QString collection );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

private:
};

#endif // REQUESTCOLLECTION_H
