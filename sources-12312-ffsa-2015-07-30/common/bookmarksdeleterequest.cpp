#include "bookmarksdeleterequest.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include "session.h"

#include "debug.h"
#include "platform_id/platform_id.h"
#define KEY_ERRORS	"errors"
#define KEY_MESSAGE	"message"
#define KEY_CODE	"code"

BookmarksDeleteRequest::BookmarksDeleteRequest(QObject *parent, QNetworkAccessManager* manager, QString baseUrl,
										 Bookmark bookmark, RequestPriority priority ) :
	AccountBaseRequest(parent, manager, baseUrl, bookmark.GetUser(), priority),
	m_bookmark( bookmark )
{
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));

	AddGetParameter( "ID", GetBookmark().Id() );
	AddGetParameter( "token", UserToken() );
    AddGetParameter(  "fingerprint", PlatformID::hash());
    AddGetParameter(  "os", REQUEST_OS_ID);
    if(GetUser().HasSession())AddGetParameter("session", GetUser().GetSession()->GetID());
}

Bookmark BookmarksDeleteRequest::GetBookmark() const
{
	return m_bookmark;
}

bool BookmarksDeleteRequest::IsComment() const
{
	return GetBookmark().IsComment();
}

void BookmarksDeleteRequest::Send()
{
	if( IsComment() ) AccountBaseRequest::Send( "notes" );
	else AccountBaseRequest::Send( "bookmark" );
}

void BookmarksDeleteRequest::SlotFinished(ErrorCode error, QByteArray data)
{
#ifdef EMULATE_ACCOUNT_REQUESTS
	emit SignalFinished( API_NO_ERROR );
	return;
#endif
	if( error != API_NO_ERROR )
	{
		UNUSED( data );
		DebugOutput( ParseErrorMessage( data ) );
	}
	emit SignalFinished( error );
}

QNetworkReply*BookmarksDeleteRequest::SendInternal(QString requestString)
{
	QUrl url( requestString );
#if QT_VERSION >= 0x050000
	url.setQuery( GetParameters() );
#else
	QList<QPair<QString, QString> > prms = GetParameters();
	for( QList<QPair<QString, QString> >::iterator iter = prms.begin(); iter != prms.end(); ++iter )
	{
		url.addQueryItem( iter->first, iter->second );
	}
#endif

	QNetworkRequest request;
	request.setUrl( url );
	request.setPriority( Priority() );

	return Manager()->deleteResource( request );
}

QString BookmarksDeleteRequest::ParseErrorMessage(QByteArray data)
{
	QJsonDocument document = QJsonDocument::fromJson( data );

	DebugAssert( document.isObject() );
	if( document.isObject() == false ) return QString();
	QJsonObject object = document.object();

	DebugAssert( object.contains( KEY_ERRORS ) );
	if( object.contains( KEY_ERRORS ) == false ) return QString();
	QJsonValue valueErrors = object.value( KEY_ERRORS );

	DebugAssert( valueErrors.isObject() );
	if( valueErrors.isObject() == false ) return QString();
	QJsonObject subObject = valueErrors.toObject();



	DebugAssert( subObject.contains( KEY_CODE ) );
	if( subObject.contains( KEY_CODE ) == false ) return QString();
	QJsonValue valueCode = subObject.value( KEY_CODE );

	DebugAssert( subObject.contains( KEY_MESSAGE ) );
	if( subObject.contains( KEY_MESSAGE ) == false ) return QString();
	QJsonValue valueMessage = subObject.value( KEY_MESSAGE );


	DebugAssert( valueCode.isDouble() );
	if( valueCode.isDouble() == false ) return QString();
	double code = valueCode.toDouble();

	DebugAssert( valueMessage.isString() );
	if( valueMessage.isString() == false ) return QString();
	QString message = valueMessage.toString();

	UNUSED( code );
	return message;
}
