#include "favoritesmanager.h"

FavoritesManager::FavoritesManager(QObject *parent, RequestPriority priority) :
	QObject(parent),
	m_priority( priority ),
	m_favorites(),
	m_getRequest( NULL ),
	m_isLoaded( false ),
	m_addRequests(),
	m_addDocuments(),
	m_deleteRequests(),
    m_deleteDocuments(),
    m_contentNotFound(false)
{
}

FavoritesManager::~FavoritesManager()
{
	Clear();
}

void FavoritesManager::Clear()
{
	if( IsEmpty() ) return;

	m_favorites.Clear();
	DeleteLaterAndNull( m_getRequest );
	m_isLoaded = false;
    m_contentNotFound = false;

	foreach( FavoritesAddRequest* r, m_addRequests ) r->deleteLater();
	m_addRequests.clear();

	foreach( FavoritesDeleteRequest* r, m_deleteRequests ) r->deleteLater();
	m_deleteRequests.clear();

	m_addDocuments.clear();
	m_deleteDocuments.clear();

	DebugAssert( IsEmpty() );
	emit SignalUpdate( this );
}

void FavoritesManager::Load(AccountBaseRequest* baseRequest)
{
	Clear();

	m_getRequest = baseRequest->NewFavoritesGet( Priority() );
	connect(m_getRequest,SIGNAL(SignalFinished(ErrorCode,FavoriteBooks)),this,SLOT(SlotFinishedGet(ErrorCode,FavoriteBooks)));
	m_getRequest->Send();
}

bool FavoritesManager::RequestAddition(Document document)
{
	DebugAssert( Contains( document ) == false );
	if( Contains( document ) ) return false;

	FavoritesAddRequest* addRequest = m_getRequest->NewFavoritesAdd( document, Priority() );
	connect(addRequest,SIGNAL(SignalFinished(ErrorCode,FavoriteBook)),this,SLOT(SlotFinishedAdd(ErrorCode,FavoriteBook)));
	connect(addRequest,SIGNAL(SignalError()),this,SLOT(SlotErrorAdd()));
	m_addRequests.insert( addRequest );
	m_addDocuments.insert( document );
	addRequest->Send();
	emit SignalUpdate( this );
	return true;
}

bool FavoritesManager::RequestRemoval(Document document)
{
	DebugAssert( Contains( document ) );
	if( Contains( document ) == false) return false;

	FavoritesDeleteRequest* deleteRequest = m_getRequest->NewFavoritesDelete( m_favorites.Get( document ), Priority() );
	connect(deleteRequest,SIGNAL(SignalFinished(ErrorCode)),this,SLOT(SlotFinishedDelete(ErrorCode)));
	m_deleteRequests.insert( deleteRequest );
	m_deleteDocuments.insert( document );
	deleteRequest->Send();
	emit SignalUpdate( this );
	return true;
}

bool FavoritesManager::IsNotFound()
{
    return m_contentNotFound;
}

bool FavoritesManager::IsActual(Document document) const
{
	if( m_isLoaded == false ) return false;
	if( m_addDocuments.contains( document ) ) return false;
	if( m_deleteDocuments.contains( document ) ) return false;
	return true;
}

bool FavoritesManager::Contains(Document document) const
{
	return m_favorites.Contains( document );
}

DocumentList FavoritesManager::GetFavoriteDocuments() const
{
	return m_favorites.Documents();
}

void FavoritesManager::SendRequest(Document document, bool favorite)
{
	DebugAssert( document.IsEmpty() == false );
	if( document.IsEmpty() ) return;

	DebugAssert( IsActual( document ) );
	if( IsActual( document ) == false ) return;

	if( favorite ) RequestAddition( document );
	else RequestRemoval( document );
}

void FavoritesManager::SlotFinishedGet(ErrorCode error, FavoriteBooks result)
{
	if( IsEmpty() ) return;
	if( sender() != m_getRequest ) return;

	if( error != API_NO_ERROR )
	{
        m_contentNotFound = true;
//		m_getRequest->Send();
		return;
	}
    m_contentNotFound = false;

	DebugAssert( m_isLoaded == false );
	if( m_isLoaded )
	{
		Clear();
		return;
	}

	DebugAssert( m_favorites.IsEmpty() );
	if( m_favorites.IsEmpty() == false )
	{
		Clear();
		return;
	}

	m_isLoaded = true;
	m_favorites = FavoritesContainer( result );
	emit SignalUpdate( this );
    emit SignalLoaded( this );
}

void FavoritesManager::SlotFinishedAdd(ErrorCode error, FavoriteBook result)
{
	if( IsEmpty() ) return;

	FavoritesAddRequest* addRequest = static_cast<FavoritesAddRequest*>( sender() );
	if( m_addRequests.contains( addRequest ) == false ) return;

	if( error != API_NO_ERROR )
	{
//		addRequest->Send();
		return;
	}

	bool test = true;
	test &= m_favorites.Add( result );
	test &= m_addRequests.remove( addRequest );
	test &= m_addDocuments.remove( addRequest->GetDocument() );

	DebugAssert( test );
	if( test == false )
	{
		Clear();
		return;
	}

	DeleteLaterAndNull( addRequest );
	emit SignalUpdate( this );
}

void FavoritesManager::SlotFinishedDelete(ErrorCode error)
{
	if( IsEmpty() ) return;

	FavoritesDeleteRequest* deleteRequest = static_cast<FavoritesDeleteRequest*>( sender() );
	if( m_deleteRequests.contains( deleteRequest ) == false ) return;

	if( error != API_NO_ERROR )
	{
//		deleteRequest->Send();
		return;
	}

	bool test = true;
	test &= m_favorites.Remove( deleteRequest->GetFavoriteBook().GetDocument() );
	test &= m_deleteRequests.remove( deleteRequest );
	test &= m_deleteDocuments.remove( deleteRequest->GetFavoriteBook().GetDocument() );

	DebugAssert( test );
	if( test == false )
	{
		Clear();
		return;
	}

	DeleteLaterAndNull( deleteRequest );
	emit SignalUpdate( this );
}

void FavoritesManager::SlotErrorAdd()
{
	if( IsEmpty() ) return;

	FavoritesAddRequest* addRequest = static_cast<FavoritesAddRequest*>( sender() );
	if( m_addRequests.contains( addRequest ) == false ) return;

	Clear();
}

bool FavoritesManager::IsEmpty() const
{
	bool result = m_getRequest == NULL;

	if( result )
	{
		DebugAssert( m_favorites.IsEmpty() );
		DebugAssert( m_getRequest == NULL );
		DebugAssert( m_isLoaded == false );
		DebugAssert( m_addRequests.isEmpty() );
		DebugAssert( m_addDocuments.isEmpty() );
		DebugAssert( m_deleteRequests.isEmpty() );
		DebugAssert( m_deleteDocuments.isEmpty() );
	}
	else
	{
		DebugAssert( m_getRequest != NULL );
		DebugAssert( CheckRequests() );
	}

	return result;
}

RequestPriority FavoritesManager::Priority() const
{
	return m_priority;
}

bool FavoritesManager::CheckRequests() const
{
	if( m_addRequests.count() != m_addDocuments.count() ) return false;
	if( m_deleteRequests.count() != m_deleteDocuments.count() ) return false;

	foreach( FavoritesAddRequest* r, m_addRequests )
	{
		if( m_addDocuments.contains( r->GetDocument() ) == false ) return false;
	}

	foreach( FavoritesDeleteRequest* r, m_deleteRequests )
	{
		if( m_deleteDocuments.contains( r->GetFavoriteBook().GetDocument() ) == false ) return false;
	}

	return true;
}
