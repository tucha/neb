#include "debug.h"

#include <QMutexLocker>
#include <QFile>

Debug Debug::g_instance( NULL );

Debug::Debug(QObject *parent) :
	QObject(parent)
{
}

Debug::~Debug()
{
	if( m_output.isEmpty() ) return;

	QFile file( "output.txt" );
	if( file.open( QIODevice::WriteOnly ) == false ) return;

	QString text;
	foreach( QString line, m_output )
	{
		text.append( line );
		text.append( "\r\n" );
	}

	file.write( text.toUtf8() );
}

Debug*Debug::Instance()
{
	return &g_instance;
}

void Debug::Output(QString text)
{
	if( text.isEmpty() ) return;

	QMutexLocker lock( &m_mutex );
	QString withIndex = QString( "%1	%2" )
		   .arg( m_output.count() )
		   .arg( text );
	m_output.append( withIndex );

	lock.unlock();
	emit SignalOutput( withIndex );
}
