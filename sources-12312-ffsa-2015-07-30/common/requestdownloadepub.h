#ifndef REQUESTDOWNLOADEPUB_H
#define REQUESTDOWNLOADEPUB_H

#include <QObject>
#include "request.h"

class RequestDownloadEpub : public Request
{
    Q_OBJECT
public:
    RequestDownloadEpub(QObject *parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority);
    void Send(void);
signals:
    void SignalFinished( ErrorCode error, QByteArray epubData );

private slots:
    void SlotFinished( ErrorCode error, QByteArray epubData );

};

#endif // REQUESTDOWNLOADEPUB_H
