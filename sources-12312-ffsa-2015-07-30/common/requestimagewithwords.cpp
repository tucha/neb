#include "requestimagewithwords.h"

#include "getparameterslice.h"

RequestImageWithWords::RequestImageWithWords(QObject* parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority,
						   int pageIndex, QStringList words, int ppi, ImageFormat format ):
    RequestPage( parent, manager, identifier, document, priority, pageIndex ),
	m_words( words ), m_ppi( ppi ), m_format( format )
{
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

void RequestImageWithWords::Send()
{
	DebugAssert( TestPpi( TYPE_IMAGES, Ppi() ) );

	QString requestString = QString( "%1/%2/%3/%4%5%6%7" )
							.arg( "search" )
							.arg( WordsString( Words() ) )
							.arg( "render" )
							.arg( "ppi" )
							.arg( QString::number( Ppi() ) )
							.arg( "." )
							.arg( FormatString( Format() ) );

	RequestPage::Send( requestString );
}

QStringList RequestImageWithWords::Words() const
{
	return m_words;
}

int RequestImageWithWords::Ppi() const
{
	return m_ppi;
}

RequestPage::ImageFormat RequestImageWithWords::Format() const
{
	return m_format;
}

void RequestImageWithWords::Slice(QRectF rectangle)
{
	AddGetParameter( new GetParameterSlice( rectangle ) );
}

void RequestImageWithWords::SlotFinished(ErrorCode error, QByteArray data)
{
	if( error == API_NO_ERROR ) emit SignalFinished( error, ParseImage( data, Format() ) );
	else emit SignalFinished( error, QImage() );
}
