#ifndef VIEWERSETTINGS_H
#define VIEWERSETTINGS_H

#include <QVariantMap>

#define SECOND_WARNING_INTERVAL_PAGE_LIMIT	1
#define SECOND_WARNING_INTERVAL_TIME_LIMIT	5

class ViewerSettings
{
public:
	enum Period
	{
		Undefined,
		ByTime,
		ByPage
	};

	ViewerSettings( QVariantMap map = QVariantMap() );
	virtual ~ViewerSettings();

	bool		IsCorrect(void)const;

	QStringList	ViewerVersions(void)const;
	QString		UpdateLink(void)const;

	bool		FirstWarningShow(void)const;
	QString		FirstWarningText(void)const;

	int			SecondWarningBookPercent(void)const;
	bool		SecondWarningShow(void)const;
	QString		SecondWarningText(void)const;
	Period		SecondWarningPeriod(void)const;
	int			SecondWarningInterval(void)const;
	bool		SecondWarningGrow(void)const;
	int			SecondWarningGrowSpeed(void)const;

	bool		HidePages(void)const;
	QString		HidePagesText(void)const;

private:
	bool		Contains( QString key )const;
	QVariant	Value( QString key )const;

	bool		IsCorrectString( QString key )const;
	QString		String( QString key )const;

	bool		IsCorrectBoolean( QString key )const;
	bool		Boolean( QString key )const;

	bool		IsCorrectInteger( QString key )const;
	int			Integer( QString key )const;

	QVariantMap m_map;
};

#endif // VIEWERSETTINGS_H
