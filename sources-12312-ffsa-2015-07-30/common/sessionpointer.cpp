#include "sessionpointer.h"

#include "session.h"
#include "debug.h"

#ifdef BUILD_DEBUG
QSet<int> SessionPointer::g_set = QSet<int>();
int SessionPointer::g_lastId = 0;
#endif

SessionPointer::SessionPointer():
	QSharedPointer<Session>()
{
	InitializeId();
}

SessionPointer::SessionPointer(Session* session):
	QSharedPointer<Session>( session )
{
	InitializeId();
}

SessionPointer::SessionPointer(const SessionPointer& another):
	QSharedPointer<Session>( another )
{
	InitializeId();
}

SessionPointer&SessionPointer::operator=(const SessionPointer& another)
{
	FreeId();
	QSharedPointer<Session>::operator =( another );
	InitializeId();
	return *this;
}

SessionPointer::~SessionPointer()
{
	FreeId();
}

bool SessionPointer::isNull() const
{
	return QSharedPointer<Session>::isNull();
}

Session*SessionPointer::operator->() const
{
	return QSharedPointer<Session>::operator ->();
}

Session*SessionPointer::data() const
{
	return QSharedPointer<Session>::data();
}

#ifdef BUILD_DEBUG
QSet<int> SessionPointer::Ids()
{
	return g_set;
}
#endif

void SessionPointer::InitializeId()
{
#ifdef BUILD_DEBUG
	if( isNull() ) return;
	m_id = g_lastId++;

	DebugAssert( g_set.contains( m_id ) == false );
	g_set.insert( m_id );
#endif
}

void SessionPointer::FreeId()
{
#ifdef BUILD_DEBUG
	if( isNull() ) return;

	DebugAssert( g_set.contains( m_id ) );
	g_set.remove( m_id );
#endif
}
