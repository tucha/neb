#ifndef DEBUG_H
#define DEBUG_H

#include <QObject>
#include <QMutex>
#include <QList>
#include <QString>

#include "defines.h"

class Debug : public QObject
{
	Q_OBJECT
private:
	DISABLE_COPY( Debug )
	explicit Debug(QObject *parent = 0);
	~Debug();

public:
	static Debug* Instance(void);

	void Output( QString text );

signals:
	void SignalOutput( QString text );

private:
	static Debug g_instance;

	QMutex m_mutex;
	QList<QString> m_output;
};

#endif // DEBUG_H
