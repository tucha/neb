#include "document.h"

#include <QStringList>

#include "request.h"
#include "defines.h"
#include "logic.h"

#define SEPARATOR_1	"?"
#define SEPARATOR_2	"&"
#define SEPARATOR_3	"="
#define KEY_ID		"id"
#define KEY_TOKEN	"token"
#define KEY_PAGE	"page"
#define KEY_SEARCH_QUERY "q"
#define MIN_ID_LENGTH	1

Document::Document(const QString& spd):
	m_baseUrl(),
	m_parameters()
{
	if( spd.isEmpty() ) return;

	DebugAssert( CheckString( spd ) );
	if( CheckString( spd ) == false ) return;

	QStringList subStrings1 = spd.split( SEPARATOR_1 );
	m_baseUrl = FormatBaseUrl( subStrings1.at( 0 ) );
	m_parameters = ExtractParameters( subStrings1.at( 1 ) );

	DebugAssert( ToString().isEmpty() == false );
}

QString Document::ToString( bool exceptToken ) const
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return QString();

	QString result = QString( "%1%2%3" )
					 .arg( BaseUrl() )
					 .arg( SEPARATOR_1 )
					 .arg( ParametersString( exceptToken ) );

	DebugAssert( CheckString( result ) );
	if( CheckString( result ) == false ) return QString();

	return result;
}

bool Document::CheckString(const QString& spd)
{
	QStringList subStrings = spd.split( SEPARATOR_1 );
	if( subStrings.count() != 2 ) return false;

	QString baseUrl = subStrings.at( 0 );
	if( baseUrl.isEmpty() ) return false;

	QString parametersString = subStrings.at( 1 );
	if( CheckParameters( parametersString ) == false ) return false;

	QHash<QString,QString> parameters = ExtractParameters( parametersString );
	if( parameters.contains( KEY_ID ) == false ) return false;

	QString documentId = parameters.value( KEY_ID );
	if( CheckDocumentId( documentId ) == false ) return false;

	return true;
}

bool Document::CheckDocumentId(const QString& id)
{
	if( id.length() < MIN_ID_LENGTH ) return false;

    if(id.contains(SEPARATOR_1) ||
       id.contains(SEPARATOR_2) ||
       id.contains(SEPARATOR_3))
        return false;
//	foreach( QChar ch, id )
//	{
//		if( CheckDocumentIdChar( ch ) == false ) return false;
//	}

	return true;
}

Document Document::Create(QString baseUrl, QString id)
{
	QString spd;
	spd.append( baseUrl );
	spd.append( SEPARATOR_1 );
	spd.append( KEY_ID );
	spd.append( SEPARATOR_3 );
	spd.append( id );

	return Document( spd );
}

Document Document::FromId(QString id)
{
	QString baseUrl = Logic::BaseUrlForDocuments();
	return Create( baseUrl, id );
}

QString Document::FormatBaseUrl(const QString &baseUrl)
{
#if QT_VERSION >= 0x050000
	QString encoded = QUrl( baseUrl ).url( QUrl::FullyEncoded );
	QString decoded = QUrl( encoded ).url( QUrl::PrettyDecoded );
	return decoded;
#else
	QString encoded = QUrl( baseUrl ).toEncoded();
	QString decoded = QUrl( encoded ).toString();
	return decoded;
#endif
}

bool Document::IsEmpty() const
{
	bool result = m_baseUrl.isEmpty();

	if( result )
	{
		DebugAssert( m_baseUrl.isEmpty() );
		DebugAssert( m_parameters.isEmpty() );
	}
	else
	{
		DebugAssert( m_baseUrl.isEmpty() == false );
		DebugAssert( m_parameters.isEmpty() == false );
	}

	return result;
}

QString Document::BaseUrl() const
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return QString();

	return m_baseUrl;
}

QString Document::DocumentId() const
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return QString();

	return m_parameters.value( KEY_ID );
}

QString Document::Token() const
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return QString();

	DebugAssert( ContainsToken() );
	if( ContainsToken() == false ) return QString();

	return m_parameters.value( KEY_TOKEN );
}

QVariant Document::Page() const
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return -1;

	DebugAssert( ContainsPage() );
	if( ContainsPage() == false ) return 1;

	bool ok;
	int result = m_parameters.value( KEY_PAGE ).toInt( &ok );
    if(ok) return result;
    else
    {
        return QString::fromUtf8(
                    QByteArray::fromPercentEncoding(m_parameters.value( KEY_PAGE ).toUtf8()));
    }
}

QString Document::SearchQuery()
{
    DebugAssert( IsEmpty() == false );
    if( IsEmpty() ) return "";

    DebugAssert( ContainsSearchQuery() );
    if( ContainsSearchQuery() == false ) return "";

    return QUrl::fromPercentEncoding(m_parameters.value( KEY_SEARCH_QUERY ).toUtf8());
}

bool Document::ContainsToken() const
{
	return m_parameters.contains( KEY_TOKEN );
}

bool Document::ContainsPage() const
{
	return m_parameters.contains( KEY_PAGE );
}

bool Document::ContainsSearchQuery() const
{
    return m_parameters.contains( KEY_SEARCH_QUERY );
}

QString Document::ParametersString(bool exceptToken ) const
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return QString();

	QString result;

	QHash<QString,QString> parameters = m_parameters;
	if( exceptToken ) parameters.remove( KEY_TOKEN );

	parameters.remove( KEY_PAGE );

	QList<QString> keys = parameters.keys();
	for( int i = 0; i < keys.count(); ++i )
	{
		QString key = keys.at( i );
		QString value = m_parameters.value( key );

		result.append( key );
		result.append( SEPARATOR_3 );
		result.append( value );

		if( i != keys.count() - 1 ) result.append( SEPARATOR_2 );
	}

	return result;
}

QHash<QString, QString> Document::ExtractParameters(const QString& parametersString)
{
	QHash<QString, QString> result;

	QStringList parameters = parametersString.split( SEPARATOR_2 );
	foreach( QString parameter, parameters )
	{
		QStringList subStrings = parameter.split( SEPARATOR_3 );
		QString key = subStrings.at( 0 );
		QString value = subStrings.at( 1 );
		result.insert( key, value );
	}

	return result;
}

bool Document::CheckParameter(QString key, QString value)
{
	if( key == KEY_ID ) return true;
	if( key == KEY_TOKEN ) return true;
	if( key == KEY_PAGE )
	{
        if(value.length() > 0) return true;
        else return false;
	}
    if( key == KEY_SEARCH_QUERY)
        return true;
	return false;
}

bool Document::CheckParameters(const QString& parametersString)
{
	QStringList parameters = parametersString.split( SEPARATOR_2 );
	if( parameters.count() < 1 ) return false;
//	if( parameters.count() > 3 ) return false;

	foreach( QString parameter, parameters )
	{
		QStringList subStrings = parameter.split( SEPARATOR_3 );
		if( subStrings.count() != 2 ) return false;

		QString key = subStrings.at( 0 );
		if( key.isEmpty() ) return false;

		QString value = subStrings.at( 1 );
		if( value.isEmpty() ) return false;

		if( CheckParameter( key, value ) == false ) return false;
	}

	return true;
}

bool Document::CheckDocumentIdChar(const QChar& ch)
{
    return true;
	if( ch.isLetterOrNumber() ) return true;

	if( ch == '-' ) return true;
	if( ch == '+' ) return true;
	if( ch == '/' ) return true;
	if( ch == '(' ) return true;
	if( ch == ')' ) return true;
	if( ch == '.' ) return true;
	if( ch == ':' ) return true;
	if( ch == '_' ) return true;
    if( ch == '|' ) return true;


}
