#ifndef REQUESTIMAGEWITHWORDS_H
#define REQUESTIMAGEWITHWORDS_H

#include "requestpage.h"

#include <QImage>
#include <QStringList>

class RequestImageWithWords : public RequestPage
{
	Q_OBJECT
public:
	RequestImageWithWords( QObject *parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority,
					int pageIndex, QStringList words, int ppi, ImageFormat format );
	void Send(void);

	QStringList Words(void)const;
	int Ppi(void)const;
	ImageFormat Format(void)const;

	void Slice( QRectF rectangle );

signals:
	void SignalFinished( ErrorCode error, QImage image );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

private:
	const QStringList m_words;
	const int m_ppi;
	const ImageFormat m_format;
};

#endif // REQUESTIMAGEWITHWORDS_H
