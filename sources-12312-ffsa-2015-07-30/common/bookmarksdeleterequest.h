#ifndef BOOKMARKSDELETEREQUEST_H
#define BOOKMARKSDELETEREQUEST_H

#include "accountbaserequest.h"
#include "document.h"
#include "bookmark.h"

class BookmarksDeleteRequest : public AccountBaseRequest
{
	Q_OBJECT
public:
	explicit BookmarksDeleteRequest(QObject *parent, QNetworkAccessManager* manager, QString baseUrl,
								 Bookmark bookmark, RequestPriority priority = PRIORITY_NORMAL );

	Bookmark GetBookmark(void)const;
	bool IsComment(void)const;

	void Send(void);

signals:
	void SignalFinished( ErrorCode error );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

protected:
	virtual QNetworkReply* SendInternal( QString requestString );

private:
	QString ParseErrorMessage( QByteArray data );

	const Bookmark m_bookmark;
};

#endif // BOOKMARKSDELETEREQUEST_H
