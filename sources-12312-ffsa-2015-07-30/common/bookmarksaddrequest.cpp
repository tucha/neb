#include "bookmarksaddrequest.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include "platform_id/platform_id.h"
#include "session.h"
#define KEY_RESULT_ID "resultID"

BookmarksAddRequest::BookmarksAddRequest(QObject *parent, QNetworkAccessManager* manager, QString baseUrl,
                                         User user, Document document, int pageIndex, QString resPath, RequestPriority priority ) :
	AccountBaseRequest(parent, manager, baseUrl, user, priority),
	m_document( document ),
	m_pageIndex( pageIndex ),
    m_text(),
    m_resPath(resPath)
{
	Construct();
}

BookmarksAddRequest::BookmarksAddRequest(QObject* parent, QNetworkAccessManager* manager, QString baseUrl,
                                         User user, Document document, int pageIndex, QString resPath, QString text, RequestPriority priority) :
	AccountBaseRequest(parent, manager, baseUrl, user, priority),
	m_document( document ),
	m_pageIndex( pageIndex ),
    m_text( text ),
    m_resPath(resPath)
{
	Construct();
	AddGetParameter( "text", Text() );
}

Document BookmarksAddRequest::GetDocument() const
{
	return m_document;
}

int BookmarksAddRequest::PageIndex() const
{
	DebugAssert( m_pageIndex >= 0 );
	return m_pageIndex;
}


QString BookmarksAddRequest::ResPath() const
{
    return m_resPath;
}


QString BookmarksAddRequest::Text() const
{
	return m_text;
}

bool BookmarksAddRequest::IsComment() const
{
	return Text().isEmpty() == false;
}

void BookmarksAddRequest::Send()
{
	if( IsComment() ) AccountBaseRequest::Send( "notes" );
    else AccountBaseRequest::Send( "bookmark" );
}

void BookmarksAddRequest::SlotFinished(ErrorCode error, QByteArray data)
{
	if( error != API_NO_ERROR )
	{
		emit SignalFinished( error, Bookmark() );
		return;
	}
	if( data.isEmpty() )
	{
		emit SignalError();
		return;
	}

	emit SignalFinished( error, ParseResult( data ) );
}

QNetworkReply*BookmarksAddRequest::SendInternal(QString requestString)
{
	QNetworkRequest request;
	request.setUrl( QUrl( requestString ) );
	request.setPriority( Priority() );

#if QT_VERSION >= 0x050000
	QByteArray data = GetParameters().query().toUtf8();
#else
	QList<QPair<QString, QString> > prms = GetParameters();

	QByteArray data;
	QUrl url;
	for( QList<QPair<QString, QString> >::iterator iter = prms.begin(); iter != prms.end(); ++iter )
	{
		data.append( iter->first + url.queryValueDelimiter() + iter->second + url.queryPairDelimiter() );
	}
	if( data.endsWith( url.queryPairDelimiter() ) ) data.remove( data.length() - 1, 1 );
#endif
//    qDebug() << requestString << data;
    return Manager()->put( request, data );
}

void BookmarksAddRequest::Construct()
{
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));

	AddGetParameter( "token", UserToken() );
    AddGetParameter(  "fingerprint", PlatformID::hash());
    AddGetParameter(  "os", REQUEST_OS_ID);
    if(GetUser().HasSession())AddGetParameter("session", GetUser().GetSession()->GetID());


	AddGetParameter( "book_id", GetDocument().DocumentId() );
    if(ResPath() != "")
    {
        AddGetParameter( "num_word", ResPath() );
        AddGetParameter( "format", "epub");
    }
    else
    {
        AddGetParameter( "num_page", QString::number( PageIndex() ) );
    }
}

Bookmark BookmarksAddRequest::ParseResult(QByteArray data)
{
	QJsonDocument document = QJsonDocument::fromJson( data );

	DebugAssert( document.isObject() );
	if( document.isObject() == false ) return Bookmark();
	QJsonObject object = document.object();

	DebugAssert( object.contains( KEY_RESULT_ID ) );
	if( object.contains( KEY_RESULT_ID ) == false ) return Bookmark();
	QJsonValue valueId = object.value( KEY_RESULT_ID );

	DebugAssert( valueId.isDouble() );
	if( valueId.isDouble() == false ) return Bookmark();
	double id = valueId.toDouble();

    return Bookmark( GetUser(), GetDocument(), PageIndex(), ResPath(), QString::number( id ), Text() );
}
