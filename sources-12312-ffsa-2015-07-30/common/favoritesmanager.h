#ifndef FAVORITESMANAGER_H
#define FAVORITESMANAGER_H

#include <QObject>

#include "common.h"
#include "favoritescontainer.h"

class FavoritesManager : public QObject
{
	Q_OBJECT
private:
	DISABLE_COPY( FavoritesManager )

public:
	explicit FavoritesManager(QObject *parent, RequestPriority priority = PRIORITY_NORMAL);
	~FavoritesManager();

	void Clear();
	void Load( AccountBaseRequest* baseRequest );
	bool IsActual( Document document )const;
	bool Contains( Document document )const;
    DocumentList GetFavoriteDocuments(void)const;
    bool IsNotFound();
public slots:
	void SendRequest( Document document, bool favorite );

signals:
	void SignalUpdate( FavoritesManager* manager );
    void SignalLoaded( FavoritesManager* manager );
private slots:
	void SlotFinishedGet( ErrorCode error, FavoriteBooks result );
	void SlotFinishedAdd( ErrorCode error, FavoriteBook result );
	void SlotFinishedDelete( ErrorCode error );
	void SlotErrorAdd();

private:
	bool RequestAddition( Document document );
	bool RequestRemoval( Document document );

    bool IsEmpty(void) const;
	RequestPriority Priority(void)const;
	bool CheckRequests(void)const;

	RequestPriority m_priority;
	FavoritesContainer m_favorites;

	FavoritesGetRequest* m_getRequest;

	bool m_isLoaded;
    bool m_contentNotFound;
	QSet<FavoritesAddRequest*> m_addRequests;
	QSet<Document> m_addDocuments;

	QSet<FavoritesDeleteRequest*> m_deleteRequests;
	QSet<Document> m_deleteDocuments;
};

#endif // FAVORITESMANAGER_H
