#ifndef APIBASE_H
#define APIBASE_H

#include <QObject>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QByteArray>
#include <QMutex>
#include <QSet>
#if QT_VERSION >= 0x050000
#include <QUrlQuery>
#endif
#include <QNetworkProxy>
#include <QTimer>
#include <QSettings>
#include <QUrl>
#include <QStringList>

#include "defines.h"

#define PRIORITY_LOW	QNetworkRequest::LowPriority
#define PRIORITY_NORMAL	QNetworkRequest::NormalPriority
#define PRIORITY_HIGHT	QNetworkRequest::HighPriority

#define API_NO_ERROR		QNetworkReply::NoError

class QNetworkAccessManager;
class GetParameter;
class QAuthenticator;

typedef QNetworkRequest::Priority RequestPriority;
typedef QNetworkReply::NetworkError ErrorCode;

class ApiBase : public QObject
{
	Q_OBJECT

private:
	ApiBase();
	DISABLE_COPY( ApiBase )

public:
	// Constructor / Destructor
	ApiBase(
			QObject *parent,
			QNetworkAccessManager* manager,
			QString baseUrl,
			RequestPriority priority = PRIORITY_NORMAL );
	virtual ~ApiBase();

	// Public methods
	void Send( QString requestString );
	void Cancel(void);
	void AddGetParameter( QString key, QString value );

	// Public properties
	QNetworkAccessManager* Manager(void)const;
	QString BaseUrl(void)const;
	RequestPriority Priority(void)const;
	bool IsRunning(void)const;

	static bool IsConfigPresented(void);
	static bool IsDownloadingError( ErrorCode error );
	static bool IsContentForbiddenError( ErrorCode error );

signals:
	// Public signals
	void SignalDownloadProgress( qint64 bytesReceived, qint64 bytesTotal );
	void SignalFinished( ErrorCode error, QByteArray data );
	void SignalAuthenticationRequired( QAuthenticator* authenticator );

protected:
	// Protected methods
	void AddGetParameter( GetParameter* parameter );
	void ClearGetParameters(void);
#if QT_VERSION >= 0x050000
	QUrlQuery GetParameters(void);
#else
	QList<QPair<QString, QString> > GetParameters(void);
#endif
	virtual QNetworkReply* SendInternal( QString requestString ) = 0;

private slots:
	// Private slots
	void SlotSslErrors(const QList<QSslError> &errors);
	void SlotDownloadProgress( qint64 bytesReceived, qint64 bytesTotal );
	void SlotFinished();
	void SlotAuthenticationRequired( QNetworkReply* reply, QAuthenticator* authenticator );
	void SendAll();

private:
	// Private methods
	void Add( QNetworkReply* reply );
	void Remove( QNetworkReply* reply );
	QString Tabulated( QStringList strings );
	QString UrlString( QNetworkReply* reply );
	QString TimeString();
	QString DataString( QByteArray data );
	QString DebugString( QNetworkReply* reply );
	QString NetworkLogString( QNetworkReply* reply, QByteArray data );
	bool NeedTimeOut(void)const;
	void CountErrors( ErrorCode error );
	QUrl RemoveQuery( QUrl url, QString key );
	static QString ConfigPath(void);
	static QSettings* Config(void);

	// Private members
	QNetworkAccessManager* const m_manager;
	const QString m_baseUrl;
	const RequestPriority m_priority;
	mutable QMutex m_mutex;
	QSet<QNetworkReply*> m_replies;
	QSet<GetParameter*> m_getParameters;

	int m_errorsCount;
	QStringList m_requests;
	QTimer m_timer;

	static QSettings* g_config;

#ifdef WINDOWS_PHONE
protected:
	bool m_workaround;
#endif
};

#endif // APIBASE_H
