#include "apibase.h"

#include <QMutexLocker>
#include <QNetworkAccessManager>
#include <QTime>

#if QT_VERSION >= 0x050000
#include <QUrlQuery>
#else
#include <QUrl>
#include <QSslError>
#endif

#include <QCoreApplication>
#include <QDir>

#include "debug.h"
#include "getparameter.h"
#include "getparametersimple.h"
#include "document.h"

#define ERRORS_LIMIT	5
#define TIME_OUT		5000

QSettings* ApiBase::g_config = NULL;

ApiBase::ApiBase(QObject* parent, QNetworkAccessManager* manager, QString baseUrl, RequestPriority priority):
	QObject( parent ),
	m_manager( manager ),
	m_baseUrl( baseUrl ),
	m_priority( priority ),
	m_mutex( QMutex::Recursive ),
	m_replies(),
	m_getParameters(),
	m_errorsCount( 0 ),
	m_requests(),
	m_timer()
#ifdef WINDOWS_PHONE
	, m_workaround( false )
#endif
{
	m_timer.setSingleShot( true );

	connect(&m_timer,SIGNAL(timeout()),this,SLOT(SendAll()));
	connect(m_manager,SIGNAL(authenticationRequired(QNetworkReply*,QAuthenticator*)),this,SLOT(SlotAuthenticationRequired(QNetworkReply*,QAuthenticator*)));
}

ApiBase::~ApiBase()
{
	Cancel();
	ClearGetParameters();
}

void ApiBase::Send(QString requestString)
{
	QMutexLocker lock( &m_mutex );
	m_requests.append( requestString );
	if( NeedTimeOut() )
	{
		if( m_timer.isActive() == false ) m_timer.start( TIME_OUT );
		return;
	}
	SendAll();
}

void ApiBase::Cancel(void)
{
	QMutexLocker lock( &m_mutex );
	foreach( QNetworkReply* reply, m_replies )
	{
		reply->deleteLater();
	}
	m_replies.clear();
}

void ApiBase::AddGetParameter(GetParameter* parameter)
{
	DebugAssert( parameter != NULL );
	if( parameter == NULL ) return;

	QMutexLocker lock( &m_mutex );
	DebugAssert( m_getParameters.contains( parameter ) == false );
	m_getParameters.insert( parameter );
}

void ApiBase::AddGetParameter(QString key, QString value)
{
	AddGetParameter( new GetParameterSimple( key, value ) );
}

void ApiBase::ClearGetParameters()
{
	QMutexLocker lock( &m_mutex );
	foreach( GetParameter* parameter, m_getParameters )
	{
		delete parameter;
	}
	m_getParameters.clear();
}

QNetworkAccessManager*ApiBase::Manager() const
{
	return m_manager;
}

QString ApiBase::BaseUrl() const
{
	return m_baseUrl;
}

RequestPriority ApiBase::Priority() const
{
	return m_priority;
}

bool ApiBase::IsRunning() const
{
	QMutexLocker lock( &m_mutex );
	if( m_requests.isEmpty() == false ) return true;
	if( m_replies.isEmpty() == false ) return true;
	return false;
}

bool ApiBase::IsConfigPresented()
{
	return Config()->allKeys().isEmpty() == false;
}

bool ApiBase::IsDownloadingError(ErrorCode error)
{
	if( error == QNetworkReply::TimeoutError ) return true;
	if( error == QNetworkReply::ConnectionRefusedError ) return true;
	if( error == QNetworkReply::ContentNotFoundError ) return true;
	if( error == QNetworkReply::ProtocolUnknownError ) return true;
	if( error == QNetworkReply::UnknownNetworkError ) return true;

	return false;
}

bool ApiBase::IsContentForbiddenError(ErrorCode error)
{
	if( error == QNetworkReply::ContentOperationNotPermittedError ) return true;

	return false;
}

void ApiBase::SlotSslErrors(const QList<QSslError>& errors)
{
	foreach( QSslError error, errors )
	{
		DebugOutput( error.errorString() );
	}

#ifdef IGNORE_SSL_ERRORS
	QNetworkReply* reply = static_cast<QNetworkReply*>( sender() );
	reply->ignoreSslErrors();
#endif
}

void ApiBase::SlotDownloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
	emit SignalDownloadProgress( bytesReceived, bytesTotal );
#ifdef WINDOWS_PHONE
	if( m_workaround ) SlotFinished();
#endif
}

void ApiBase::SlotFinished()
{
	QNetworkReply* reply = static_cast<QNetworkReply*>( sender() );

	ErrorCode error = reply->error();
	QByteArray data = reply->readAll();
	QString debugString = DebugString( reply );
    if(error != QNetworkReply::NoError && data.size() < 10000) qDebug() << reply->url() << data;
#ifdef REQUESTS_DUMP_CREATE
	if( data.count() < 10000 )
	{
		QString url = RemoveQuery( reply->url(), "token" ).toString();
		if( url.endsWith( "auth/" ) == false ) Config()->setValue( url, QString( data ) );
	}
#endif
#ifdef REQUESTS_DUMP_APPLY
	QString url = RemoveQuery( reply->url(), "token" ).toString();
	if( Config()->contains( url ) ) data = Config()->value( url ).toByteArray();
#endif

#ifdef NETWORK_LOG
	Debug::Instance()->Output( NetworkLogString( reply, data ) );
#endif

	reply->deleteLater();
	Remove( reply );

	CountErrors( error );

	emit SignalFinished( error, data );

#ifndef NETWORK_LOG
	DebugOutput( debugString );
#endif
}

void ApiBase::SlotAuthenticationRequired(QNetworkReply* reply, QAuthenticator* authenticator)
{
	QMutexLocker lock( &m_mutex );
	if( m_replies.contains( reply ) == false ) return;
	lock.unlock();

	emit SignalAuthenticationRequired( authenticator );
}

void ApiBase::SendAll()
{
	QMutexLocker lock( &m_mutex );
	foreach( QString request, m_requests )
	{
		Add( SendInternal( request ) );
	}
	m_requests.clear();
}

void ApiBase::Add(QNetworkReply* reply)
{
	reply->setParent( NULL );

	connect(reply,SIGNAL(sslErrors(QList<QSslError>)),this,SLOT(SlotSslErrors(QList<QSslError>)));
	connect(reply,SIGNAL(downloadProgress(qint64,qint64)),this,SLOT(SlotDownloadProgress(qint64,qint64)));
	connect(reply,SIGNAL(finished()),this,SLOT(SlotFinished()));

	QMutexLocker lock( &m_mutex );
	m_replies.insert( reply );
}

void ApiBase::Remove(QNetworkReply* reply)
{
	QMutexLocker lock( &m_mutex );
	m_replies.remove( reply );
}

QString ApiBase::Tabulated(QStringList strings)
{
	if( strings.isEmpty() ) return QString();

	QString result;
	foreach( QString string, strings )
	{
		result.append( string );
		result.append( "\t" );
	}
	result = result.left( result.length() - 1 );
	return result;
}

QString ApiBase::UrlString(QNetworkReply *reply)
{
	if( reply->error() == API_NO_ERROR )
	{
		return Document::FormatBaseUrl( reply->url().toString() );
	}
	else
	{
		return reply->errorString();
	}
}

QString ApiBase::TimeString()
{
	return QTime::currentTime().toString();
}

QString ApiBase::DataString(QByteArray data)
{
	int limit = 500;
	QString result = data.left( limit );
	if( data.count() > limit ) result += "...";
	return result;
}

QString ApiBase::DebugString(QNetworkReply* reply)
{
	if( reply->error() == API_NO_ERROR ) return QString();

	QStringList parts;
	parts.append( TimeString() );
	parts.append( reply->errorString() );
	return Tabulated( parts );
}

QString ApiBase::NetworkLogString(QNetworkReply *reply, QByteArray data)
{
	QStringList parts;
	parts.append( TimeString() );
	parts.append( UrlString( reply ) );
	parts.append( DataString( data ) );
	return Tabulated( parts );
}

bool ApiBase::NeedTimeOut() const
{
	return m_errorsCount > ERRORS_LIMIT;
}

void ApiBase::CountErrors(ErrorCode error)
{
	QMutexLocker lock( &m_mutex );

	if( error == API_NO_ERROR )
	{
		m_errorsCount = 0;
		return;
	}

	m_errorsCount += 1;
}

QUrl ApiBase::RemoveQuery(QUrl url, QString key)
{
#if QT_VERSION >= 0x050000
	QUrlQuery query = QUrlQuery( url );
	if( query.hasQueryItem( key ) ) query.removeQueryItem( key );

	QUrl result = QUrl( url );
	result.setQuery( query );
#else
	QUrl result( url );
	if( result.hasQueryItem( key ) ) result.removeQueryItem( key );
#endif
	return result;
}

QString ApiBase::ConfigPath()
{
	QString path = QCoreApplication::applicationDirPath();
	QString result = QDir( path ).filePath( "requests.ini" );
	return result;
}

QSettings* ApiBase::Config()
{
	if( g_config == NULL ) g_config = new QSettings( ConfigPath(), QSettings::IniFormat );
	return g_config;
}

#if QT_VERSION >= 0x050000
QUrlQuery ApiBase::GetParameters(void )
{
	QMutexLocker lock( &m_mutex );

	QUrlQuery result;
	foreach( GetParameter* parameter, m_getParameters )
	{
		QString key = parameter->Key();
		QString value = parameter->Value();

		DebugAssert( key.isEmpty() == false );
		DebugAssert( value.isEmpty() == false );
		if( key.isEmpty() || value.isEmpty() ) continue;

		result.addQueryItem( key, value );
	}
	return result;
}
#else
QList<QPair<QString, QString> > ApiBase::GetParameters(void)
{
	QMutexLocker lock( &m_mutex );

	QList<QPair<QString, QString> > result;
	foreach( GetParameter* parameter, m_getParameters )
	{
		QString key = parameter->Key();
		QString value = parameter->Value();

		DebugAssert( key.isEmpty() == false );
		DebugAssert( value.isEmpty() == false );
		if( key.isEmpty() || value.isEmpty() ) continue;

		result.append( qMakePair<QString, QString>( key, value ) );
	}
	return result;
}
#endif
