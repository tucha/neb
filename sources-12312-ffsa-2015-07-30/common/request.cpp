#include "common.h"

#include <QNetworkAccessManager>
#include <QMutexLocker>

Request::Request(QObject* parent, QNetworkAccessManager* manager, Identifier identifier, Document document, RequestPriority priority):
	RequestBase( parent, manager, identifier, document.BaseUrl(), priority ),
	m_document( document )
{
}

Request::~Request()
{

}

Request*Request::New(Document document, RequestPriority priority)
{
	return new Request( parent(), Manager(), GetIdentifier(), document, priority );
}

Request*Request::CreateCopy(RequestPriority priority)
{
	return New( GetDocument(), priority );
}

RequestFileExtensions*Request::NewFileExtensions(RequestPriority priority)
{
	return new RequestFileExtensions( parent(), Manager(), GetIdentifier(), GetDocument(), priority );
}

RequestFile*Request::NewFile(QString fileExtension, RequestPriority priority)
{
	return new RequestFile( parent(), Manager(), GetIdentifier(), GetDocument(), priority, fileExtension );
}

RequestFileSize* Request::NewFileSize(QString fileExtension, RequestPriority priority)
{
	return new RequestFileSize( parent(), Manager(), GetIdentifier(), GetDocument(), priority, fileExtension );
}

RequestDescription*Request::NewDescription(QString field, QString subField, RequestPriority priority)
{
	return new RequestDescription( parent(), Manager(), GetIdentifier(), GetDocument(), priority, field, subField );
}

RequestInformation*Request::NewInformation(RequestPriority priority)
{
	return new RequestInformation( parent(), Manager(), GetIdentifier(), GetDocument(), priority );
}

RequestDocumentType*Request::NewDocumentType(RequestPriority priority)
{
	return new RequestDocumentType( parent(), Manager(), GetIdentifier(), GetDocument(), priority );
}

RequestCollection*Request::NewCollection(RequestPriority priority)
{
    return new RequestCollection( parent(), Manager(), GetIdentifier(), GetDocument(), priority );
}

RequestDocumentFormat *Request::NewDocumentFormat(RequestPriority priority)
{
    return new RequestDocumentFormat( parent(), Manager(), GetIdentifier(), GetDocument(), priority );
}

RequestContent *Request::NewContent(RequestPriority priority)
{
    return new RequestContent( parent(), Manager(), GetIdentifier(), GetDocument(), priority );
}

RequestDocumentSearch *Request::NewSearch(QStringList phraseList, RequestPriority priority)
{
    return new RequestDocumentSearch( parent(), Manager(), GetIdentifier(), GetDocument(), priority,  phraseList);
}

RequestDownloadEpub *Request::NewDownloadEpub(RequestPriority priority)
{
    return new RequestDownloadEpub( parent(), Manager(), GetIdentifier(), GetDocument(), priority );
}


RequestPagesCount*Request::NewPagesCount(RequestPriority priority)
{
	return new RequestPagesCount( parent(), Manager(), GetIdentifier(), GetDocument(), priority );
}

RequestPageSet*Request::NewPageSet(PageSet pageSet, RequestPriority priority)
{
	return new RequestPageSet( parent(), Manager(), GetIdentifier(), GetDocument(), priority, pageSet );
}

RequestPage*Request::NewPage(int pageIndex, RequestPriority priority)
{
	return new RequestPage( parent(), Manager(), GetIdentifier(), GetDocument(), priority, pageIndex );
}

RequestWordPages*Request::NewWordPages(QStringList words, RequestPriority priority)
{
	return new RequestWordPages( parent(), Manager(), GetIdentifier(), GetDocument(), priority, words );
}

QString Request::WordsString(QStringList words)
{
	DebugAssert( words.isEmpty() == false );

	QString result;
	const QString splitter = "|";
	foreach( QString word, words )
	{
		if( word.isEmpty() ) continue;
		result.append( word );
		result.append( splitter );
	}
	if( result.isEmpty() == false ) result = result.left( result.length() - splitter.length() );
	return result;
}

Document Request::GetDocument() const
{
	return m_document;
}

QNetworkReply* Request::SendInternal(QString requestString)
{
	QString request = QString( "%1/%2" )
					  .arg( GetDocument().DocumentId() )
					  .arg( requestString );

	return RequestBase::SendInternal( request );
}
