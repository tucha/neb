#include "addtrustedhostrequest.h"
#include <QJsonDocument>
#include <QJsonObject>
#include "user.h"
#include "platform_id/platform_id.h"
#include <QHostInfo>
AddTrustedHostRequest::AddTrustedHostRequest(QObject* parent, QNetworkAccessManager* manager, QString baseUrl, User user, RequestPriority priority):
	ApiBase( parent, manager, baseUrl, priority )
{
    AddGetParameter("token", user.GetToken());
    AddGetParameter("fingerprint", PlatformID::hash());
    AddGetParameter("os", REQUEST_OS_ID);
    AddGetParameter("action", "insert");
    AddGetParameter("host", QHostInfo::localHostName());


	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));
}

void AddTrustedHostRequest::Send()
{
    ApiBase::Send( QString( "%1/%2/%3/" )
                      .arg( BaseUrl() )
                      .arg( "rest_api" )
                      .arg( "hosts" )
                       );
}

void AddTrustedHostRequest::SlotFinished(ErrorCode error, QByteArray data)
{
    Q_UNUSED(data)
	if( error != API_NO_ERROR )
	{
        emit SignalFinished( error, false );
		return;
	}

    emit SignalFinished( error, false );
}

QNetworkReply*AddTrustedHostRequest::SendInternal(QString requestString)
{
	QUrl url = QUrl( requestString );
    url.setQuery( GetParameters() );
	QNetworkRequest request;
	request.setUrl( url );
	request.setPriority( Priority() );
	return Manager()->get( request );
}
