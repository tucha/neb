#include "requestpageset.h"

#include "defines.h"

RequestPageSet::RequestPageSet(QObject* parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority,
							   PageSet pageSet):
	Request( parent, manager, identifier, document, priority ),
	m_pageSet( pageSet )
{
}

void RequestPageSet::Send()
{
	QString requestString = QString( "%1/%2/%3" )
							.arg( "document" )
							.arg( "pageSet" )
							.arg( GetPageSet().ToString() );

	ApiBase::Send( requestString );
}

PageSet RequestPageSet::GetPageSet() const
{
	return m_pageSet;
}
