#ifndef REQUESTDOCUMENTTYPE_H
#define REQUESTDOCUMENTTYPE_H

#include "request.h"

class RequestDocumentType : public Request
{
	Q_OBJECT
public:
	RequestDocumentType( QObject *parent, QNetworkAccessManager* manager,  Identifier identifier, Document document, RequestPriority priority );
	void Send(void);

signals:
	void SignalFinished( ErrorCode error, QString documentType );

private slots:
	void SlotFinished( ErrorCode error, QByteArray data );

private:
};


#endif // REQUESTDOCUMENTTYPE_H
