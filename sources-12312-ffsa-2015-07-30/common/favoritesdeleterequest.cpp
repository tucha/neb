#include "favoritesdeleterequest.h"
#include "platform_id/platform_id.h"
#include "session.h"

FavoritesDeleteRequest::FavoritesDeleteRequest(QObject* parent, QNetworkAccessManager* manager, QString baseUrl,
											   FavoriteBook favoriteBook, RequestPriority priority):
	AccountBaseRequest( parent, manager, baseUrl, favoriteBook.GetUser(), priority ),
	m_favoriteBook( favoriteBook )
{
	connect(this,SIGNAL(SignalFinished(ErrorCode,QByteArray)),this,SLOT(SlotFinished(ErrorCode,QByteArray)));

	AddGetParameter( "ID", GetFavoriteBook().Id() );
	AddGetParameter( "token", UserToken() );
    AddGetParameter(  "fingerprint", PlatformID::hash());
    AddGetParameter(  "os", REQUEST_OS_ID);
    if(GetUser().HasSession())AddGetParameter("session", GetUser().GetSession()->GetID());

}

FavoriteBook FavoritesDeleteRequest::GetFavoriteBook() const
{
	return m_favoriteBook;
}

void FavoritesDeleteRequest::Send()
{
	AccountBaseRequest::Send( "favorites" );
}

void FavoritesDeleteRequest::SlotFinished(ErrorCode error, QByteArray data)
{
#ifdef EMULATE_ACCOUNT_REQUESTS
	emit SignalFinished( API_NO_ERROR );
	return;
#endif
	UNUSED( data );
	emit SignalFinished( error );
}

QNetworkReply*FavoritesDeleteRequest::SendInternal(QString requestString)
{
	QUrl url( requestString );
#if QT_VERSION >= 0x050000
	url.setQuery( GetParameters() );
#else
	QList<QPair<QString,QString> > prms = GetParameters();
	for( QList<QPair<QString,QString> >::iterator iter = prms.begin(); iter != prms.end(); ++iter )
	{
		url.addQueryItem( iter->first, iter->second );
	}
#endif

	QNetworkRequest request;
	request.setUrl( url );
	request.setPriority( Priority() );

	return Manager()->deleteResource( request );
}
