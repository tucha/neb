#include "processmanager2.h"

#include <QByteArray>
#include <QCoreApplication>

#include <debug.h>

#include "defines.h"

#define SIZE_LIMIT 1000
#define TIMER_INTERVAL 30
#define GUID "{5B8FC4A1-4E94-4DD5-BCC5-1E535283398C}"

ProcessManager2::ProcessManager2(QObject *parent, QString key):
	QObject(parent),
	m_memory( key + GUID ),
	m_timer()
{
	while( true )
	{
		if( m_memory.create( SIZE_LIMIT ) )
		{
			m_isPrimaryProcess = true;
			break;
		}
		if( m_memory.attach() )
		{
			m_isPrimaryProcess = false;
			break;
		}
	}
}

bool ProcessManager2::IsPrimaryProcess() const
{
	return m_isPrimaryProcess;
}

void ProcessManager2::StartListening()
{
	DebugAssert( m_timer.isActive() == false );
	if( m_timer.isActive() ) return;

	connect(&m_timer,SIGNAL(timeout()),this,SLOT(SlotListen()));
	m_timer.start( TIMER_INTERVAL );
}

bool ProcessManager2::CloseOtherProcesses(void)
{
	return WriteMemory( ProcessId() );
}

void ProcessManager2::SlotListen()
{
	QString data = ReadMemory();

	DebugAssert( data.isEmpty() == false );
	if( data.isEmpty() ) return;

	if( ProcessId() != data ) emit SignalCloseProcess();
}

QString ProcessManager2::ProcessId() const
{
	return QString::number( QCoreApplication::applicationPid() );
}

QString ProcessManager2::ReadMemory()
{
	// Check memory
	DebugAssert( m_memory.isAttached() );
	if( m_memory.isAttached() == false ) return QString();

	// Lock memory
	bool locked = m_memory.lock();
	DebugAssert( locked );
	if( locked == false ) { DebugOutput( m_memory.errorString() ); return QString(); }

	// Read memory
	QByteArray data = QByteArray( static_cast<char*>( m_memory.data() ), m_memory.size() );
	QString result = QString::fromUtf8( data );

	// Unlock memory
	bool unlocked = m_memory.unlock();
	DebugAssert( unlocked );
	if( unlocked == false ) { DebugOutput( m_memory.errorString() ); return QString(); }

	// Return result
	return result;
}

bool ProcessManager2::WriteMemory(QString text)
{
	// Check memory
	DebugAssert( m_memory.isAttached() );
	if( m_memory.isAttached() == false ) return false;

	// Check data
	QByteArray data = text.toUtf8();
	DebugAssert( data.size() <= m_memory.size() );
	if( data.size() > m_memory.size() ) return false;

	// Lock memory
	bool locked = m_memory.lock();
	DebugAssert( locked );
	if( locked == false ) return false;

	// Write memory
    #ifdef OS_WINDOWS_
	errno_t status = memcpy_s( m_memory.data(), m_memory.size(), data.data(), data.size() );
	DebugAssert( status == 0 ); UNUSED( status );
	#else // OS_WINDOWS
	memcpy( m_memory.data(), data.data(), data.size() );
	#endif // OS_WINDOWS

	// Unlock memory
	bool unlocked = m_memory.unlock();
	DebugAssert( unlocked );
	if( unlocked == false ) return false;

	return true;
}
