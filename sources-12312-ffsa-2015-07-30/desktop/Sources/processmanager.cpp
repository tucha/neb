#include "processmanager.h"

#include <QByteArray>
#include <QThread>

#include <debug.h>

#include "defines.h"

#define SIZE_LIMIT 1000
#define TIMER_INTERVAL 30

ProcessManager::ProcessManager(QObject *parent, QString key):
	QObject(parent),
	m_memory( key ),
	m_timer()
{
	while( true )
	{
		if( m_memory.create( SIZE_LIMIT ) )
		{
			m_isPrimaryProcess = true;
			break;
		}
		if( m_memory.attach() )
		{
			m_isPrimaryProcess = false;
			break;
		}
	}
}

bool ProcessManager::IsPrimaryProcess() const
{
	return m_isPrimaryProcess;
}

bool ProcessManager::IsSecondaryProcess() const
{
	return IsPrimaryProcess() == false;
}

void ProcessManager::StartListening()
{
	DebugAssert( m_timer.isActive() == false );
	if( m_timer.isActive() ) return;

	connect(&m_timer,SIGNAL(timeout()),this,SLOT(SlotListen()));
	m_timer.start( TIMER_INTERVAL );
}

bool ProcessManager::SendParameters(Parameters parameters)
{
	QString command = parameters.ToJSon();
	return WriteMemory( command );
}

void ProcessManager::SlotListen()
{
	QString data = ReadMemoryAndErase();
	if( data.isEmpty() ) return;

	emit SignalReceiveParameters( Parameters::FromJson( data ) );
}

QString ProcessManager::ReadMemoryAndErase()
{
	// Check memory
	DebugAssert( m_memory.isAttached() );
	if( m_memory.isAttached() == false ) return QString();

	// Lock memory
	bool locked = m_memory.lock();
	DebugAssert( locked );
	if( locked == false ) { DebugOutput( m_memory.errorString() ); return QString(); }

	// Read memory
	QByteArray data = QByteArray( static_cast<char*>( m_memory.data() ), m_memory.size() );
	QString result = QString::fromUtf8( data );

	// Erase memory
	memset( m_memory.data(), 0, m_memory.size() );

	// Unlock memory
	bool unlocked = m_memory.unlock();
	DebugAssert( unlocked );
	if( unlocked == false ) { DebugOutput( m_memory.errorString() ); return QString(); }

	// Return result
	return result;
}

bool ProcessManager::WriteMemory(QString text)
{
	// Check memory
	DebugAssert( m_memory.isAttached() );
	if( m_memory.isAttached() == false ) return false;

	// Check data
	QByteArray data = text.toUtf8();
	DebugAssert( data.size() <= m_memory.size() );
	if( data.size() > m_memory.size() ) return false;

	bool finished = false;
	while( finished == false )
	{
		// Lock memory
		bool locked = m_memory.lock();
		DebugAssert( locked );
		if( locked == false ) return false;

		// Write memory if it is empty
		if( QString( QByteArray( static_cast<char*>( m_memory.data() ), m_memory.size() ) ).isEmpty() )
		{
            #ifdef OS_WINDOWS_
			errno_t status = memcpy_s( m_memory.data(), m_memory.size(), data.data(), data.size() );
			DebugAssert( status == 0 ); UNUSED( status );
			#else // OS_WINDOWS
			memcpy( m_memory.data(), data.data(), data.size() );
			#endif // OS_WINDOWS
			finished = true;
		}

		// Unlock memory
		bool unlocked = m_memory.unlock();
		DebugAssert( unlocked );
		if( unlocked == false ) return false;

		// Take a rest
		if( finished == false )
		{
#if QT_VERSION >= 0x050000
			QThread::sleep( TIMER_INTERVAL );
#else
			QThread::currentThread()->wait( TIMER_INTERVAL );
#endif
		}
	}

	DebugAssert( finished );
	return true;
}
