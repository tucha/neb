#include <QApplication>
#include <QTranslator>
#include <QNetworkProxyFactory>
#include <QDateTime>
#include <QMessageBox>
#include <QIcon>
#include <QProcess>
#include <QFile>
#include "logic.h"
#include "miscellaneous/parameters.h"
#include "processmanager.h"
#include "processmanager2.h"

// Protection stuff
#ifdef OS_WINDOWS
#include "protection/suspendprotection.h"
#endif

#ifdef OS_MACOSX
#include "miscellaneous/macx/maceventshandler.h"
#include "protection/pcemulatordetection.h"
#endif

#ifdef OS_LINUX
#include "protection/linux/linuxwindowcaptureprotection.h"
#endif

//#define LINUX_CAPTURE_PROTECTION_VERBOSE

#ifndef EXAMPLE
QString ProcessKey( QString applicationFilePath )
{
#ifdef OS_WINDOWS
	return applicationFilePath.toLower();
#endif
	return applicationFilePath;
}


void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QString t = "%1 %2: %3 (%4:%5, %6)\n";
    auto dt = QDateTime::currentDateTime().toString("yyyy/MM/dd, hh:mm:ss:zzz");
    switch (type) {
    case QtDebugMsg:
        t = t.arg(dt).arg("Debug").arg(msg).arg(context.file).arg(context.line).arg(context.function);
        break;
    case QtInfoMsg:
        t = t.arg(dt).arg("Info").arg(msg).arg(context.file).arg(context.line).arg(context.function);
        break;
    case QtWarningMsg:
        t = t.arg(dt).arg("Warning").arg(msg).arg(context.file).arg(context.line).arg(context.function);
        break;
    case QtCriticalMsg:
        t = t.arg(dt).arg("Critical").arg(msg).arg(context.file).arg(context.line).arg(context.function);
        break;
    case QtFatalMsg:
        t = t.arg(dt).arg("Fatal").arg(msg).arg(context.file).arg(context.line).arg(context.function);
    }
#ifdef BUILD_DEBUG
    QFile logFile("debug.txt");
    if(logFile.open(QIODevice::WriteOnly|QIODevice::Append))
        logFile.write(t.toUtf8());
    else
        fprintf(stderr,"%s",  "Cannot open debug file");
#endif
     fprintf(stderr,"%s",  t.toLocal8Bit().constData());
}




int main(int argc, char *argv[])
{
    qputenv("QT_LOGGING_RULES", "qt.network.ssl.warning=false");

    QFile logFile("debug.txt");
    if(logFile.exists())
        logFile.remove();
    qInstallMessageHandler(myMessageOutput);

	QApplication a(argc, argv);


	QStringList arguments = a.arguments();
	if( arguments.count() == argc ) arguments.removeFirst();
	Parameters p( arguments );

#ifdef OS_LINUX
	if( p.ContainsPrintScreenOption() )
	{
		LinuxWindowCaptureProtection wcp;
#ifdef LINUX_CAPTURE_PROTECTION_VERBOSE
		foreach( QString s, wcp.Log() ) qWarning() << s;
#endif
		if( wcp.IsValid() == false )
		{
#ifdef LINUX_CAPTURE_PROTECTION_VERBOSE
			qWarning() << "ERROR: PrintScreen option blocking FAILED";
#endif
			return 1;
		}

		// Just do nothing, dry run
		return a.exec();
	}
#endif

	QTranslator t;
	t.load( ":/translations/ru" );
	a.installTranslator( &t );

#ifdef OS_MACOSX
#ifdef PCEMULATORDETECTION
	// Jira CR ELARII-14
	if( QSysInfo::MacintoshVersion <= QSysInfo::MV_10_8 )
	{
		PCEmulatorDetection ped;
		if( ped.VMWareDetected() || ped.VirtualBoxDetected() )
		{
			QMessageBox::warning( NULL, "INFO",
								  QObject::tr( "An attempt to use a virtual PC has been detected. Please close all PC emulators and try again." ) );
			return 1;
		}
	}
#endif
#endif

#if !defined(OS_WINDOWS) || QT_VERSION < 0x050000
    a.setWindowIcon( QIcon( ":/icons/logo" ) );
#endif

#ifdef TIME_LIMITED_MODE
	if( QDateTime::currentDateTime() >= QDateTime::fromString( TIME_LIMIT, TIME_LIMIT_FORMAT ) )
	{
		QMessageBox::warning( NULL, QObject::tr( "TimeLimitWarningTitle" ), QObject::tr( "TimeLimitWarningLabel" ) );
		return 0;
	}
#endif

#ifdef OS_WINDOWS
	SuspendProtection sp;
#endif

#ifdef OS_WINDOWS
	if( p.ContainsProcessId() ) return sp.Run( p.ProcessId() );
#endif
	if( p.ContainsBrokenDocument() ) return 0;

	ProcessManager m( NULL, ProcessKey( a.applicationFilePath() ) );
	if( m.IsSecondaryProcess() && p.ContainsNoToken() )
	{
        m.SendParameters( p );
        return 0;
	}

#ifdef OS_LINUX	
	QString filePath = QCoreApplication::applicationFilePath();
	QString argument = Parameters::CreatePrintScreenParameter();
	QStringList anotherArguments = QStringList() << argument;

	QScopedPointer<QProcess> anotherInstance( new QProcess() );
	if( anotherInstance.data() == NULL ) return 1;

	anotherInstance->setProgram( filePath );
	anotherInstance->setArguments( anotherArguments );
	anotherInstance->start();
	anotherInstance->waitForStarted();
	if( anotherInstance->state() != QProcess::Running ) return 1;
#endif

	Logic l( NULL, &a );

#ifdef OS_MACOSX
	EventFilterInstaller efi( &a, &l );
	UNUSED( efi );

	// NOTE: here we should alreasy get FileOpenEvent, if it occurs
	a.processEvents();

	ViewerDryRunEvent* runEv = new ViewerDryRunEvent( p );
	a.postEvent( &l, runEv, Qt::LowEventPriority );
#endif

	ProcessManager2 m2( NULL, ProcessKey( a.applicationFilePath() ) );
	m2.CloseOtherProcesses();

	QObject::connect(  &m,  SIGNAL(SignalReceiveParameters( Parameters ) ), &l, SLOT( SlotReceiveParameters( Parameters ) ) );
	m.StartListening();

	QObject::connect(&m2,SIGNAL(SignalCloseProcess()),&l,SLOT(Stop()));
	m2.StartListening();

#ifdef OS_WINDOWS
	QObject::connect(&sp,SIGNAL(AnotherInstanceSuspended()),&l,SLOT(Stop()));
	if( sp.StartAnotherInstance() == false ) return 0;
#endif

#ifndef OS_MACOSX
	if( l.Start( p ) == false ) return 0;
#endif

	int status = a.exec();
#ifdef OS_LINUX
	anotherInstance->kill();
	anotherInstance->waitForFinished();
#endif
	return status;
}
#endif

#ifdef EXAMPLE
#include <example.h>
int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	QNetworkProxyFactory::setUseSystemConfiguration( true );
	Example e;
	e.show();
	return a.exec();
}
#endif
