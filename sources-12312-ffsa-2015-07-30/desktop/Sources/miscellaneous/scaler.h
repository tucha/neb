#ifndef SCALER_H
#define SCALER_H

#include <QSize>
#include <QPixmap>

#include <defines.h>

class Scaler : public QObject
{
	Q_OBJECT

public:
	explicit Scaler( QObject* parent = NULL );

	void SetDisplaySize( QSize displaySize );
	void SetPageSize( QSize pageSize );
	void SetExplicitScale( int scale );
	void SetScaleWidth(bool value);
	void SetScaleFull(bool value);
	void Rotate( int angle );
	void ResetRotation(void);

	QPixmap Transform( QPixmap pixmap )const;
	int Restriction(void)const;

	int ExplicitScale(void)const;
	bool ScaleWidth(void)const;
	bool ScaleFull(void)const;
	int Angle(void)const;

signals:
	void ImplicitScaleChanged( int value );

private:
	int ImplicitScale(void)const;

	QPixmap TransformScaled( QPixmap pixmap )const;
	int RestrictionScaled(void)const;

	QPixmap TransformFull( QPixmap pixmap )const;
	int RestrictionFull(void)const;
	qreal ImplicitScaleFull(void)const;

	QPixmap TransformWidth( QPixmap pixmap )const;
	int RestrictionWidth(void)const;
	qreal ImplicitScaleWidth(void)const;

	bool IsRotated(void)const;
	bool IsValid(void)const;

	QSize m_pageSize;
	QSize m_displaySize;

	int m_scale;
	bool m_scaleWidth;
	bool m_scaleFull;
	int m_angle;
};

#endif // SCALER_H
