#include "parameters.h"

#include <QFile>
#include <QVariantList>
#include <QMessageBox>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>

#define PROTOCOL_START	"spd:"
#define FILE_EXTENSION	".spd"
#define PROCESS_ID		"PROCESS_ID:"
#define PRINT_SCREEN_OPT "PRINT_SCREEN"

#include <debug.h>

Parameters::Parameters():
	m_parameters(),
	m_documents(),
	m_containsProcessId( false ),
	m_processId( 0 ),
	m_brokenDocuments(),
	m_containsPrintScreenOption( false )
{
}

Parameters::Parameters(QStringList parameters):
	m_parameters( parameters ),
	m_documents(),
	m_containsProcessId( false ),
	m_processId( 0 ),
	m_brokenDocuments(),
	m_containsPrintScreenOption( false )
{
	ParseParameters();
}

DocumentList Parameters::Documents() const
{
	return m_documents;
}

QString Parameters::ToJSon() const
{
	QJsonDocument document;
	document.setArray( QJsonArray::fromStringList( m_parameters ) );
	return document.toJson( QJsonDocument::Compact );
}

Parameters Parameters::FromJson(QString json)
{
	QJsonDocument document = QJsonDocument::fromJson( json.toUtf8() );

	DebugAssert( document.isArray() );
	QJsonArray array = document.array();

	QStringList parameters;
	for( int i = 0; i < array.count(); ++i )
	{
		DebugAssert( array.at( i ).isString() );
		parameters.append( array.at( i ).toString() );
	}

	return Parameters( parameters );
}

bool Parameters::ContainsToken() const
{
	if( Documents().isEmpty() ) return false;
	return Documents().last().ContainsToken();
}

bool Parameters::ContainsNoToken() const
{
	return ContainsToken() == false;
}

QString Parameters::Token() const
{
	DebugAssert( ContainsToken() );
	if( ContainsToken() == false ) return QString();
	return Documents().last().Token();
}

bool Parameters::ContainsProcessId() const
{
	return m_containsProcessId;
}

qint64 Parameters::ProcessId() const
{
	DebugAssert( ContainsProcessId() );
	if( ContainsProcessId() == false ) return 0;

	return m_processId;
}

bool Parameters::ContainsBrokenDocument() const
{
	return m_brokenDocuments.isEmpty() == false;
}

bool Parameters::ContainsPrintScreenOption() const
{
	return m_containsPrintScreenOption;
}

QString Parameters::CreateProcessIdParameter(qint64 processId)
{
	QString result = PROCESS_ID;
	result.append( QString::number( processId ) );
	return result;
}

QString Parameters::CreatePrintScreenParameter()
{
	return PRINT_SCREEN_OPT;
}

void Parameters::ParseParameters()
{
	foreach( QString parameter, m_parameters )
	{
		if( ParseProtocol( parameter ) ) continue;
		if( ParseFile( parameter ) ) continue;
		if( ParseProcessId( parameter ) ) continue;
		if( ParsePrintScreenOption( parameter ) ) continue;
	}
}

bool Parameters::ParseProtocol(QString parameter)
{
	if( parameter.startsWith( PROTOCOL_START ) == false ) return false;

	QString documentString = parameter.right( parameter.length() - QString( PROTOCOL_START ).length() );
	if( IsCorrect( documentString ) == false ) return false;

	m_documents.append( Document( documentString ) );
	return true;
}

bool Parameters::ParseFile(QString parameter)
{
	if( parameter.endsWith( FILE_EXTENSION ) == false ) return false;

	QFile file( parameter );
	if( file.exists() == false ) return false;
	if( file.open( QIODevice::ReadOnly ) == false ) return false;

	QString documentString = file.readAll();
	if( IsCorrect( documentString ) == false ) return false;

	m_documents.append( Document( documentString ) );
	return true;
}

bool Parameters::IsCorrect(QString documentString)
{
	bool result = Document::CheckString( documentString );

	if( result == false )
	{
		m_brokenDocuments.append( documentString );
		Debug::Instance()->Output( QObject::tr( "Wrong parameter %1" ).arg( documentString ) );
		QMessageBox::information(
					NULL,
					QObject::tr( "ErrorTitle" ),
					QObject::tr( "Wrong parameter %1" ).arg( documentString ) );
	}

	return result;
}

bool Parameters::ParseProcessId( QString parameter )
{
	QString start = PROCESS_ID;
	if( parameter.startsWith( start ) == false ) return false;
	QString processIdString = parameter.mid( start.length() );

	DebugAssert( processIdString.isEmpty() == false );
	if( processIdString.isEmpty() ) return false;

	bool ok;
	qint64 processId = processIdString.toLongLong( &ok );

	DebugAssert( ok );
	if( ok == false ) return false;

	DebugAssert( m_containsProcessId == false );
	if( m_containsProcessId ) return false;

	m_containsProcessId = true;
	m_processId = processId;
	return true;
}

bool Parameters::ParsePrintScreenOption( QString parameter )
{
	if( parameter.compare( PRINT_SCREEN_OPT, Qt::CaseInsensitive ) != 0 ) return false;

	m_containsPrintScreenOption = true;
	return true;
}

