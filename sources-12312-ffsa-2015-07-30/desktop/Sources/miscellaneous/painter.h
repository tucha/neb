#ifndef PAINTER_H
#define PAINTER_H

#include <QPainter>
#include <QImage>

#include <common.h>

class Painter
{
public:
	static QImage Highlight(QImage image, WordCoordinates coordinates);
	static QImage Invert(QImage image);

private:
	static QRectF Place(QSize size, QRectF percents);
};

#endif // PAINTER_H
