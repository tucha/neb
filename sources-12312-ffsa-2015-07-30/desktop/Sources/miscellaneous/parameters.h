#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <QStringList>
#include <QVariantMap>

#include <document.h>

class Parameters
{
public:
	Parameters();
	Parameters( QStringList parameters );

	DocumentList Documents(void)const;

	QString ToJSon(void)const;
	static Parameters FromJson( QString json );

	bool ContainsToken(void)const;
	bool ContainsNoToken(void)const;
	QString Token(void)const;

	bool ContainsProcessId(void)const;
	qint64 ProcessId(void)const;

	bool ContainsBrokenDocument(void)const;

	bool ContainsPrintScreenOption(void)const;

	static QString CreateProcessIdParameter( qint64 processId );
	static QString CreatePrintScreenParameter();

private:
	void ParseParameters(void);
	bool ParseProtocol( QString parameter );
	bool ParseFile( QString parameter );
	bool IsCorrect(QString documentString );
	bool ParseProcessId(QString parameter);
	bool ParsePrintScreenOption( QString parameter );

	QStringList m_parameters;
	DocumentList m_documents;
	bool m_containsProcessId;
	qint64 m_processId;
	QStringList m_brokenDocuments;
	bool m_containsPrintScreenOption;
};

#endif // PARAMETERS_H
