#include "config.h"

#include <QApplication>
#include <QSettings>

#include <user.h>

#define DEFAULT_CLIENT_ID	"99Apaf8YwtAskq5sdfyNMnbD3SDU7qUH"

#define KEY_CLIENT_ID		"ClientId"
#define KEY_DOCUMENT		"Document"
#define KEY_HISTORY			"History"
#define KEY_STATES			"States"
#define KEY_STATE			"State"
#define KEY_WINDOW_STATE	"WindowState"

#ifndef OS_LINUX
#define ORGANIZATION	"Elar"
#define APPLICATION		"Viewer"
#else
// NOTE: Linux has case-sensitive file system; try to avoid the future headache using all lower letters
// NOTE: Linux third-party application usually live together in the /usr/bin directory;
//       so, the application name must be unique
#define ORGANIZATION    "elar"
#define APPLICATION     "nelrfviewer"
#endif

#define KEY_EMAIL		"Email"

Config::Config( QObject *parent ) :
	QObject( parent ),
	m_settings( NULL ),
	m_clientId(),
	m_history(),
	m_states(),
	m_isLoaded( false ),
	m_email()
{
	m_settings = new QSettings( ORGANIZATION, APPLICATION );
	m_email = m_settings->value( KEY_EMAIL ).toString();
}

Config::~Config()
{
	if( IsLoaded() ) Save();
	delete m_settings;
}

QString Config::ClientId() const
{
	DebugAssert( IsLoaded() );
	if( IsLoaded() == false ) return QString();

	return m_clientId;
}

Document Config::LastDocument() const
{
	DebugAssert( IsLoaded() );
	if( IsLoaded() == false ) return Document();

	if( m_history.isEmpty() ) return Document();

	return m_history.last();
}

void Config::AddToHistory(Document document)
{
	DebugAssert( IsLoaded() );
	if( IsLoaded() == false ) return;

	DebugAssert( document.IsEmpty() == false );
	if( document.IsEmpty() ) return;

	DocumentList newHistory;
	foreach( Document history, m_history )
	{
		if( history != document ) newHistory.append( history );
	}
	newHistory.append( document );
	m_history = newHistory;

	emit WasChanged();
}

void Config::RemoveFromHistory(Document document)
{
	DebugAssert( IsLoaded() );
	if( IsLoaded() == false ) return;

	DebugAssert( document.IsEmpty() == false );
	if( document.IsEmpty() ) return;

	DocumentList newHistory;
	foreach( Document history, m_history )
	{
		if( history != document ) newHistory.append( history );
	}
	m_history = newHistory;

	emit WasChanged();
}

DocumentList Config::GetHistory() const
{
	DebugAssert( IsLoaded() );
	if( IsLoaded() == false ) return DocumentList();

	return m_history;
}

void Config::SetState(Document document, DocumentState state)
{
	DebugAssert( IsLoaded() );
	if( IsLoaded() == false ) return;

	m_states.insert( document, state );

	emit WasChanged();
}

DocumentState Config::GetState(Document document) const
{
	DebugAssert( IsLoaded() );
	if( IsLoaded() == false ) return DocumentState();

	return m_states.value( document, DocumentState() );
}

void Config::Load( const User& user )
{
	DebugAssert( IsLoaded() == false );
	if( IsLoaded() ) return;

	SetLastEmail( user.GetEmail() );
	m_settings->setValue( KEY_EMAIL, LastEmail() );
	m_settings->beginGroup( QString::number( user.GetId() ) );

	m_clientId = m_settings->value( KEY_CLIENT_ID, DEFAULT_CLIENT_ID ).toString();

	LoadHistory();
	LoadStates();

	m_isLoaded = true;
}

void Config::Save()
{
	DebugAssert( IsLoaded() );
	if( IsLoaded() == false ) return;

//	m_settings->setValue( KEY_CLIENT_ID, ClientId() );

	SaveHistory();
	SaveStates();
}

bool Config::IsLoaded() const
{
	return m_isLoaded;
}

QString Config::LastEmail() const
{
	return m_email;
}

void Config::SetLastEmail(QString value)
{
	m_email = value;
}

QByteArray Config::GetWindowState() const
{
	DebugAssert( IsLoaded() );
	if( IsLoaded() == false ) return QByteArray();

	return QByteArray::fromBase64( m_settings->value( KEY_WINDOW_STATE, QString() ).toByteArray() );
}

void Config::SetWindowState(QByteArray value)
{
	DebugAssert( IsLoaded() );
	if( IsLoaded() == false ) return;

	m_settings->setValue( KEY_WINDOW_STATE, QString( value.toBase64() ) );
}

QString Config::Application()
{
	return APPLICATION;
}

QString Config::Organization()
{
	return ORGANIZATION;
}

void Config::SaveHistory()
{
	m_settings->beginWriteArray( KEY_HISTORY, m_history.count() );
	for( int i = 0; i < m_history.count(); ++i )
	{
		m_settings->setArrayIndex( i );
        m_settings->setValue( KEY_DOCUMENT, m_history.at( i ).DocumentId() );
	}
	m_settings->endArray();
}

void Config::LoadHistory()
{
	int count = m_settings->beginReadArray( KEY_HISTORY );
	for( int i = 0; i < count; ++i )
	{
		m_settings->setArrayIndex( i );

		QString string = m_settings->value( KEY_DOCUMENT ).toString();
        if( Document::CheckDocumentId( string ) == false ) continue;

        m_history.append( Document::FromId( string ) );
	}
	m_settings->endArray();
}

void Config::SaveStates()
{
	QList<DocumentState> states = m_states.values();
	m_settings->beginWriteArray( KEY_STATES, states.count() );
	for( int i = 0; i < states.count(); ++i )
	{
		DocumentState state = states.at( i );

		m_settings->setArrayIndex( i );
		m_settings->setValue( KEY_STATE, state.ToJSon() );
	}
	m_settings->endArray();
}

void Config::LoadStates()
{
	int count = m_settings->beginReadArray( KEY_STATES );
	for( int i = 0; i < count; ++i )
	{
		m_settings->setArrayIndex( i );
		DocumentState state = DocumentState::FromJSon( m_settings->value( KEY_STATE ).toString() );
		if( state.IsCorrect() == false ) continue;
		m_states.insert( state.GetDocument(), state );
	}
	m_settings->endArray();
}
