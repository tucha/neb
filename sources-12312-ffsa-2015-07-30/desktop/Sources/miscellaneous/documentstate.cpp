#include "documentstate.h"

#include <QJsonDocument>
#include <QJsonObject>

#define KEY_DOCUMENT		"Document"
#define KEY_PAGE_INDEX		"PageIndex"
#define KEY_ANGLE			"Angle"
#define KEY_INVERTED		"Inverted"
#define KEY_SCALE_WIDTH		"ScaleWidth"
#define KEY_SCALE_FULL		"ScaleFull"
#define KEY_SCALE			"Scale"
#define KEY_WINDOW_STATE	"WindowState"

DocumentState::DocumentState():
	m_data()
{
}

Document DocumentState::GetDocument() const
{
	QString documentId = m_data.value( KEY_DOCUMENT ).toString();
	if( Document::CheckDocumentId( documentId ) == false ) return Document();
	return Document::FromId( documentId );
}

QVariant    DocumentState::GetPageIndex() const		{ return m_data.value( KEY_PAGE_INDEX,	0 ); }
int			DocumentState::GetAngle() const			{ return m_data.value( KEY_ANGLE,		0 ).toInt(); }
bool		DocumentState::GetInverted() const		{ return m_data.value( KEY_INVERTED,	false ).toBool(); }
bool		DocumentState::GetScaleWidth() const	{ return m_data.value( KEY_SCALE_WIDTH,	false ).toBool(); }
bool		DocumentState::GetScaleFull() const		{ return m_data.value( KEY_SCALE_FULL,	false ).toBool(); }
int			DocumentState::GetScale() const			{ return m_data.value( KEY_SCALE,		100 ).toInt(); }
QByteArray	DocumentState::GetWindowState() const	{ return QByteArray::fromBase64( m_data.value( KEY_WINDOW_STATE, QString() ).toByteArray() ); }

void DocumentState::SetDocument(Document document)	{ m_data.insert( KEY_DOCUMENT, document.DocumentId() ); }
void DocumentState::SetPageIndex(QVariant pageIndex)    { m_data.insert( KEY_PAGE_INDEX, pageIndex ); }
void DocumentState::SetAngle(int angle)					{ m_data.insert( KEY_ANGLE, angle ); }
void DocumentState::SetInverted(bool inverted)			{ m_data.insert( KEY_INVERTED, inverted ); }
void DocumentState::SetScaleWidth(bool value)			{ m_data.insert( KEY_SCALE_WIDTH, value ); }
void DocumentState::SetScaleFull(bool value)			{ m_data.insert( KEY_SCALE_FULL, value ); }
void DocumentState::SetScale(int scale)					{ m_data.insert( KEY_SCALE, scale ); }
void DocumentState::SetWindowState(QByteArray value)	{ m_data.insert( KEY_WINDOW_STATE, QString( value.toBase64() ) ); }

bool DocumentState::IsCorrect() const
{
	if( GetDocument().IsEmpty() ) return false;
	if( GetPageIndex() <= 0 ) return false;
	if( GetAngle() % 90 != 0 ) return false;
	if( GetScaleWidth() && GetScaleFull() ) return false;
	if( GetScale() <= 0 ) return false;
	return true;
}

QString DocumentState::ToJSon() const
{
	QJsonDocument document;
	document.setObject( QJsonObject::fromVariantMap( m_data ) );
	return document.toJson( QJsonDocument::Compact );
}

DocumentState DocumentState::FromJSon(QString json)
{
	QJsonDocument document = QJsonDocument::fromJson( json.toUtf8() );
	DocumentState result;
	result.m_data = document.object().toVariantMap();
	return result;
}
