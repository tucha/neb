#ifndef RENDERER_H
#define RENDERER_H

#include <QObject>
#include <QSize>
#include <QPixmap>
#include <QTimer>

#include "scaler.h"

#include <common.h>

class Renderer : public QObject
{
	Q_OBJECT
private:
	DISABLE_COPY( Renderer )

public:
	explicit Renderer(QObject *parent = 0);
	virtual ~Renderer();

	bool IsEmpty(void)const;
	void Clear(void);
	void Load( int pageIndex, Request* baseRequest );
	void SetSearchResult( WordCoordinates searchResult );
	int PageIndex(void)const;

	void SetScaleExact( int scale );
	void SetScaleType( bool full, bool width );
	void ResetRotation(void);

	bool Inverted(void)const;
	Scaler* GetScaler(void);

signals:
	void UpdatePixmap( QPixmap pixmap );
	void DownloadProgress( qint64 bytesReceived, qint64 bytesTotal );
	void Select(int pageIndex);
	void Cleared(void);
	void DownloadingError( Document document );

public slots:
	void DisplayResized( QSize newDisplaySize );
	void RotateRight(void);
	void RotateLeft(void);
	void SetInverted(bool value);

private slots:
	void RequestUpdate(void);
	void FinishedSize( ErrorCode error, QSize pageSize );
	void ImageProgress( qint64 bytesReceived, qint64 bytesTotal );
	void ImageFinished( ErrorCode error, QImage image );

private:
	void UpdateScaler(void);
	void SetImage( QImage image );

	QTimer m_timer;
	RequestPageSize*	m_requestSize;
	RequestImageFixed*	m_requestImage;

	QImage m_image;
	QPixmap m_pixmap;
	WordCoordinates m_searchResult;

	Scaler m_scaler;
	bool m_inverted;
};

#endif // RENDERER_H
