#include "dbusservice.h"

#ifndef OS_LINUX
#error "D-Bus support can be compiled only under Linux"
#endif

#include <QtCore/QCoreApplication>
#include <QtCore/QDebug>

#include <QtDBus/QDBusConnection>
#include <QtDBus/QDBusConnectionInterface>
#include <QtDBus/QDBusInterface>
#include <QtDBus/QDBusReply>

DBusService::DBusService( StartupOptions options, const QString& serviceName, QObject* parent ) :
    QObject( parent ),
	m_registered( false ),
	m_serviceName( serviceName ),
	m_errorMessage(),
	m_exitValue( 0 )
{
    QDBusConnectionInterface* bus = NULL;

    if (!QDBusConnection::sessionBus().isConnected()
	 || !(bus = QDBusConnection::sessionBus().interface() ) ) {
        m_errorMessage = QLatin1String( "Session bus not found\n"
                                        "To circumvent this problem try the following command (with Linux and bash)\n"
                                        "export $(dbus-launch)");
	}
	
	if( bus == NULL ) return;
	
	QString objectPath = QLatin1Char('/') + m_serviceName;
	objectPath.replace(QLatin1Char('.'), QLatin1Char('/'));
	if (objectPath.contains(QLatin1Char('-')))
	{
		QString previous = objectPath;
		objectPath.remove( QLatin1Char( '-' ) );
		qWarning() << "Invalid service name" << previous << "using:" << objectPath;
	}
	
	if( options & Multiple )
	{
		const QString pid = QString::number( QCoreApplication::applicationPid() );
		m_serviceName += QLatin1Char( '-' ) + pid;
	}
	
	QDBusConnection::sessionBus().registerObject( QLatin1String("/MainApplication"), QCoreApplication::instance(),
												  QDBusConnection::ExportAllSlots |
												  QDBusConnection::ExportScriptableProperties |
												  QDBusConnection::ExportAdaptors);
	QDBusConnection::sessionBus().registerObject( objectPath, this,
												  QDBusConnection::ExportAdaptors);
	
	m_registered = bus->registerService(m_serviceName) == QDBusConnectionInterface::ServiceRegistered;
	
	if( m_registered == false )
	{
		if( options & Unique )
		{
			// Already running so it's ok!
			QDBusInterface iface( m_serviceName, objectPath );
			iface.setTimeout( 5 * 60 * 1000 ); // Application can take time to answer
			QVariantMap platform_data;
			platform_data.insert( QStringLiteral("desktop-startup-id"), QString::fromUtf8( qgetenv( "DESKTOP_STARTUP_ID" ) ) );
			if( QCoreApplication::arguments().count() > 1 )
			{
				QDBusReply<int> reply = iface.call(QLatin1String("CommandLine"), QCoreApplication::arguments(), QDir::currentPath(), platform_data);
				if (reply.isValid()) exit( reply.value() );
				else m_errorMessage = reply.error().message();
			}
			else
			{
				QDBusReply<void> reply = iface.call( QLatin1String("Activate"), platform_data );
				if (reply.isValid()) exit( 0 );
				else m_errorMessage = reply.error().message();
			}
		}
		else
		{
			m_errorMessage = QLatin1String( "Couldn't register name '" )
					+ m_serviceName
					+ QLatin1String("' with DBUS - another process owns it already!");
		}
	}
	else
	{
		if( QCoreApplication* app = QCoreApplication::instance() )
		{
			connect( app, SIGNAL( aboutToQuit() ), this, SLOT( unregister() ) );
		}
	}
	
	if ( ( m_registered == false ) && ((options & NoExitOnFailure) == 0))
	{
//		qCritical() << m_errorMessage;
		exit( 1 );
	}
}

DBusService::~DBusService()
{
}

bool DBusService::isRegistered() const
{
    return m_registered;
}

QString DBusService::errorMessage() const
{
    return m_errorMessage;
}

void DBusService::setExitValue( int value )
{
    m_exitValue = value;
}

void DBusService::unregister()
{
    QDBusConnectionInterface* bus = NULL;
    if ( !m_registered || !QDBusConnection::sessionBus().isConnected() || !(bus = QDBusConnection::sessionBus().interface()))
	{
        return;
    }
    bus->unregisterService( m_serviceName );
}
