#ifndef DBUSSERVICE_H
#define DBUSSERVICE_H

#include <QtCore/QObject>
#include <QtCore/QUrl>
#include <QtCore/QDir>

#ifndef OS_LINUX
#error "D-Bus support can be compiled only under Linux"
#endif

class DBusServicePrivate;

class DBusService : public QObject
{
    Q_OBJECT
    Q_ENUMS(StartupOption)
    Q_FLAGS(StartupOptions)

public:
    enum StartupOption
    {
        Unique = 1,
        Multiple = 2,
        NoExitOnFailure = 4
    };

    Q_DECLARE_FLAGS(StartupOptions, StartupOption)

    explicit DBusService( StartupOptions options, const QString& serviceName, QObject* parent = NULL );
    ~DBusService();

    bool isRegistered() const;
    QString errorMessage() const;
    void setExitValue(int value);

signals:
    void activateRequested(const QStringList &arguments, const QString &workingDirectory);
    void openRequested(const QList<QUrl> &uris);
    void activateActionRequested(const QString &actionName, const QVariant &parameter);

public Q_SLOTS:
    void unregister();

private:
	bool m_registered;
    QString m_serviceName;
    QString m_errorMessage;
    int m_exitValue;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(DBusService::StartupOptions)

#endif // DBUSSERVICE_H
