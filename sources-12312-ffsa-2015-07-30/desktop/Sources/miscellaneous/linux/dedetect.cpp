#include "dedetect.h"

#ifndef OS_LINUX
#error "D-Bus support can be compiled only under Linux"
#endif

#include <QtCore/QProcessEnvironment>
#include <QtCore/QDir>
#include <QtCore/QDebug>

//#define LINUX_DE_DETECTION_VERBOSE

LinuxUtils::LinuxDE LinuxUtils::DetectLinuxDE()
{
	// 1) Test the XDG_CURRENT_DESKTOP environment variable
	// NOTE: it is currently not in the XDG standard and thus not always implemented
	QString currentDesktop = QProcessEnvironment::systemEnvironment().value( "XDG_CURRENT_DESKTOP" );

#ifdef LINUX_DE_DETECTION_VERBOSE
	qWarning() << "NOTE: XDG_CURRENT_DESKTOP = " << currentDesktop;
#endif

	if( currentDesktop.compare( "kde", Qt::CaseInsensitive ) == 0 ) return LinuxUtils::DE_KDE;
	if( currentDesktop.compare( "gnome", Qt::CaseInsensitive ) == 0 ) return LinuxUtils::DE_GNOME;
	if( currentDesktop.compare( "unity", Qt::CaseInsensitive ) == 0 ) return LinuxUtils::DE_UNITY;
	if( currentDesktop.compare( "xfce", Qt::CaseInsensitive ) == 0 ) return LinuxUtils::DE_XFCE;
	if( currentDesktop.compare( "lxde", Qt::CaseInsensitive ) == 0 ) return LinuxUtils::DE_LXDE;
	
	// 2) Test GDMSESSION environment variable
	QString gdmSession = QProcessEnvironment::systemEnvironment().value( "GDMSESSION" );

#ifdef LINUX_DE_DETECTION_VERBOSE
	qWarning() << "NOTE: GDMSESSION = " << gdmSession;
#endif

	if( gdmSession.contains( "kde", Qt::CaseInsensitive ) ) return LinuxUtils::DE_KDE;
	if( gdmSession.contains( "gnome", Qt::CaseInsensitive ) ) return LinuxUtils::DE_GNOME;
	if( gdmSession.contains( "ubuntu", Qt::CaseInsensitive ) ) return LinuxUtils::DE_UNITY;
	if( gdmSession.contains( "xfce", Qt::CaseInsensitive ) ) return LinuxUtils::DE_XFCE;
	
	// 3) Test share data directories - XDG_DATA_DIRS environment variable
	QString dataDirs = QProcessEnvironment::systemEnvironment().value( "XDG_DATA_DIRS" );

#ifdef LINUX_DE_DETECTION_VERBOSE
	qWarning() << "NOTE: XDG_DATA_DIRS = " << dataDirs;
#endif

	if( dataDirs.contains( "kde", Qt::CaseInsensitive ) ) return LinuxUtils::DE_KDE;
	if( dataDirs.contains( "gnome", Qt::CaseInsensitive ) ) return LinuxUtils::DE_GNOME;
	if( dataDirs.contains( "xfce", Qt::CaseInsensitive ) ) return LinuxUtils::DE_XFCE;

	// 3) The last chance: detect config directories "manually"
	// NOTE: this method will fail in case of multiple DEs' installed on system
	QString homeDir = QDir::homePath();
	if( homeDir.endsWith( QDir::separator() ) == false ) homeDir += QDir::separator();
	QString kdeConfigDir = homeDir + ".kde4";
	if( QDir( kdeConfigDir ).exists() ) return LinuxUtils::DE_KDE;
	kdeConfigDir = homeDir + ".kde";
	if( QDir( kdeConfigDir ).exists() ) return LinuxUtils::DE_KDE;

	QString gnomeConfigDir = homeDir + ".gnome2";
	if( QDir( gnomeConfigDir ).exists() ) return LinuxUtils::DE_GNOME;

	gnomeConfigDir = homeDir + ".gnome";
	if( QDir( gnomeConfigDir ).exists() ) return LinuxUtils::DE_GNOME;
	
#ifdef LINUX_DE_DETECTION_VERBOSE
	qWarning() << "WARNING: unknown Linux DE detected";
#endif
	return LinuxUtils::DE_UNKNOWN;
}

LinuxUtils::LinuxDistro LinuxUtils::DetectLinuxDistro()
{
	if( QFile::exists( "/etc/fedora-release" ) ) return LinuxUtils::DISTRO_FEDORA;
	return LinuxUtils::DISTRO_UNKNOWN;
}

