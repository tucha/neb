#ifndef DEDETECT
#define DEDETECT

#ifndef OS_LINUX
#error "D-Bus support can be compiled only under Linux"
#endif

namespace LinuxUtils
{
	enum LinuxDE { DE_UNKNOWN, DE_KDE, DE_GNOME, DE_UNITY, DE_XFCE, DE_LXDE };
	
	LinuxDE DetectLinuxDE();

	enum LinuxDistro { DISTRO_UNKNOWN, DISTRO_FEDORA };

	LinuxDistro DetectLinuxDistro();
}

#endif // DEDETECT
