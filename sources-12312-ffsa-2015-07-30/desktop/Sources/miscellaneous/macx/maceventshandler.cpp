#include "maceventshandler.h"

#include <QDebug>
#include <QMessageBox>

#include <QFileOpenEvent>
#include <QFileInfo>

#include <miscellaneous/parameters.h>

#ifndef OS_MACOSX
#error "This file can be compiled only under Mac OS X"
#endif

QEvent::Type ViewerDryRunEvent::eventType = QEvent::None;

BaseEvent::BaseEvent( QEvent::Type& eType ) : QEvent( getEventType( eType ) )
{
}

BaseEvent::~BaseEvent()
{
}

QEvent::Type BaseEvent::getEventType( QEvent::Type& eType )
{
	if( eType == QEvent::None )
	{
		eType = static_cast<QEvent::Type>( QEvent::registerEventType() );
	}
	return eType;
}

ViewerDryRunEvent::ViewerDryRunEvent( const Parameters& params ) :
	BaseEvent( eventType ), m_params( params )
{
}

ViewerDryRunEvent::~ViewerDryRunEvent()
{
}

Parameters ViewerDryRunEvent::GetCmdLineParams() const
{
	return m_params;
}

//-------------------------------------------------------------------------------

EventFilterInstaller::EventFilterInstaller( QObject* eventSource, QObject* eventFilter )
{
	DebugAssert( eventSource != NULL );
	if( eventSource == NULL ) return;
	m_eventSource = eventSource;

	DebugAssert( eventFilter != NULL );
	if( eventFilter == NULL ) return;
	m_eventFilter = eventFilter;

	eventSource->installEventFilter( eventFilter );
}

EventFilterInstaller::~EventFilterInstaller()
{
	DebugAssert( m_eventSource != NULL );
	if( m_eventSource == NULL ) return;

	DebugAssert( m_eventFilter != NULL );
	if( m_eventFilter == NULL ) return;

	m_eventSource->removeEventFilter( m_eventFilter );

	m_eventSource = NULL;
	m_eventFilter = NULL;
}
