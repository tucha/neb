#include "renderer.h"

#include "painter.h"

#define TIMER 500

Renderer::Renderer(QObject *parent) :
	QObject(parent),

	m_timer(),
	m_requestSize( NULL ),
	m_requestImage( NULL ),

	m_image(),
	m_pixmap(),
	m_searchResult(),

	m_scaler(),
	m_inverted( false )
{
	m_timer.setSingleShot( true );
	connect(&m_timer,SIGNAL(timeout()),this,SLOT(RequestUpdate()));
}

Renderer::~Renderer()
{
	Clear();
}

bool Renderer::IsEmpty() const
{
	return m_requestSize == NULL;
}

void Renderer::Clear()
{
	m_timer.stop();

	if( m_requestSize != NULL )
	{
		m_requestSize->deleteLater();
		m_requestSize = NULL;
	}

	if( m_requestImage != NULL )
	{
		m_requestImage->deleteLater();
		m_requestImage = NULL;
	}

	m_image = QImage();
	m_pixmap = QPixmap();
	m_searchResult = WordCoordinates();

	m_scaler.SetPageSize( QSize() );

	emit Cleared();
}

void Renderer::Load(int pageIndex, Request* baseRequest)
{
	emit Select( pageIndex - 1 );
	if( IsEmpty() == false && PageIndex() == pageIndex ) return;

	Clear();

	RequestPage* tmp = baseRequest->NewPage( pageIndex );

	m_requestSize = tmp->NewSize( PRIORITY_HIGHT );
	connect(m_requestSize,SIGNAL(SignalFinished(ErrorCode,QSize)),this,SLOT(FinishedSize(ErrorCode,QSize)));
	m_requestSize->Send();

	tmp->deleteLater();
}

void Renderer::SetSearchResult(WordCoordinates searchResult)
{
	m_searchResult = searchResult;
	if( IsEmpty() ) return;

	if( m_image.isNull() ) return;
	m_pixmap = QPixmap::fromImage( Painter::Highlight( m_image, m_searchResult ) );
	emit UpdatePixmap( m_scaler.Transform( m_pixmap ) );
}

int Renderer::PageIndex() const
{
	DebugAssert( IsEmpty() == false );
	if( IsEmpty() ) return 0;

	return m_requestSize->PageIndex();
}

void Renderer::SetScaleExact(int scale)
{
	m_scaler.SetExplicitScale( scale );
	UpdateScaler();
}

void Renderer::SetScaleType(bool full, bool width)
{
	m_scaler.SetScaleFull( full );
	m_scaler.SetScaleWidth( width );
	UpdateScaler();
}

void Renderer::ResetRotation()
{
	m_scaler.ResetRotation();
	UpdateScaler();
}

bool Renderer::Inverted() const
{
	return m_inverted;
}

Scaler*Renderer::GetScaler()
{
	return &m_scaler;
}

void Renderer::RotateRight()
{
	m_scaler.Rotate( 90 );
	UpdateScaler();
}

void Renderer::RotateLeft()
{
	m_scaler.Rotate( -90 );
	UpdateScaler();
}

void Renderer::SetInverted(bool value)
{
	if( m_inverted == value ) return;
	m_inverted = value;

	if( m_image.isNull() ) return;
	m_image = Painter::Invert( m_image );
	m_pixmap = QPixmap::fromImage( Painter::Highlight( m_image, m_searchResult ) );

	emit UpdatePixmap( m_scaler.Transform( m_pixmap ) );
}

void Renderer::DisplayResized(QSize newDisplaySize)
{
	m_scaler.SetDisplaySize( newDisplaySize );
	UpdateScaler();
}

void Renderer::RequestUpdate()
{
	if( IsEmpty() ) return;

	m_requestImage->SetRestriction( m_scaler.Restriction() );
	m_requestImage->Send();
}

void Renderer::FinishedSize(ErrorCode error, QSize pageSize)
{
	if( IsEmpty() ) return;
	if( sender() != m_requestSize ) return;

	if( ApiBase::IsDownloadingError( error ) )
	{
		emit DownloadingError( m_requestSize->GetDocument() );
		return;
	}

	if( ApiBase::IsContentForbiddenError( error ) )
	{
		emit UpdatePixmap( QPixmap() );
		return;
	}

	if( error != API_NO_ERROR )
	{
		m_requestSize->Send();
		return;
	}

	m_scaler.SetPageSize( pageSize );

	m_requestImage = SENDER_PAGE->NewImageFixed( TYPE_IMAGES, GEOMETRY_WIDTH, m_scaler.Restriction(), FORMAT_JPEG, PRIORITY_HIGHT );
	connect(m_requestImage,SIGNAL(SignalDownloadProgress(qint64,qint64)),this,SLOT(ImageProgress(qint64,qint64)));
	connect(m_requestImage,SIGNAL(SignalFinished(ErrorCode,QImage)),this,SLOT(ImageFinished(ErrorCode,QImage)));
	m_requestImage->Send();
}

void Renderer::ImageProgress(qint64 bytesReceived, qint64 bytesTotal)
{
	if( IsEmpty() ) return;
	if( sender() != m_requestImage ) return;
	if( m_pixmap.isNull() == false ) return;
	emit DownloadProgress( bytesReceived, bytesTotal );
}

void Renderer::ImageFinished(ErrorCode error, QImage image)
{
	if( IsEmpty() ) return;
	if( sender() != m_requestImage ) return;

	if( error == QNetworkReply::ContentOperationNotPermittedError )
	{
		emit UpdatePixmap( QPixmap() );
		return;
	}

	if( error != API_NO_ERROR )
	{
		m_requestImage->Send();
		return;
	}

	SetImage( image );
}

void Renderer::UpdateScaler()
{
	if( IsEmpty() ) return;

	if( m_requestImage != NULL )
	{
		if( m_requestImage->Restriction() != m_scaler.Restriction() )
		{
			m_requestImage->Cancel();
			m_timer.start( TIMER );
		}
	}

	if( m_pixmap.isNull() == false ) emit UpdatePixmap( m_scaler.Transform( m_pixmap ) );
}

void Renderer::SetImage(QImage image)
{
	m_image = image;
	if( m_inverted ) m_image = Painter::Invert( m_image );
	m_pixmap = QPixmap::fromImage( Painter::Highlight( m_image, m_searchResult ) );

	emit UpdatePixmap( m_scaler.Transform( m_pixmap ) );
}
