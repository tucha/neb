#ifndef DOCUMENTSTATE_H
#define DOCUMENTSTATE_H

#include <QString>
#include <QVariantMap>

#include <document.h>

class DocumentState
{
public:
	DocumentState();

	Document	GetDocument(void)const;
    QVariant    GetPageIndex(void)const;
	int			GetAngle(void)const;
	bool		GetInverted(void)const;
	bool		GetScaleWidth(void)const;
	bool		GetScaleFull(void)const;
	int			GetScale(void)const;
	QByteArray	GetWindowState(void)const;

	void SetDocument( Document document );
    void SetPageIndex( QVariant pageIndex );
	void SetAngle( int angle );
	void SetInverted( bool inverted );
	void SetScaleWidth( bool value );
	void SetScaleFull( bool value );
	void SetScale( int scale );
	void SetWindowState( QByteArray value );

	bool IsCorrect(void)const;

	QString ToJSon(void)const;
	static DocumentState FromJSon(QString json );

private:
	QVariantMap m_data;
};

#endif // DOCUMENTSTATE_H
