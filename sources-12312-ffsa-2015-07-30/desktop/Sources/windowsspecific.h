#ifndef WINDOWSSPECIFIC_H
#define WINDOWSSPECIFIC_H

#ifndef OS_WINDOWS
#error "This file must be compiled on Windows OS only"
#endif

#include <Windows.h>
#include <TlHelp32.h>

#include <QList>

typedef QList<DWORD>	DwordList;
typedef QList<HANDLE>	HandleList;

class WindowsSpecific
{
private:
	WindowsSpecific();
	~WindowsSpecific();

public:
	static DwordList	GetProcessThreads( qint64 processId );
	static HandleList	OpenThreads( DwordList threadIds );
	static void			CloseThreads( HandleList threadHandles );

	static bool IsThreadSuspended( DWORD threadId );
	static bool IsThreadSuspended( HANDLE threadHandle );
	static void KillProcess( qint64 processId );

//	static void	HideThreadsFromDebugger( HandleList threadHandles );
};

#endif // WINDOWSSPECIFIC_H
