%define        __spec_install_post %{nil}
%define          debug_package %{nil}
%define        __os_install_post %{_dbpath}/brp-compress

Name:    elar-nelrfviewer
Version: VERSION_STUB
Release: RELEASE_STUB
Summary: NEL RF Book Viewer
Summary(ru): Просмотрщик НЭБ РФ
Group:   Amusements/Graphics
License: Proprietary
Vendor: Elar, Corp.
URL: http://нэб.рф
# FIXME: provide correct dependencies!
#Requires: bash >= 4.0, libc6 >= 2.11, libstdc++6 >= 4.4
##BuildRequires: desktop-file-utils
Requires(post): desktop-file-utils, xdg-utils
Requires(postun): desktop-file-utils, xdg-utils
Requires(preun): desktop-file-utils, xdg-utils

SOURCE0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root

%description
 This package provides protected viewer application for the
 Russian Federation National Digital Library

%description -l ru
 Пакет предоставляет защищенный просмотрщик НЭБ РФ

%changelog
* Thu May 21 2015 Elar Corporation <support@elar.ru> VERSION_STUB
- Initial release for Linux hosts

%prep
%setup -q

%build
# NOTE: Empty section

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}
cp -a * %{buildroot}

%clean
rm -rf %{buildroot}

# Package file list (all belong to 'root' user)
%files
%defattr(-,root,root,-)
#%config(noreplace) %{_sysconfdir}/%{name}/%{name}.conf
%{_bindir}/*
%{_libdir}/*
%{_datadir}/*
# TODO: add doc and man directories

# Integrate application to Desktop Environment (DE) - GNOME, KDE, etc.##########
%post
# Increase XDG utilities verbose level to the maximum one
export XDG_UTILS_DEBUG_LEVEL=100
# Set the installation mode
export XDG_UTILS_INSTALL_MODE=system

# 1) Install shortcut to the Desktop
#xdg-desktop-icon install "${SHARE_DIR}/elar-nelrfviewer.desktop"

# Install MIME type for *.spd files
xdg-mime install "%_datadir/elar-nelrfviewer/elar-nelrfviewer.xml"

# Install shortcut to the KDE Desktop Menu (or GNOME/Unity Launcher)
xdg-desktop-menu install "%_datadir/elar-nelrfviewer/elar-nelrfviewer.desktop"

# Install application icon
xdg-icon-resource install --context apps      --size 128 "%{_datadir}/elar-nelrfviewer/elar-nelrfviewer-128.png"
xdg-icon-resource install --context apps      --size 64  "%{_datadir}/elar-nelrfviewer/elar-nelrfviewer-64.png"
xdg-icon-resource install --context apps      --size 48  "%{_datadir}/elar-nelrfviewer/elar-nelrfviewer-48.png"
xdg-icon-resource install --context apps      --size 32  "%{_datadir}/elar-nelrfviewer/elar-nelrfviewer-32.png"
# Install MIME type icon
xdg-icon-resource install --context mimetypes --size 128 "%{_datadir}/elar-nelrfviewer/elar-nelrfviewer-128.png" application-vnd.elar.viewer.spd
xdg-icon-resource install --context mimetypes --size 64  "%{_datadir}/elar-nelrfviewer/elar-nelrfviewer-64.png"  application-vnd.elar.viewer.spd
xdg-icon-resource install --context mimetypes --size 48  "%{_datadir}/elar-nelrfviewer/elar-nelrfviewer-48.png"  application-vnd.elar.viewer.spd
xdg-icon-resource install --context mimetypes --size 32  "%{_datadir}/elar-nelrfviewer/elar-nelrfviewer-32.png"  application-vnd.elar.viewer.spd

# Correcting rights
#chmod 644 %{_datadir}/icons/hicolor/128x128/mimetypes/gnome-mime-text-x-spd.png
#chmod 644 %{_datadir}/icons/hicolor/128x128/mimetypes/text-x-spd.png
#chmod 644 %{_datadir}/icons/hicolor/32x32/mimetypes/gnome-mime-text-x-spd.png
#chmod 644 %{_datadir}/icons/hicolor/32x32/mimetypes/text-x-spd.png
#chmod 644 %{_datadir}/icons/hicolor/48x48/mimetypes/gnome-mime-text-x-spd.png
#chmod 644 %{_datadir}/icons/hicolor/48x48/mimetypes/text-x-spd.png
#chmod 644 %{_datadir}/icons/hicolor/64x64/mimetypes/gnome-mime-text-x-spd.png
#chmod 644 %{_datadir}/icons/hicolor/64x64/mimetypes/text-x-spd.png

# Update icon database again
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
################################################################################

# Remove application integration into DE
%preun
APP_NAME=nelrfviewer
VIEWER_PID=$( pidof ${APP_NAME} 2> /dev/null )

if [ $1 = 0 ]; then
    # Check whether viewer is running and try to close it ######################
    if [ -n "${VIEWER_PID}" ]; then
        echo "WARNING: a copy of ${APP_NAME} is currently running. Trying to kill it..."
        kill -9 ${VIEWER_PID}
        sleep 1
        if pidof ${APP_NAME} > /dev/null 2>&1; then
            echo "ERROR: a copy of ${APP_NAME} is currently running.  Please close it and try again."
            echo "NOTE: it can take up to several seconds for ${APP_NAME} to finish running."
            exit 1
        else 
            echo "NOTE: ${APP_NAME} instance was killed successfully"
        fi
    else
        echo "NOTE: no ${APP_NAME} instance was detected"
    fi

    # Increase XDG utilities verbose level to the maximum one
    export XDG_UTILS_DEBUG_LEVEL=100
    # Set the installation mode
    #export XDG_UTILS_INSTALL_MODE=system

    # 1) Install shortcut to the Desktop
    #xdg-desktop-icon uninstall "%_datadir/elar-nelrfviewer/elar-nelrfviewer.desktop"

    # 2) Uninstall custom application icon
    xdg-icon-resource uninstall --context apps --size 128 "%_datadir/elar-nelrfviewer/elar-nelrfviewer-128.png"
    xdg-icon-resource uninstall --context apps --size 64  "%_datadir/elar-nelrfviewer/elar-nelrfviewer-64.png"
    xdg-icon-resource uninstall --context apps --size 48  "%_datadir/elar-nelrfviewer/elar-nelrfviewer-48.png"
    xdg-icon-resource uninstall --context apps --size 32  "%_datadir/elar-nelrfviewer/elar-nelrfviewer-32.png"

    # 3) Uninstall shortcut to the Desktop Menu (or Unity Launcher)
    xdg-desktop-menu uninstall "%_datadir/elar-nelrfviewer/elar-nelrfviewer.desktop"

    # 4) Uninstall MIME type for *.spd files
    xdg-mime uninstall "%_datadir/elar-nelrfviewer/elar-nelrfviewer.xml"

    # 5) Uninstall MIME type icon
    xdg-icon-resource uninstall --context mimetypes --size 128 "%_datadir/elar-nelrfviewer/elar-nelrfviewer-128.png" application-vnd.elar.viewer.spd
    xdg-icon-resource uninstall --context mimetypes --size 64  "%_datadir/elar-nelrfviewer/elar-nelrfviewer-64.png"  application-vnd.elar.viewer.spd
    xdg-icon-resource uninstall --context mimetypes --size 48  "%_datadir/elar-nelrfviewer/elar-nelrfviewer-48.png"  application-vnd.elar.viewer.spd
    xdg-icon-resource uninstall --context mimetypes --size 32  "%_datadir/elar-nelrfviewer/elar-nelrfviewer-32.png"  application-vnd.elar.viewer.spd
fi
################################################################################

