#! /bin/bash
# Integrate NEL RF application into major Linux Desktop Environments (DE) - KDE, GNOME and Unity
# Input: integration mode, current user only or system wide (requires root)
# Output: none

RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
BOLD=$(tput bold)
NORMAL=$(tput sgr0)

APP_COMPANY="elar"
APP_NAME="nelrfviewer"

SHARE_DIR_SPECIFIED=false
SHARE_DIR=""
INTEGRATION_MODE_SPECIFIED=false
INTEGRATION_MODE=""
INSTALLATION_MODE_SPECIFIED=false
INSTALLATION_MODE=""

function usage {
    echo "---------------------------------------------------------------"
    echo "USAGE: ./integrate-to-DE.sh [-p <share dir>] [-m <installation mode>] [-u <integration mode>] [-h]"
    echo "       share dir (${BOLD}optional${NORMAL}): directory with DE integration stuff, like desktop file and MIME xml"
    echo "       installation mode (${BOLD}optional${NORMAL}):"
    echo "           ${RED}install${NORMAL} or ${RED}uninstall${NORMAL} DE integration"
    echo "       integration mode (${BOLD}optional${NORMAL}):"
    echo "           specify ${RED}user${NORMAL} to current user only DE integration,"
    echo "           and ${RED}system${NORMAL} for system-wide one (for all users)"
    echo "       h (${BOLD}optional${NORMAL}): show this help"
    echo
    echo "NOTES:"
    echo "       ${RED}system${NORMAL} integration mode requires root priviligies,"
    echo "       so script will ask you for the root password"
    echo
    echo "DEFAULTS:"
    echo "       installation mode: ${RED}install${NORMAL}"
    echo "       integration mode: ${RED}user${NORMAL}"
    echo "---------------------------------------------------------------"
}

# Reset the getopts index
# NOTE: this is required to call build script more than once time
OPTIND=0

# Read command line arguments using Bash getopts function
while getopts ":p:m:u:h" opt; do
  case $opt in
    p)
      SHARE_DIR_SPECIFIED=true
      SHARE_DIR=${OPTARG}
      ;;
    m)
      INSTALLATION_MODE_SPECIFIED=true
      INSTALLATION_MODE=${OPTARG}
      ;;
    u)
      INTEGRATION_MODE_SPECIFIED=true
      INTEGRATION_MODE=${OPTARG}
      ;;
    h)
      usage
      exit 0
      ;;
    \?)
      echo "Invalid option: -$OPTARG"
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument."
      exit 1
      ;;
  esac
done

START_PATH=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

if [ "${SHARE_DIR_SPECIFIED}" == "true" ]; then
    if [ ! -d "${SHARE_DIR}" ]; then
        echo "${RED}ERROR: specified share directory not found. Aborting.${NORMAL}"
    fi
else
    SHARE_DIR=${START_PATH}
    echo "INFO: using  current directory as default share directory: ${START_PATH}"
fi

# Check whether share directory contains all required files #######################################

if [ ! -f "${SHARE_DIR}/${APP_COMPANY}-${APP_NAME}.desktop" ]; then
    echo "${RED}ERROR: .desktop file not found. Aborting.${NORMAL}"
    exit 1
fi
if [ ! -f "${SHARE_DIR}/${APP_COMPANY}-${APP_NAME}.png" ]; then
    echo "${RED}ERROR: icon file not found. Aborting.${NORMAL}"
    exit 1
fi
if [ ! -f "${SHARE_DIR}/${APP_COMPANY}-${APP_NAME}.xml" ]; then
    echo "${RED}ERROR: MIME types description file not found. Aborting.${NORMAL}"
    exit 1
fi

###################################################################################################

if [ "${INSTALLATION_MODE_SPECIFIED}" == "true" ]; then
    if [[ ( ${INSTALLATION_MODE} != "install" ) && ( ${INSTALLATION_MODE} != "uninstall" ) ]]; then
        echo "ERROR: invalid installation mode ${RED}${INTEGRATION_MODE}${NORMAL}"
        exit 1
    fi

    echo "NOTE: using ${RED}${INSTALLATION_MODE}${NORMAL} installation mode"
else
    INSTALLATION_MODE=install
    echo "NOTE: using ${RED}${INSTALLATION_MODE}${NORMAL} installation mode by default"
fi

if [ "${INTEGRATION_MODE_SPECIFIED}" == "true" ]; then
    if [[ ( ${INTEGRATION_MODE} != "user" ) && ( ${INTEGRATION_MODE} != "system" ) ]]; then
        echo "ERROR: invalid integration mode ${RED}${INTEGRATION_MODE}${NORMAL}"
        exit 1
    fi

    echo "NOTE: using ${RED}${INTEGRATION_MODE}${NORMAL} integration mode"
else
    INTEGRATION_MODE=user
    echo "NOTE: using ${RED}${INTEGRATION_MODE}${NORMAL} integration mode by default"
fi

###################################################################################################

# Increase XDG utilities verbose level to the maximum one
export XDG_UTILS_DEBUG_LEVEL=100
# Set the installation mode
export XDG_UTILS_INSTALL_MODE=${INTEGRATION_MODE}

CURRENT_PATH=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
echo $CURRENT_PATH
echo "TryExec=${CURRENT_PATH}/nelrfviewer" >> ${SHARE_DIR}/${APP_COMPANY}-${APP_NAME}.desktop
echo "Exec=${CURRENT_PATH}/nelrfviewer %u" >> ${SHARE_DIR}/${APP_COMPANY}-${APP_NAME}.desktop

# 1) Install shortcut to the Desktop
xdg-desktop-icon ${INSTALLATION_MODE} "${SHARE_DIR}/${APP_COMPANY}-${APP_NAME}.desktop"

###################################################################################################

if [ ${INTEGRATION_MODE} == "user" ]; then
    # 2) Install custom application icon
    xdg-icon-resource ${INSTALLATION_MODE} --context apps --size 128 "${SHARE_DIR}/${APP_COMPANY}-${APP_NAME}.png"

    # 3) Install shortcut to the Desktop Menu (or Launcher)
    xdg-desktop-menu ${INSTALLATION_MODE} "${SHARE_DIR}/${APP_COMPANY}-${APP_NAME}.desktop"

    # 4) Install MIME type for *.spd files
    xdg-mime ${INSTALLATION_MODE} "${SHARE_DIR}/${APP_COMPANY}-${APP_NAME}.xml"

    # 5) Install MIME type icon
    xdg-icon-resource ${INSTALLATION_MODE} --context mimetypes --size 128 "${SHARE_DIR}/${APP_COMPANY}-${APP_NAME}.png" document-x-spd
else
    # 2) Install custom application icon
    sudo xdg-icon-resource ${INSTALLATION_MODE} --context apps --size 128 "${SHARE_DIR}/${APP_NAME}-${APP_NAME}.png"

    # 3) Install shortcut to the Desktop Menu (or Launcher)
    sudo xdg-desktop-menu ${INSTALLATION_MODE} "${SHARE_DIR}/${APP_COMPANY}-${APP_NAME}.desktop"

    # 4) Install MIME type for *.spd files
    sudo xdg-mime ${INSTALLATION_MODE} "${SHARE_DIR}/${APP_COMPANY}-${APP_NAME}.xml"

    # 5) Install MIME type icon
    sudo xdg-icon-resource ${INSTALLATION_MODE} --context mimetypes --size 128 "${SHARE_DIR}/${APP_COMPANY}-${APP_NAME}.png" document-x-spd
fi
