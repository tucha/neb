#! /bin/bash
# Elar NEL RF Viewer startup script for Linux OS

clear

RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
BOLD=$(tput bold)
NORMAL=$(tput sgr0)

# Setup library/plugin directories
START_PATH=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
SELF_PATH="${START_PATH}/elar-nelrfviewer.sh"

SYMLINK_TARGET=$(readlink -f "$0")

if [ "${SYMLINK_TARGET}" != "${SELF_PATH}" ]; then
    echo "${GREEN}INFO: starting NEL RF Viewer using symlink... ${NORMAL}"
    # /usr/bin/elar-nelrfviewer.sh --> /opt/elar/nelrfviewer/bin/elar-nelrfviewer.sh
    START_PATH="$(dirname ${SYMLINK_TARGET})"
fi

QT5_DIR=$( cd "${START_PATH}/../lib" && pwd )
QT5_PLUGINS_DIR=$( cd "${START_PATH}/../plugins" && pwd )

if [ ! -d "${QT5_DIR}" ]; then
    echo ${RED}
    echo "ERROR: Qt5 directory not found"
    echo "Path used:  ${QT5_DIR}"
    echo ${NORMAL}
    exit 1
fi

if [ ! -d "${QT5_PLUGINS_DIR}" ]; then
    echo ${RED}
    echo "ERROR: Qt5 plugins directory not found"
    echo "Path used: ${QT5_PLUGINS_DIR}"
    echo ${NORMAL}
    exit 1
fi

export LD_LIBRARY_PATH="${QT5_DIR}:${LD_LIBRARY_PATH}"
export QT_PLUGIN_PATH=${QT5_PLUGINS_DIR}

# Launch the NEL RF viewer application
# NOTE: 'exec' command *replaces* the shell process with specified one
exec ${START_PATH}/nelrfviewer $*
