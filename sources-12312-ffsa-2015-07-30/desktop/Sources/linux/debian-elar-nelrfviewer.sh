#! /bin/bash
# Elar NEL RF Viewer startup script for Linux OS

clear

RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
BOLD=$(tput bold)
NORMAL=$(tput sgr0)

# Setup Qt5 library/plugin directories
QT5_DIR=/usr/lib/elar-nelrfviewer
QT5_PLUGINS_DIR=/usr/lib/elar-nelrfviewer/plugins

if [ ! -d "${QT5_DIR}" ]; then
    echo ${RED}
    echo "ERROR: Qt5 directory not found"
    echo "Path used:  ${QT5_DIR}"
    echo ${NORMAL}
    exit 1
fi

if [ ! -d "${QT5_PLUGINS_DIR}" ]; then
    echo ${RED}
    echo "ERROR: Qt5 plugins directory not found"
    echo "Path used: ${QT5_PLUGINS_DIR}"
    echo ${NORMAL}
    exit 1
fi

export LD_LIBRARY_PATH="${QT5_DIR}:${LD_LIBRARY_PATH}"
export QT_PLUGIN_PATH="${QT5_PLUGINS_DIR}"

# Launch the NEL RF viewer application
# NOTE: 'exec' command *replaces* the shell process with specified one
exec /usr/bin/elar-nelrfviewer-binary "$*"
