QMAKE_TARGET_DESCRIPTION = ELAR

QT += core gui
QT += network
QT += printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
lessThan(QT_MAJOR_VERSION, 5){

	HEADERS += miscellaneous/json/QJsonArray.h \
			   miscellaneous/json/QJsonDocument.h \
			   miscellaneous/json/QJsonObject.h \
			   miscellaneous/json/QJsonParseError.h \
			   miscellaneous/json/QJsonParser.h \
			   miscellaneous/json/QJsonRoot.h \
			   miscellaneous/json/QJsonValue.h \
			   miscellaneous/json/QJsonValueRef.h

	SOURCES += miscellaneous/json/QJsonArray.cpp \
			   miscellaneous/json/QJsonDocument.cpp \
			   miscellaneous/json/QJsonObject.cpp \
			   miscellaneous/json/QJsonParseError.cpp \
			   miscellaneous/json/QJsonParser.cpp \
			   miscellaneous/json/QJsonValue.cpp \
			   miscellaneous/json/QJsonValueRef.cpp

	INCLUDEPATH += miscellaneous/json
}

TEMPLATE = app
message($$VERSION)
DEFINES += VERSION=\\\"$$VERSION\\\"

windows{
    TARGET = viewer

    DEFINES += OS_WINDOWS

msvc{
    LIBS += User32.lib
}

    HEADERS += windowsspecific.h \
	       protection/suspendprotection.h \
	       protection/threadcollection.h

    SOURCES += windowsspecific.cpp \
	       protection/suspendprotection.cpp \
	       protection/threadcollection.cpp

    LIBS += -luser32
}

unix:!macx {
    # NOTE: in Unix application usually installed to the /usr/bin directory,
    #       among the thousands others - so it's name must be unique
    # NEL: National Electronic Library of Russian Federation, Национальная Электронная Библиотека РФ
    TARGET = nelrfviewer

    DEFINES += OS_LINUX

    QT += dbus
    HEADERS += protection/linux/ksnapshotprotection.h \
	       protection/linux/linuxwindowcaptureprotection.h
    SOURCES += protection/linux/ksnapshotprotection.cpp \
	       protection/linux/linuxwindowcaptureprotection.cpp

    HEADERS += miscellaneous/linux/dedetect.h \
	       miscellaneous/linux/dbusservice.h
    SOURCES += miscellaneous/linux/dedetect.cpp \
	       miscellaneous/linux/dbusservice.cpp

    DISTFILES += \
	linux/elar-nelrfviewer.desktop \
	linux/elar-nelrfviewer.png \
	linux/elar-nelrfviewer.sh \
	linux/elar-nelrfviewer.spec \
	linux/elar-nelrfviewer.xml \
	linux/integrate-to-DE.sh \
	linux/makedist.sh \
	linux/install.sh \
	linux/uninstall.sh

    # 'Make install' target description #######################################
    # Install viewer executable (using 'make install')
    isEmpty(BINARY_INSTALL_PATH):BINARY_INSTALL_PATH = /usr/lib/nelrfviewer
    target.path = $${BINARY_INSTALL_PATH}
    INSTALLS += target

    # Install Unix viewer launch script
    isEmpty(SCRIPT_INSTALL_PATH):SCRIPT_INSTALL_PATH = /usr/bin
    script.files = linux/elar-nelrfviewer.sh
    script.path  = $${SCRIPT_INSTALL_PATH}
    INSTALLS += script

    # Install Unix *.desktop file and custom application icon
    isEmpty(DESKTOP_INSTALL_PATH):DESKTOP_INSTALL_PATH = /usr/share/applications
    desktopfile.files = linux/elar-nelrfviewer.desktop
    desktopfile.path  = $${DESKTOP_INSTALL_PATH}
    INSTALLS += desktopfile

    isEmpty(APPICON_INSTALL_PATH):APPICON_INSTALL_PATH = /usr/share/pixmaps
    appicon.files = icons/logo.png
    appicon.path  = $${APPICON_INSTALL_PATH}
    INSTALLS += appicon
    ###########################################################################
    QMAKE_LFLAGS_RPATH=
    QMAKE_LFLAGS += "-Wl,-rpath,\'\$$ORIGIN/../lib\'"
}

macx {
    # NOTE: looks like only on Mac OS X qmake allows to use non-latin chars in TARGET
    TARGET = "НЭБ РФ"
#    TARGET = nelrfviewer

    DEFINES += OS_MACOSX

    # Custom file type (*.spd) and custom URL scheme (spd://) handlers
    HEADERS += miscellaneous/macx/maceventshandler.h
    SOURCES += miscellaneous/macx/maceventshandler.cpp

    # Screen capture protection
    HEADERS += protection/windowcaptureprotection.h
    OBJECTIVE_SOURCES += protection/windowcaptureprotection.mm
    LIBS += -framework Cocoa

    # PC emulators protection (currently only VMWare and VirtualBox)
    PCEMULATORDETECTION {
	DEFINES += PCEMULATORDETECTION

	HEADERS += protection/pcemulatordetection.h
	SOURCES += protection/pcemulatordetection.cpp
}

    # Deployment stuff (for signing application bundle and building DMG file)

    # Custom Property List file
    QMAKE_INFO_PLIST = ViewerInfo.plist
    # Minimal Mac OS X version viewer can be installed on
    QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.7
    # Icon file for application in dmg, dock and launchpad
    ICON = $$PWD/icons/ViewerMacIcon.icns

    # Build dmg package #######################################################
    # To execute this target type "make dmg"
    dmg.depends += all
    dmg.commands += rm -f -v -- $$shell_quote( $${TARGET}.dmg ) &&
    dmg.commands += macdeployqt $$shell_quote( $${TARGET}.app ) -verbose=2;
    QMAKE_EXTRA_TARGETS += dmg
    ###########################################################################

    # Sign application bundle #################################################
    # APPCERT: Name of the application signing certificate, like "3rd Party Mac Developer Application: <yourcompany>"
#    APPCERT = "OS X Developer"

    # Bundle identifier for your application
    codesign.depends  += all
#    codesign.commands += macdeployqt $$shell_quote( $${TARGET}.app ) -codesign=$$shell_quote( $${APPCERT} ) -verbose=2 -dmg;
    codesign.commands += macdeployqt $$shell_quote( $${TARGET}.app ) -verbose=2;
 #   codesign.commands += rm -rfv -- $$shell_quote( $${TARGET}.app );

    # Sign the application bundle, using the provided entitlements
#    codesign.commands += codesign -d -f -s $$shell_quote( $${APPCERT} ) --verbose $$shell_quote( $${TARGET}.app );
#    codesign.commands += codesign --verify --verbose $$shell_quote( $${TARGET}.app );

    QMAKE_EXTRA_TARGETS += codesign
    ###########################################################################
}

CONFIG(debug, debug|release):DEFINES += BUILD_DEBUG

#DEFINES += EXAMPLE
#DEFINES += PLAN_B
#DEFINES += TIME_LIMITED_MODE
#DEFINES += TIME_LIMIT=\\\"01.03.2015\\\"
#DEFINES += TIME_LIMIT_FORMAT=\\\"dd.MM.yyyy\\\"
DEFINES += DISABLE_THUMBNAILS
#DEFINES += CORRECT_VIEWER_SETTINGS
#DEFINES += BASE_URL_DEMO
#DEFINES += REQUESTS_DUMP_CREATE
DEFINES += REQUESTS_DUMP_APPLY
DEFINES += ALLOW_SUSPENDING_IN_DEBUG
#DEFINES += EMULATE_ACCOUNT_REQUESTS
#DEFINES += IGNORE_SSL_ERRORS
#DEFINES += NETWORK_LOG

INCLUDEPATH += $$PWD/../../common/
INCLUDEPATH += $$PWD/

RC_ICONS = $$PWD/icons/icon.ico

SOURCES += main.cpp\
	gui/mainwindow.cpp \
    gui/display.cpp \
    gui/searchpanel.cpp \
    gui/thumbnails.cpp \
	gui/flowlayout.cpp \
    ../../common/pageset.cpp \
    ../../common/range.cpp \
    ../../common/request.cpp \
    ../../common/requestfileextensions.cpp \
    ../../common/requestfile.cpp \
    ../../common/requestfilesize.cpp \
    ../../common/requestdescription.cpp \
    ../../common/requestinformation.cpp \
    ../../common/requestdocumenttype.cpp \
    ../../common/requestcollection.cpp \
    ../../common/requestpagescount.cpp \
    ../../common/requesttype.cpp \
    ../../common/requestcontent.cpp \
    ../../common/requestpageset.cpp \
    ../../common/requestpage.cpp \
    ../../common/requestpagesize.cpp \
    ../../common/requestimage.cpp \
    ../../common/requestimagefixed.cpp \
    ../../common/requestwords.cpp \
    ../../common/requestsearch.cpp \
    ../../common/word.cpp \
    ../../common/requestwordcoordinates.cpp \
    ../../common/requestimagewithwords.cpp \
    ../../common/example.cpp \
    gui/thumbnail.cpp \
    gui/searchresults.cpp \
    gui/searchresult.cpp \
    miscellaneous/painter.cpp \
    ../../common/debug.cpp \
    gui/debugpanel.cpp \
    gui/pagescontrol.cpp \
    gui/zoompanel.cpp \
    miscellaneous/config.cpp \
    miscellaneous/renderer.cpp \
    miscellaneous/scaler.cpp \
    gui/description.cpp \
    gui/record.cpp \
    logic.cpp \
    gui/downloadwindow.cpp \
    gui/pagesetwindow.cpp \
    gui/printingwindow.cpp \
    miscellaneous/documentstate.cpp \
    ../../common/requestwordpages.cpp \
    ../../common/getparameter.cpp \
    ../../common/getparameterslice.cpp \
    ../../common/getparameterhighlight.cpp \
    gui/history.cpp \
    gui/historyrecord.cpp \
    protection/printscreenprotection.cpp \
    miscellaneous/parameters.cpp \
    ../../common/apibase.cpp \
    ../../common/getparametersimple.cpp \
    ../../common/authorizationrequest.cpp \
    gui/authorizationdialog.cpp \
    ../../common/authorizationbase.cpp \
    ../../common/user.cpp \
    ../../common/authorizationresult.cpp \
    ../../common/authorizationcheck.cpp \
    ../../common/document.cpp \
    ../../common/jsonfinder.cpp \
    gui/userpassworddialog.cpp \
    ../../common/identifier.cpp \
    ../../common/requestbase.cpp \
    ../../common/requestcollections.cpp \
    ../../common/requestmarks.cpp \
    ../../common/requesttexts.cpp \
    ../../common/requeststat.cpp \
    processmanager.cpp \
    ../../common/bookmark.cpp \
    ../../common/accountbaserequest.cpp \
    ../../common/bookmarksaddrequest.cpp \
    ../../common/bookmarksgetrequest.cpp \
    ../../common/bookmarksdeleterequest.cpp \
    ../../common/bookmarksmanager.cpp \
    ../../common/bookmarkscontainer.cpp \
    ../../common/bookmarksmanagerbase.cpp \
    ../../common/commentsmanager.cpp \
    gui/commentwindow.cpp \
    ../../common/favoritebook.cpp \
    ../../common/favoritesaddrequest.cpp \
    ../../common/favoritesgetrequest.cpp \
    ../../common/favoritesdeleterequest.cpp \
    ../../common/favoritescontainer.cpp \
    ../../common/favoritesmanager.cpp \
    gui/bookmarkscontrol.cpp \
    processmanager2.cpp \
    protection/blacklistedprocessesdatabase.cpp \
    protection/blacklistedprocessesfinder.cpp \
    ../../common/statusrequest.cpp \
    ../../common/session.cpp \
    ../../common/sessionpointer.cpp \
    ../../common/viewersettingsrequest.cpp \
    ../../common/viewersettings.cpp \
    ../../common/requestdownloadepub.cpp \
    gui/viewerscontainer.cpp \
    gui/contentdisplay.cpp \
    gui/admindialog.cpp\
    ../../common/addtrustedhostrequest.cpp \
    ../../common/blockfingerprintrequest.cpp \
    ../../common/checktrustedhostrequest.cpp \
    ../../common/AnonymToken.cpp

HEADERS  += gui/mainwindow.h \
    gui/display.h \
    gui/searchpanel.h \
    gui/thumbnails.h \
	gui/flowlayout.h \
    ../../common/defines.h \
    ../../common/pageset.h \
    ../../common/range.h \
    ../../common/request.h \
    ../../common/requestfileextensions.h \
    ../../common/requestfile.h \
    ../../common/requestfilesize.h \
    ../../common/common.h \
    ../../common/addtrustedhostrequest.h \
    ../../common/blockfingerprintrequest.h \
    ../../common/checktrustedhostrequest.h \
    ../../common/requestdescription.h \
    ../../common/requestinformation.h \
    ../../common/requestdocumenttype.h \
    ../../common/requestsearch.h \
    ../../common/requestcollection.h \
    ../../common/requestpagescount.h \
    ../../common/requesttype.h \
    ../../common/requestcontent.h \
    ../../common/requestpageset.h \
    ../../common/requestpage.h \
    ../../common/requestpagesize.h \
    ../../common/requestimage.h \
    ../../common/requestimagefixed.h \
    ../../common/requestwords.h \
    ../../common/word.h \
    ../../common/requestwordcoordinates.h \
    ../../common/requestimagewithwords.h \
    ../../common/example.h \
    gui/thumbnail.h \
    gui/searchresults.h \
    gui/searchresult.h \
    miscellaneous/painter.h \
    ../../common/debug.h \
    gui/debugpanel.h \
    gui/pagescontrol.h \
    gui/zoompanel.h \
    miscellaneous/config.h \
    miscellaneous/renderer.h \
    miscellaneous/scaler.h \
    gui/description.h \
    gui/record.h \
    logic.h \
    gui/downloadwindow.h \
    gui/pagesetwindow.h \
    gui/printingwindow.h \
    miscellaneous/documentstate.h \
    ../../common/requestwordpages.h \
    ../../common/getparameter.h \
    ../../common/getparameterslice.h \
    ../../common/getparameterhighlight.h \
    gui/history.h \
    gui/historyrecord.h \
    protection/printscreenprotection.h \
    miscellaneous/parameters.h \
    ../../common/apibase.h \
    ../../common/getparametersimple.h \
    ../../common/authorizationrequest.h \
    gui/authorizationdialog.h \
    ../../common/authorizationbase.h \
    ../../common/user.h \
    ../../common/authorizationresult.h \
    ../../common/authorizationcheck.h \
    ../../common/document.h \
    ../../common/jsonfinder.h \
    gui/userpassworddialog.h \
    ../../common/identifier.h \
    ../../common/requestbase.h \
    ../../common/requestcollections.h \
    ../../common/requestmarks.h \
    ../../common/requesttexts.h \
    ../../common/requeststat.h \
    processmanager.h \
    ../../common/bookmark.h \
    ../../common/accountbaserequest.h \
    ../../common/bookmarksaddrequest.h \
    ../../common/bookmarksgetrequest.h \
    ../../common/bookmarksdeleterequest.h \
    ../../common/bookmarksmanager.h \
    ../../common/bookmarkscontainer.h \
    ../../common/bookmarksmanagerbase.h \
    ../../common/commentsmanager.h \
    gui/commentwindow.h \
    ../../common/favoritebook.h \
    ../../common/favoritesaddrequest.h \
    ../../common/favoritesgetrequest.h \
    ../../common/favoritesdeleterequest.h \
    ../../common/favoritescontainer.h \
    ../../common/favoritesmanager.h \
    gui/bookmarkscontrol.h \
    processmanager2.h \
    protection/blacklistedprocessesdatabase.h \
    protection/blacklistedprocessesfinder.h \
    ../../common/statusrequest.h \
    ../../common/session.h \
    ../../common/sessionpointer.h \
    ../../common/viewersettingsrequest.h \
    ../../common/viewersettings.h \
    ../../common/requestdownloadepub.h \
    gui/viewerscontainer.h \
    gui/contentdisplay.h \
    gui/admindialog.h\
    ../../common/AnonymToken.h

RESOURCES += \
    icons.qrc \
    translations.qrc

TRANSLATIONS = viewer_ru.ts

OTHER_FILES += viewer_ru.ts

include(gui/QEpubRenderer/QEpubRenderer.pri)
include(platform_id/platform_id.pri)

DEFINES += 'BUILD_TOKEN=\'\"942a30ff0bf8094e4c6dcb29ee9e2c07\"\''
DEFINES += IGNORE_SSL_ERRORS

CONFIG += c++11 c++14

QMAKE_CXXFLAGS += -std=c++0x -std=c++11
QMAKE_LFLAGS +=  -std=c++11 -std=c++0x


linux: {
    DEFINES += 'REQUEST_OS_ID=\'\"linux\"\''
}

macx: {
    DEFINES += 'REQUEST_OS_ID=\'\"macos\"\''
}

windows: {
    DEFINES += 'REQUEST_OS_ID=\'\"windows\"\''
#    DEFINES += BUILD_DEBUG
#    CONFIG += console
}
