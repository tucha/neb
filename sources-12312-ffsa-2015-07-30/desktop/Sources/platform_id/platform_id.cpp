#include "platform_id.h"
#ifdef Q_OS_WIN
#define _WIN32_WINNT 0x0601
#include <Windows.h>
QString systemHardwareId ()
{
    DWORD dwVolSerial;
    BOOL bIsRetrieved;
    TCHAR buf[1024] ;
    GetSystemWindowsDirectory( buf , 1024 );
    buf[3] = 0 ;
    bIsRetrieved = GetVolumeInformation(buf, NULL, NULL, &dwVolSerial, NULL, NULL, NULL, NULL);
    QString ret;
    if (bIsRetrieved)
        ret += QString::number ( dwVolSerial , 16 ) ;
    else
         qWarning("Cannot get harddisk id" ) ;
    MEMORYSTATUSEX memory_status;
    ZeroMemory(&memory_status, sizeof(MEMORYSTATUSEX));
    memory_status.dwLength = sizeof(MEMORYSTATUSEX);
    if (GlobalMemoryStatusEx(&memory_status)) {
       ret += QString::number (memory_status.ullTotalPhys);
    }

    return "" ;
}
#endif
#ifdef Q_OS_MAC
#include <QProcess>
#include <CoreFoundation/CoreFoundation.h>
#include <IOKit/IOKitLib.h>
// Returns the serial number as a CFString.
// It is the caller's responsibility to release the returned CFString when done with it.
void CopySerialNumber(CFStringRef *serialNumber)
{
    if (serialNumber != NULL)
    {
        *serialNumber = NULL;
        io_service_t    platformExpert = IOServiceGetMatchingService(kIOMasterPortDefault,
                                                                     IOServiceMatching("IOPlatformExpertDevice"));
        if (platformExpert)
        {
            CFTypeRef serialNumberAsCFString =
                    IORegistryEntryCreateCFProperty(platformExpert,
                                                    CFSTR(kIOPlatformSerialNumberKey),
                                                    kCFAllocatorDefault, 0);
            if (serialNumberAsCFString)
                *serialNumber = (CFStringRef )serialNumberAsCFString;
            IOObjectRelease(platformExpert);
        }
    }
}

QString toQString(CFStringRef str)
{
    if (!str)
        return QString();

    CFIndex length = CFStringGetLength(str);
    if (length == 0)
        return QString();

    QString string(length, Qt::Uninitialized);
    CFStringGetCharacters(str, CFRangeMake(0, length), reinterpret_cast<UniChar *>
                          (const_cast<QChar *>(string.unicode())));
    CFRelease(str);

    QProcess p;
    p.start("sysctl", QStringList() << "hw.memsize");
    p.waitForFinished() ;
    string += p.readAll() ;

    return string;
}

QString systemHardwareId ()
{
    CFStringRef  r ;
    CopySerialNumber(&r) ;
    QString str = toQString ( r ) ;
    if ( str  != "" )
        return str ;
    else
        qWarning("Cannot get Mac id" ) ;

    return "" ;
}
#endif

#ifdef Q_OS_LINUX
#include <QProcess>

QString systemHardwareId ()
{
    QProcess p ;
    p.start("cat", QStringList() << "/proc/mounts");
    p.waitForFinished() ;
    QByteArray ret ;
    QByteArray data = p.readAll() ;
    QString pat = "/dev/disk/by-uuid/" ;
    int from = 0 ;
    int i = data.indexOf(pat, from) ;
    while ( i != - 1 )
    {
        ret = data.mid( i + pat.length() , data.indexOf( " " , i + pat.length() ) - i - pat.length() ) ;
        from = i + pat.length() + ret.length() ;

        i = data.indexOf(pat, from) ;
        if ( ret != "" )
            break ;
    }
    p.start("cat", QStringList() << "/proc/cpuinfo");
    p.waitForFinished() ;
    ret += p.readAll() ;

    p.start("grep", QStringList() << "MemTotal" << "/proc/meminfo");
    p.waitForFinished() ;
    ret += p.readAll() ;

//    qWarning("Cannot get hard disk uuid" ) ;
    return "" ;
}

#endif

#include <QNetworkInterface>
#include <QCryptographicHash>
QString PlatformID::hash()
{
    if(hashCache != "") return hashCache;
    QByteArray forHashing ;

    foreach ( QNetworkInterface iface , QNetworkInterface::allInterfaces())
        if ( iface.hardwareAddress() != "" )
        {
            forHashing.append(iface.hardwareAddress().toUtf8()) ;
            break ;
        }
    forHashing.append( systemHardwareId().toUtf8() ) ;
    hashCache = QCryptographicHash::hash(forHashing,QCryptographicHash::Md5).toHex();
    return hashCache;
}

bool PlatformID::check(QString h)
{
    return h == hash() ;
}

QString PlatformID::hashCache = "";
