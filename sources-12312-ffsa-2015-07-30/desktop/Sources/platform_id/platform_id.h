#ifndef PLATFORM_ID_H
#define PLATFORM_ID_H
#include <QString>

class  PlatformID
{
    static QString hashCache;
public:

    static QString hash() ;
    static bool check(QString hash) ;
};


#endif // PLATFORM_ID_H
