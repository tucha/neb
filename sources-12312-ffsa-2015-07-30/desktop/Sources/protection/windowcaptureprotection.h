#ifndef WINDOWCAPTUREPROTECTION_H
#define WINDOWCAPTUREPROTECTION_H

#include <QWidget>

#include <objc/objc.h>

class WindowCaptureProtection
{
public:
    static BOOL ProtectWidget( QWidget* w );
};

#endif // WINDOWCAPTUREPROTECTION_H
