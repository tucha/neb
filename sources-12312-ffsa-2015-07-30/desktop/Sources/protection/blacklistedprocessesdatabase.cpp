#include "blacklistedprocessesdatabase.h"

#ifdef OS_WINDOWS

#include "blacklistedprocessesfinder.h"

#define PROTECT_STR( T ) T

#include <defines.h>

BlacklistedProcessDescription::BlacklistedProcessDescription( QString processDescription, QString processName, uint reportNumber ) :
	m_processDescription( processDescription ), m_processName( processName.toLower() ), m_reportNumber( reportNumber )
{
}

BlacklistedProcessDescription::BlacklistedProcessDescription( QString processDescription, uint reportNumber ) :
	m_processDescription( processDescription ), m_reportNumber( reportNumber )
{
}

bool BlacklistedProcessDescription::ContainsProcessName()
{
	return m_processName.isEmpty() == false;
}

QString BlacklistedProcessDescription::GetProcessDescription()
{
	return m_processDescription;
}

uint BlacklistedProcessDescription::GetReportNumber()
{
	return m_reportNumber;
}

bool BlacklistedProcessDescription::ProcessNameEquals( QString processName )
{
	return processName == m_processName;
}

void BlacklistedProcessDescription::AddFullTitleWindowInfo( const QString& windowTitle )
{
	m_windowFullTitlesInfo.push_back( BlacklistWindowInfo( windowTitle.toLower() ) );
}

void BlacklistedProcessDescription::AddPartialTitleWindowInfo( const QString& windowTitle, bool searchSubstringFromBegin )
{
	m_windowPartTitlesInfo .push_back( BlacklistWindowInfo( windowTitle.toLower(), searchSubstringFromBegin ) );
}

void BlacklistedProcessDescription::AddFullTitleWindowInfo( const QString& windowTitle, const QString& windowClassName )
{
	m_windowFullTitlesInfo.push_back( BlacklistWindowInfo( windowTitle.toLower(), windowClassName ) );
}

void BlacklistedProcessDescription::AddPartialTitleWindowInfo( const QString& windowTitlePart, const QString& windowClassName, bool searchSubstringFromBegin )
{
	m_windowPartTitlesInfo.push_back( BlacklistWindowInfo( windowTitlePart.toLower(), windowClassName, searchSubstringFromBegin ) );
}

void BlacklistedProcessDescription::AddFullTitleWindowInfo( const QString& windowTitle, const long winStyle )
{
	m_windowFullTitlesInfo.push_back( BlacklistWindowInfo( windowTitle.toLower(), winStyle ) );
}

void BlacklistedProcessDescription::AddPartialTitleWindowInfo( const QString& windowTitlePart, const long winStyle, bool searchSubstringFromBegin )
{
	m_windowPartTitlesInfo.push_back( BlacklistWindowInfo( windowTitlePart.toLower(), winStyle, searchSubstringFromBegin ) );
}

void BlacklistedProcessDescription::AddFullTitleWindowInfo( const QString& windowTitle, const QString& windowClassName, const long winStyle )
{
	m_windowFullTitlesInfo.push_back( BlacklistWindowInfo( windowTitle.toLower(), windowClassName, winStyle ) );
}

void BlacklistedProcessDescription::AddPartialTitleWindowInfo( const QString& windowTitlePart, const QString& windowClassName, const long winStyle, bool searchSubstringFromBegin )
{
	m_windowPartTitlesInfo.push_back( BlacklistWindowInfo( windowTitlePart.toLower(), windowClassName, winStyle, searchSubstringFromBegin ) );
}

bool BlacklistedProcessDescription::IsWindowTitlePresent( HWND hWnd, QString windowCaption )
{
	foreach( BlacklistWindowInfo winInfo, m_windowFullTitlesInfo )
	{
		if( ( windowCaption == winInfo.Text() )
			&& winInfo.CheckWindowAttributes( hWnd ) )
			return true;
	}

	// Check in partial titles list
	foreach( BlacklistWindowInfo winInfo, m_windowPartTitlesInfo )
	{
		if( winInfo.IsSearchSubstringfromBegin() )
		{
			if( windowCaption.startsWith( winInfo.Text() )
				&& winInfo.CheckWindowAttributes( hWnd ) )
				return true;
		}
		else
		{
			if( windowCaption.contains( winInfo.Text() )
				&& winInfo.CheckWindowAttributes( hWnd ) )
				return true;
		}
	}

	return false;
}

bool BlacklistedProcessDescription::ContainsWindowsTitles()
{
	return ( m_windowFullTitlesInfo.size() > 0 ) || ( m_windowPartTitlesInfo.size() > 0 );
}

// ------------------------------------------------------------------------------------

QList< BlacklistedProcessDescription* > BlacklistedProcessesDatabase::m_processesInfo;

// Database initialization and cleanup
void BlacklistedProcessesDatabase::InitializeProcessesDatabase()
{
	// 2. CamStudio http://camstudio.org/
	// Version 2.0
	BlacklistedProcessDescription* info = new BlacklistedProcessDescription( PROTECT_STR( "CamStudio" ), 2 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "CamStudio" ));
	m_processesInfo.push_back( info );

	// 3. Tanida Demo Builder http://www.demo-builder.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Tanida Demo Builder" ), 3 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Tanida Demo Builder" ));
	m_processesInfo.push_back( info );

	// 4. ViewletCam 2    http://www.qarbon.com/presentation-software/vc/
	info = new BlacklistedProcessDescription( PROTECT_STR( "ViewletCam2" ), 4 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "ViewletCam2" ));
	m_processesInfo.push_back( info );

	// 5. Techsmith Camtasia Studio 3.1 http://www.techsmith.com/ - can not open process !!?
	info = new BlacklistedProcessDescription( PROTECT_STR( "Techsmith Camtasia Recorder" ), 5 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "CamRecorder" ));
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Camtasia Recorder" ));
	m_processesInfo.push_back( info );

	// 6. Snipping Tool, does not work for 64 bit version
	info = new BlacklistedProcessDescription( PROTECT_STR( "Microsoft Snipping Tool" ), PROTECT_STR( "snippingtool.exe" ), 6  );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Snipping Tool" ));
	m_processesInfo.push_back( info );

	// 7. Wink http://www.debugmode.com/wink/
	info = new BlacklistedProcessDescription( PROTECT_STR( "DebugMode Wink" ), PROTECT_STR( "wink.exe" ), 7 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Wink" ));
	m_processesInfo.push_back( info );

	// 9. SnagIT, http://www.techsmith.com/snagit/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Techsmith SnagIt" ), 9 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Snagit" ));
	m_processesInfo.push_back( info );

	// 10. Expression Encoder 4 Pro Screen Capture http://www.microsoft.com/expression/products/EncoderPro_Overview.aspx
	info = new BlacklistedProcessDescription( PROTECT_STR( "Microsoft Expression Encoder Screen Capture" ), 10 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Screen Capture" ));
	m_processesInfo.push_back( info );

	// 11. 5 Clicks http://www.screen-capture.net/
	info = new BlacklistedProcessDescription( PROTECT_STR( "DB3nf 5 Clicks" ), PROTECT_STR( "5Clicks.exe" ), 11 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "5 Clicks" ));
	m_processesInfo.push_back( info );

	// 12. 12Ghosts Clip http://12g.com/ghosts/clip.htm/
	info = new BlacklistedProcessDescription( PROTECT_STR( "12Ghosts™ Clip" ), PROTECT_STR( "12clip.exe" ), 12 );
	m_processesInfo.push_back( info );

	// 14. Ashampoo Magical Snap http://www.ashampoo.com/ru/rub/pin/0124/Offline/Ashampoo-Magical-Snap
	info = new BlacklistedProcessDescription( PROTECT_STR( "Ashampoo Snap" ), 13 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Ashampoo" ), PROTECT_STR( "Ashampoo" ), 0x86000000, true );
	m_processesInfo.push_back( info );

	// 15. AutoScreen .NET http://dmitri.3dn.ru/blog/2007-12-06-1/
	info = new BlacklistedProcessDescription( PROTECT_STR( "AutoScreen .NET" ), PROTECT_STR( "AutoScreen.exe" ), 14 );
	m_processesInfo.push_back( info );

	// 16. ALLCapture Enterprise (V3.0) http://www.balesio.com/allcapture/eng/index.php/
	info = new BlacklistedProcessDescription( PROTECT_STR( "ALLCapture Enterprise" ), 15 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "ALLCapture Enterprise" ));
	m_processesInfo.push_back( info );

	// 17. Capture By George http://www.svet-soft.com/capture.shtml/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Capture By George!" ), 16 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Capture By George" ));
	m_processesInfo.push_back( info );

	// 18. Capturelib Screen Recorder http://www.capturelib.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Capturelib Screen Recorder" ), 17 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Capturelib Screen Recorder" ));
	m_processesInfo.push_back( info );

	// 19. Capturemation http://www.capturemation.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Capturemation" ), PROTECT_STR( "cap.exe" ), 18 );
	m_processesInfo.push_back( info );

	// 20. CapturePad http://www.capturepad.com/html/capturepad1.htm/
	info = new BlacklistedProcessDescription( PROTECT_STR( "CapturePad" ), 19 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "CapturePad" ));
	m_processesInfo.push_back( info );

	// 21. CodeCharge Studio http://www.yessoftware.com/index2.php/
	info = new BlacklistedProcessDescription( PROTECT_STR( "CodeCharge Studio" ), 20 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "CodeCharge Studio" ));
	m_processesInfo.push_back( info );

	// 22. ComponentOne DemoWorks http://www.componentsource.com/products/componentone-demoworks/index.html/
	info = new BlacklistedProcessDescription( PROTECT_STR( "ComponentOne DemoWorks" ), 21 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "ComponentOne DemoWorks" ));
	m_processesInfo.push_back( info );

	// 23. Crazy Boomerang Screen Shot http://www.crazyboomerang.com/crazy-boomerang-screen-shot.html/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Crazy Boomerang Screen Shot" ), 22 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Crazy Boomerang Screen Shot" ));
	m_processesInfo.push_back( info );

	// 25. Desktop Activity Recorder http://desktop-activity-recorder.en.softonic.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Desktop Activity Recorder" ), 24 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Desktop Activity Recorder" ));
	m_processesInfo.push_back( info );

	// 26. DScaler http://deinterlace.sourceforge.net/
	info = new BlacklistedProcessDescription( PROTECT_STR( "DScaler" ), 25 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "DScaler" ));
	m_processesInfo.push_back( info );

	// 28. FlashDemo Studio http://www.flashdemo.net/flashdemo.html/
	info = new BlacklistedProcessDescription( PROTECT_STR( "FlashDemo Studio" ), PROTECT_STR( "FlashDemo.exe" ), 27 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "FlashDemo" ));
	m_processesInfo.push_back( info );

	// 29. Floomby http://floomby.ru/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Floomby" ), 28 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Floomby" ));
	m_processesInfo.push_back( info );

	// 30. Fraps http://www.fraps.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Fraps" ), 29 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Fraps" ));
	m_processesInfo.push_back( info );

	// 32. Growler Gun Cam http://www.growlersoftware.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Growler Gun Cam" ), 30 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Growler Gun Cam" ));
	m_processesInfo.push_back( info );

	// 33. HandySnap http://www.wisepixel.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "HandySnap" ), PROTECT_STR( "HandySnap.exe" ), 31 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Screenshot" ));
	m_processesInfo.push_back( info );

	// 34. HyperCam http://www.hyperionics.com/hc/
	info = new BlacklistedProcessDescription( PROTECT_STR( "HyperCam" ), 32 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "HyperCam" ));
	m_processesInfo.push_back( info );

	// 36. Instant Demo http://www.instant-demo.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Instant Demo" ), 35 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Instant Demo" ));
	m_processesInfo.push_back( info );

	// 37. Jing http://www.techsmith.com/jing/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Jing" ), PROTECT_STR( "Jing.exe" ), 36 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Jing" ));
	m_processesInfo.push_back( info );

	// 39. Longtion FlashDemo Pro http://www.longtion.com/flashdemopro/flashdemopro.htm/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Longtion FlashDemo Pro" ), 38 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "FlashDemo Pro" ));
	m_processesInfo.push_back( info );

	// 40. Lotus ScreenCam by IBM http://www.smartguyz.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Lotus ScreenCam by IBM" ), 39 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "ScreenCam" ));
	m_processesInfo.push_back( info );

	// 41. Magic Screenshot http://www.magicscreenshot.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Magic Screenshot" ), 40 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Magic Screenshot" ));
	m_processesInfo.push_back( info );

	// 42. PowerShrink MagiCapture http://www.magicapture.com/index.php?id=24/
	info = new BlacklistedProcessDescription( PROTECT_STR( "PowerShrink MagiCapture" ), PROTECT_STR( "MagiCapture.exe" ), 41 );
	m_processesInfo.push_back( info );

	// 43. MiniCapture http://www.ikicsoft.com/MiniCapture.html/
	info = new BlacklistedProcessDescription( PROTECT_STR( "MiniCapture" ), 42 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "MiniCapture" ));
	m_processesInfo.push_back( info );

	// 44. ScreenVirtuoso Mr. Captor http://www.fox-magic.com/screenvirtuoso_mrcaptor.php#/
	info = new BlacklistedProcessDescription( PROTECT_STR( "ScreenVirtuoso Mr. Captor" ), 43 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "ScreenVirtuoso" ));
	m_processesInfo.push_back( info );

	// 45. Open Video Capture http://www.008soft.com/products/video-capture.htm
	info = new BlacklistedProcessDescription( PROTECT_STR( "Open Video Capture" ), 44 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Open Video Capture" ));
	m_processesInfo.push_back( info );

	// 47. PixClip http://www.pixclip.net/
	info = new BlacklistedProcessDescription( PROTECT_STR( "PixClip" ), 46 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "PixClip" ));
	m_processesInfo.push_back( info );

	// 48. PrintKey http://www.warecentral.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "PrintKey" ), 47 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "PrintKey-Pro" ));
	m_processesInfo.push_back( info );

	// 49. PrtScr http://www.fiastarta.com/PrtScr/
	info = new BlacklistedProcessDescription( PROTECT_STR( "PrtScr" ), PROTECT_STR( "PrtScr.exe" ), 48 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "PrtScr" ));
	m_processesInfo.push_back( info );

	// 50. Screen Shot Maker http://www.fastosoft.ru/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Screen Shot Maker" ), 49 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Screenshot maker" ));
	m_processesInfo.push_back( info );

	// 51. River Past Screen Recorder http://www.riverpast.com/en/prod/screenrecorder/
	info = new BlacklistedProcessDescription( PROTECT_STR( "River Past Screen Recorder" ), 50 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "River Past Screen Recorder" ));
	m_processesInfo.push_back( info );

	// 52. Screenshot Creator http://www.softportal.com/software-5454-screenshot-creator.html/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Screenshot Creator" ), 51 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Screenshot Creator" ));
	m_processesInfo.push_back( info );

	// 53. Simple Screenshot http://www.das-download-archiv.de/software_71_download_simple-screenshot.html/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Simple Screenshot" ), 52 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "SimpleScreenshot" ));
	m_processesInfo.push_back( info );

	// 53.1. Simple Screenshot http://pion-nt.ru/2472-simple-screenshot-105a.html/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Simple Screenshot" ), 53 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Simple Screenshot" ));
	m_processesInfo.push_back( info );

	// 54. Tanida Quiz Builder http://www.quiz-builder.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Tanida Quiz Builder" ), PROTECT_STR( "qb.exe" ), 54 );
	m_processesInfo.push_back( info );

	// 55. TurboDemo & Turbo Demo http://www.turbodemo.com/eng/index.php/
	info = new BlacklistedProcessDescription( PROTECT_STR( "TurboDemo & Turbo Demo" ), 55 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "TurboDemo" ));
	m_processesInfo.push_back( info );

	// 56. UserMonitor http://www.neuber.com/usermonitor/download.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "UserMonitor (Neuber Software)" ), 56 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "UserMonitor" ));
	m_processesInfo.push_back( info );

	// 57. uvScreenCamera http://uvsoftium.ru/
	info = new BlacklistedProcessDescription( PROTECT_STR( "uvScreenCamera" ), 57 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "UVScreenCamera" ));
	m_processesInfo.push_back( info );

	// 59. Wondershare DemoCreato http://www.sameshow.com/demo-creator.html/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Wondershare DemoCreato" ), 59 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Wondershare DemoCreator" ));
	m_processesInfo.push_back( info );

	// 60. X-fire http://www.xfire.com/about_video/
	info = new BlacklistedProcessDescription( PROTECT_STR( "X-fire" ), 60 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Xfire" ));
	m_processesInfo.push_back( info );

	// 61. 1AVCapture http://www.pcwinsoft.com/1avcapture/
	info = new BlacklistedProcessDescription( PROTECT_STR( "1AVCapture" ), PROTECT_STR( "1AVCapture.exe" ), 61 );
	m_processesInfo.push_back( info );

	// 62. 3D Desktop Recorder http://www.dvdwindow.com/3dgame/
	info = new BlacklistedProcessDescription( PROTECT_STR( "3D Desktop Recorder" ), PROTECT_STR( "qq.exe" ), 62 );
	m_processesInfo.push_back( info );

	// 63. Able Video Snapshot http://www.graphicregion.com/videosnapshot.htm
	info = new BlacklistedProcessDescription( PROTECT_STR( "Able Video Snapshot" ), 63 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Able Video Snapshot" ));
	m_processesInfo.push_back( info );

	// 64. ACA Screen Recorder http://www.acasystems.com/en/screenrecorder/
	info = new BlacklistedProcessDescription( PROTECT_STR( "ACA Screen Recorder" ), 64 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "ACA Screen Recorder" ));
	m_processesInfo.push_back( info );

	// 65. Acala Screen Recorder http://www.cutedvd.com/html/free_screen_recorder.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Acala Screen Recorder" ), 65 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Acala Screen Recorder" ));
	m_processesInfo.push_back( info );

	// 66. AimOne Screen Recorder http://www.aimonesoft.com/screenrecorder.html/
	info = new BlacklistedProcessDescription( PROTECT_STR( "AimOne Screen Recorder" ), 66 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "AimOne Screen Recorder" ));
	m_processesInfo.push_back( info );

	// 67. Altarsoft Video Capture http://rus.altarsoft.com/altarsoft_video_capture.shtml/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Altarsoft Video Capture" ), 67 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Altarsoft Video Capture" ));
	m_processesInfo.push_back( info );

	// 68. AMCap http://www.afterdawn.com/software/audio_video/capture_video/amcap.cfm
	info = new BlacklistedProcessDescription( PROTECT_STR( "AMCap" ), 68 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "AMCap" ));
	m_processesInfo.push_back( info );

	// 69. AutoScreenRecorder Pro http://www.softpedia.com/progDownload/AutoScreenRecorder-Pro-Download-41675.html/
	info = new BlacklistedProcessDescription( PROTECT_STR( "AutoScreenRecorder Pro" ), 69 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "AutoScreenRecorder" ));
	m_processesInfo.push_back( info );

	// 70. AVD Video Processor http://www.avlandesign.com/vp.htm/
	info = new BlacklistedProcessDescription( PROTECT_STR( "AVD Video Processor" ), 70 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "AVD Video Processor" ));
	m_processesInfo.push_back( info );

	// 71. AviScreen Classic http://www.banksofta.ru/downloadsoft/MTI1/
	info = new BlacklistedProcessDescription( PROTECT_STR( "AviScreen Classic" ), 71 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "AviScreen Classic" ));
	m_processesInfo.push_back( info );

	// 73. Breeze Standard Edition http://www.onlinedown.com/detail/18329.htm#download
	info = new BlacklistedProcessDescription( PROTECT_STR( "Breeze" ), PROTECT_STR( "Breeze.exe" ), 73 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Breeze" ));
	m_processesInfo.push_back( info );

	// 74. BSR Screen Recorder http://www.bsrsoft.com/downloadbsr.asp
	info = new BlacklistedProcessDescription( PROTECT_STR( "BSR Screen Recorder" ), 74 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "BSR Screen Recorder" ));
	m_processesInfo.push_back( info );

	// 75. Capturex http://www.afreecodec.com/windows/capturex-download-47375.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Capturex" ), 75 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Capturex" ));
	m_processesInfo.push_back( info );

	// 76. Cute Screen Recorder Free http://www.softpedia.com/progDownload/Cute-Screen-Recorder-Download-167489.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Cute Screen Recorder Free" ), 76 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Cute Screen Recorder" ));
	m_processesInfo.push_back( info );

	// 77. Debut Video Capture http://www.nchsoftware.com/capture/index.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Debut Video Capture" ), PROTECT_STR( "debut.exe" ), 77 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Debut" ));
	m_processesInfo.push_back( info );

	// 79. Desktop Video Recorder http://www.softpedia.com/get/Multimedia/Video/Video-Recording/Desktop-Video-Recorder.shtml
	info = new BlacklistedProcessDescription( PROTECT_STR( "Desktop Video Recorder" ), 79 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Desktop Video Recorder" ));
	m_processesInfo.push_back( info );

	// 80. Easy Screen Capture http://www.longfine.com/, http://download.cnet.com/Easy-Screen-Capture-And-Annotation/3000-2192_4-10515954.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Easy Screen Capture" ), 80 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Easy Screen Capture" ));
	m_processesInfo.push_back( info );

	// 81. Easy Screen Recorder http://www.easyscreenrecorder.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Easy Screen Recorder" ), 81 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Easy Screen Recorder" ));
	m_processesInfo.push_back( info );

	// 82. Easy Video Capture http://www.video-capture.info/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Easy Video Capture" ), 82 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Easy Video Capture" ));
	m_processesInfo.push_back( info );

	// 83. EZV Video Capture http://www.soft32.com/windows/video/video-capture/ezv-video-capture
	info = new BlacklistedProcessDescription( PROTECT_STR( "EZV Video Capture" ), 83 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "EZV" ));
	m_processesInfo.push_back( info );

	// 84. Fast Video Indexer http://www.fastvideoindexer.com/index2.htm
	info = new BlacklistedProcessDescription( PROTECT_STR( "Fast Video Indexer" ), 84 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Fast Video Indexer" ));
	m_processesInfo.push_back( info );

	// 85. Free Screen Video Capture http://www.superdownloads.com.br/download/9/free-screen-video-capture/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Free Screen Video Capture" ), 85 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Free Screen Video Capture" ));
	m_processesInfo.push_back( info );

	// 86. Free VeeCool Video Capture http://download.cnet.com/Free-VeeCool-Video-Capture/3000-13633_4-75449937.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Free VeeCool Video Capture" ), 86 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Free VeeCool Video Capture" ));
	m_processesInfo.push_back( info );

	// 87. Grabilla http://grabilla.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Grabilla" ), 87 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Grabilla" ));
	m_processesInfo.push_back( info );

	// 89. Mediachase Screen Capture http://www.topdownloadseeker.com/Audio-and-Video/Other-Audio--and--Video/Mediachase-Screen-Capture-4.5.39.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Mediachase Screen Capture" ), 89 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Mediachase ScreenCapture" ));
	m_processesInfo.push_back( info );

	// 90. Micro Video Capture http://www.microvideosoft.com/micro_video_capture/index.htm
	info = new BlacklistedProcessDescription( PROTECT_STR( "Micro Video Capture" ), 90 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Micro Video Capture" ));
	m_processesInfo.push_back( info );

	// 91. Movavi Screen Capture http://www.movavi.ru/screen-capture/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Movavi Screen Capture" ), PROTECT_STR( "ScreenCapture.exe" ), 91 );
	m_processesInfo.push_back( info );

	// 92. MsDVR 2005 http://www.maksil.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "MsDVR 2005" ), 92 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "MsDVR 2005" ));
	m_processesInfo.push_back( info );

	// 93. My Screen Recorder http://www.deskshare.com/screen-recorder.aspx
	info = new BlacklistedProcessDescription( PROTECT_STR( "My Screen Recorder" ), 93 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "My Screen Recorder" ));
	m_processesInfo.push_back( info );

	// 93.1 My Screen Recorder http://www.deskshare.com/screen-recorder.aspx
	info = new BlacklistedProcessDescription( PROTECT_STR( "My Screen Recorder" ), 94 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "My Screen Recorder" ));
	m_processesInfo.push_back( info );

	// 94. Power Screen Capture http://download.cnet.com/Power-Screen-Capture/3000-13633_4-10895532.html?part=dl-10044195&subj=dl&tag=button
	info = new BlacklistedProcessDescription( PROTECT_STR( "Power Screen Capture" ), 95 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Power Screen Capture" ));
	m_processesInfo.push_back( info );

	// 96. Screen Recorder Gold http://www.capture-screen.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Screen Recorder Gold" ), 97 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Screen Recorder Gold" ));
	m_processesInfo.push_back( info );

	// 97. Screen Recorder Suite http://www.screenrecord.net/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Screen Recorder Suite" ), 98 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Screen Recording Suite" ));
	m_processesInfo.push_back( info );

	// 98. Screen Video Recorder http://www.wordaddin.com/screenvcr/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Screen Video Recorder" ), 100 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Screen Video Recorder" ));
	m_processesInfo.push_back( info );

	// 99. MatchWare ScreenCorder http://www.matchware.com/en/products/screencorder/default.htm
	info = new BlacklistedProcessDescription( PROTECT_STR( "MatchWare ScreenCorder" ), 101 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "MatchWare ScreenCorder" ));
	m_processesInfo.push_back( info );

	// 100. ScreenGrabber http://www.softportal.com/software-19727-screen-grabber.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "ScreenGrabber" ), 102 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Screen Grabber" ));
	m_processesInfo.push_back( info );

	// 101. ScreenVirtuoso PRO http://protoplex.ru/soft/?showid=6095
	info = new BlacklistedProcessDescription( PROTECT_STR( "ScreenVirtuoso PRO" ), 103 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "ScreenVirtuoso PRO" ));
	m_processesInfo.push_back( info );

	// 102. SMRecorder http://www.video2down.com/index.php/documentation/87-capture-audiovideo
	info = new BlacklistedProcessDescription( PROTECT_STR( "SMRecorder" ), 104 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "smrecorder" ));
	m_processesInfo.push_back( info );

	// 103. Sonne Screen Video Capture http://sonnesoftware.com/sonne_screen_video_capture/index.htm
	info = new BlacklistedProcessDescription( PROTECT_STR( "Sonne Screen Video Capture" ), 105 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Sonne Screen Video Capture" ));
	m_processesInfo.push_back( info );

	// 104. Super Screen Recorder http://www.acasystems.com/en/screenrecorder/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Super Screen Recorder" ), 106 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Super Screen Recorder" ));
	m_processesInfo.push_back( info );

	// 105. SuperVideoCap http://www.xmediasoft.ru/products/512-supervideocap.aspx
	info = new BlacklistedProcessDescription( PROTECT_STR( "SuperVideoCap" ), 107 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "SuperVideoCap" ));
	m_processesInfo.push_back( info );

	// 106. TipCam http://www.utipu.com/tipcam-download/
	info = new BlacklistedProcessDescription( PROTECT_STR( "TipCam" ), 108 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "TipCam" ));
	m_processesInfo.push_back( info );

	// 107. Video Capture Factory http://www.videocapturefactory.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Video Capture Factory" ), 109 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Video Capture Factory" ));
	m_processesInfo.push_back( info );

	// 108. Video Capture Master http://www.videocapturemaster.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Video Capture Master" ), 110 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Video Capture Master" ));
	m_processesInfo.push_back( info );

	// 109. Video Capturix 2011 http://www.capturix.com/default.asp?target=consumer&product=vcap
	info = new BlacklistedProcessDescription( PROTECT_STR( "Video Capturix 2011" ), 111 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Video Capturix" ));
	m_processesInfo.push_back( info );

	// 110. Video Snapshot Genius http://www.acasystems.com/en/screenrecorder/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Video Snapshot Genius" ), 112 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Video Snapshot Genius" ));
	m_processesInfo.push_back( info );

	// 111. VirtualDub http://virtualdub.sourceforge.net/
	info = new BlacklistedProcessDescription( PROTECT_STR( "VirtualDub" ), 113 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "VirtualDub" ));
	m_processesInfo.push_back( info );

	// 113. Webcam and Screen Recorder http://www.softpedia.com/get/Multimedia/Video/Video-Recording/Webcam-and-Screen-Recorder.shtml
	info = new BlacklistedProcessDescription( PROTECT_STR( "Webcam and Screen Recorder" ), 115 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "WebcamAndScreenRecorder" ));
	m_processesInfo.push_back( info );

	// 114. Webinaria http://download.cnet.com/Webinaria/3000-13633_4-10705452.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Webinaria" ), 116 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "webinaria" ));
	m_processesInfo.push_back( info );

	// 115. WM Capture http://wmrecorder.com/products/wm-capture/
	info = new BlacklistedProcessDescription( PROTECT_STR( "WM Capture" ), 117 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "WM Capture" ) );
	m_processesInfo.push_back( info );

	// 116. WM Recorder http://wmrecorder.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "WM Recorder" ), 118 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "WM Recorder" ));
	m_processesInfo.push_back( info );

	// 117. YouRecorder http://www.yourecorder.com/en.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "YouRecorder" ), 119 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "YouRecorder" ));
	m_processesInfo.push_back( info );

	// 118. ZD Soft Video Recorder http://www.zdsoft.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "ZD Soft Video Recorder" ), 120 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "ZD Soft Screen Recorder" ));
	m_processesInfo.push_back( info );

	// 119. Zebra Screen Recorder http://www.zebra-media.com/zebrascreenrecorder.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Zebra Screen Recorder" ), 121 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Zebra Screen Recorder" ));
	m_processesInfo.push_back( info );

	// 120. Screen Capture + Print http://www.infonautics.ch/screencaptureprint/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Screen Capture + Print" ), 122 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Screen Capture + Print" ));
	m_processesInfo.push_back( info );

	// 121. FrameShots http://www.frame-shots.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "FrameShots" ), 123 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "FrameShots - Video Frame Capture" ));
	m_processesInfo.push_back( info );

	// 122. VideoGet http://www.izone.ru/internet/downloads/videoget.htm
	info = new BlacklistedProcessDescription( PROTECT_STR( "VideoGet" ), 124 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "VideoGet" ));
	m_processesInfo.push_back( info );

	// 123. Topaz Moment http://www.topazlabs.com/moment/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Topaz Moment" ), 125 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Topaz Moment" ));
	m_processesInfo.push_back( info );

	// 124. VH Capture http://www.hmelyoff.com/index.php?section=22
	info = new BlacklistedProcessDescription( PROTECT_STR( "VH Capture" ), 126 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "VHCapture" ));
	m_processesInfo.push_back( info );

	// 124. Windows 7 Snipping Tool
	info = new BlacklistedProcessDescription( PROTECT_STR( "Snipping Tool" ), PROTECT_STR( "SnippingTool.exe" ), 127 );
	m_processesInfo.push_back( info );

	///////////////////////////////////////////////
	// 125. 1AVCenter http://www.pcwinsoft.com/1avcenter/
	info = new BlacklistedProcessDescription( PROTECT_STR( "1AVCenter" ), 128 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "1AVCenter" ));
	m_processesInfo.push_back( info );

	// 126. CaptureWiz  http://www.pixelmetrics.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "CaptureWiz" ), PROTECT_STR( "CAPTUREWIZ.exe" ), 129 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Capture" ));
	m_processesInfo.push_back( info );

	// 127. HyperCam 3 MainDialog  http://www.hyperionics.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "HyperCam" ), PROTECT_STR( "SMM_HYPERCAM.exe" ), 130 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "HyperCam" ));
	m_processesInfo.push_back( info );

	// 128. PCHand Screen Recorder
	info = new BlacklistedProcessDescription( PROTECT_STR( "HyperCam 3 MainDialog" ), 131 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "PCHand Screen Recorder" ));
	m_processesInfo.push_back( info );

	// 129. ScreenshotMaker v8.1 Professional
	info = new BlacklistedProcessDescription( PROTECT_STR( "ScreenshotMaker Professional" ), 132 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "ScreenshotMaker" ));
	m_processesInfo.push_back( info );

	// 130. Camtasia Studio
	info = new BlacklistedProcessDescription( PROTECT_STR( "Camtasia Studio" ), 133 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Camtasia Studio" ));
	info->AddPartialTitleWindowInfo( PROTECT_STR( "CamtasiaStudio" ));
	m_processesInfo.push_back( info );

	// 131. Webcam Video Capture 7.0
	info = new BlacklistedProcessDescription( PROTECT_STR( "Webcam Video Capture" ), 134 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Webcam Video Capture" ));
	m_processesInfo.push_back( info );

	// 132. SCAPTURE 0.8 http://solron.ucoz.com/index/scapture/0-4
	info = new BlacklistedProcessDescription( PROTECT_STR( "SCAPTURE" ), 135 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "SCapture" ));
	m_processesInfo.push_back( info );

	// 133. Boilsoft Screen Recorder http://www.boilsoft.com/screenrecorder/index.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Boilsoft Screen Recorder" ), 136 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Boilsoft Screen Recorder" ));
	m_processesInfo.push_back( info );

	// 134. Screenpresso http://www.screenpresso.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Screenpresso" ), PROTECT_STR( "SCREENPRESSO.exe" ), 137 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Screenpresso" ) );
	m_processesInfo.push_back( info );

	// 135. Total Screen Recorder Standard http://www.totalscreenrecorder.com/download.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Total Screen Recorder Standard" ), 138 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Total Screen Recorder" ));
	m_processesInfo.push_back( info );

	// 136. Bandicam http://www.bandicam.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Bandicam" ), 139 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Bandicam" ));
	m_processesInfo.push_back( info );

	// 138. Shotty http://shotty.devs-on.net/en/Download.aspx
	info = new BlacklistedProcessDescription( PROTECT_STR( "Shotty" ), PROTECT_STR( "SHOTTY.exe" ), 140 );
	m_processesInfo.push_back( info );

	// 139. Free Screen Video Recorder http://www.softportal.com/software-17774-free-screen-video-recorder.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Free Screen Video Recorder" ), 141 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Free Screen Video Recorder" ));
	m_processesInfo.push_back( info );

	// 140. Jing http://jingproject.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Jing" ), PROTECT_STR( "JING.exe" ), 142 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Jing" ));
	m_processesInfo.push_back( info );

	// 141. Windows Snapshot Grabber http://www.batchwork.com/wingrab/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Windows Snapshot Grabber" ), 143 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Windows Snapshot Grabber" ));
	m_processesInfo.push_back( info );

	// 142. Screenshot Captor http://www.donationcoder.com/Software/Mouser/screenshotcaptor/index.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Screenshot Captor" ), 144 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Screenshot Captor" ));
	m_processesInfo.push_back( info );

	// 143. PicPick http://www.picpick.org/
	info = new BlacklistedProcessDescription( PROTECT_STR( "PicPick" ), PROTECT_STR( "PICPICK.exe" ), 145 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "PicPick" ) );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Pic Pick" ) );
	m_processesInfo.push_back( info );

	// 144. BB FlashBack http://www.bbsoftware.co.uk/BBFlashBack.aspx
	info = new BlacklistedProcessDescription( PROTECT_STR( "BB FlashBack" ), 146 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "BB FlashBack" ));
	info->AddPartialTitleWindowInfo( PROTECT_STR( "FlashBack" ) );
	m_processesInfo.push_back( info );

	// 145. D3DGear http://www.d3dgear.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "D3DGear" ), 147 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "D3DGear" ));
	m_processesInfo.push_back( info );

	// 146. Kahlown http://www.brothersoft.com/kahlown-440843.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Kahlown" ), 148 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Kahlown screen spy monitor" ));
	m_processesInfo.push_back( info );

	// 148. oCam http://ohsoft.net/download.php
	info = new BlacklistedProcessDescription( PROTECT_STR( "oCam" ), PROTECT_STR( "OCAM.exe" ), 150 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "oCam" ) );
	m_processesInfo.push_back( info );

	// 150. Replay Video Capture http://applian.com/replay-video-capture
	info = new BlacklistedProcessDescription( PROTECT_STR( "Replay Video Capture" ), PROTECT_STR( "ReplayVideo.exe" ), 152 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Replay Video" ) );
	m_processesInfo.push_back( info );

	// 152. HyperSnap http://www.hyperionics.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "HyperSnap" ), 154 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "HyperSnap" ));
	m_processesInfo.push_back( info );

	// 153. FastStone Capture http://www.faststone.org/
	info = new BlacklistedProcessDescription( PROTECT_STR( "FastStone Capture" ), 155 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "FastStone" ));
	m_processesInfo.push_back( info );

	// 154. Odin Screen Capture http://www.odinshare.com/download.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Odin Screen Capture" ), 156 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Odin Screen Capture" ));
	m_processesInfo.push_back( info );

	// 155. Total Recorder VideoPro Edition http://www.highcriteria.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Total Recorder" ), 157 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Total Recorder" ));
	m_processesInfo.push_back( info );

	// 157. Microsoft OneNote 2010 http://office.microsoft.com/ru-ru/onenote/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Microsoft OneNote" ), 159 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "OneNote" ));
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Microsoft OneNote" ));
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Microsoft Office OneNote" ));
	m_processesInfo.push_back( info );

	// 159. Greenshot http://getgreenshot.org/
	info = new BlacklistedProcessDescription( PROTECT_STR( "Greenshot - screenshot utility" ), 161 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Greenshot" ));
	m_processesInfo.push_back( info );

	// 160. SnapCrab http://www.fenrir-inc.com/us/snapcrab/
	info = new BlacklistedProcessDescription( PROTECT_STR( "SnapCrab for Windows" ), 162 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Snapcrab" ));
	info->AddPartialTitleWindowInfo( PROTECT_STR( "SnapCrab" ));
	m_processesInfo.push_back( info );

	// 161. Recordzilla http://www.softdivshareware.com/recordzilla-screen-recorder.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Recordzilla" ), 163 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Recordzilla" ));
	m_processesInfo.push_back( info );

	// 162. ActivePresenter http://atomisystems.com/activepresenter/
	info = new BlacklistedProcessDescription( PROTECT_STR( "ActivePresenter" ), 164 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "ActivePresenter" ));
	m_processesInfo.push_back( info );

	//Circle 27
	// 163. HardCopy Pro http://www.desksoft.com/HardCopy.htm
	info = new BlacklistedProcessDescription( PROTECT_STR( "HardCopy Pro" ), 165 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "HardCopy Pro" ));
	m_processesInfo.push_back( info );

	// 164. ScreenSnag http://www.brothersoft.com/screensnag-download-498645-s1.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "ScreenSnag" ), 166 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "ScreenSnag" ));
	m_processesInfo.push_back( info );

	// FIMXE: this grabber falsely detected when jEdit editor launched
	// 165. Screencast-O-Matic http://soft.mydiv.net/win/files-Screencast-O-Matic.html
//	info = new BlacklistedProcessDescription( PROTECT_STR( "Screencast-O-Matic" ), 167 );
//	info->AddFullTitleWindowInfo( PROTECT_STR( "D3DFocusWindow" ));
//	m_processesInfo.push_back( info );

	// 166. SmartCapture http://soft.oszone.net/download/6073/SmartCapture.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "SmartCapture" ), PROTECT_STR( "SMARTCAPTURE.exe" ), 168 );
	m_processesInfo.push_back( info );

	// 167. Any Video Recorder http://www.anvsoft.com/any-video-recorder.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Any Video Recorder" ), 169 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Any Video Recorder" ));
	m_processesInfo.push_back( info );

	// 168. SnapShot http://bluefive.pair.com/snapshot.htm
	info = new BlacklistedProcessDescription( PROTECT_STR( "SnapShot" ), 170 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "SnapShot" ));
	m_processesInfo.push_back( info );

	// 169. Cropper http://cropper.codeplex.com/downloads/get/173114
	info = new BlacklistedProcessDescription( PROTECT_STR( "Cropper" ), PROTECT_STR( "CROPPER.exe" ), 171 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Cropper" ));
	m_processesInfo.push_back( info );

	// 170. Screen Face Cam http://screenfacecam.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "ScreenFaceCam" ), 172 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "ScreenFaceCam" ));
	m_processesInfo.push_back( info );

	// 171. Postimage http://postimage.org/app.php
	info = new BlacklistedProcessDescription( PROTECT_STR( "Postimage" ), PROTECT_STR( "POSTIMAGE.exe" ), 173 );
	m_processesInfo.push_back( info );

	// 172. Clip2Net http://clip2net.com/ru/
	info = new BlacklistedProcessDescription( PROTECT_STR( "clip2net" ), 174 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Clip2Net" ));
	m_processesInfo.push_back( info );

	// 173. Easy Screenshot http://download.cnet.com/Easy-Screenshot/3000-2192_4-75914851.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Easy Screenshot" ), 175 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Easy Screenshot" ));
	m_processesInfo.push_back( info );

	// 174. RobotSoft Screen OCR http://download.cnet.com/RobotSoft-Screen-OCR/3000-20432_4-75913725.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "RobotSoft Screen OCR" ), 176 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "RobotSoft Screen OCR" ));
	m_processesInfo.push_back( info );

	// 175. SnipSnip http://download.cnet.com/SnipSnip/3000-6675_4-75905920.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "SnipSnip" ), 177 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "SnipSnip" ));
	m_processesInfo.push_back( info );

	// 176. BastaPix http://download.cnet.com/BastaPix/3000-2192_4-75453571.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "BastaPix" ), 178 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "BastaPix" ));
	m_processesInfo.push_back( info );

	// 177. Clarify download.cnet.com/Clarify/3000-2192_4-75595411.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Clarify" ), PROTECT_STR( "CLARIFY.exe" ), 179 );
	m_processesInfo.push_back( info );

	// 178. My Screen Capture http://download.cnet.com/My-Screen-Capture/3000-20432_4-75684177.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "My Screen Capture" ), 180 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "My Screen Capture" ));
	m_processesInfo.push_back( info );

	// 179. Snappy http://download.cnet.com/Snappy/3000-12511_4-75450483.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Snappy" ), PROTECT_STR( "SNAPPY.exe" ), 181 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Snappy" ));
	m_processesInfo.push_back( info );

	// 180. Screen Grab Pro Deluxe http://download.cnet.com/Screen-Grab-Pro-Deluxe/3000-2094_4-10690018.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Screen Grab Pro Deluxe" ), 182 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Screen Grab Pro" ));
	m_processesInfo.push_back( info );

	// 181. Capture Screenshot lite http://download.cnet.com/Capture-Screenshot-lite/3000-2192_4-75910581.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Capture Screenshot lite" ), 183 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Capture Screenshot lite" ));
	m_processesInfo.push_back( info );

	// 182. AC Picture Clicker http://download.cnet.com/AC-Picture-Clicker/3000-2084_4-75913564.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "AC Picture Clicker" ), 184 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "AC Picture Clicker" ));
	m_processesInfo.push_back( info );

	// 183. Bug Shooting http://download.cnet.com/Bug-Shooting/3000-2383_4-75326775.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Bug Shooting" ), 185 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Bug Shooting" ));
	m_processesInfo.push_back( info );

	// 184. Kleptomania http://www.structurise.com/kleptomania
	info = new BlacklistedProcessDescription( PROTECT_STR( "Kleptomania " ), PROTECT_STR( "KMANIA.exe" ), 186 );
	m_processesInfo.push_back( info );

	// 185. Kleptomania http://www.structurise.com/kleptomania
	info = new BlacklistedProcessDescription( PROTECT_STR( "Kleptomania " ), PROTECT_STR( "k-mania.exe" ), 187 );
	m_processesInfo.push_back( info );

	// 186. Screen Loupe 2000 http://download.cnet.com/Screen-Loupe-2000/3000-2192_4-10396164.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Screen Loupe 2000" ), PROTECT_STR( "LOUPE.exe" ), 188 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Loupe" ));
	m_processesInfo.push_back( info );

	// 187. Joxi http://joxi.ru/download
	info = new BlacklistedProcessDescription( PROTECT_STR( "Joxi" ), 189 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "joxi" ));
	m_processesInfo.push_back( info );

	// 188. Free Video Capture http://download.cnet.com/Free-Video-Capture/3000-13633_4-75915893.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Free Video Capture" ), 190 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Free Video Capture" ));
	m_processesInfo.push_back( info );

	// 189. Apowersoft Free Screen Recorder
	info = new BlacklistedProcessDescription( PROTECT_STR( "Apowersoft Free Screen Recorder" ), 191 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Apowersoft Free Screen Recorder" ));
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Apowersoft  Screen Recorder" ) );
	m_processesInfo.push_back( info );

	// 190. PrtScr Assistant http://download.cnet.com/PrtScr-Assistant/3000-20432_4-75761853.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "PrtScr Assistant" ), 192 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "PrtScr Assistant" ));
	m_processesInfo.push_back( info );

	// 191. Screenshoter
	info = new BlacklistedProcessDescription( PROTECT_STR( "Screenshoter" ), 193 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Screenshoter" ));
	m_processesInfo.push_back( info );

	// 193 Capture View http://download.cnet.com/Capture-View/3000-2192_4-10818836.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Capture View" ), PROTECT_STR( "SCREENCAPTURE.exe" ), 195 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Capture View" ));
	m_processesInfo.push_back( info );

	// 194 ShareX http://download.cnet.com/ShareX/3000-6675_4-75852614.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "ShareX" ), 196 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "ShareX" ));
	m_processesInfo.push_back( info );

	// 195 liteCamHD http://download.cnet.com/LiteCam-HD/3000-13633_4-10643835.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "liteCam" ), 197 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "liteCamHD" ));
	info->AddFullTitleWindowInfo( PROTECT_STR( "liteCam" ));
	m_processesInfo.push_back( info );

	// 196 VideoRecorder http://download.cnet.com/Free-Screen-Recorder/3000-13633_4-75893007.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Ainishare Screen Recorder" ), 198 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Ainishare Free Screen Recorder" ) );
	m_processesInfo.push_back( info );

	// 197. Super Screen Capture http://download.cnet.com/Super-Screen-Capture/3000-13633_4-10504831.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Super Screen Capture" ), 199 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Super Screen Capture" ));
	m_processesInfo.push_back( info );

	// 198. Free Screen Capture http://download.cnet.com/Free-Screen-Capture/3000-20418_4-76038565.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Free Screen Capture" ), 200 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Free Screen Capture" ));
	info->AddPartialTitleWindowInfo( PROTECT_STR( "revolutionary screenshot utility" ), false );
	m_processesInfo.push_back( info );

	// 199. Open Screen Recorder http://download.cnet.com/Open-Screen-Recorder/3000-13633_4-76091322.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Open Screen Recorder" ), 201 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Open Screen Recorder" ));
	m_processesInfo.push_back( info );

	// 200. Gadwin PrintScreen http://download.cnet.com/Gadwin-PrintScreen/3000-2094_4-10123018.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Gadwin PrintScreen" ), 202 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Gadwin PrintScreen" ));
	m_processesInfo.push_back( info );

	// 201. ScreenHunter http://www.wisdom-soft.com/products/screenhunter.htm
	info = new BlacklistedProcessDescription( PROTECT_STR( "ScreenHunter" ), 203 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "ScreenHunter" ));
	m_processesInfo.push_back( info );

	// 202. yascu http://www.yascu.com/
	info = new BlacklistedProcessDescription( PROTECT_STR( "yascu" ), 204 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "yascu" ));
	m_processesInfo.push_back( info );

	// 203. Video Snapshot Wizard http://download.cnet.com/Video-Snapshot-Wizard/3000-13633_4-10856004.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "Video Snapshot Wizard" ), 205 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "Video Snapshot Wizard" ));
	m_processesInfo.push_back( info );

	// 204. WinSnap http://www.ntwind.com/software/winsnap.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "WinSnap" ), 206 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "WinSnap" ) );
	m_processesInfo.push_back( info );

	// 205. GrabXP7 http://download.cnet.com/GrabXP7/3000-2193_4-10421037.html
	info = new BlacklistedProcessDescription( PROTECT_STR( "GrabXP7" ), 207 );
	info->AddFullTitleWindowInfo( PROTECT_STR( "GrabXP7" ) );
	m_processesInfo.push_back( info );

	// 206. Gyazo
	info = new BlacklistedProcessDescription( PROTECT_STR( "Gyazo" ), 208 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "GYAZO" ) );
	m_processesInfo.push_back( info );

	// 207. Puu.sh
	info = new BlacklistedProcessDescription( PROTECT_STR( "Puu.sh" ), 209 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "puush" ) );
	m_processesInfo.push_back( info );

	// 208. LightShot
	info = new BlacklistedProcessDescription( PROTECT_STR( "LightShot" ), 210 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "Lightshot_Tray_Wnd" ) );
	m_processesInfo.push_back( info );

	// 209. QIP Shot
	info = new BlacklistedProcessDescription( PROTECT_STR( "QIP Shot" ), 211 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "QIP Shot" ) );
	m_processesInfo.push_back( info );

	// 210. 3d Ripper
	info = new BlacklistedProcessDescription( PROTECT_STR( "3d Ripper" ), 212 );
	info->AddPartialTitleWindowInfo( PROTECT_STR( "3D RIPPER DX" ) );
	m_processesInfo.push_back( info );

#ifdef BUILD_DEBUG
	TestBlackListDatabase(); // Test unique report numbers
#endif
}

#ifdef BUILD_DEBUG
void BlacklistedProcessesDatabase::TestBlackListDatabase()
{
	QList< BlacklistedProcessDescription* >::iterator itEnd = m_processesInfo.end();
	size_t i = 0, uCount = m_processesInfo.size();
	if( uCount )
	{
		--uCount;
		QList<BlacklistedProcessDescription*>::iterator itFirst = m_processesInfo.begin();
		DebugAssert( (*itFirst)->ContainsWindowsTitles() || (*itFirst)->ContainsProcessName() );
	}
	for( QList<BlacklistedProcessDescription*>::iterator it = m_processesInfo.begin(); i < uCount; ++it, ++i )
	{
		QList<BlacklistedProcessDescription*>::iterator itNext = it;
		++itNext;
		DebugAssert( (*itNext)->ContainsWindowsTitles() || (*itNext)->ContainsProcessName() );
		for( ; itNext != itEnd; ++itNext )
		{
			DebugAssert( (*it)->GetReportNumber() != (*itNext)->GetReportNumber() );
		}
	}
}
#endif

QString BlacklistedProcessesDatabase::GetWindowTitle(HWND window)
{
	// Get the window title: max is 1024 chars
	QString result;
	result.resize( MAX_WINDOW_CAPTION_LEN );

	int length = GetWindowTextW( window, (LPWSTR)result.data(), MAX_WINDOW_CAPTION_LEN );
	if( length == 0 ) return QString();

	return result.toLower();
}

bool BlacklistedProcessesDatabase::FindProcessByProcessName(QString processName, BlacklistedProcessDescription** ppProcessInfoResult)
{
	processName = processName.toLower();
	foreach( BlacklistedProcessDescription* pProcessInfo, m_processesInfo )
	{
		if( pProcessInfo->ContainsProcessName() && pProcessInfo->ProcessNameEquals( processName ) )
		{
			*ppProcessInfoResult = pProcessInfo;
			return true;
		}
	}
	return false;
}

bool BlacklistedProcessesDatabase::FindProcessByWindowTitle(DWORD procId, BlacklistedProcessDescription** ppProcessInfoResult)
{
	WinProcessInfo pwndProcessInfo = BlacklistedProcessesFinder::GetInstance()->GetWinProcessInfo( procId );
	foreach( HWND window, pwndProcessInfo.GetWindows() )
	{
		QString windowTitle = GetWindowTitle( window );
		if( windowTitle.isEmpty() ) continue;

		foreach( BlacklistedProcessDescription* pProcessInfo, m_processesInfo )
		{
			if( pProcessInfo->ContainsWindowsTitles() )
			{
				if( pProcessInfo->IsWindowTitlePresent( window, windowTitle ) )
				{
					*ppProcessInfoResult = pProcessInfo;
					return true;
				}
			}
		}
	}
	return false;
}

void BlacklistedProcessesDatabase::DeinitializeProcessesDatabase()
{
	QList< BlacklistedProcessDescription* >::iterator infoIter;
	for( infoIter = m_processesInfo.begin(); infoIter != m_processesInfo.end(); infoIter++ ) delete *infoIter;
	m_processesInfo.clear();
}

bool BlacklistedProcessesDatabase::FindProcess( const DWORD procId, QString processName, BlacklistedProcessDescription** ppProcessInfoResult )
{
	if( FindProcessByProcessName( processName, ppProcessInfoResult ) ) return true;
	if( FindProcessByWindowTitle( procId, ppProcessInfoResult ) ) return true;
	return false;
}

#endif // OS_WINDOWS
