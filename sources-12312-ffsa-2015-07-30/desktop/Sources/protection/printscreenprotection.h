#ifndef PRINTSCREENPROTECTION_H
#define PRINTSCREENPROTECTION_H

#ifdef OS_WINDOWS

#ifndef NOMINMAX
#define NOMINMAX
#endif
#include <Windows.h>

class QMessageBox;
#include <QList>

class PrintScreenProtection
{
private:
	PrintScreenProtection();

public:
	static void Install();
	static void Remove();

private:
	static LRESULT CALLBACK getMessageHookForPrintScreenProtection( int code, WPARAM wParam, LPARAM lParam );

	static HHOOK g_getMessageHook;

    static QList<bool> g_registered;
    static QList<unsigned short> g_HookId;

	static QMessageBox* g_messageBox;
};

#endif // OS_WINDOWS

#endif // PRINTSCREENPROTECTION_H
