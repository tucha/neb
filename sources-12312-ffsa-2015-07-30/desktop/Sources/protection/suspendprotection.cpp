#include "suspendprotection.h"

#include <QProcess>
#include <QCoreApplication>
#include <QThread>

#include <memory>

#include <defines.h>

#ifndef OS_WINDOWS
#error "This file must be compiled on Windows OS only"
#endif

#include "threadcollection.h"
#include "../miscellaneous/parameters.h"

#define CHECK_INTERVAL 30

SuspendProtection::SuspendProtection(QObject* parent) :
	QObject( parent ),
	m_anotherInstance( NULL ),
	m_timer(),
	m_threads()
{
	connect(&m_timer,SIGNAL(timeout()),this,SLOT(CheckAnotherInstance()));
}

SuspendProtection::~SuspendProtection()
{
    m_anotherInstance->terminate();
    m_anotherInstance->waitForFinished(1000);
	delete m_anotherInstance;
}

int SuspendProtection::Run(qint64 processId)
{
#if defined(BUILD_DEBUG) && defined(ALLOW_SUSPENDING_IN_DEBUG)
	return 0;
#endif

	ThreadCollection threads( processId );

	if( threads.IsEmpty() )
	{
		WindowsSpecific::KillProcess( processId );
		return 0;
	}

	while( true )
	{
		if( threads.IsAnyThreadSuspended() || !threads.IsProcessAlive() )
		{
			WindowsSpecific::KillProcess( processId );
			return 0;
		}
#if QT_VERSION >= 0x050000
		QThread::msleep( CHECK_INTERVAL );
#else
		Sleep( CHECK_INTERVAL );
#endif
	}
	return 0;
}

bool SuspendProtection::StartAnotherInstance()
{
#if defined(BUILD_DEBUG) && defined(ALLOW_SUSPENDING_IN_DEBUG)
	return true;
#endif

	QString filePath = QCoreApplication::applicationFilePath();
	qint64 currentProcessId = QCoreApplication::applicationPid();
	QString argument = Parameters::CreateProcessIdParameter( currentProcessId );
	QStringList arguments = QStringList() << argument;

	std::auto_ptr<QProcess> anotherInstance( new QProcess() );
	if( anotherInstance.get() == NULL ) return false;

#if QT_VERSION >= 0x050000
	anotherInstance->setProgram( filePath );
	anotherInstance->setArguments( arguments );
	anotherInstance->start();
#else
	anotherInstance->start( filePath, arguments );
#endif
	anotherInstance->waitForStarted();
	if( anotherInstance->state() != QProcess::Running ) return false;

	qint64 processId;
#if QT_VERSION >= 0x050000
	processId = anotherInstance->processId();
#else
	processId = anotherInstance->pid()->dwProcessId;
#endif

	QSharedPointer<ThreadCollection> threads( new ThreadCollection( processId ) );
	if( threads->IsEmpty() ) return false;

	m_anotherInstance = anotherInstance.release();
	m_threads = threads;
	m_timer.start( CHECK_INTERVAL );
	return true;
}

void SuspendProtection::CheckAnotherInstance()
{
	DebugAssert( m_threads.isNull() == false );
	if( m_threads.isNull() ) return;

	DebugAssert( m_threads->IsEmpty() == false );
	if( m_threads->IsEmpty() ) return;

	if( m_threads->IsAnyThreadSuspended() || !m_threads->IsProcessAlive() ) emit AnotherInstanceSuspended();
}

