#import "windowcaptureprotection.h"

#import <Cocoa/Cocoa.h>

// Define the private undocumented CGS methods to exclude window from being captured
typedef int CGSConnectionID;
typedef int CGSWindowID;
typedef CFTypeRef CGSRegionRef;

CG_EXTERN CGSConnectionID CGSMainConnectionID();

CG_EXTERN CGError CGSGetWindowBounds(CGSConnectionID cid, CGSWindowID wid, CGRect *rectOut);

CG_EXTERN CGError CGSNewRegionWithRect(const CGRect* rect, CGSRegionRef* regionOut);
CG_EXTERN bool CGSRegionIsEmpty(CGSRegionRef region);
CG_EXTERN CGError CGSReleaseRegion(CGSRegionRef region);

CG_EXTERN CGError CGSSetWindowCaptureExcludeShape(CGSConnectionID cid, CGSWindowID wid, CGSRegionRef region);
//-----------------------------------------------------------------------------------------------------------

void MarkWindowAsNonShared( NSWindow* window)
{
    window.sharingType = NSWindowSharingNone;
}

BOOL MarkWindowAsExcludedForCapture( NSWindow* window )
{
    CGSConnectionID cid = CGSMainConnectionID();
    CGSWindowID wid=(int)window.windowNumber;

    CGRect windowRectOut;
    CGError err = CGSGetWindowBounds(cid, wid, &windowRectOut);
    if(err != kCGErrorSuccess)
    {
        NSLog( @"ERROR: failed to get window bounds, error %i", err );
        return NO;
    }
    windowRectOut.origin = CGPointZero;

    CGSRegionRef regionOut;
    err = CGSNewRegionWithRect(&windowRectOut, &regionOut);
    if(err!=kCGErrorSuccess)
    {
        NSLog( @"ERROR: failed to create CGSRegion, error %i", err );
        return NO;
    }

    err = CGSSetWindowCaptureExcludeShape(cid, wid, regionOut);
    if(err != kCGErrorSuccess)
    {
        NSLog( @"ERROR: failed to set capture exclude shape for the specified window, error %i", err );
        CGSReleaseRegion( regionOut );
        return NO;
    }

    CGSReleaseRegion( regionOut );
    return YES;
}

BOOL CreatePortWhatDisablesGrabApp()
{
    CFMessagePortRef port = CFMessagePortCreateRemote( NULL, CFSTR( "OSXDisableScreenGrab" ) );
    if( port == NULL )
    {
        //[NSException raise:@"Unable to create port to prevent Grab utility to work" format:@""];
        NSLog( @"ERROR: failed to create port named OSXDisableScreenGrab" );
        return NO;
    }
    CFMessagePortInvalidate( port );
    return YES;
}

//--------------------------------------------------------------------------------------------------------

BOOL WindowCaptureProtection::ProtectWidget( QWidget* w )
{
    NSView* view = ( NSView* )w->winId();
    NSWindow* window = view.window;
    Q_ASSERT( window != NULL );

    // 1) Disable ability of screenshot the app window using Cmd+Shift+4+Space;
    //    all other ways will still work (at least Cmd+Shift+3)
	// NOTE: brake the FullScreen mode (Kiosk)! see defect ELARII-6 for details
//    MarkWindowAsNonShared( window );

    // 2) Mark the window rect as excluded for capture
    if( !MarkWindowAsExcludedForCapture( window) ) return NO;

    // 3) Disable the Grab application that can take screenshots
//	BOOL portResult = CreatePortWhatDisablesGrabApp();

//	if( ( portResult == NO ) && ( QSysInfo::MacintoshVersion >= QSysInfo::MV_10_9 ) ) return NO;

    return YES;
}
