#ifndef LINUX_WINDOWCAPTUREPROTECTION
#define LINUX_WINDOWCAPTUREPROTECTION

#include <miscellaneous/linux/dedetect.h>
#include <miscellaneous/linux/dbusservice.h>

#include <protection/linux/ksnapshotprotection.h>

class LinuxWindowCaptureProtection
{
	QStringList m_log;
	LinuxUtils::LinuxDE m_de;
	DBusService m_dbus;
	KSnapshotProtection m_ksnapshot;

	QString GrabberServiceName();
	QString KdeHotkeysConfigPath();
	bool ToggleKSnapshot( bool enabled );
	
public:
	LinuxWindowCaptureProtection();
	~LinuxWindowCaptureProtection();
	
	bool IsValid() const;
	QStringList Log() const;
};

#endif // LINUX_WINDOWCAPTUREPROTECTION
