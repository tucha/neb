#include <protection/linux/linuxwindowcaptureprotection.h>

#include <QtCore/QSettings>
#include <QtCore/QProcess>

using namespace LinuxUtils;

QString LinuxWindowCaptureProtection::GrabberServiceName()
{
	LinuxDE de = DetectLinuxDE();

	QString serviceName;
	if( de == DE_KDE )
	{
		m_log << "NOTE: KDE detected, KSnapshot protection will be used";
		serviceName = "org.kde.ksnapshot";
	}
	else if( ( de == LinuxUtils::DE_GNOME ) || ( de == LinuxUtils::DE_UNITY ) )
	{
		m_log << "NOTE: GNOME or Unity detected, Gnome Screenshot protection will be used";
		serviceName = "org.gnome.Screenshot";
	}
	else
	{
		m_log << "ERROR: unsupported DE detected";
		serviceName = "ru.elar.nelrfviewer";
	}
	
	return serviceName;
}

LinuxWindowCaptureProtection::LinuxWindowCaptureProtection() :
	m_log(),
	m_de( DetectLinuxDE() ),
	m_dbus( DBusService::Unique, GrabberServiceName() ),
	m_ksnapshot( m_de == DE_KDE )
{
}

LinuxWindowCaptureProtection::~LinuxWindowCaptureProtection()
{
}

bool LinuxWindowCaptureProtection::IsValid() const
{
	return ( m_de == DE_KDE || m_de == DE_GNOME || m_de == DE_UNITY )
			&& m_dbus.isRegistered()
			&& m_ksnapshot.IsValid();
}

QStringList LinuxWindowCaptureProtection::Log() const
{
	return m_log + m_ksnapshot.Log();
}
