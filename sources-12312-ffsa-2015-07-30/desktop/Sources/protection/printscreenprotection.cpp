#include "printscreenprotection.h"

#ifdef OS_WINDOWS

#include <QObject>
#include <QMessageBox>

#include <defines.h>
#include <QDebug>
HHOOK PrintScreenProtection::g_getMessageHook = 0;


QList<unsigned int> modifiers = QList<unsigned int>() << MOD_ALT << MOD_CONTROL << MOD_SHIFT << MOD_WIN;

QList<unsigned short> PrintScreenProtection::g_HookId = QList<unsigned short>();
QList<bool> PrintScreenProtection::g_registered =  QList<bool>();

QMessageBox* PrintScreenProtection::g_messageBox = NULL;

void PrintScreenProtection::Install()
{
	DebugAssert( g_messageBox == NULL );
	if( g_messageBox != NULL ) return;

    g_registered << RegisterHotKey( NULL, IDHOT_SNAPDESKTOP, 0, VK_SNAPSHOT );
    g_HookId << IDHOT_SNAPDESKTOP;
    if(!g_registered.last())
        qDebug() << "Failed for IDHOT_SNAPDESKTOP";
    else
        qDebug() << "Registered for IDHOT_SNAPDESKTOP";
    for (int i = 0; i <  (1<<modifiers.size()); i++)
    {
        auto hookId = GlobalAddAtom( QString("IDHOT_NEB_DIS_PRINTSCREEN%1").arg(i).toStdWString().data() );
        g_HookId << hookId;
        qDebug() << "g_HookId" << hookId;
        unsigned int flag = 0x0000;
        for (int j = 0; j <  modifiers.size(); j++)
        {
            if((1<<j) & i)
                flag |= modifiers[j];
        }
        auto registered = RegisterHotKey( NULL, hookId, flag, VK_SNAPSHOT );
        g_registered << registered;
        if(registered)
            qDebug() << "Registered for" << flag;
        else
            qDebug() << "Failed for " <<  flag;
    }


	// Set hook for hotkey to show message when PrintScreen is used.
	DebugAssert( g_getMessageHook == NULL );
	g_getMessageHook = SetWindowsHookEx( WH_GETMESSAGE, getMessageHookForPrintScreenProtection, NULL, GetCurrentThreadId() );
	DebugAssert( g_getMessageHook != NULL );

	g_messageBox = new QMessageBox();
	g_messageBox->setWindowTitle( QObject::tr( "Print Screen option is disabled." ) );
	g_messageBox->setText( QObject::tr( "To make a snapshot, please close the protected document." ) );
	g_messageBox->setWindowModality( Qt::ApplicationModal );
	g_messageBox->setIcon( QMessageBox::Information );
	g_messageBox->setWindowFlags( Qt::Window | Qt::WindowCloseButtonHint );
}

void PrintScreenProtection::Remove()
{
	DebugAssert( g_messageBox != NULL );
	if( g_messageBox == NULL ) return;

	if( g_getMessageHook != NULL )
	{
		UnhookWindowsHookEx( g_getMessageHook );
		g_getMessageHook = NULL;
	}


    for (int i = 0; i < g_HookId.size(); i ++)
    {
        UnregisterHotKey( NULL,  g_HookId[i] );
        if(i > 0)
            g_HookId[i] = GlobalDeleteAtom( g_HookId[i] );
    }

    DeleteAndNull( g_messageBox );
}

LRESULT PrintScreenProtection::getMessageHookForPrintScreenProtection(int code, WPARAM wParam, LPARAM lParam)
{
	if( code == HC_ACTION )
	{
		MSG* msg = ( MSG* )lParam;
		if( msg->message == WM_HOTKEY )
		{
			int hotKeyID = ( int )msg->wParam;

            for(int i = 0; i < g_HookId.size(); i ++)
            {
                if(hotKeyID == g_HookId[i])
                {
                    if( wParam == PM_REMOVE ) g_messageBox->exec();
                    break;
                }
            }
		}
	}

	return CallNextHookEx( 0, code, wParam, lParam );
}

#endif // OS_WINDOWS
