#ifndef SUSPENDPROTECTION_H
#define SUSPENDPROTECTION_H

#ifndef OS_WINDOWS
#error "This file must be compiled on Windows OS only"
#endif

#include <QObject>
#include <QTimer>
#include <QSharedPointer>

class QProcess;
class ThreadCollection;

class SuspendProtection : public QObject
{
	Q_OBJECT

private:
	Q_DISABLE_COPY( SuspendProtection )

public:
	explicit SuspendProtection( QObject *parent = 0 );
	~SuspendProtection();

	int Run( qint64 processId );
	bool StartAnotherInstance(void);

signals:
	void AnotherInstanceSuspended();

private slots:
	void CheckAnotherInstance();

private:
	QProcess*							m_anotherInstance;
	QTimer								m_timer;
	QSharedPointer<ThreadCollection>	m_threads;
};

#endif // SUSPENDPROTECTION_H
