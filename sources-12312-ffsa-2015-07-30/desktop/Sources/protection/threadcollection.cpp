#include "threadcollection.h"

ThreadCollection::ThreadCollection(qint64 processId):
	m_processHandle( OpenProcess( PROCESS_TERMINATE | SYNCHRONIZE, false, processId ) ),
	m_threadHandles()
{
	if( m_processHandle == NULL ) return;
	m_threadHandles = WindowsSpecific::OpenThreads( WindowsSpecific::GetProcessThreads( processId ) );
	if( m_threadHandles.isEmpty() ) CloseHandle( m_processHandle );

//	WindowsSpecific::HideThreadsFromDebugger( m_threadHandles );
}

ThreadCollection::~ThreadCollection()
{
	if( IsEmpty() ) return;
	WindowsSpecific::CloseThreads( m_threadHandles );
	CloseHandle( m_processHandle );
}

bool ThreadCollection::IsEmpty() const
{
	return m_threadHandles.isEmpty();
}

bool ThreadCollection::IsAnyThreadSuspended() const
{
	foreach( HANDLE threadHandle, m_threadHandles )
	{
		if( WindowsSpecific::IsThreadSuspended( threadHandle ) ) return true;
	}
	return false;
}

bool ThreadCollection::IsProcessAlive() const
{
	 DWORD waitResult = WaitForSingleObject( m_processHandle, 0 );
	 if( waitResult == WAIT_OBJECT_0 ) return false;
	 if( waitResult == WAIT_TIMEOUT ) return true;
	 return false;
}
