#ifndef PROCESSMANAGER2_H
#define PROCESSMANAGER2_H

#include <QObject>
#include <QSharedMemory>
#include <QTimer>

#include "miscellaneous/parameters.h"

class ProcessManager2 : public QObject
{
	Q_OBJECT
public:
	explicit ProcessManager2( QObject *parent, QString key );
	bool IsPrimaryProcess(void)const;

	void StartListening(void);
	bool CloseOtherProcesses( void );

signals:
	void SignalCloseProcess();

private slots:
	void SlotListen();

private:
	QString ProcessId(void)const;
	QString ReadMemory(void);
	bool WriteMemory( QString text );

	QSharedMemory m_memory;
	bool m_isPrimaryProcess;
	QTimer m_timer;
};

#endif // PROCESSMANAGER2_H
