#ifndef LOGIC_H
#define LOGIC_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QTimer>

#include <defines.h>
#include <pageset.h>
#include <document.h>
#include <user.h>
#include <identifier.h>
#include <favoritesmanager.h>
#include <session.h>
#include <viewersettingsrequest.h>

#include "miscellaneous/parameters.h"
#include "miscellaneous/documentstate.h"

#ifdef OS_LINUX
#include <miscellaneous/linux/dedetect.h>
#endif

#define AUTHENTICATION_USER		"user"
#define AUTHENTICATION_PASSWORD	"kEAVyhx9tn"

class QNetworkAccessManager;

class MainWindow;
class Card;
class Request;
class Config;
class DownloadWindow;
class PrintingWindow;
class Description;
class History;
class AuthorizationDialog;
class AuthorizationBase;
class AuthorizationCheck;
class AccountBaseRequest;
class QAuthenticator;
class QNetworkProxy;
class QMainWindow;
class StatusRequest;
class Session;
class QApplication;

class Logic : public QObject
{
	Q_OBJECT
private:
	DISABLE_COPY( Logic )

public:
	explicit Logic(QObject *parent, QApplication* application);
	virtual ~Logic();

	FavoritesManager*	GetFavoritesManager(void);
	ViewerSettings		GetSettings(void)const;
	bool				IsEmpty(void)const;

	static QString BaseUrlForDocuments();

public slots:
	bool Start( Parameters parameters );
	void SlotReceiveParameters( Parameters parameters );
	void Stop();

private slots:
	void AuthorizeByToken( QString token );
	void AuthorizeByPassword( QString email, QString password );
	void OpenDocument( Document document );
	void SaveDocument( Document document, PageSet pageSet );
	void PrintDocument( Document document, PageSet pageSet );
	void DownloadingError( Document document );
	void DownloadingSuccess( Document document );
	void UserAuthorized(User user );
	void AuthorizationFinished();
	void UserStatusCheckFinished(ErrorCode error, User::UserStatus userStatus );
	void IpStatusCheckFinished(ErrorCode error, bool ipIsAllowed );
	void RequestRemovalFromHistory( Document document );
	void RequestRemovalFromFavorites( Document document );
	void ProxyAuthenticationRequired(const QNetworkProxy &proxy, QAuthenticator *authenticator);
	void UpdateFavorites( FavoritesManager* manager );
	void SaveDocumentState( DocumentState state );
	void AuthorizationByTokenFinished( ErrorCode error, AuthorizationResult result );
	void AuthorizationByPasswordFinished( ErrorCode error, AuthorizationResult result );
	void SlotAuthenticationRequired(QAuthenticator* authenticator);
	void SearchBlackListedProcesses();
	void UserTokenExpired();
	void SettingsRequestFinished(ErrorCode error, ViewerSettings settings );
	void SettingsRequestTimeout();

private:
    void HandleUserRole(User &user);
	void DeleteMembers(void);
	void CreateMembers(void);
	void ConnectSlots(void);
	void Initialize(void);
	void Deinitialize(void);
	void ShowAuthorization( QString message = QString() );
	QString GetAppToken(void)const;
	Identifier CreateId(void)const;
	void BringToFront( QMainWindow* window );
	void CreateNewConfig(void);
	void StartInternal(void);
#ifdef OS_MACOSX
	bool WasStarted(void)const;
#endif
	
#ifdef OS_LINUX
	bool ProcessIsGrabber( const QString& pid, QString& processName, QString& processDescription );

	QString GetGnomeShortcut( QString name );
	bool SetGnomeShortcut( QString name, QString value );
	bool ClearGnomeShortcut( QString name );
#endif

private:
#ifdef OS_MACOSX
	// Virtual functions
	bool eventFilter( QObject* obj, QEvent* event );
#endif
	static QString			g_baseUrlForDocuments;

	MainWindow*				m_mainWindow;
	History*				m_history;
	History*				m_favorites;
	Request*				m_baseRequest;
	Config*					m_config;
	QNetworkAccessManager*	m_manager;
	DownloadWindow*			m_downloadWindow;
	PrintingWindow*			m_printingWindow;
	AuthorizationDialog*	m_authorizationDialog;
	AuthorizationBase*		m_authorizationBase;
	DocumentList			m_startDocuments;
	User					m_user;
	AccountBaseRequest*		m_accountBase;
	FavoritesManager		m_favoritesManager;
	AuthorizationCheck*		m_authorizationCheck;
	AuthorizationRequest*	m_authorizationRequest;
	StatusRequest*			m_statusUserRequest;
	StatusRequest*			m_statusIpRequest;

	QTimer					m_blacklistedProcessSearch;

	SessionPointer			m_session;
	Parameters				m_startParameters;
	ViewerSettingsRequest*	m_settingsRequest;
	ViewerSettings			m_settings;
	QTimer					m_settingsRequestTimer;

	bool					m_stopped;
	QApplication*			m_application;

#ifdef OS_LINUX
	LinuxUtils::LinuxDistro m_distro;
	QMap<QString,QString> m_gnomeShortcuts;
#endif
};

#endif // LOGIC_H
