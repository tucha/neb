<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>AdminDialog</name>
    <message>
        <location filename="gui/admindialog.cpp" line="10"/>
        <source>Server API error: %1</source>
        <translation>Ошибка сервера: %1</translation>
    </message>
    <message>
        <location filename="gui/admindialog.cpp" line="16"/>
        <source>Trusted registry administration</source>
        <translation>Администрирование реестра доверенных машин</translation>
    </message>
    <message>
        <location filename="gui/admindialog.cpp" line="27"/>
        <source>Add to registry</source>
        <translation>Добавить в реестр</translation>
    </message>
    <message>
        <location filename="gui/admindialog.cpp" line="31"/>
        <source>Remove from registry</source>
        <translation>Удалить из реестра</translation>
    </message>
    <message>
        <location filename="gui/admindialog.cpp" line="89"/>
        <source>This machine is not in the trusted registry</source>
        <translation>Эта машина не содержится в реестре доверенных</translation>
    </message>
    <message>
        <location filename="gui/admindialog.cpp" line="71"/>
        <location filename="gui/admindialog.cpp" line="94"/>
        <source>This machine is in the trusted registry now</source>
        <translation>Эта машина находится в реестре доверенных</translation>
    </message>
    <message>
        <location filename="gui/admindialog.cpp" line="99"/>
        <source>This machine is in the trusted registry and blocked</source>
        <translation>Эта машина в реестре доверенных, но заблокирована</translation>
    </message>
</context>
<context>
    <name>AuthorizationDialog</name>
    <message>
        <location filename="gui/authorizationdialog.cpp" line="108"/>
        <location filename="gui/authorizationdialog.cpp" line="117"/>
        <source>title</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="gui/authorizationdialog.cpp" line="108"/>
        <source>error</source>
        <translation>Не удаётся подключиться к серверу. Проверьте подключение к Интернет и доступность сайта &lt;a href=&quot;http://нэб.рф&quot;&gt;http://нэб.рф&lt;/a&gt; с вашего компьютера.</translation>
    </message>
    <message>
        <location filename="gui/authorizationdialog.cpp" line="147"/>
        <source>Cannot get anonym access</source>
        <translation>Ошибка получения анонимного доступа</translation>
    </message>
    <message>
        <location filename="gui/authorizationdialog.cpp" line="180"/>
        <source>UserName</source>
        <translation>E-mail / Логин / ЕЭЧБ</translation>
    </message>
    <message>
        <location filename="gui/authorizationdialog.cpp" line="181"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="gui/authorizationdialog.cpp" line="182"/>
        <source>Ok</source>
        <translation>Ок</translation>
    </message>
    <message>
        <location filename="gui/authorizationdialog.cpp" line="183"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="gui/authorizationdialog.cpp" line="184"/>
        <source>Anonymous access</source>
        <translation>Войти анонимно</translation>
    </message>
    <message>
        <location filename="gui/authorizationdialog.cpp" line="186"/>
        <source>Authentication</source>
        <translation>Авторизация</translation>
    </message>
</context>
<context>
    <name>BookmarksControl</name>
    <message>
        <location filename="gui/bookmarkscontrol.cpp" line="247"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
</context>
<context>
    <name>Card</name>
    <message>
        <source>DebugOutput</source>
        <translation type="vanished">Оповещения</translation>
    </message>
    <message>
        <source>DocumentDescription</source>
        <translation type="vanished">Карточка документа</translation>
    </message>
</context>
<context>
    <name>CardTabs</name>
    <message>
        <source>Favorite</source>
        <translation type="vanished">Избранное</translation>
    </message>
    <message>
        <source>History</source>
        <translation type="vanished">Последние</translation>
    </message>
</context>
<context>
    <name>CommentWindow</name>
    <message>
        <location filename="gui/commentwindow.cpp" line="84"/>
        <source>AddComment</source>
        <oldsource>AddBookmark</oldsource>
        <translation>Добавить комментарий</translation>
    </message>
    <message>
        <source>SaveBookmark</source>
        <translation type="vanished">Сохранить закладку</translation>
    </message>
    <message>
        <location filename="gui/commentwindow.cpp" line="84"/>
        <source>SaveComment</source>
        <translation>Сохранить комментарий</translation>
    </message>
    <message>
        <location filename="gui/commentwindow.cpp" line="86"/>
        <source>DeleteComment</source>
        <oldsource>DeleteBookmark</oldsource>
        <translation>Удалить комментарий</translation>
    </message>
    <message>
        <location filename="gui/commentwindow.cpp" line="87"/>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="gui/commentwindow.cpp" line="88"/>
        <source>Comment</source>
        <translation>Введите новый комментарий</translation>
    </message>
    <message>
        <location filename="gui/commentwindow.cpp" line="89"/>
        <source>CommentWindow</source>
        <translation>Комментарий</translation>
    </message>
</context>
<context>
    <name>ContentDisplay</name>
    <message>
        <location filename="gui/contentdisplay.cpp" line="6"/>
        <source>Content</source>
        <translation>Содержание</translation>
    </message>
</context>
<context>
    <name>Description</name>
    <message>
        <location filename="gui/description.cpp" line="106"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="gui/description.cpp" line="115"/>
        <source>Title</source>
        <translation>Заглавие:</translation>
    </message>
    <message>
        <location filename="gui/description.cpp" line="116"/>
        <source>Author</source>
        <translation>Автор:</translation>
    </message>
    <message>
        <location filename="gui/description.cpp" line="117"/>
        <source>Place</source>
        <translation>Место издания:</translation>
    </message>
    <message>
        <location filename="gui/description.cpp" line="118"/>
        <source>Publisher</source>
        <translation>Издательство:</translation>
    </message>
    <message>
        <location filename="gui/description.cpp" line="119"/>
        <source>Year</source>
        <translation>Год издания:</translation>
    </message>
    <message>
        <location filename="gui/description.cpp" line="120"/>
        <source>Description</source>
        <translation>Физическое описание:</translation>
    </message>
    <message>
        <location filename="gui/description.cpp" line="121"/>
        <source>ISBN</source>
        <translation>ISBN:</translation>
    </message>
    <message>
        <location filename="gui/description.cpp" line="122"/>
        <source>UDK</source>
        <translation>УДК:</translation>
    </message>
    <message>
        <location filename="gui/description.cpp" line="123"/>
        <source>BBK</source>
        <translation>ББК:</translation>
    </message>
    <message>
        <location filename="gui/description.cpp" line="124"/>
        <source>Sign</source>
        <translation>Авторский знак:</translation>
    </message>
    <message>
        <location filename="gui/description.cpp" line="125"/>
        <source>Annotation</source>
        <translation>Аннотация:</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Открыть</translation>
    </message>
    <message>
        <source>Download</source>
        <translation type="vanished">Скачать</translation>
    </message>
    <message>
        <location filename="gui/description.cpp" line="127"/>
        <source>DocumentDescription</source>
        <translation>Библиографическое описание</translation>
    </message>
</context>
<context>
    <name>Display</name>
    <message>
        <source>Page %1/%2
Forbidden</source>
        <translation type="vanished">Страница %1/%2
Доступ запрещён</translation>
    </message>
    <message>
        <location filename="gui/display.cpp" line="71"/>
        <source>AccessForbidden</source>
        <translation>Доступ запрещён</translation>
    </message>
</context>
<context>
    <name>DownloadWindow</name>
    <message>
        <location filename="gui/downloadwindow.cpp" line="57"/>
        <source>Save document</source>
        <translation>Сохранить документ</translation>
    </message>
    <message>
        <location filename="gui/downloadwindow.cpp" line="57"/>
        <source>PDF Files (*.pdf)</source>
        <translation>PDF файл (*.pdf)</translation>
    </message>
    <message>
        <location filename="gui/downloadwindow.cpp" line="55"/>
        <source>Downloading...</source>
        <translation>Скачивание...</translation>
    </message>
    <message>
        <location filename="gui/downloadwindow.cpp" line="171"/>
        <source>Cancel</source>
        <translation>Прервать</translation>
    </message>
    <message>
        <location filename="gui/downloadwindow.cpp" line="172"/>
        <source>Saving Document</source>
        <translation>Сохранение документа</translation>
    </message>
    <message>
        <location filename="gui/downloadwindow.cpp" line="195"/>
        <source>Downloading page %1 ( %2 of %3 ) ...</source>
        <translation>Скачивание страницы %1 (%2 из %3) ...</translation>
    </message>
    <message>
        <location filename="gui/downloadwindow.cpp" line="221"/>
        <source>AccessError</source>
        <translation>Ошибка доступа</translation>
    </message>
    <message>
        <location filename="gui/downloadwindow.cpp" line="222"/>
        <source>Pages where not saved: %1</source>
        <translation>У вас нет прав на электронное копирование следующих страниц: %1</translation>
    </message>
    <message>
        <source>Downloading page %1 of %2 ...</source>
        <oldsource>Downloading page %1...</oldsource>
        <translation type="vanished">Скачивание страницы %1 из %2...</translation>
    </message>
</context>
<context>
    <name>Favorite</name>
    <message>
        <source>Favorite</source>
        <translation type="vanished">Избранное</translation>
    </message>
    <message>
        <source>History</source>
        <translation type="vanished">Последние</translation>
    </message>
</context>
<context>
    <name>FavoriteAndHistory</name>
    <message>
        <source>Favorite</source>
        <translation type="vanished">Избранное</translation>
    </message>
    <message>
        <source>History</source>
        <translation type="vanished">Последние</translation>
    </message>
</context>
<context>
    <name>History</name>
    <message>
        <location filename="gui/history.cpp" line="36"/>
        <source>delete</source>
        <translation>Удалить</translation>
    </message>
</context>
<context>
    <name>HistoryRecord</name>
    <message>
        <location filename="gui/historyrecord.cpp" line="162"/>
        <source>Page %1
Forbidden</source>
        <translation>Страница %1
Доступ запрещён</translation>
    </message>
    <message>
        <location filename="gui/historyrecord.cpp" line="191"/>
        <source>Author: %1</source>
        <translation>Автор: %1</translation>
    </message>
    <message>
        <location filename="gui/historyrecord.cpp" line="201"/>
        <source>DownloadingError</source>
        <translation>Ошибка при скачивании</translation>
    </message>
    <message>
        <location filename="gui/historyrecord.cpp" line="212"/>
        <source>Title: %1</source>
        <translation>%1</translation>
    </message>
    <message>
        <source>Remove from history</source>
        <translation type="vanished">Удалить из списка последних документов</translation>
    </message>
</context>
<context>
    <name>Logic</name>
    <message>
        <source>NotAllowedIpTitle</source>
        <translation type="vanished">НЭБ РФ</translation>
    </message>
    <message>
        <source>NotAllowedIpMessage</source>
        <translation type="vanished">Уважаемый читатель! Вы приступаете к чтению издания, защищенного авторским правом. Доступ к документу предоставляется в соответствии со статьей 1274 IV Части Гражданского Кодекса Российский Федерации.</translation>
    </message>
    <message>
        <source>NotAllowedIpButtonText</source>
        <translation type="vanished">Продолжить чтение</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="111"/>
        <source>ConfigPresentedTitle</source>
        <translation>НЭБ РФ</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="112"/>
        <source>ConfigPresentedMessage</source>
        <translation>Внимание! Используется файл переопределения ответов от сервера.</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="266"/>
        <source>DownloadingError</source>
        <translation>Документ не найден</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="267"/>
        <source>Downloading %1 was unsuccessful.</source>
        <translation>Не удалось открыть документ %1.</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="503"/>
        <location filename="logic.cpp" line="512"/>
        <location filename="logic.cpp" line="521"/>
        <location filename="logic.cpp" line="532"/>
        <location filename="logic.cpp" line="543"/>
        <location filename="logic.cpp" line="557"/>
        <location filename="logic.cpp" line="718"/>
        <source>title</source>
        <translation>Внимание</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="504"/>
        <location filename="logic.cpp" line="719"/>
        <source>message</source>
        <translation>Программа НЭБ РФ не может выполняться одновременно с приложением %1 (%2).</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="513"/>
        <location filename="logic.cpp" line="522"/>
        <location filename="logic.cpp" line="533"/>
        <location filename="logic.cpp" line="544"/>
        <source>loadavg_message</source>
        <translation>Обнаружен неподдерживаемый дистрибутив Linux. Программа НЭБ РФ будет завершена.</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="558"/>
        <source>message %1 %2.</source>
        <translation>Программа НЭБ РФ не может выполняться одновременно с приложением %1 (%2).</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="582"/>
        <source>UserTokenExpired</source>
        <translation>Уважаемый пользователь! Сеанс Вашей работы в системе просмотра документов истёк или был прерван. Введите пароль для продолжения.</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="604"/>
        <source>SettingsErrorTitle</source>
        <translation>НЭБ РФ</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="605"/>
        <source>SettingsErrorMessage</source>
        <translation>Получены некорректные настройки приложения. Повторите попытку позднее.</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="613"/>
        <source>NeedUpdateTitle</source>
        <translation>НЭБ РФ</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="614"/>
        <source>NeedUpdateMessage</source>
        <translation>Уважаемый пользователь! Вам необходимо скачать новую версию программы.</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="627"/>
        <source>SettingsRequestTimeoutTitle</source>
        <translation>НЭБ РФ</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="628"/>
        <source>SettingsRequestTimeoutMessage</source>
        <translation>Не удаётся подключиться к серверу. Проверьте подключение к Интернет и доступность сайта &lt;a href=&quot;http://нэб.рф&quot;&gt;http://нэб.рф&lt;/a&gt; с вашего компьютера.</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="863"/>
        <source>Screenshot application for GNOME</source>
        <translation>стандартное приложение GNOME для получения снимков экрана</translation>
    </message>
    <message>
        <location filename="logic.cpp" line="871"/>
        <source>KDE screen capture tool</source>
        <translation>стандартное приложение KDE для получения снимков экрана</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>ToolBar</source>
        <translation type="vanished">Панель инструментов</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1294"/>
        <source>SearchPanel</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1292"/>
        <source>Thumbnails</source>
        <translation>Содержание</translation>
    </message>
    <message>
        <source>Open document</source>
        <translation type="vanished">Открыть документ</translation>
    </message>
    <message>
        <source>DocumentId</source>
        <translation type="vanished">Код документа</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="552"/>
        <source>PrintDocument</source>
        <translation>Печать документа</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="553"/>
        <source>ChoosePagesToBePrinted</source>
        <translation>Укажите страницы для печати через запятую (например 1,3-5,7,9)</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="554"/>
        <source>Print</source>
        <translation>Распечатать</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="568"/>
        <source>SaveDocument</source>
        <translation>Копирование документа</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="569"/>
        <source>ChoosePagesToBeSaved</source>
        <translation>Укажите страницы для копирования через запятую (например 1,3-5,7,9)</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="570"/>
        <source>Save</source>
        <translation>Скопировать</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="579"/>
        <source>Buy</source>
        <translation>Заказ издания</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="579"/>
        <source>Service is unavailable</source>
        <translation>Данный сервис не доступен</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1480"/>
        <source>PageIsUnavaliableTitle</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1481"/>
        <source>PageIsUnavailableMessage</source>
        <translation>Запрашиваемая страница недоступна</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1485"/>
        <source>Not available</source>
        <translation>Не доступно</translation>
    </message>
    <message>
        <source>Favorites not allowed for unregistered user</source>
        <translation type="vanished">Для добавления в избранное необходимо быть зарегистрированным пользователем</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1526"/>
        <source>Page %1/%2
Forbidden</source>
        <translation>Страница %1/%2
Доступ запрещён</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1744"/>
        <source>RemoveBookmark</source>
        <translation>Удалить закладку</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1744"/>
        <source>AddBookmark</source>
        <translation>Добавить закладку</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1758"/>
        <source>EditComment</source>
        <translation>Редактировать комментарий</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1758"/>
        <source>AddComment</source>
        <translation>Добавить комментарий</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="791"/>
        <source>openTitle</source>
        <translation>Открыть документ</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="792"/>
        <source>openLabel</source>
        <translation>Введите ссылку на документ</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="794"/>
        <source>openCancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="803"/>
        <location filename="gui/mainwindow.cpp" line="816"/>
        <source>documentWrongTitle</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="803"/>
        <location filename="gui/mainwindow.cpp" line="816"/>
        <source>documentWrongMessage</source>
        <translation>Некорректная ссылка на документ</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="948"/>
        <source>AdminDialogAction</source>
        <translation>Администрирование</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1201"/>
        <source>Opening EPUB...</source>
        <translation>Открываем Epub...</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1295"/>
        <source>Bookmarks</source>
        <translation>Закладки</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1296"/>
        <source>Comments</source>
        <translation>Комментарии</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1297"/>
        <source>Favorite</source>
        <translation>Избранное</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1298"/>
        <source>History</source>
        <translation>Недавно открытые</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1299"/>
        <source>Content</source>
        <translation>Содержание</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1301"/>
        <source>zoomIn</source>
        <translation>Увеличить масштаб</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1302"/>
        <source>zoomOut</source>
        <translation>Уменьшить масштаб</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1362"/>
        <source>MainWindowTitleLimited%1</source>
        <translation>НЭБ РФ. Тестовая сборка. Время использования - до %1</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1364"/>
        <source>MainWindowTitle</source>
        <translation>НЭБ РФ %1</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1467"/>
        <source>AskUserToContinueTitle</source>
        <translation>НЭБ РФ</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1468"/>
        <source>AskUserToContinueMessage</source>
        <translation>Уважаемый читатель! Документ находится в ограниченном доступе. Доступ к документу предоставляется в соответствии с Частью четвертой Гражданского Кодекса Российский Федерации. В полном объеме текст документа доступен в Читальных залах библиотек-участников НЭБ.
Подтвердите, что Вы находитесь в помещении библиотеки.</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1470"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1471"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1475"/>
        <source>PendingRequestTitle</source>
        <translation>НЭБ РФ</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1476"/>
        <source>PendingRequestMessage</source>
        <translation>В настоящий момент ожидается ответ от сервера. Повторите попытку позднее.</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1478"/>
        <location filename="gui/mainwindow.cpp" line="1483"/>
        <location filename="gui/mainwindow.cpp" line="1488"/>
        <source>Ok</source>
        <translation>Продолжить</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1461"/>
        <source>NotAllowedIpTitle</source>
        <translation>НЭБ РФ</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1486"/>
        <source>Feature not allowed for unregistered user</source>
        <translation>Доступно только для зарегистрированных пользователей</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1751"/>
        <source>Removefavorites</source>
        <translation>Убрать издание из избранного</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1751"/>
        <source>AddFavorites</source>
        <translation>Добавить издание в избранное</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1462"/>
        <source>NotAllowedIpMessage</source>
        <translation>Уважаемый читатель! Вы приступаете к чтению издания, защищенного авторским правом. Доступ к документу предоставляется в соответствии с Частью четвертой Гражданского Кодекса Российский Федерации.</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1463"/>
        <source>NotAllowedIpButtonText</source>
        <translation>Продолжить чтение</translation>
    </message>
    <message>
        <source>settings</source>
        <translation type="vanished">Настройки</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1303"/>
        <source>ror</source>
        <translation>Повернуть страницу по часовой стрелке</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1304"/>
        <source>rol</source>
        <translation>Повернуть страницу против часовой стрелки</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1305"/>
        <source>favorite</source>
        <translation>Добавить издание в избранное</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1306"/>
        <source>buy</source>
        <translation>Заказ издания</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1307"/>
        <source>fullScreen</source>
        <translation>Полноэкранный режим</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1309"/>
        <source>copy</source>
        <translation>Скопировать</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1311"/>
        <source>scaleWidth</source>
        <translation>Масштаб по ширине страницы</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1312"/>
        <source>scaleFull</source>
        <translation>Страница целиком</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1313"/>
        <source>nextPage</source>
        <translation>Следующая страница</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1314"/>
        <source>firstPage</source>
        <translation>Первая страница</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1315"/>
        <source>prevPage</source>
        <translation>Предыдущая страница</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1316"/>
        <source>lastPage</source>
        <translation>Последняя страница</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1317"/>
        <source>bookmark</source>
        <translation>Добавить закладку</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1318"/>
        <source>comment</source>
        <translation>Добавить комментарий</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1320"/>
        <source>print</source>
        <translation>Распечатать</translation>
    </message>
    <message>
        <source>revertColors</source>
        <translation type="vanished">Инвертировать цвета</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1322"/>
        <source>invert</source>
        <translation>Инвертировать цвета</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1323"/>
        <source>ShowCard</source>
        <translation>Библиографическое описание</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1324"/>
        <source>open</source>
        <translation>Открыть документ</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1350"/>
        <source>ToolBarScaling</source>
        <translation>Масштабирование</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1352"/>
        <source>ToolBarDocument</source>
        <translation>Операции с документами</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1354"/>
        <source>ToolBarNavigation</source>
        <translation>Навигация по документу</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1355"/>
        <source>ToolBarView</source>
        <translation>Вид документа</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1356"/>
        <source>ToolBarPrivate</source>
        <translation>Личный кабинет</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1357"/>
        <source>ToolBarOthers</source>
        <translation>Другие инструменты</translation>
    </message>
    <message>
        <source>MainWindowTitile</source>
        <translation type="obsolete">НЭБ РФ. Тестовая сборка. Время использования - до %1</translation>
    </message>
    <message>
        <location filename="gui/mainwindow.cpp" line="1359"/>
        <source>DebugOutput</source>
        <translation>Оповещения</translation>
    </message>
    <message>
        <source>Zoom In</source>
        <translation type="vanished">Увеличить масштаб</translation>
    </message>
    <message>
        <source>Zoom Out</source>
        <translation type="vanished">Уменьшить масштаб</translation>
    </message>
</context>
<context>
    <name>PageSetWindow</name>
    <message>
        <source>SaveDocument</source>
        <translation type="vanished">Копирование документа</translation>
    </message>
    <message>
        <source>ChoosePagesToBeSaved</source>
        <translation type="vanished">Укажите страницы для копирования</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="vanished">Скопировать</translation>
    </message>
    <message>
        <location filename="gui/pagesetwindow.cpp" line="74"/>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
</context>
<context>
    <name>PagesControl</name>
    <message>
        <location filename="gui/pagescontrol.cpp" line="44"/>
        <source>Page</source>
        <translation>Страница</translation>
    </message>
    <message>
        <location filename="gui/pagescontrol.cpp" line="45"/>
        <source>/</source>
        <translation>/</translation>
    </message>
</context>
<context>
    <name>PrintingWindow</name>
    <message>
        <location filename="gui/printingwindow.cpp" line="58"/>
        <source>Downloading...</source>
        <translation>Скачивание...</translation>
    </message>
    <message>
        <source>Save document</source>
        <translation type="obsolete">Сохранить документ</translation>
    </message>
    <message>
        <source>PDF Files (*.pdf)</source>
        <translation type="vanished">PDF файл (*.pdf)</translation>
    </message>
    <message>
        <location filename="gui/printingwindow.cpp" line="67"/>
        <source>PrintDocument</source>
        <translation>Печать документа</translation>
    </message>
    <message>
        <location filename="gui/printingwindow.cpp" line="158"/>
        <source>Cancel</source>
        <translation>Прервать</translation>
    </message>
    <message>
        <location filename="gui/printingwindow.cpp" line="159"/>
        <source>Printing Document</source>
        <oldsource>Saving Document</oldsource>
        <translation>Печать документа</translation>
    </message>
    <message>
        <location filename="gui/printingwindow.cpp" line="182"/>
        <source>Downloading page %1 ( %2 of %3 ) ...</source>
        <translation>Скачивание страницы %1 (%2 из %3) ...</translation>
    </message>
    <message>
        <location filename="gui/printingwindow.cpp" line="249"/>
        <source>AccessError</source>
        <translation>Ошибка доступа</translation>
    </message>
    <message>
        <location filename="gui/printingwindow.cpp" line="250"/>
        <source>Pages where not printed: %1</source>
        <translation>У Вас нет прав на печать следующих страниц: %1</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="protection/printscreenprotection.cpp" line="57"/>
        <source>Print Screen option is disabled.</source>
        <translation>Создание снимков экрана невозможно.</translation>
    </message>
    <message>
        <location filename="protection/printscreenprotection.cpp" line="58"/>
        <source>To make a snapshot, please close the protected document.</source>
        <translation>Закройте защищённый документ и попробуйте снова.</translation>
    </message>
    <message>
        <location filename="miscellaneous/parameters.cpp" line="164"/>
        <location filename="miscellaneous/parameters.cpp" line="168"/>
        <source>Wrong parameter %1</source>
        <translation>Некорректная ссылка на документ: %1</translation>
    </message>
    <message>
        <location filename="miscellaneous/parameters.cpp" line="167"/>
        <source>ErrorTitle</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../../common/authorizationresult.cpp" line="30"/>
        <source>Unknown server reply</source>
        <translation>Некорректный ответ от сервера</translation>
    </message>
    <message>
        <source>Only KDE, GNOME and Unity desktop environments are supported.</source>
        <translation type="vanished">Поддерживаются только следующие окружения рабочего стола: KDE, GNOME, Unity.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="122"/>
        <source>An attempt to use a virtual PC has been detected. Please close all PC emulators and try again.</source>
        <translation>Была обнаружена попытка использования системного эмулятора. Пожалуйста, закройте все эмулирующие программы и попробуйте снова.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="136"/>
        <source>TimeLimitWarningTitle</source>
        <translation>Не удаётся запустить приложение</translation>
    </message>
    <message>
        <location filename="main.cpp" line="136"/>
        <source>TimeLimitWarningLabel</source>
        <translation>Срок действия приложения истёк</translation>
    </message>
    <message>
        <source>Print Screen option is disabled.
Please close GNOME ScreenShot application first.</source>
        <translation type="vanished">Создание снимков экрана невозможно.</translation>
    </message>
</context>
<context>
    <name>SearchPanel</name>
    <message>
        <location filename="gui/searchpanel.cpp" line="32"/>
        <source>Text for search:</source>
        <translation>Текст</translation>
    </message>
    <message>
        <location filename="gui/searchpanel.cpp" line="34"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="gui/searchpanel.cpp" line="35"/>
        <source>Clear</source>
        <translation>Очистить</translation>
    </message>
    <message>
        <location filename="gui/searchpanel.cpp" line="52"/>
        <source>Search results:</source>
        <translation>Результаты поиска:</translation>
    </message>
    <message>
        <location filename="gui/searchpanel.cpp" line="101"/>
        <source>Page %1 </source>
        <oldsource>Page %1</oldsource>
        <translation>Страница №%1</translation>
    </message>
    <message>
        <source>UncorrectSearchTitle</source>
        <translation type="vanished">НЭБ РФ</translation>
    </message>
    <message>
        <source>UncorrectSearchMessage</source>
        <translation type="vanished">Поисковая строка может содержать только буквы, цифры и пробелы</translation>
    </message>
    <message>
        <source>Searching...</source>
        <translation type="vanished">Поиск...</translation>
    </message>
    <message>
        <location filename="gui/searchpanel.cpp" line="127"/>
        <source>Searching was finished</source>
        <translation>Поиск завершён</translation>
    </message>
    <message>
        <location filename="gui/searchpanel.cpp" line="132"/>
        <source>Nothing was found</source>
        <translation>Ничего не найдено</translation>
    </message>
    <message>
        <location filename="gui/searchpanel.cpp" line="137"/>
        <source>Searching %1 percents where finished</source>
        <translation>Поиск... %1% завершено</translation>
    </message>
</context>
<context>
    <name>SearchResult</name>
    <message>
        <location filename="gui/searchresult.cpp" line="144"/>
        <source>Unexpected service respose</source>
        <translation>Некорректный ответ от сервиса выдачи документов</translation>
    </message>
</context>
<context>
    <name>Thumbnails</name>
    <message>
        <location filename="gui/thumbnails.cpp" line="48"/>
        <source>loading...</source>
        <translation>Загрузка...</translation>
    </message>
    <message>
        <location filename="gui/thumbnails.cpp" line="67"/>
        <source>empty</source>
        <translation>Пусто</translation>
    </message>
    <message>
        <location filename="gui/thumbnails.cpp" line="177"/>
        <source>loading %1%...</source>
        <oldsource>loading %1/%...</oldsource>
        <translation>Загрузка... %1% завершено</translation>
    </message>
    <message>
        <location filename="gui/thumbnails.cpp" line="181"/>
        <source>finished</source>
        <translation>Загрузка завершена</translation>
    </message>
</context>
<context>
    <name>UserPasswordDialog</name>
    <message>
        <location filename="gui/userpassworddialog.cpp" line="65"/>
        <source>UserName</source>
        <translation>Имя пользователя</translation>
    </message>
    <message>
        <location filename="gui/userpassworddialog.cpp" line="66"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="gui/userpassworddialog.cpp" line="67"/>
        <source>Ok</source>
        <translation>Ок</translation>
    </message>
    <message>
        <location filename="gui/userpassworddialog.cpp" line="68"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="gui/userpassworddialog.cpp" line="70"/>
        <source>Authentication</source>
        <translation>Аутентификация</translation>
    </message>
</context>
<context>
    <name>ZoomPanel</name>
    <message>
        <location filename="gui/zoompanel.cpp" line="36"/>
        <source>Scale</source>
        <translation>Масштаб</translation>
    </message>
    <message>
        <location filename="gui/zoompanel.cpp" line="44"/>
        <source>%1%</source>
        <translation>%1%</translation>
    </message>
</context>
</TS>
