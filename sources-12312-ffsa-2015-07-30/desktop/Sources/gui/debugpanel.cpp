#include "debugpanel.h"

#include <QTextEdit>
#include <QLineEdit>
#include <QVBoxLayout>

#include <common.h>

DebugPanel::DebugPanel(QWidget *parent) :
	QDockWidget(parent),
	m_textEdit( new QTextEdit() ),
	m_filter( new QLineEdit() ),
	m_layout( new QVBoxLayout() ),
	m_widget( new QWidget() ),
	m_lines()
{
	m_textEdit->setReadOnly( true );
	m_textEdit->setAcceptRichText( false );
	m_textEdit->setWordWrapMode( QTextOption::NoWrap );

	m_layout->addWidget( m_filter );
	m_layout->addWidget( m_textEdit );

	m_widget->setLayout( m_layout );

	connect(Debug::Instance(),SIGNAL(SignalOutput(QString)),this,SLOT(Output(QString)));
	connect(m_filter,SIGNAL(textChanged(QString)),this,SLOT(FilterChanged(QString)));

	this->setWidget( m_widget );
	this->setFeatures( QDockWidget::DockWidgetClosable | QDockWidget::DockWidgetMovable );
}

void DebugPanel::Output(QString text)
{
	this->show();
	m_lines.append( text );
	AppendLine( text );
}

void DebugPanel::FilterChanged(QString newFilter)
{
	UNUSED( newFilter );
	m_textEdit->clear();
	foreach( QString line, m_lines )
	{
		AppendLine( line );
	}
}

void DebugPanel::AppendLine(QString line)
{
	if( m_filter->text().isEmpty() ||
		line.contains( m_filter->text(), Qt::CaseInsensitive ) )
	{
		m_textEdit->append( line );
	}
}
