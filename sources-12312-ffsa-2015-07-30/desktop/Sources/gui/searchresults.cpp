#include "searchresults.h"
#include "flowlayout.h"
#include "searchresult.h"

#include <QScrollBar>

SearchResults::SearchResults(QWidget *parent) :
	QScrollArea(parent),
	m_flowLayout( NULL ),
	m_widget( NULL ),
	m_sent( 0 ),
	m_finished( 0 ),
	m_nothingWasFound( 0 ),
	m_baseRequest( NULL ),
	m_text()
{
	CreateGui();
}

SearchResults::~SearchResults()
{
	Clear();
}

void SearchResults::Load(int pagesCount, Request* baseRequest, QString text)
{
	Clear();
	m_sent = pagesCount;
	m_baseRequest = baseRequest->CreateCopy();
	m_text = text;
	LoadSomePages();
}

void SearchResults::Clear()
{
	m_sent = 0;
	m_finished = 0;
	m_nothingWasFound = 0;
	m_text.clear();
	DeleteLaterAndNull( m_baseRequest );
	for( int i = 0; i < Count(); ++i )
	{
		SearchResult* searchResult = Get( i );
		searchResult->Clear();
	}
	Select( Count() );
}

void SearchResults::Select(int pageIndex)
{
	for( int i = 0; i < Count(); ++i )
	{
		SearchResult* searchResult = Get( i );
		searchResult->setBackgroundRole( QPalette::Light );
		if( i == pageIndex && searchResult->IsEmpty() == false ) searchResult->setBackgroundRole( QPalette::Highlight );
	}

	DebugAssert( pageIndex >= 0 );
	if( pageIndex < 0 ) return;

	if( pageIndex < Count() ) this->ensureWidgetVisible( Get( pageIndex ) );
}

void SearchResults::showEvent(QShowEvent* event)
{
	UNUSED( event );
	m_flowLayout->DoLayout();
}

void SearchResults::SlotGotoSearch(int pageIndex, WordCoordinates coordinates)
{
	emit GotoSearch( pageIndex, coordinates );
}

void SearchResults::ResultFinished()
{
	m_finished += 1;
	EmitFinished();
	LoadSomePages();
}

void SearchResults::ResultNothingWasFound()
{
	m_nothingWasFound += 1;
	EmitFinished();
	LoadSomePages();
}

void SearchResults::LoadSomePages()
{
	DebugAssert( m_sent > 0 );
	if( m_sent <= 0 ) return;

	int loaded = m_finished + m_nothingWasFound;

	DebugAssert( loaded <= m_sent );
	if( loaded > m_sent ) return;

	if( loaded % 5 != 0 ) return;
	if( loaded == m_sent ) return;

	int pageStart = loaded + 1;
	int pageEnd = pageStart + 4;
	if( pageEnd > m_sent ) pageEnd = m_sent;

	for( int i = pageStart; i <= pageEnd; i++ )
	{
		while( Count() < i ) Add( NewSearchResult() );
		SearchResult* searchResult = Get( i - 1 );
		searchResult->Load( i, m_baseRequest, m_text );
	}
}

void SearchResults::CreateGui()
{
	m_flowLayout = new FlowLayout( 0, 0, 0 );
	m_widget = new QWidget();

	m_widget->setLayout( m_flowLayout );
	this->setWidgetResizable( true );
	this->setWidget( m_widget );
	this->setFrameStyle( QFrame::NoFrame );
	this->setAutoFillBackground( true );
	this->setBackgroundRole( QPalette::Light );
	this->verticalScrollBar()->setContextMenuPolicy( Qt::NoContextMenu );
	this->horizontalScrollBar()->setContextMenuPolicy( Qt::NoContextMenu );
}

SearchResult*SearchResults::NewSearchResult()
{
	SearchResult* result = new SearchResult();

	connect(result,SIGNAL(GotoSearch(int,WordCoordinates)),this,SLOT(SlotGotoSearch(int,WordCoordinates)));
	connect(result,SIGNAL(Finished()),this,SLOT(ResultFinished()));
	connect(result,SIGNAL(NothingWasFount()),this,SLOT(ResultNothingWasFound()));
	result->setAutoFillBackground( true );

	return result;
}

void SearchResults::EmitFinished()
{
	DebugAssert( m_sent > 0 );
	if( m_sent <= 0 ) return;

	DebugAssert( m_finished + m_nothingWasFound <= m_sent );
	if( m_finished + m_nothingWasFound > m_sent ) return;

	if( m_finished + m_nothingWasFound < m_sent )
	{
		emit Progress( m_finished + m_nothingWasFound, m_sent );
		return;
	}

	if( m_finished == 0 ) emit NothingWasFound();
	else emit SearchFinished();
}

int SearchResults::Count() const
{
	return m_flowLayout->count();
}

SearchResult*SearchResults::Get(int index) const
{
	return static_cast<SearchResult*>( m_flowLayout->itemAt( index )->widget() );
}

void SearchResults::Add(SearchResult* searchResult)
{
	m_flowLayout->addWidget( searchResult );
}
