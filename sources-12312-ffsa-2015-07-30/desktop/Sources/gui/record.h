#ifndef RECORD_H
#define RECORD_H

#include <QObject>

#include <common.h>

class QLabel;

class Record : public QObject
{
	Q_OBJECT
public:
	explicit Record(QObject *parent = 0);
	~Record();

	bool IsEmpty(void)const;
	void Load( Request* baseRequest, QString field, QString subField, RequestPriority priority = PRIORITY_NORMAL );
	void Clear(void);

	QLabel* Label(void)const;
	QLabel* Value(void)const;

	void SetLabel(QString label );

signals:
	void Ready();

private slots:
	void Finished( ErrorCode error, QString description );

private:
	bool IsActual( Request* baseRequest, QString field, QString subField )const;

	QLabel* m_label;
	QLabel* m_value;
	RequestDescription* m_request;
};

#endif // RECORD_H
