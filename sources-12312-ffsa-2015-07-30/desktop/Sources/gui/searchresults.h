#ifndef SEARCHRESULTS_H
#define SEARCHRESULTS_H

#include <QScrollArea>

#include <common.h>

class FlowLayout;
class Request;
class SearchResult;

class SearchResults : public QScrollArea
{
	Q_OBJECT
public:
	explicit SearchResults(QWidget *parent = 0);
	virtual ~SearchResults();

	void Load( int pagesCount, Request* baseRequest, QString text );
	void Clear(void);

public slots:
	void Select( int pageIndex );

signals:
	void GotoSearch( int pageIndex, WordCoordinates coordinates );
	void SearchFinished();
	void NothingWasFound();
	void Progress( int received, int sent );

protected:
	virtual void showEvent(QShowEvent* event);

private slots:
	void SlotGotoSearch( int pageIndex, WordCoordinates coordinates );
	void ResultFinished();
	void ResultNothingWasFound();

private:
	// Private methods
	void LoadSomePages(void);
	void CreateGui(void);
	SearchResult* NewSearchResult(void);
	void EmitFinished(void);

	int Count(void)const;
	SearchResult* Get( int index )const;
	void Add( SearchResult* thumbnail );

	// Gui items
	FlowLayout*		m_flowLayout;
	QWidget*		m_widget;

	int m_sent;
	int m_finished;
	int m_nothingWasFound;
	Request* m_baseRequest;
	QString m_text;
};

#endif // SEARCHRESULTS_H
