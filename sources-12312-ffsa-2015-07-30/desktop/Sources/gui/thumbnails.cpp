#include "thumbnails.h"
#include "flowlayout.h"
#include "thumbnail.h"

#include <QObjectList>
#include <QList>
#include <QScrollBar>
#include <QScrollArea>
#include <QVBoxLayout>
#include <QLabel>

#include <algorithm>

#include <bookmarksmanager.h>
#include <commentsmanager.h>

Thumbnails::Thumbnails(QWidget *parent) :
	QWidget(parent),
	m_flowLayout( NULL ),
	m_widget( NULL ),
	m_scrollArea( NULL ),
	m_boxLayout( NULL ),
	m_label( NULL ),
	m_selectionRangeStart( -1 ),
	m_selectionRangeEnd( -1 ),
	m_selection(),
	m_selectedPosition(),
	m_pagesLoaded( 0 ),
	m_pagesCount( 0 ),
	m_baseRequest( NULL )
{
	CreateGui();
	Clear();
}

Thumbnails::~Thumbnails()
{
	Clear();
}

void Thumbnails::Load(int pagesCount, Request* baseRequest)
{
	Clear();
	while( Count() < pagesCount ) Add( NewThumbnail() );
	m_pagesCount = pagesCount;
	m_baseRequest = baseRequest->CreateCopy();
	LoadSomePages();
	m_label->setText( tr( "loading..." ) );
	m_label->show();
}

void Thumbnails::Clear()
{
	m_selectionRangeStart = -1;
	m_selectionRangeEnd = -1;
	m_selection.clear();
	m_selectedPosition = QPoint();
	for( int i = 0; i < Count(); ++i )
	{
		Thumbnail* thumbnail = Get( i );
		thumbnail->Clear();
		thumbnail->SetBookmark( false );
		thumbnail->SetComment( QString() );
	}
	m_pagesLoaded = 0;
	m_pagesCount = 0;
	m_label->setText( tr( "empty" ) );
	m_label->hide();
	DeleteLaterAndNull( m_baseRequest );
}

PageSet Thumbnails::SelectedPages() const
{
	PageSet result;

	for( int i = 0; i < Count(); ++i )
	{
		if( IsSelected( i ) ) result.Add( i + 1 );
	}

	return result;
}

void Thumbnails::UpdateBookmarks(BookmarksManager* bookmarks)
{
	for( int i = 0; i < Count(); ++i )
	{
		Thumbnail* thumbnail = Get( i );
        thumbnail->SetBookmark( bookmarks->Contains( i + 1, "" ) );
        //TODO
	}
}

void Thumbnails::UpdateComments(CommentsManager* comments)
{
	for( int i = 0; i < Count(); ++i )
	{
		Thumbnail* thumbnail = Get( i );
        thumbnail->SetComment( comments->Get( i + 1, "" ) );
        //TODO
	}
}

void Thumbnails::resizeEvent(QResizeEvent* event)
{
	QWidget::resizeEvent( event );
	EnsureSelectedIsVisible();
}

void Thumbnails::showEvent(QShowEvent* event)
{
	UNUSED( event );
	m_flowLayout->DoLayout();
}

void Thumbnails::Select(int pageIndex)
{
	DebugAssert( pageIndex >=0 && pageIndex < Count() );
	if( pageIndex < 0 || pageIndex >= Count() ) return;

	m_selection.clear();
	m_selection.insert( pageIndex );

	m_selectionRangeStart = pageIndex;
	m_selectionRangeEnd = pageIndex;

	m_selectedPosition = QPoint();

	HighlightSelected();
	EnsureSelectedIsVisible();
}

void Thumbnails::SlotGotoPage(int pageIndex)
{
	emit GotoPage( pageIndex );
}

void Thumbnails::AddToSelection(int pageIndex)
{
	if( IsSelected( pageIndex ) ) m_selection.remove( pageIndex );
	else m_selection.insert( pageIndex );

	int start = m_selectionRangeStart;
	int end = m_selectionRangeEnd;
	if( start > end ) std::swap( start, end );
	if( start != end )
	{
		for( int i = start; i <= end; ++i )
		{
			if( i == pageIndex ) continue;
			m_selection.insert( i );
		}
	}

	m_selectionRangeStart = pageIndex;
	m_selectionRangeEnd = pageIndex;

	HighlightSelected();
}

void Thumbnails::AddRangeToSelection(int pageIndex)
{
	m_selectionRangeEnd = pageIndex;
	HighlightSelected();
}

void Thumbnails::Loaded(int pageIndex)
{
	UNUSED( pageIndex );
	EnsureSelectedIsVisible();
	m_pagesLoaded += 1;
	LoadSomePages();

	if( m_pagesCount > 0 )
	{
		int percent = ( 100 * m_pagesLoaded ) / m_pagesCount;
		m_label->setText( tr( "loading %1%..." ).arg( percent ) );
	}
	if( m_pagesLoaded == m_pagesCount )
	{
		m_label->setText( tr( "finished" ) );
		m_label->hide();
	}
}

void Thumbnails::CreateGui()
{
	m_flowLayout = new FlowLayout();
	m_widget = new QWidget();
	m_scrollArea = new QScrollArea();
	m_boxLayout = new QVBoxLayout();
	m_label = new QLabel();

	m_widget->setLayout( m_flowLayout );
	m_scrollArea->setWidgetResizable( true );
	m_scrollArea->setWidget( m_widget );
	m_scrollArea->setFrameStyle( QFrame::NoFrame );
	m_scrollArea->setAutoFillBackground( true );
	m_scrollArea->setBackgroundRole( QPalette::Light );
	m_scrollArea->verticalScrollBar()->setContextMenuPolicy( Qt::NoContextMenu );
	m_scrollArea->horizontalScrollBar()->setContextMenuPolicy( Qt::NoContextMenu );

	m_label->setFrameStyle( QFrame::StyledPanel );
	m_label->setMargin( 5 );

	m_boxLayout->addWidget( m_scrollArea );
	m_boxLayout->addWidget( m_label );
	m_boxLayout->setMargin( 0 );
	m_boxLayout->setSpacing( 0 );
	this->setLayout( m_boxLayout );
}

Thumbnail*Thumbnails::NewThumbnail()
{
	Thumbnail* result = new Thumbnail();

	connect(result,SIGNAL(GotoPage(int)),this,SLOT(SlotGotoPage(int)));
	connect(result,SIGNAL(AddRangeToSelection(int)),this,SLOT(AddRangeToSelection(int)));
	connect(result,SIGNAL(AddToSelection(int)),this,SLOT(AddToSelection(int)));
	connect(result,SIGNAL(Loaded(int)),this,SLOT(Loaded(int)));
	result->setAutoFillBackground( true );

	return result;
}

void Thumbnails::HighlightSelected()
{
	for( int i = 0; i < Count(); ++i )
	{
		Thumbnail* thumbnail = Get( i );

		if( IsSelected( i ) )
		{
			thumbnail->setBackgroundRole( QPalette::Highlight );
		}
		else
		{
			thumbnail->setBackgroundRole( QPalette::Light );
		}
	}
}

bool Thumbnails::IsSelected(int index) const
{
	if( m_selection.contains( index ) ) return true;

	int start = m_selectionRangeStart;
	int end = m_selectionRangeEnd;
	if( start > end ) std::swap( start, end );

	if( start != end )
	{
		if( index >= start && index <= end ) return true;
	}

	return false;
}

void Thumbnails::EnsureSelectedIsVisible()
{
	if( m_selectionRangeStart == -1 ) return;

	DebugAssert( m_selectionRangeStart >= 0 && m_selectionRangeStart < Count() );
	if( m_selectionRangeStart < 0 || m_selectionRangeStart >= Count() ) return;

	Thumbnail* selected = Get( m_selectionRangeStart );
	if( selected->IsLoaded() == false ) return;
	if( m_selectedPosition == selected->pos() ) return;

	m_selectedPosition = selected->pos();
	m_scrollArea->ensureWidgetVisible( selected );
}

void Thumbnails::LoadSomePages()
{
	DebugAssert( m_pagesLoaded <= m_pagesCount );
	if( m_pagesLoaded > m_pagesCount ) return;

	if( m_pagesLoaded % 5 != 0 ) return;
	if( m_pagesLoaded == m_pagesCount ) return;

	int pageStart = m_pagesLoaded + 1;
	int pageEnd = pageStart + 4;
	if( pageEnd > m_pagesCount ) pageEnd = m_pagesCount;

	for( int i = pageStart; i <= pageEnd; ++i )
	{
		Thumbnail* thumbnail = Get( i - 1 );
        thumbnail->Load( QPair<int, QString>(i, ""), m_baseRequest );
	}
}

int Thumbnails::Count() const
{
	return m_flowLayout->count();
}

Thumbnail*Thumbnails::Get(int index) const
{
	return static_cast<Thumbnail*>( m_flowLayout->itemAt( index )->widget() );
}

void Thumbnails::Add(Thumbnail* thumbnail)
{
	m_flowLayout->addWidget( thumbnail );
	thumbnail->move( -thumbnail->width(), 0 );
}
