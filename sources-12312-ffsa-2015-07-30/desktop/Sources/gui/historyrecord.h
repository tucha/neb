#ifndef HISTORYRECORD_H
#define HISTORYRECORD_H

#include <QWidget>

class QHBoxLayout;
class QVBoxLayout;
class QLabel;
class QAction;

#include <common.h>
#include <document.h>

class HistoryRecord : public QWidget
{
	Q_OBJECT
public:
	explicit HistoryRecord(QWidget *parent = 0);
	virtual ~HistoryRecord();

	void Load( RequestPage* baseRequest );
	void Clear(void);
	bool IsEmpty(void)const;

	void SetSelected( bool value );
	bool GetSelected( void )const;

	Document GetDocument(void)const;

signals:
	void MouseClicked( HistoryRecord* record );
	void DownloadingError( Document document );
	void OpenDocument( Document document );

protected:
	virtual void mousePressEvent(QMouseEvent* event);
	virtual void mouseDoubleClickEvent(QMouseEvent* event);

private slots:
	void IconFinished( ErrorCode error, QImage image );
	void AuthorFinished( ErrorCode error, QString author );
	void TitleFinished( ErrorCode error, QString title );

    void TypeRequestFinished(ErrorCode err, Document::Type type);
private:
	void CreateGui(void);
	void ConnectSlots(void);
	void Translate(void);

	bool IsActual( RequestPage* baseRequest )const;

	QHBoxLayout*	m_horizontalLayout;
	QVBoxLayout*	m_verticalLayout;
	QLabel*			m_icon;
	QLabel*			m_author;
	QLabel*			m_title;

	RequestImageFixed*	m_requestIcon;
	RequestDescription*	m_requestAuthor;
	RequestDescription*	m_requestTitle;
};

#endif // HISTORYRECORD_H
