#include "downloadwindow.h"

#include <QPrinter>
#include <QPainter>
#include <QVBoxLayout>
#include <QLabel>
#include <QProgressBar>
#include <QPushButton>
#include <QFileDialog>
#include <QMessageBox>

DownloadWindow::DownloadWindow(QWidget *parent) :
	QMainWindow(parent),

	m_baseRequest( NULL ),
	m_requestImage( NULL ),
	m_printer( NULL ),
	m_painter( NULL ),

	m_layout( new QVBoxLayout() ),
	m_label( new QLabel( this ) ),
	m_progressBar( new QProgressBar( this ) ),
	m_button( new QPushButton( this ) ),

	m_pages(),
	m_current( -1 ),

	m_forbiddenPages()
{
	CreateGui();
	Translate();

	connect(m_button,SIGNAL(clicked()),this,SLOT(Cancel()));
	setFixedSize( 400, sizeHint().height() );
	setWindowModality( Qt::ApplicationModal );
}

DownloadWindow::~DownloadWindow()
{
	Cancel();
}

void DownloadWindow::StartDownloading(Request* baseRequest, PageSet pageSet)
{
	DebugAssert( pageSet.IsEmpty() == false );
	if( pageSet.IsEmpty() ) return;

	if( this->isVisible() ) return;
	this->show();

	m_pages = pageSet.ToList();
	m_current = 0;
	m_baseRequest = baseRequest;

	m_label->setText( tr( "Downloading..." ) );

	QString fileName = QFileDialog::getSaveFileName( this, tr( "Save document" ), QString(), tr( "PDF Files (*.pdf)" ) );
	if( fileName.isEmpty() )
	{
		Cancel();
		return;
	}

	DeleteAndNull( m_painter );
	DeleteAndNull( m_printer );

	m_printer = new QPrinter( QPrinter::ScreenResolution );
	m_printer->setOutputFormat( QPrinter::PdfFormat );
	m_printer->setOutputFileName( fileName );
	if( m_printer->isValid() == false )
	{
		Cancel();
		return;
	}

	m_painter = new QPainter();

	DownloadPage( m_pages.at( m_current ) );
}

void DownloadWindow::closeEvent(QCloseEvent* event)
{
	UNUSED( event );
	Cancel();
}

void DownloadWindow::SlotImageFinished(ErrorCode error, QImage image)
{
	if( m_requestImage == NULL ) return;
	if( sender() != m_requestImage ) return;

	if( error == QNetworkReply::ContentOperationNotPermittedError )
	{
		m_forbiddenPages.Add( SENDER_PAGE->PageIndex() );
		DownloadNextPage();
		return;
	}

	if( error != API_NO_ERROR )
	{
		m_requestImage->Send();
		return;
	}

#if QT_VERSION >= 0x050000
	bool test = m_printer->setPageSize( QPageSize( image.size(), QPageSize::Point ) );
	DebugAssert( test );
	UNUSED( test );
#else
	m_printer->setPaperSize( image.size(), QPrinter::Point );
#endif

	if( m_current == 0 )
	{
		bool test = m_painter->begin( m_printer );
		DebugAssert( test );
		DebugAssert( m_painter->isActive() );
		if( test == false || m_painter->isActive() == false )
		{
			Cancel();
			return;
		}
	}

	if( m_current != 0 ) m_printer->newPage();
	m_painter->drawImage( m_printer->pageRect(), image );

	DownloadNextPage();
}

void DownloadWindow::SlotDownloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
	m_progressBar->setMaximum( bytesTotal );
	m_progressBar->setValue( bytesReceived );
}

void DownloadWindow::Cancel()
{
	m_baseRequest  = NULL;

	DeleteLaterAndNull( m_requestImage );
	DeleteAndNull( m_painter );
	DeleteAndNull( m_printer );

	m_pages.clear();
	m_current = -1;

	m_progressBar->setValue( 0 );
	m_label->clear();

	m_forbiddenPages = PageSet();

	close();
}

void DownloadWindow::CreateGui()
{
	m_layout->addWidget( m_label );
	m_layout->addWidget( m_progressBar );
	m_layout->addWidget( m_button, 0, Qt::AlignCenter );

	this->setWindowFlags( Qt::Window | Qt::WindowCloseButtonHint );

	QWidget* tmp = new QWidget( this );
	tmp->setLayout( m_layout );
	this->setCentralWidget( tmp );
}

void DownloadWindow::Translate()
{
	m_button->setText( tr( "Cancel" ) );
	this->setWindowTitle( tr( "Saving Document" ) );
}

void DownloadWindow::DownloadPage(int pageIndex)
{
	DebugAssert( pageIndex > 0 );
	if( pageIndex <= 0 )
	{
		Cancel();
		return;
	}

	DeleteLaterAndNull( m_requestImage );

	RequestPage* tmp = m_baseRequest->NewPage( pageIndex );

	m_requestImage = tmp->NewImage( TYPE_IMAGES, 300, FORMAT_JPEG );
	connect(m_requestImage,SIGNAL(SignalFinished(ErrorCode,QImage)),this,SLOT(SlotImageFinished(ErrorCode,QImage)));
	connect(m_requestImage,SIGNAL(SignalDownloadProgress(qint64,qint64)),this,SLOT(SlotDownloadProgress(qint64,qint64)));
	m_requestImage->Send();

	DeleteLaterAndNull( tmp );

	m_label->setText( tr( "Downloading page %1 ( %2 of %3 ) ..." )
					  .arg( pageIndex )
					  .arg( m_current + 1 )
					  .arg( m_pages.count() ) );
	m_progressBar->setValue( 0 );
}

void DownloadWindow::DownloadNextPage()
{
	m_current += 1;
	if( m_current == m_pages.count() )
	{
		ShowForbiddenPages();
		Cancel();
		return;
	}

	DownloadPage( m_pages.at( m_current ) );
}

void DownloadWindow::ShowForbiddenPages()
{
	if( m_forbiddenPages.IsEmpty() ) return;

	QString pages = m_forbiddenPages.ToString();

	QString title = tr( "AccessError" );
	QString message = tr( "Pages where not saved: %1" ).arg( pages );
	QMessageBox::information( this, title, message );
}
