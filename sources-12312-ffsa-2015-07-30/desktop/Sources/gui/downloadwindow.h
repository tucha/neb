#ifndef DOWNLOADWINDOW_H
#define DOWNLOADWINDOW_H

#include <QMainWindow>
#include <QVector>

#include <common.h>

class QPrinter;
class QProgressBar;
class QLabel;
class QPushButton;
class QVBoxLayout;
class QPainter;

class DownloadWindow : public QMainWindow
{
	Q_OBJECT
public:
	explicit DownloadWindow(QWidget *parent = 0);
	virtual ~DownloadWindow();

	void StartDownloading(Request* baseRequest, PageSet pageSet);

protected:
	virtual void closeEvent(QCloseEvent* event);

private slots:
	void SlotImageFinished( ErrorCode error, QImage image );
	void SlotDownloadProgress( qint64 bytesReceived, qint64 bytesTotal );
	void Cancel(void);

private:
	void CreateGui(void);
	void Translate(void);
	void DownloadPage( int pageIndex );
	void DownloadNextPage(void);
	void ShowForbiddenPages(void);

	Request*			m_baseRequest;
	RequestImage*		m_requestImage;
	QPrinter*			m_printer;
	QPainter*			m_painter;

	QVBoxLayout*	m_layout;
	QLabel*			m_label;
	QProgressBar*	m_progressBar;
	QPushButton*	m_button;

	QVector<int>	m_pages;
	int				m_current;

	PageSet			m_forbiddenPages;
};

#endif // DOWNLOADWINDOW_H
