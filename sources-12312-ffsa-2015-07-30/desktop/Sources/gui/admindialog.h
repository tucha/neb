#ifndef ADMINDIALOG_H
#define ADMINDIALOG_H

#include <QWidget>
#include <QDialog>
#include <accountbaserequest.h>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>

class CheckTrustedHostRequest;
class AddTrustedHostRequest;
class BlockFingerprintRequest;

class AdminDialog : public QDialog
{
    Q_OBJECT
    AccountBaseRequest* m_baseRequest;

    CheckTrustedHostRequest* m_checkRequest;
    AddTrustedHostRequest*   m_addRequest;
    BlockFingerprintRequest* m_blockRequest;

    void ShowError(ErrorCode err);

    QLabel* m_infoLabel;
    QPushButton *m_addButton;
    QPushButton *m_removeButton;

public:
    AdminDialog(QWidget * parent = 0);
    virtual ~AdminDialog();
    void setBaseRequest(AccountBaseRequest * req);
public slots:
    void Exec();
private slots:
    void CheckFinished(ErrorCode error, QVariantMap res);
    void AddFinished(ErrorCode error, bool res);
};

#endif // ADMINDIALOG_H
