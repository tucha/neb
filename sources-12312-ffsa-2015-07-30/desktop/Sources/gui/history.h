#ifndef HISTORY_H
#define HISTORY_H

#include <QScrollArea>

#include <QStringList>

class Request;
class QVBoxLayout;
class HistoryRecord;
class QWidget;
class Config;
class QAction;

#include <document.h>

class History : public QScrollArea
{
	Q_OBJECT
public:
	explicit History(QWidget *parent = 0);
	~History();

	void Load(Request* baseRequest, DocumentList documents, Config* config );
	void Clear(void);

	void RemoveDocument( Document document );

signals:
	void OpenDocument( Document document );
	void DownloadingError( Document document );
	void RequestRemoval( Document document );

protected:
	virtual void keyPressEvent(QKeyEvent* event);

private slots:
	void RecordClicked( HistoryRecord* record );
	void ActionRemoveTriggered();

private:
	HistoryRecord* NewRecord(void);
	void Add( HistoryRecord* record );
	void Remove( HistoryRecord* record );
	int Count(void)const;
	HistoryRecord* Get( int index )const;
	int SelectedIndex(void)const;
	DocumentList Reverse( DocumentList documents )const;
	void OptimizeRecords( DocumentList needDocuments );
	QHash<Document,int> CollectDocumentHash(void)const;
	void Swap( int index1, int index2 );
	void UpdateDocumentHash(QHash<Document, int>& documentHash, int index1, int index2 )const;
	bool TestDocumentHash( const QHash<Document,int>& documentHash )const;

	QVBoxLayout*	m_layout;
	QWidget*		m_widget;
	QAction*		m_actionRemove;
};

#endif // HISTORY_H
