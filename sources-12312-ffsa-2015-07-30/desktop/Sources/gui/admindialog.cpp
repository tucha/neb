#include "admindialog.h"
#include <addtrustedhostrequest.h>
#include <blockfingerprintrequest.h>
#include <checktrustedhostrequest.h>

void AdminDialog::ShowError(ErrorCode err)
{
    m_addButton->hide();
    m_removeButton->hide();
    m_infoLabel->setText(tr("Server API error: %1").arg(err));
}

AdminDialog::AdminDialog(QWidget *parent):
    QDialog(parent)
{
    setWindowTitle(tr("Trusted registry administration"));
    m_checkRequest = nullptr;
    m_addRequest = nullptr;
    m_blockRequest = nullptr;

    auto l = new QVBoxLayout(this);
    setLayout(l);

    m_infoLabel = new QLabel(this);
    l->addWidget(m_infoLabel);

    m_addButton = new QPushButton(tr("Add to registry"), this);
    l->addWidget(m_addButton);
    m_addButton->hide();

    m_removeButton = new QPushButton(tr("Remove from registry"), this);
    l->addWidget(m_removeButton);
    m_removeButton->hide();
}

AdminDialog::~AdminDialog()
{
    delete m_checkRequest;
    delete m_addRequest ;
    delete m_blockRequest;
}

void AdminDialog::setBaseRequest(AccountBaseRequest *req)
{
    m_baseRequest = req;

    m_checkRequest = m_baseRequest->NewCheckTrustedHostRequest();
    m_addRequest = m_baseRequest->NewAddTrustedHostRequest();
    m_blockRequest = m_baseRequest->NewBlockFingerprintRequest();


    connect(m_checkRequest, &CheckTrustedHostRequest::SignalFinished, this, &AdminDialog::CheckFinished);


    connect(m_addButton, &QPushButton::clicked, [=](){
        m_addRequest->Send();
    });

    connect(m_addRequest, &AddTrustedHostRequest::SignalFinished, this, &AdminDialog::AddFinished);
}

void AdminDialog::AddFinished(ErrorCode error, bool res)
{
    Q_UNUSED(res)
    if(error != ErrorCode::NoError)
    {
        ShowError(error);
    }
    else
    {
        m_infoLabel->setText(tr("This machine is in the trusted registry now"));
        m_addButton->hide();
        m_removeButton->hide();
    }
}

void AdminDialog::CheckFinished(ErrorCode error, QVariantMap res)
{

    if(error != ErrorCode::NoError)
    {
        ShowError(error);
    }
    else {
        auto code = res.value("result", -1).toInt();
        switch(code)
        {
        case 0:
            m_infoLabel->setText(tr("This machine is not in the trusted registry"));
            m_addButton->show();
            m_removeButton->hide();
            break;
        case 1:
            m_infoLabel->setText(tr("This machine is in the trusted registry now"));
            m_addButton->hide();
            m_removeButton->hide();
            break;
        case 2:
            m_infoLabel->setText(tr("This machine is in the trusted registry and blocked"));
            m_addButton->hide();
            m_removeButton->hide();
            break;
        default:
            ShowError(ErrorCode::UnknownContentError);
        }
    }
//        qDebug() <<  res;
    show();
}

void AdminDialog::Exec()
{
    m_checkRequest->Send();

}
