#include "mainwindow.h"
#include "display.h"
#include "thumbnails.h"
#include "searchpanel.h"
#include "thumbnail.h"
#include "debugpanel.h"
#include "pagescontrol.h"
#include "zoompanel.h"
#include "pagesetwindow.h"
#include "../miscellaneous/renderer.h"
#include "commentwindow.h"
#include "bookmarkscontrol.h"
#include "history.h"
#include "description.h"
#include "../logic.h"
#include <favoritesmanager.h>
#include "contentdisplay.h"
#include <QPainter>

#ifdef OS_MACOSX
#include "protection/windowcaptureprotection.h"
#endif

#include <QToolBar>
#include <QDesktopWidget>
#include <QSize>
#include <QApplication>
#include <QSplitter>
#include <QTabWidget>
#include <QAction>
#include <QActionGroup>
#include <QMessageBox>
#include <QWheelEvent>
#include <QInputDialog>
#include <QStackedLayout>
#include "gui/QEpubRenderer/QEpubRenderer.h"
#include "gui/viewerscontainer.h"
#include "gui/admindialog.h"

#ifdef OS_MACOSX
#define ToolTip( T ) T->setToolTip( T->toolTip() + " (" + T->shortcut().toString( QKeySequence::NativeText   ) + ")" )
#else
#define ToolTip( T ) T->setToolTip( T->toolTip() + " (" + T->shortcut().toString( QKeySequence::PortableText ) + ")" )
#endif

MainWindow::MainWindow(QWidget *parent, Logic* owner):
    QMainWindow(parent),

    m_guiSplitter( NULL ),
    m_guiTabWidget( NULL ),

    m_display( NULL ),
    #ifndef DISABLE_THUMBNAILS
    m_thumbnails( NULL ),
    #endif
    m_searchPanel( NULL ),
    m_debugPanel( NULL ),
    m_pagesControl( NULL ),
    m_zoomPanel( NULL ),
    m_pageSetWindow( NULL ),
    m_bookmarksControl( NULL ),
    m_commentsControl( NULL ),

    m_renderer( new Renderer( this ) ),
    m_request( NULL ),
    m_user(),
    m_pagesCount( 0 ),
    m_previousState( Qt::WindowNoState ),

    m_state(),

    m_bookmarksManager( NULL, PRIORITY_HIGHT ),
    m_commentsManager( NULL, PRIORITY_HIGHT ),

    m_commentWindow( new CommentWindow( NULL ) ),
    m_favorite( NULL ),
    m_history( NULL ),
    m_contentDisplay(nullptr),

    m_description( new Description( NULL ) ),

    m_firstWarningDialog( NULL ),
    m_secondWarningDialog( NULL ),
    m_secondWarningResult( QMessageBox::NoButton ),
    m_pendingRequestMessage( NULL ),
    m_featureNotAllowedMessage(NULL),

    m_owner( owner ),

    m_secondWarningPagesLeft( 0 ),
    m_secondWarningLastTime(),
    m_secondWarningInterval( 0 ),
    m_secondWarningShowOnlyOnce( false ),

    m_pageIsUnavailableMessage( NULL ),

    m_EpubRenderer(nullptr),
    m_documentType(Document::UNKNOWN),
    m_epubRequest(nullptr),
    m_epubRestored(false)
{
    CreateActions();
    CreateGui();
    ConnectSlots();
    Translate();
    PrepareWindow();
    CreateWarningDialogs();

    Clear();

    m_description->setParent( this, Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowCloseButtonHint );
    m_commentWindow->setParent( this, Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowCloseButtonHint );
}

MainWindow::~MainWindow()
{
    Clear();
    DeleteWarningDialogs();
    delete m_EpubRenderer;
}

bool MainWindow::IsEmpty() const
{
    bool result = m_request == NULL;

    if( result )
    {
        DebugAssert( m_user.IsEmpty() );
    }
    else
    {
        DebugAssert( m_user.IsEmpty() == false );
    }

    return result;
}

void MainWindow::Load(QPointer<Request> baseRequest, AccountBaseRequest* accountBase, DocumentState state, FavoritesManager *favoritesManager)
{
    DebugAssert( baseRequest != NULL );
    if( baseRequest == NULL ) return;

    DebugAssert( accountBase != NULL );
    if( accountBase == NULL ) return;

    Clear();

    m_user = accountBase->GetUser();
    m_state = state;
    m_request = baseRequest->NewPagesCount( PRIORITY_HIGHT );

    auto typeRequest = baseRequest->NewDocumentFormat(PRIORITY_HIGHT);
    connect(typeRequest, &RequestDocumentFormat::SignalFinished, [=](ErrorCode err, Document::Type type)
    {
        typeRequest->deleteLater();
        m_documentType = type;
        if(err != ErrorCode::NoError){
            qDebug() << "RequestDocumentFormat error:" << err;
            qDebug() << baseRequest->GetDocument().DocumentId();
            return;
        }
        else
        {

        }
        if(baseRequest.isNull())
        {
            return;
        }
        //        qDebug() << err << type;

        switch(type)
        {
        case Document::EPUB:
        {
            if(m_epubRequest) m_epubRequest->deleteLater();
            m_epubRequest = m_request->NewDownloadEpub(PRIORITY_HIGHT);
            connect(m_epubRequest, &RequestDownloadEpub::SignalDownloadProgress, m_display, &Display::SlotDownloadProgress);
            connect(m_epubRequest, &RequestDownloadEpub::SignalFinished,
                    [=]( ErrorCode error, QByteArray epubData){
                if(m_epubRequest)m_epubRequest->deleteLater();
                 m_epubRequest = nullptr;
                if(error != ErrorCode::NoError) {
                    qDebug() << "DownloadEpub error:" << error << "\nEpubData" << epubData;
                    return;
                }

                m_EpubRenderer->SetEpubData(epubData);

                //                qDebug() << error << epubData.size();

                EnableGui();

                BookmarksUpdate( &m_bookmarksManager );
                CommentsUpdate( &m_commentsManager );
#ifndef DISABLE_THUMBNAILS
                m_thumbnails->UpdateBookmarks( &m_bookmarksManager );
                m_thumbnails->UpdateComments( &m_commentsManager );
#endif
            }
            );
            m_epubRequest->Send();
            m_bookmarksControl->Load( baseRequest );
            m_commentsControl->Load( baseRequest );

            m_bookmarksManager.Load( accountBase, baseRequest->GetDocument() );
            m_commentsManager.Load( accountBase, baseRequest->GetDocument() );
            UpdateFavorites( favoritesManager );
        }
            break;
        case Document::PDF:
            if(m_request.isNull()) return;
            connect(m_request,SIGNAL(SignalFinished(ErrorCode,int)),this,SLOT(SlotFinishedPagesCount(ErrorCode,int)));
            m_request->Send();

            m_bookmarksControl->Load( baseRequest );
            m_commentsControl->Load( baseRequest );

            m_bookmarksManager.Load( accountBase, baseRequest->GetDocument() );
            m_commentsManager.Load( accountBase, baseRequest->GetDocument() );

            m_secondWarningInterval = Settings().SecondWarningInterval();
            m_secondWarningShowOnlyOnce = false;
            UpdateFavorites( favoritesManager );
            {
                auto contentRequest = baseRequest->NewContent(PRIORITY_HIGHT);
                connect(contentRequest, &RequestContent::SignalFinished,[=](ErrorCode , QVariantList c){
                    parsePdfToc(c);
                });
                contentRequest->Send();
            }
            break;
        case Document::UNKNOWN:
            qDebug() << "Document::UNKNOWN, shouldn't be here";
            break;
        }

    });
    typeRequest->Send();
}

void MainWindow::Clear()
{
    emit SaveState( GetDocumentState() );

#ifndef DISABLE_THUMBNAILS
    m_thumbnails->Clear();
#endif
    m_display->Clear();
    m_searchPanel->Clear();
    m_renderer->Clear();
    m_pagesControl->Clear();
    m_bookmarksManager.Clear();
    m_commentsManager.Clear();
    m_renderer->ResetRotation();
    m_bookmarksControl->Clear();
    m_commentsControl->Clear();
    m_description->Clear();
    m_EpubRenderer->clear();
    m_contentDisplay->Clear();

    DeleteLaterAndNull( m_request );
    m_user = User();

    m_pagesCount = 0;

    DisableGui();

    m_commentWindow->hide();
    m_description->hide();

    HideWarningDialogs();
    m_secondWarningResult = QMessageBox::NoButton;
    m_secondWarningPagesLeft = 0;
    m_secondWarningLastTime = QDateTime();
    m_secondWarningInterval = 0;
    m_secondWarningShowOnlyOnce = false;


    if(m_epubRequest)m_epubRequest->deleteLater();
    m_epubRequest = nullptr;

    m_documentType = Document::UNKNOWN;
    m_epubRestored = false;
}

DocumentState MainWindow::GetDocumentState() const
{
    DocumentState result;

    if( IsEmpty() ) return result;
    result.SetDocument( m_request->GetDocument() );

    if (m_documentType == Document::PDF)
    {
        if( m_renderer->IsEmpty() ) return result;
        result.SetPageIndex( m_renderer->PageIndex() );
        result.SetAngle( m_renderer->GetScaler()->Angle() );
        result.SetInverted( m_renderer->Inverted() );
        result.SetScaleWidth( m_renderer->GetScaler()->ScaleWidth() );
        result.SetScaleFull( m_renderer->GetScaler()->ScaleFull() );
        result.SetScale( m_renderer->GetScaler()->ExplicitScale() );
    }
    else if(m_documentType == Document::EPUB)
    {
        result.SetPageIndex(m_EpubRenderer->currentCfi());
        result.SetInverted(m_EpubRenderer->invertedColors());
        result.SetScale(int(m_EpubRenderer->zoomMultiplier()*100.0f));
    }
    //	result.SetWindowState( saveState() );

    return result;
}

History*MainWindow::GetFavorite() const
{
    return m_favorite;
}

History*MainWindow::GetHistory() const
{
    return m_history;
}

void MainWindow::EnableAdminInterface(bool enabled, AccountBaseRequest* baseRequest)
{
    m_adminDialog->setBaseRequest(baseRequest);
    m_toolBarAdmin->setVisible(enabled);
}

void MainWindow::UpdateFavorites(FavoritesManager* favorites)
{
    if( IsEmpty() ) return;

    Document document = m_request->GetDocument();
    if( favorites->IsActual( document ) == false ) return;

    m_actionFavorite->setEnabled( true );
    m_actionFavorite->setChecked( favorites->Contains( document ) );

    SetFavoritesTooltip( favorites->Contains( document ) );
}

void MainWindow::closeEvent(QCloseEvent* event)
{
    UNUSED( event );
    if( IsEmpty() == false ) emit SaveState( GetDocumentState() );
    Owner()->Stop();
}

void MainWindow::wheelEvent(QWheelEvent* event)
{
    if( event->modifiers() == Qt::ControlModifier )
    {
        if( event->delta() > 0 ) m_actionZoomIn->trigger();
        else m_actionZoomOut->trigger();
        event->accept();
    }
}

QMenu*MainWindow::createPopupMenu()
{
    return NULL;
}

void MainWindow::keyPressEvent(QKeyEvent* event)
{
//    qDebug() << event->key();
    if( event->key() == Qt::Key_Escape )
    {
        //		close();
    }
    // FIXME: Linux-only Qt5 bug workaround;
    //        Ctrl+/Ctrl- shortcuts not work;
    //        see defect ELARIII-1
#ifdef OS_LINUX
    else if( event->matches( QKeySequence::ZoomIn ) )
    {
        m_actionZoomIn->trigger();
    }
    else if( event->matches( QKeySequence::ZoomOut ) )
    {
        m_actionZoomOut->trigger();
    }
#endif
    else
    {
        QMainWindow::keyPressEvent( event );
    }
}

void MainWindow::resizeEvent( QResizeEvent* event )
{
    Q_UNUSED( event );

#ifdef OS_MACOSX
#if 1
    bool protectResult = WindowCaptureProtection::ProtectWidget( this );
    DebugAssert( protectResult );
#endif
#endif
}

void MainWindow::NextPage()
{
    if(m_documentType == Document::EPUB)
    {
        m_EpubRenderer->pageDown();
    }else{
        if( m_renderer->IsEmpty() ) return;

        DebugAssert( m_pagesCount > 0 );
        if( m_pagesCount <= 0 ) return;

        int pageIndex = m_renderer->PageIndex();
        pageIndex += 1;

        if( pageIndex > m_pagesCount ) pageIndex = 1;
        GotoPageIndex( pageIndex );
    }
}

void MainWindow::PrevPage()
{
    if(m_documentType == Document::EPUB)
    {
        m_EpubRenderer->pageUp();
    } else
    {
        if( m_renderer->IsEmpty() ) return;

        DebugAssert( m_pagesCount > 0 );
        if( m_pagesCount <= 0 ) return;

        int pageIndex = m_renderer->PageIndex();
        pageIndex -= 1;

        if( pageIndex < 1 ) pageIndex = m_pagesCount;
        GotoPageIndex( pageIndex);
    }
}

void MainWindow::GotoPageIndex(int pageIndex)
{
    GotoPage(pageIndex, "");
}

void MainWindow::GotoPage(int pageIndex, QString resPath)
{
    if(m_documentType == Document::EPUB)
    {
        if(resPath != "")
            m_EpubRenderer->setCurrentCfi(resPath);
        else
            m_EpubRenderer->setCurrentPageIndex(pageIndex);
    }else
    {
        GotoSearch( pageIndex, WordCoordinates() );
    }
}

void MainWindow::GotoSearch(int pageIndex, WordCoordinates coordinates)
{
    if( m_renderer->IsEmpty() ) return;

    m_renderer->Load( pageIndex, m_request );
    m_renderer->SetSearchResult( coordinates );

    BookmarksUpdate( &m_bookmarksManager );
    CommentsUpdate( &m_commentsManager );

    m_actionFirstPage->setEnabled( pageIndex != 1 );
    m_actionLastPage->setEnabled( pageIndex != m_pagesCount );

    ShowSecondWarning();
}

void MainWindow::FirstPage()
{
    if(m_documentType == Document::EPUB)
    {
        m_EpubRenderer->toBegin();
    }
    else
    {
        if( m_renderer->IsEmpty() ) return;

        DebugAssert( m_pagesCount > 0 );
        if( m_pagesCount <= 0 ) return;

        GotoPageIndex( 1);
    }
}

void MainWindow::LastPage()
{
    if(m_documentType == Document::EPUB)
    {
        m_EpubRenderer->toEnd();
    }else
    {
        if( m_renderer->IsEmpty() ) return;

        DebugAssert( m_pagesCount > 0 );
        if( m_pagesCount <= 0 ) return;

        GotoPageIndex( m_pagesCount);
    }
}

void MainWindow::ScaleSelected(int scale)
{
    m_actionScaleFull->setChecked( false );
    m_actionScaleWidth->setChecked( false );
    if(m_documentType == Document::EPUB)
    {
        m_EpubRenderer->setZoomMultiplier(float(scale)/100.0f);
    }else
    {
        m_renderer->SetScaleExact( scale );
    }
}

void MainWindow::ScaleFullToggled(bool checked)
{
    if( checked ) m_actionScaleWidth->setChecked( false );
    m_renderer->SetScaleType( m_actionScaleFull->isChecked(), m_actionScaleWidth->isChecked() );
}

void MainWindow::ScaleWidthToggled(bool checked)
{
    if( checked ) m_actionScaleFull->setChecked( false );
    m_renderer->SetScaleType( m_actionScaleFull->isChecked(), m_actionScaleWidth->isChecked() );
}

void MainWindow::FullScreen(bool enabled)
{
    if( enabled )
    {
        DebugAssert( windowState() != Qt::WindowFullScreen );
        if( windowState() != Qt::WindowFullScreen ) m_previousState = windowState();
        setWindowState( Qt::WindowFullScreen );
    }
    else
    {
        setWindowState( m_previousState );
    }
}

void MainWindow::PrintPages()
{
    if( IsEmpty() ) return;

#ifndef DISABLE_THUMBNAILS
    m_pageSetWindow->SetPageSet( m_thumbnails->SelectedPages() );
#endif
    m_pageSetWindow->SetTitle( tr( "PrintDocument" ) );
    m_pageSetWindow->SetLabel( tr( "ChoosePagesToBePrinted" ) );
    m_pageSetWindow->SetOkButton( tr( "Print" ) );

    if( m_pageSetWindow->exec() != QDialog::Accepted ) return;

    emit PrintDocument( m_request->GetDocument(), m_pageSetWindow->GetPageSet() );
}

void MainWindow::SavePages()
{
    if( IsEmpty() ) return;

#ifndef DISABLE_THUMBNAILS
    m_pageSetWindow->SetPageSet( m_thumbnails->SelectedPages() );
#endif
    m_pageSetWindow->SetTitle( tr( "SaveDocument" ) );
    m_pageSetWindow->SetLabel( tr( "ChoosePagesToBeSaved" ) );
    m_pageSetWindow->SetOkButton( tr( "Save" ) );

    if( m_pageSetWindow->exec() != QDialog::Accepted ) return;

    emit SaveDocument( m_request->GetDocument(), m_pageSetWindow->GetPageSet() );
}

void MainWindow::Buy()
{
    QMessageBox::information( this, tr( "Buy" ), tr( "Service is unavailable" ), QMessageBox::Ok );
}

void MainWindow::BookmarksUpdate(BookmarksManager* manager)
{
    if(m_documentType == Document::PDF)
    {
        if( m_renderer->IsEmpty() ) return;

        int currentPage = m_renderer->PageIndex();
        if( manager->IsActual( currentPage, "" ) == false ) return;

        m_actionBookmark->setChecked( manager->Contains( currentPage, "" ) );
        m_actionBookmark->setEnabled( true );

        SetBookmarksTooltip( manager->Contains( currentPage, "" ) );
    }
    else if(m_documentType == Document::EPUB)
    {
        int currentPage = m_EpubRenderer->currentPageIndex();
        if( manager->IsActual( currentPage, m_EpubRenderer->currentCfi() ) == false ) return;

        m_actionBookmark->setChecked( manager->Contains( currentPage , m_EpubRenderer->currentCfi()) );
        m_actionBookmark->setEnabled( true );

        SetBookmarksTooltip( manager->Contains( currentPage, m_EpubRenderer->currentCfi() ) );
    }
}

void MainWindow::CommentsUpdate(CommentsManager* manager)
{
    if(m_documentType == Document::PDF)
    {
        if( m_renderer->IsEmpty() ) return;

        int currentPage = m_renderer->PageIndex();
        if( manager->IsActual( currentPage, "" ) == false ) return;

        m_actionComment->setChecked( manager->Contains( currentPage, "" ) );
        m_actionComment->setEnabled( true );

        SetCommentsTooltip( manager->Get( currentPage, "" ).isEmpty() == false );
    }
    else if(m_documentType == Document::EPUB)
    {
        int currentPage = m_EpubRenderer->currentPageIndex();
        if( manager->IsActual( currentPage, m_EpubRenderer->currentCfi() ) == false ) return;

        m_actionComment->setChecked( manager->Contains( currentPage, m_EpubRenderer->currentCfi() ) );
        m_actionComment->setEnabled( false );

        SetCommentsTooltip( manager->Get( currentPage, m_EpubRenderer->currentCfi() ).isEmpty() == false );
    }
}

void MainWindow::TriggerBookmark(bool checked)
{
    if(m_documentType == Document::PDF)
    {
        int currentPage = m_renderer->PageIndex();
        if( m_bookmarksManager.IsNotFound() )
        {
            m_actionBookmark->setChecked( !checked );
            ShowFeatureNotAllowedMessage();
            return;
        }

        if( m_bookmarksManager.IsActual( currentPage, "" ) == false )
        {
            m_actionBookmark->setChecked( !checked );
            ShowPendingRequestMessage();
            return;
        }

        m_bookmarksManager.SendRequest( currentPage, "", checked );
        SetBookmarksTooltip( checked );
    }
    else if(m_documentType == Document::EPUB)
    {
        if( m_bookmarksManager.IsNotFound() )
        {
            m_actionBookmark->setChecked( !checked );
            ShowFeatureNotAllowedMessage();
            return;
        }
        QString cfi = m_EpubRenderer->currentCfi();

        if( m_bookmarksManager.IsActual( m_EpubRenderer->currentPageIndex(), cfi ) == false )
        {
            m_actionBookmark->setChecked( !checked );
            ShowPendingRequestMessage();
            return;
        }

        m_bookmarksManager.SendRequest(m_EpubRenderer->currentPageIndex() , cfi, checked );
        SetBookmarksTooltip( checked );
    }
}

void MainWindow::TriggerComment()
{
    if(m_documentType == Document::PDF)
    {
        int currentPage = m_renderer->PageIndex();

        if( m_commentsManager.IsNotFound() )
        {
            m_actionComment->setChecked( !m_actionComment->isChecked() );
            ShowFeatureNotAllowedMessage();
            return;
        }


        if( m_commentsManager.IsActual( currentPage, "" ) == false )
        {
            m_actionComment->setChecked( !m_actionComment->isChecked() );
            ShowPendingRequestMessage();
            return;
        }

        QString text = m_commentsManager.Get( currentPage, "" );
        bool checked = text.isEmpty() == false;
        m_actionComment->setChecked( checked );
        SetCommentsTooltip( checked );

        m_commentWindow->SetText( text );
        m_commentWindow->show();
    }
    else if(m_documentType == Document::EPUB)
    {
        if( m_commentsManager.IsNotFound() )
        {
            m_actionComment->setChecked( !m_actionComment->isChecked() );
            ShowFeatureNotAllowedMessage();
            return;
        }
        int currentPage = m_EpubRenderer->currentPageIndex();
        if( m_commentsManager.IsActual( currentPage, m_EpubRenderer->currentCfi() ) == false )
        {
            m_actionComment->setChecked( !m_actionComment->isChecked() );
            ShowPendingRequestMessage();
            return;
        }

        QString text = m_commentsManager.Get( currentPage, m_EpubRenderer->currentCfi() );
        bool checked = text.isEmpty() == false;
        m_actionComment->setChecked( checked );
        SetCommentsTooltip( checked );

        m_commentWindow->SetText( text );
        m_commentWindow->show();
    }
}

void MainWindow::SetComment(QString text)
{
    int currentPage = 0;
    if(m_documentType == Document::PDF)
    {
        currentPage = m_renderer->PageIndex();
        m_commentsManager.SendRequest( currentPage, "", text );

    }
    else if(m_documentType == Document::EPUB)
    {
        currentPage = m_EpubRenderer->currentPageIndex();
        m_commentsManager.SendRequest( currentPage, m_EpubRenderer->currentCfi(), text );
    }

    bool checked = text.isEmpty() == false;
    m_actionComment->setChecked( checked );
    SetCommentsTooltip( checked );
}

void MainWindow::TriggerFavorites(bool checked)
{

    Document document = m_request->GetDocument();

    if( Owner()->GetFavoritesManager()->IsNotFound() )
    {
        m_actionFavorite->setChecked( !checked );
        ShowFeatureNotAllowedMessage();
        return;
    }

    if( Owner()->GetFavoritesManager()->IsActual( document ) == false )
    {
        m_actionFavorite->setChecked( !checked );
        ShowPendingRequestMessage();
        return;
    }

    emit SendFavoritesRequest( document, checked );

    SetFavoritesTooltip( checked );
}

void MainWindow::ShowCard()
{
    DebugAssert( IsEmpty() == false );
    if( IsEmpty() ) return;

    m_description->Load( m_request );
    m_description->show();
}

void MainWindow::Open()
{
    QString currentDocumentString = IsEmpty() ? QString() : m_request->GetDocument().ToString();

    QInputDialog dialog( this, Qt::Dialog | Qt::WindowCloseButtonHint );
    dialog.setWindowTitle( tr( "openTitle" ) );
    dialog.setLabelText( tr( "openLabel" ) );
    dialog.setTextValue( currentDocumentString );
    dialog.setCancelButtonText( tr( "openCancel" ) );
    dialog.resize( 400, dialog.sizeHint().height() );

    if( dialog.exec() != QDialog::Accepted ) return;

    QString documentString = dialog.textValue();
    auto tmpUrl = QUrl::fromEncoded(documentString.toUtf8() );
    if(!tmpUrl.isValid())
    {
        QMessageBox::warning( this, tr( "documentWrongTitle" ), tr( "documentWrongMessage" ) );
        return;
    }
    if(documentString.indexOf("?") != -1)
    {
        auto spl = documentString.split("?");
        documentString = spl[0] + "?" + tmpUrl.query(QUrl::FullyDecoded);
    }

    if( documentString == currentDocumentString ) return;

    if( Document::CheckString( documentString ) == false )
    {
        QMessageBox::warning( this, tr( "documentWrongTitle" ), tr( "documentWrongMessage" ) );
        return;
    }

    emit OpenDocument( Document( documentString ) );
}

void MainWindow::SecondWarningDialogFinished(int result)
{
    m_secondWarningResult = result;
    m_secondWarningPagesLeft = m_secondWarningInterval;
    m_secondWarningLastTime = QDateTime::currentDateTime();

    if( result == QMessageBox::Yes )
    {
        m_secondWarningShowOnlyOnce = m_user.IpIsAllowed();
        m_display->ShowPage();
        return;
    }

    ApplyHidePages();
}

void MainWindow::SetImageInverted(bool v)
{

    if(m_documentType == Document::PDF)
    {
        m_renderer->SetInverted(v);
    }
    else if(m_documentType== Document::EPUB)
    {
        m_EpubRenderer->setInvertedColors(v);
    }
}

void MainWindow::SlotFinishedPagesCount(ErrorCode error, int pagesCount)
{
    if( IsEmpty() ) return;
    if( sender() != m_request ) return;

    if( ApiBase::IsDownloadingError( error ) )
    {
        emit DownloadingError( m_request->GetDocument() );
        return;
    }

    if( error != API_NO_ERROR )
    {
        m_request->Send();
        return;
    }

    DebugAssert( pagesCount > 0 );
    if( pagesCount <= 0 ) return;

    emit DownloadingSuccess( m_request->GetDocument() );

    int pageIndex = 1;
    bool restorePageIndex = true;
    Document document = m_request->GetDocument();
    if( document.ContainsPage() )
    {
        bool ok = false;
        int pI = document.Page().toInt(&ok);
        if( pI > pagesCount ) ShowPageIsUnavailableMessage();
        else if(pI > 0)
        {
            pageIndex = pI;
            restorePageIndex = false;
        }
    }

    ShowFirstWarning();

    m_pagesCount = pagesCount;
#ifndef DISABLE_THUMBNAILS
    m_thumbnails->Load( pagesCount, SENDER );
#endif
    m_renderer->Load( pageIndex, SENDER );
    m_searchPanel->Load( pagesCount, SENDER );
    m_pagesControl->SetPagesCount( pagesCount );

    EnableGui();
    GotoPageIndex( pageIndex);
    RestoreDocumentState( restorePageIndex );
    if( document.ContainsSearchQuery())
    {
        m_searchPanel->PerformSearch(document.SearchQuery());
    }

    BookmarksUpdate( &m_bookmarksManager );
    CommentsUpdate( &m_commentsManager );
#ifndef DISABLE_THUMBNAILS
    m_thumbnails->UpdateBookmarks( &m_bookmarksManager );
    m_thumbnails->UpdateComments( &m_commentsManager );
#endif
}

void MainWindow::CreateActions()
{
    m_groupScaling = new QActionGroup( this );
#ifdef PRINT_SAVE
    m_groupDocument = new QActionGroup( this );
#endif
    m_groupNavigation = new QActionGroup( this );
    m_groupView = new QActionGroup( this );
    m_groupPrivate = new QActionGroup( this );
    m_groupOthers = new QActionGroup( this );

    m_actionZoomOut = new QAction( QIcon( ":/icons/zoomOut" ), "", m_groupScaling );
    m_actionZoomIn = new QAction( QIcon( ":/icons/zoomIn" ), "", m_groupScaling );
    m_actionScaleWidth = new QAction( QIcon( ":/icons/scaleWidth" ), "", this );
    m_actionScaleFull = new QAction( QIcon( ":/icons/scaleFull" ), "", this );

#ifdef PRINT_SAVE
    m_actionPrint = new QAction( QIcon( ":/icons/print" ), "", m_groupDocument );
    m_actionCopy = new QAction( QIcon( ":/icons/copy" ), "", m_groupDocument );
#endif

    m_actionFirstPage = new QAction( QIcon( ":/icons/firstPage" ), "", m_groupNavigation );
    m_actionPrevPage = new QAction( QIcon( ":/icons/prevPage" ), "", m_groupNavigation );
    m_actionNextPage = new QAction( QIcon( ":/icons/nextPage" ), "", m_groupNavigation );
    m_actionLastPage = new QAction( QIcon( ":/icons/lastPage" ), "", m_groupNavigation );

    m_actionRol = new QAction( QIcon( ":/icons/rol" ), "", m_groupView );
    m_actionInvert = new QAction( QIcon( ":/icons/invert" ), "", this );
    m_actionRor = new QAction( QIcon( ":/icons/ror" ), "", m_groupView );

    m_actionFavorite = new QAction( QIcon( ":/icons/favorite" ), "", m_groupPrivate );
    m_actionBookmark = new QAction( QIcon( ":/icons/bookmark" ), "", m_groupPrivate );
    m_actionComment = new QAction( QIcon( ":/icons/comment" ), "", this );
    m_actionBuy = new QAction( QIcon( ":/icons/buy" ), "", m_groupPrivate );

    m_actionInfo = new QAction( QIcon( ":/icons/info" ), "", m_groupOthers );
    m_actionOpen = new QAction( QIcon( ":/icons/open" ), "", m_groupOthers );
    m_actionFullScreen = new QAction( QIcon( ":/icons/fullScreen" ), "", m_groupOthers );

    m_actionShowAdminDialog = new QAction(tr("AdminDialogAction"), this);
    m_actionShowAdminDialog->setVisible(true);

    m_actionScaleFull->setCheckable( true );
    m_actionScaleWidth->setCheckable( true );
    m_groupScaling->setExclusive( false );

    m_actionInvert->setCheckable( true );
    m_groupView->setExclusive( false );

    m_actionFullScreen->setCheckable( true );
    m_groupOthers->setExclusive( false );

    m_actionBookmark->setCheckable( true );
    m_actionFavorite->setCheckable( true );
    m_actionComment->setCheckable( true );
    m_groupPrivate->setExclusive( false );

    m_actionZoomIn->setShortcut( QKeySequence::ZoomIn );
    m_actionZoomOut->setShortcut( QKeySequence::ZoomOut );

    m_actionFavorite->setShortcut( Qt::Key_F | Qt::CTRL );
    m_actionBuy->setShortcut( Qt::Key_P | Qt::SHIFT | Qt::CTRL );
#if QT_VERSION >= 0x050000
    m_actionFullScreen->setShortcut( QKeySequence::FullScreen );
#else
#if defined(OS_WINDOWS) || defined(OS_LINUX)
    m_actionFullScreen->setShortcut( Qt::Key_F11 );
#else
    m_actionFullScreen->setShortcut( Qt::Key_F | Qt::CTRL | Qt::META );
#endif
#endif
#ifdef PRINT_SAVE
    m_actionCopy->setShortcut( QKeySequence::Save );
#endif
#ifdef OS_MACOSX
    m_actionScaleWidth->setShortcut( Qt::Key_8 | Qt::CTRL );
    m_actionScaleFull->setShortcut( Qt::Key_9 | Qt::CTRL );

    m_actionFirstPage->setShortcut( Qt::Key_Home );
    m_actionLastPage->setShortcut( Qt::Key_End );

    m_actionRor->setShortcut( Qt::Key_R | Qt::CTRL );
    m_actionRol->setShortcut( Qt::Key_L | Qt::CTRL );

    m_actionComment->setShortcut( Qt::Key_E | Qt::CTRL );
#else
    m_actionScaleWidth->setShortcut( Qt::Key_W | Qt::CTRL );
    m_actionScaleFull->setShortcut( Qt::Key_E | Qt::CTRL );

    m_actionFirstPage->setShortcut( QKeySequence::MoveToStartOfDocument );
    m_actionLastPage->setShortcut( QKeySequence::MoveToEndOfDocument );

    m_actionRor->setShortcut( Qt::Key_Right | Qt::SHIFT | Qt::CTRL );
    m_actionRol->setShortcut( Qt::Key_Left | Qt::SHIFT | Qt::CTRL );

    m_actionComment->setShortcut( Qt::Key_C | Qt::CTRL );
#endif

    m_actionNextPage->setShortcut( QKeySequence::MoveToNextPage );
    m_actionPrevPage->setShortcut( QKeySequence::MoveToPreviousPage );

    m_actionBookmark->setShortcut( Qt::Key_B | Qt::CTRL );

#ifdef PRINT_SAVE
    m_actionPrint->setShortcut( QKeySequence::Print );
#endif
    m_actionInvert->setShortcut( Qt::Key_I | Qt::CTRL );
    m_actionOpen->setShortcut( QKeySequence::Open );


}

void MainWindow::CreateGui()
{
    m_toolBarScaling = new QToolBar();
#ifdef PRINT_SAVE
    m_toolBarDocument = new QToolBar();
#endif
    m_toolBarNavigation = new QToolBar();
    m_toolBarView = new QToolBar();
    m_toolBarPrivate = new QToolBar();
    m_toolBarOthers = new QToolBar();
    m_toolBarAdmin = addToolBar("Admin");
    m_toolBarAdmin->setVisible(false);
    m_toolBarAdmin->addAction(m_actionShowAdminDialog);

    m_toolBarScaling->setObjectName( "ToolBarScaling" );
#ifdef PRINT_SAVE
    m_toolBarDocument->setObjectName( "ToolBarDocument" );
#endif
    m_toolBarNavigation->setObjectName( "ToolBarNavigation" );
    m_toolBarView->setObjectName( "ToolBarView" );
    m_toolBarPrivate->setObjectName( "ToolBarPrivate" );
    m_toolBarOthers->setObjectName( "ToolBarOthers" );

    m_searchPanel = new SearchPanel();
    m_searchPanel->SetMainWindow(this);
#ifndef DISABLE_THUMBNAILS
    m_thumbnails = new Thumbnails();
#endif
    m_display = new Display();
    m_EpubRenderer = new QEpubRenderer(nullptr);
//    m_EpubRenderer->show();

    m_guiSplitter = new QSplitter();
    m_guiTabWidget = new QTabWidget();
    m_debugPanel = new DebugPanel();
    m_debugPanel->setObjectName( "DebugPanel" );
    m_pagesControl = new PagesControl();
    m_zoomPanel = new ZoomPanel();
    m_pageSetWindow = new PageSetWindow( this );
    m_bookmarksControl = new BookmarksControl();
    m_commentsControl = new BookmarksControl();

    m_favorite = new History();
    m_history = new History();
    m_contentDisplay = new ContentDisplay();

    m_guiTabWidget->addTab( m_history, QIcon( ":/icons/history" ), "" );
    m_guiTabWidget->addTab( m_favorite, QIcon( ":/icons/favorite" ), "" );
    m_guiTabWidget->addTab( m_searchPanel, QIcon( ":/icons/search" ), "" );
#ifndef DISABLE_THUMBNAILS
    m_guiTabWidget->addTab( m_thumbnails, QIcon( ":/icons/thumbnails" ), "" );
#endif
    m_guiTabWidget->addTab( m_bookmarksControl, QIcon( ":/icons/bookmark" ), "" );
    m_guiTabWidget->addTab( m_commentsControl, QIcon( ":/icons/comment" ), "" );
    m_guiTabWidget->addTab( m_contentDisplay, QIcon( ":/icons/thumbnails" ), "" );

    m_guiTabWidget->setTabPosition( QTabWidget::West );

    auto viewersContainer = new ViewersContainer(this);
    m_EpubRenderer->setParent(viewersContainer);
    m_EpubRenderer->move(1,1);
    m_display->setParent(viewersContainer);
    m_EpubRenderer->stackUnder(m_display);
    m_display->stackUnder(m_EpubRenderer);
    m_EpubRenderer->stackUnder(m_display);
    connect(viewersContainer, &ViewersContainer::resized, [=](QSize s){m_EpubRenderer->resize(s - QSize(4, 4));});
    connect(viewersContainer, &ViewersContainer::resized, [=](QSize s){m_display->resize(s );});

    m_guiSplitter->addWidget( m_guiTabWidget );
    m_guiSplitter->addWidget( viewersContainer );
    m_guiSplitter->setStretchFactor( 0, 0 );
    m_guiSplitter->setStretchFactor( 1, 1 );
    m_guiSplitter->setSizes( QList<int>() << THUMBNAIL_SIZE * 4 << 1 );
    m_guiSplitter->setChildrenCollapsible( false );

    m_toolBarNavigation->addWidget( m_pagesControl );
    m_toolBarScaling->addWidget( m_zoomPanel );

    m_toolBarScaling->addActions( m_groupScaling->actions() << m_actionScaleWidth << m_actionScaleFull);
#ifdef PRINT_SAVE
    m_toolBarDocument->addActions( m_groupDocument->actions() );
#endif
    m_toolBarNavigation->addActions( m_groupNavigation->actions() );
    m_toolBarView->addActions( m_groupView->actions() << m_actionInvert );
    m_toolBarPrivate->addActions( m_groupPrivate->actions() << m_actionComment );
    m_toolBarOthers->addActions( m_groupOthers->actions() );

    this->setCentralWidget( m_guiSplitter );

#ifdef PRINT_SAVE
    this->addToolBar( Qt::TopToolBarArea, m_toolBarDocument );
#endif
    this->addToolBar( Qt::TopToolBarArea, m_toolBarScaling );
    this->addToolBar( Qt::TopToolBarArea, m_toolBarNavigation );
    this->addToolBar( Qt::TopToolBarArea, m_toolBarView );
    this->addToolBar( Qt::TopToolBarArea, m_toolBarPrivate );
    this->addToolBar( Qt::TopToolBarArea, m_toolBarOthers );

    this->addDockWidget( Qt::BottomDockWidgetArea, m_debugPanel );

    m_debugPanel->hide();

#ifdef PRINT_SAVE
    m_thumbnails->addAction( m_actionPrint );
    m_thumbnails->addAction( m_actionCopy );
    m_thumbnails->setContextMenuPolicy( Qt::ActionsContextMenu );

    m_display->addAction( m_actionPrint );
    m_display->addAction( m_actionCopy );
    m_display->setContextMenuPolicy( Qt::ActionsContextMenu );
#endif
    m_adminDialog = new AdminDialog(this);
}

void MainWindow::ConnectSlots()
{
    qApp->installEventFilter(this);
    connect(m_actionNextPage,SIGNAL(triggered()),this,SLOT(NextPage()));
    connect(m_actionPrevPage,SIGNAL(triggered()),this,SLOT(PrevPage()));
    connect(m_actionFirstPage,SIGNAL(triggered()),this,SLOT(FirstPage()));
    connect(m_actionLastPage,SIGNAL(triggered()),this,SLOT(LastPage()));
    connect(m_actionFullScreen,SIGNAL(toggled(bool)),this,SLOT(FullScreen(bool)));
    connect(m_actionZoomIn,SIGNAL(triggered()),m_zoomPanel,SLOT(ZoomIn()));
    connect(m_actionZoomOut,SIGNAL(triggered()),m_zoomPanel,SLOT(ZoomOut()));
    connect(m_actionScaleFull,SIGNAL(toggled(bool)),this,SLOT(ScaleFullToggled(bool)));
    connect(m_actionScaleWidth,SIGNAL(toggled(bool)),this,SLOT(ScaleWidthToggled(bool)));
    connect(m_actionRol,SIGNAL(triggered()),m_renderer,SLOT(RotateLeft()));
    connect(m_actionRor,SIGNAL(triggered()),m_renderer,SLOT(RotateRight()));
    connect(m_actionInvert,&QAction::toggled,this, &MainWindow::SetImageInverted);
#ifdef PRINT_SAVE
    connect(m_actionPrint,SIGNAL(triggered()),this,SLOT(PrintPages()));
    connect(m_actionCopy,SIGNAL(triggered()),this,SLOT(SavePages()));
#endif
    connect(m_actionBuy,SIGNAL(triggered()),this,SLOT(Buy()));
    connect(m_actionBookmark,SIGNAL(triggered(bool)),this,SLOT(TriggerBookmark(bool)));
    connect(m_actionComment,SIGNAL(triggered()),this,SLOT(TriggerComment()));
    connect(m_actionFavorite,SIGNAL(triggered(bool)),this,SLOT(TriggerFavorites(bool)));

    connect(m_actionInfo,SIGNAL(triggered()),this,SLOT(ShowCard()));
    connect(m_actionOpen,SIGNAL(triggered()),this,SLOT(Open()));

#ifndef DISABLE_THUMBNAILS
    connect(m_thumbnails,SIGNAL(GotoPage(int)),this,SLOT(GotoPageIndex(int)));
    connect(m_renderer,SIGNAL(Select(int)),m_thumbnails,SLOT(Select(int)));
#endif

    connect(m_renderer,SIGNAL(Select(int)),m_searchPanel,SLOT(Select(int)));
    connect(m_renderer,SIGNAL(Select(int)),m_pagesControl,SLOT(SetPageIndex(int)));
    connect(m_renderer,SIGNAL(DownloadProgress(qint64,qint64)),m_display,SLOT(SlotDownloadProgress(qint64,qint64)));
    connect(m_renderer,SIGNAL(UpdatePixmap(QPixmap)),m_display,SLOT(SetPixmap(QPixmap)));
    connect(m_renderer,SIGNAL(Cleared()),m_display,SLOT(Clear()));
    connect(m_renderer,SIGNAL(DownloadingError(Document)),this,SIGNAL(DownloadingError(Document)));


    connect(m_EpubRenderer, &QEpubRenderer::pageCountChanged, [=](int c){
        if(m_documentType == Document::EPUB)
        {
            m_pagesCount = c;
            m_pagesControl->SetPagesCount(c);
            m_actionFirstPage->setEnabled( m_EpubRenderer->currentPageIndex() != 1 );
            m_actionLastPage->setEnabled( m_EpubRenderer->currentPageIndex() != c );
        }
    });
    connect(m_EpubRenderer, &QEpubRenderer::currentPageIndexChanged, [=](int c){
        if(m_documentType == Document::EPUB)
        {
            m_pagesControl->SetPageIndex(c-1);
            m_actionFirstPage->setEnabled( c != 1 );
            m_actionLastPage->setEnabled( c != m_EpubRenderer->pageCount() );
            BookmarksUpdate( &m_bookmarksManager );
            CommentsUpdate( &m_commentsManager );
        }
    });
    connect(m_EpubRenderer, &QEpubRenderer::currentPageImageChanged, [=](QPixmap c){
        if(m_documentType == Document::EPUB)
        {
            if(!this->m_epubRestored)
            {
                c.fill(Qt::white);
                c.setDevicePixelRatio(qApp->devicePixelRatio());
                QPainter p(&c);
                p.drawText(QRect(QPoint(0,0), c.size()/qApp->devicePixelRatio()), Qt::AlignCenter, tr("Opening EPUB..."));
            }

            m_display->SetPixmap(c);
        }
    });
    connect(m_EpubRenderer, &QEpubRenderer::pageRestored, [=](){
        m_epubRestored = true;
    });

    connect(m_EpubRenderer, &QEpubRenderer::currentCfiChanged, [=](QString c){
        Q_UNUSED(c);
//        if(m_documentType == Document::EPUB) qDebug() << c;
    });
    connect(m_EpubRenderer, &QEpubRenderer::renderingChanged, [=](bool v){
        if(m_documentType == Document::EPUB)
        {
            m_pagesControl->setEnabled(!v);

            if(!v)
            {
                m_bookmarksControl->RequestCFi();
            }
        }
    });
    connect(m_EpubRenderer, &QEpubRenderer::tableOfContentsChanged, this, &MainWindow::parseEpubToc);
    connect(m_EpubRenderer,&QEpubRenderer::readyToView , [=](){
        RestoreDocumentState(!m_request->GetDocument().ContainsPage());
    });
    connect(this, &MainWindow::EpubDownloadProgress,m_display, &Display::SlotDownloadProgress);


    connect(m_display,SIGNAL(Resized(QSize)),m_renderer,SLOT(DisplayResized(QSize)));


    connect(m_searchPanel,SIGNAL(GotoSearch(int,WordCoordinates)),this,SLOT(GotoSearch(int,WordCoordinates)));
    connect(m_pagesControl,SIGNAL(GotoPage(int)),this,SLOT(GotoPageIndex(int)));
    connect(m_zoomPanel,SIGNAL(SetZoomInEnabled(bool)),m_actionZoomIn,SLOT(setEnabled(bool)));
    connect(m_zoomPanel,SIGNAL(SetZoomOutEnabled(bool)),m_actionZoomOut,SLOT(setEnabled(bool)));
    connect(m_zoomPanel,SIGNAL(ScaleSelected(int)),this,SLOT(ScaleSelected(int)));

    connect(&m_bookmarksManager,SIGNAL(SignalUpdate(BookmarksManager*)),this,SLOT(BookmarksUpdate(BookmarksManager*)));
    connect(&m_commentsManager,SIGNAL(SignalUpdate(CommentsManager*)),this,SLOT(CommentsUpdate(CommentsManager*)));

#ifndef DISABLE_THUMBNAILS
    connect(&m_bookmarksManager,SIGNAL(SignalUpdate(BookmarksManager*)),m_thumbnails,SLOT(UpdateBookmarks(BookmarksManager*)));
    connect(&m_commentsManager,SIGNAL(SignalUpdate(CommentsManager*)),m_thumbnails,SLOT(UpdateComments(CommentsManager*)));
#endif

    connect(&m_bookmarksManager,SIGNAL(SignalUpdate(BookmarksManager*)),m_bookmarksControl,SLOT(UpdateBookmarks(BookmarksManager*)));
    connect(&m_commentsManager,SIGNAL(SignalUpdate(CommentsManager*)),m_commentsControl,SLOT(UpdateComments(CommentsManager*)));

    connect(m_commentWindow,SIGNAL(SetComment(QString)),this,SLOT(SetComment(QString)));
    connect(m_bookmarksControl,SIGNAL(GotoPage(int, QString)),this,SLOT(GotoPage(int, QString)));
    connect(m_bookmarksControl, &BookmarksControl::DeleteBookmark,this,[=](int index, QString resPath){
        m_bookmarksManager.SendRequest(index , resPath, false );
    });

    connect(m_bookmarksControl, &BookmarksControl::CfiUpdated,this,[=](QStringList l){
        auto r = m_EpubRenderer->GetCFIToPages(l);
        m_bookmarksControl->UpdateCfiToPage(r);
    });


    connect(m_commentsControl,SIGNAL(GotoPage(int, QString)),this,SLOT(GotoPage(int, QString)));
    connect(m_commentsControl, &BookmarksControl::DeleteBookmark,this,[=](int index, QString resPath){
        m_commentsManager.SendRequest(index , resPath, "" );
    });

    connect(m_renderer,SIGNAL(Select(int)),m_bookmarksControl,SLOT(Select(int)));
    connect(m_renderer,SIGNAL(Select(int)),m_commentsControl,SLOT(Select(int)));

    connect(m_renderer->GetScaler(),&Scaler::ImplicitScaleChanged, [=](int c){
        if(m_documentType != Document::EPUB)
        {
            m_zoomPanel->ImplicitScaleChanged(c);
        }
    });

    connect(m_contentDisplay, &ContentDisplay::linkActivated, this, &MainWindow::gotoContentLink);

    connect(m_actionShowAdminDialog, &QAction::triggered, m_adminDialog, &AdminDialog::Exec);
}


#define TAB_TEXT( tab, text ) m_guiTabWidget->setTabText( m_guiTabWidget->indexOf( tab ), text )

void MainWindow::Translate()
{
#ifndef DISABLE_THUMBNAILS
    TAB_TEXT( m_thumbnails, tr( "Thumbnails" ) );
#endif
    TAB_TEXT( m_searchPanel, tr( "SearchPanel" ) );
    TAB_TEXT( m_bookmarksControl, tr( "Bookmarks" ) );
    TAB_TEXT( m_commentsControl, tr( "Comments" ) );
    TAB_TEXT( m_favorite, tr( "Favorite" ) );
    TAB_TEXT( m_history, tr( "History" ) );
    TAB_TEXT( m_contentDisplay, tr( "Content" ) );

    m_actionZoomIn->setText( tr( "zoomIn" ) );
    m_actionZoomOut->setText( tr( "zoomOut" ) );
    m_actionRor->setText( tr( "ror" ) );
    m_actionRol->setText( tr( "rol" ) );
    m_actionFavorite->setText( tr( "favorite" ) );
    m_actionBuy->setText( tr( "buy" ) );
    m_actionFullScreen->setText( tr( "fullScreen" ) );
#ifdef PRINT_SAVE
    m_actionCopy->setText( tr( "copy" ) );
#endif
    m_actionScaleWidth->setText( tr( "scaleWidth" ) );
    m_actionScaleFull->setText( tr( "scaleFull" ) );
    m_actionNextPage->setText( tr( "nextPage" ) );
    m_actionFirstPage->setText( tr( "firstPage" ) );
    m_actionPrevPage->setText( tr( "prevPage" ) );
    m_actionLastPage->setText( tr( "lastPage" ) );
    m_actionBookmark->setText( tr( "bookmark" ) );
    m_actionComment->setText( tr( "comment" ) );
#ifdef PRINT_SAVE
    m_actionPrint->setText( tr( "print" ) );
#endif
    m_actionInvert->setText( tr( "invert" ) );
    m_actionInfo->setText( tr( "ShowCard" ) );
    m_actionOpen->setText( tr( "open" ) );

    ToolTip( m_actionZoomIn );
    ToolTip( m_actionZoomOut );
    ToolTip( m_actionRor );
    ToolTip( m_actionRol );
    ToolTip( m_actionFavorite );
    ToolTip( m_actionBuy );
    ToolTip( m_actionFullScreen );
#ifdef PRINT_SAVE
    ToolTip( m_actionCopy );
#endif
    ToolTip( m_actionScaleWidth );
    ToolTip( m_actionScaleFull );
    ToolTip( m_actionNextPage );
    ToolTip( m_actionFirstPage );
    ToolTip( m_actionPrevPage );
    ToolTip( m_actionLastPage );
    ToolTip( m_actionBookmark );
    ToolTip( m_actionComment );
#ifdef PRINT_SAVE
    ToolTip( m_actionPrint );
#endif
    ToolTip( m_actionInvert );
    ToolTip( m_actionOpen );

    m_toolBarScaling->setWindowTitle( tr( "ToolBarScaling" ) );
#ifdef PRINT_SAVE
    m_toolBarDocument->setWindowTitle( tr( "ToolBarDocument" ) );
#endif
    m_toolBarNavigation->setWindowTitle( tr( "ToolBarNavigation" ) );
    m_toolBarView->setWindowTitle( tr( "ToolBarView" ) );
    m_toolBarPrivate->setWindowTitle( tr( "ToolBarPrivate" ) );
    m_toolBarOthers->setWindowTitle( tr( "ToolBarOthers" ) );

    m_debugPanel->setWindowTitle( tr( "DebugOutput" ) );

#ifdef TIME_LIMITED_MODE
    this->setWindowTitle( tr( "MainWindowTitleLimited%1" ).arg( TIME_LIMIT ) );
#else
    this->setWindowTitle( tr( "MainWindowTitle" ).arg( VERSION ) );
#endif

    m_searchPanel->Translate();
    m_pagesControl->Translate();
    m_zoomPanel->Translate();

    m_description->Translate();
}

void MainWindow::PrepareWindow()
{
    QDesktopWidget* desktop = QApplication::desktop();
    QSize size = desktop->screen( desktop->primaryScreen() )->size() / 1.2;
    this->resize( size );
#ifdef OS_WINDOWS
    this->setWindowIcon( QIcon( ":/icons/mainWindow") );
#endif
}

void MainWindow::RestoreDocumentState( bool restorePageIndex )
{
    if(m_documentType == Document::PDF)
    {
        if( m_state.IsCorrect() == false )
        {
            m_renderer->GetScaler()->ResetRotation();
            m_actionInvert->setChecked( false );
            m_zoomPanel->SetCurrentScale( 100 );
            m_actionScaleFull->setChecked( false );
            m_actionScaleWidth->setChecked( false );
            return;
        }

        DebugAssert( IsEmpty() == false );
        if( IsEmpty() ) return;

        DebugAssert( m_renderer->IsEmpty() == false );
        if( m_renderer->IsEmpty() ) return;

        DebugAssert( m_state.GetDocument() == m_request->GetDocument() );
        if( m_state.GetDocument() != m_request->GetDocument() ) return;

        m_renderer->GetScaler()->ResetRotation();
        m_renderer->GetScaler()->Rotate( m_state.GetAngle() );
        m_actionInvert->setChecked( m_state.GetInverted() );
        m_zoomPanel->SetCurrentScale( m_state.GetScale() );
        m_actionScaleFull->setChecked( m_state.GetScaleFull() );
        m_actionScaleWidth->setChecked( m_state.GetScaleWidth() );
        //	restoreState( m_state.GetWindowState() );

        if( restorePageIndex ) GotoPageIndex(m_state.GetPageIndex().toInt() );
    }
    else if(m_documentType == Document::EPUB)
    {
        if( m_state.IsCorrect() == false )
        {
            m_actionInvert->setChecked( false );
            m_zoomPanel->SetCurrentScale( 100 );
            m_epubRestored = true;
            return;
        }
        DebugAssert( IsEmpty() == false );
        if( IsEmpty() )
        {
            m_epubRestored = true;
            return;
        }
        DebugAssert( m_state.GetDocument() == m_request->GetDocument() );
        if( m_state.GetDocument() != m_request->GetDocument() )
        {
            m_epubRestored = true;
            return;
        }
        m_actionInvert->setChecked( m_state.GetInverted() );
        m_zoomPanel->SetCurrentScale( m_state.GetScale() );
        m_EpubRenderer->setZoomMultiplier(float(m_state.GetScale())/100.0f);
        QString cfi;
        if( restorePageIndex ) {
            cfi = m_state.GetPageIndex().toString();
            qDebug() << "restoring from local settings: "<< cfi;
        }else
        {
            qDebug() << "restoring from SPD: "<< cfi;
            cfi = m_request->GetDocument().Page().toString();
        }

        m_EpubRenderer->RestoreWhenReady(cfi);
    }
}

void MainWindow::CreateWarningDialogs()
{
    DebugAssert( m_firstWarningDialog == NULL );
    DebugAssert( m_secondWarningDialog == NULL );
    DebugAssert( m_pendingRequestMessage == NULL );
    DebugAssert( m_featureNotAllowedMessage == NULL );
    DebugAssert( m_pageIsUnavailableMessage == NULL );

    DeleteWarningDialogs();

    QString title1 = tr( "NotAllowedIpTitle" );
    QString message1 = tr( "NotAllowedIpMessage" );
    QString buttonText1 = tr( "NotAllowedIpButtonText" );
    m_firstWarningDialog = new QMessageBox( QMessageBox::Warning, title1, message1, QMessageBox::Ok );
    m_firstWarningDialog->setButtonText( QMessageBox::Ok, buttonText1 );

    QString title2 = tr( "AskUserToContinueTitle" );
    QString message2 = tr( "AskUserToContinueMessage" );
    m_secondWarningDialog = new QMessageBox( QMessageBox::Warning, title2, message2, QMessageBox::Yes | QMessageBox::No );
    m_secondWarningDialog->setButtonText( QMessageBox::Yes, tr( "Yes" ) );
    m_secondWarningDialog->setButtonText( QMessageBox::No, tr( "No" ) );
    m_secondWarningDialog->setDefaultButton( QMessageBox::Yes );
    connect(m_secondWarningDialog,SIGNAL(finished(int)),this,SLOT(SecondWarningDialogFinished(int)));

    QString title3 = tr( "PendingRequestTitle" );
    QString message3 = tr( "PendingRequestMessage" );
    m_pendingRequestMessage = new QMessageBox( QMessageBox::Information, title3, message3, QMessageBox::Ok );
    m_pendingRequestMessage->setButtonText( QMessageBox::Ok, tr( "Ok" ) );

    QString title4 = tr( "PageIsUnavaliableTitle" );
    QString message4 = tr( "PageIsUnavailableMessage" );
    m_pageIsUnavailableMessage = new QMessageBox( QMessageBox::Information, title4, message4, QMessageBox::Ok );
    m_pageIsUnavailableMessage->setButtonText( QMessageBox::Ok, tr( "Ok" ) );

    QString title5 = tr( "Not available" );
    QString message5 = tr( "Feature not allowed for unregistered user" );
    m_featureNotAllowedMessage = new QMessageBox( QMessageBox::Information, title5, message5, QMessageBox::Ok );
    m_featureNotAllowedMessage->setButtonText( QMessageBox::Ok, tr( "Ok" ) );
}

void MainWindow::DeleteWarningDialogs()
{
    DeleteAndNull( m_firstWarningDialog );
    DeleteAndNull( m_secondWarningDialog );
    DeleteAndNull( m_pendingRequestMessage );
    DeleteAndNull( m_pageIsUnavailableMessage );
    DeleteAndNull( m_featureNotAllowedMessage );
}

void MainWindow::HideWarningDialogs()
{
    DebugAssert( m_firstWarningDialog != NULL );
    if( m_firstWarningDialog != NULL ) m_firstWarningDialog->hide();

    DebugAssert( m_secondWarningDialog != NULL );
    if( m_secondWarningDialog != NULL ) m_secondWarningDialog->hide();

    DebugAssert( m_pendingRequestMessage != NULL );
    if( m_pendingRequestMessage != NULL ) m_pendingRequestMessage->hide();

    DebugAssert( m_pageIsUnavailableMessage != NULL );
    if( m_pageIsUnavailableMessage != NULL ) m_pageIsUnavailableMessage->hide();

    DebugAssert( m_featureNotAllowedMessage != NULL );
    if( m_featureNotAllowedMessage != NULL ) m_featureNotAllowedMessage->hide();
}

void MainWindow::ApplyHidePages()
{
    if( Settings().HidePages() )
    {
        m_display->ShowText( Settings().HidePagesText() );
    }
    else
    {
        m_display->ShowText( tr( "Page %1/%2\nForbidden" ).arg( PageIndex() ).arg( m_pagesCount ) );
    }
}

void MainWindow::ShowSecondWarning()
{
    DebugAssert( IsEmpty() == false );
    if( IsEmpty() ) return;

    DebugAssert( m_pagesCount > 0 );
    if( m_pagesCount <= 0 ) return;

    if( Settings().SecondWarningShow() == false ) return;
    if( m_user.GetStatus() == User::VIP ) return;
    if( m_secondWarningShowOnlyOnce == true ) return;

    if( m_secondWarningResult == QMessageBox::No )
    {
        ApplyHidePages();
        return;
    }

    int pageLimit = ( m_pagesCount * Settings().SecondWarningBookPercent() ) / 100;

    if( m_secondWarningResult == QMessageBox::NoButton ) // First occurance
    {
        if( PageIndex() > pageLimit ) ShowSecondWarningInternal();
        return;
    }

    if( CheckSecondWarningInterval() == false ) return;

    if( PageIndex() > pageLimit )
    {
        ApplySecondWarningGrow();
        ShowSecondWarningInternal();
    }
}

bool MainWindow::CheckSecondWarningInterval()
{
    if( Settings().SecondWarningPeriod() == ViewerSettings::ByPage )
    {
        m_secondWarningPagesLeft -= 1;
        if( m_secondWarningPagesLeft > 0 ) return false;
    }
    else if( Settings().SecondWarningPeriod() == ViewerSettings::ByTime )
    {
        if( m_secondWarningLastTime.isValid() )
        {
            if( m_secondWarningLastTime.secsTo( QDateTime::currentDateTime() ) < m_secondWarningInterval ) return false;
        }
    }
    else NotImplemented();
    return true;
}

void MainWindow::ApplySecondWarningGrow()
{
    if( Settings().SecondWarningGrow() == false ) return;

    m_secondWarningInterval -= Settings().SecondWarningGrowSpeed();
    if( Settings().SecondWarningPeriod() == ViewerSettings::ByPage )
    {
        if( m_secondWarningInterval < SECOND_WARNING_INTERVAL_PAGE_LIMIT ) m_secondWarningInterval = SECOND_WARNING_INTERVAL_PAGE_LIMIT;
    }
    else if( Settings().SecondWarningPeriod() == ViewerSettings::ByTime )
    {
        if( m_secondWarningInterval < SECOND_WARNING_INTERVAL_TIME_LIMIT ) m_secondWarningInterval = SECOND_WARNING_INTERVAL_TIME_LIMIT;
    }
    else NotImplemented();
}

void MainWindow::ShowSecondWarningInternal()
{
    m_display->HidePage();

    m_secondWarningDialog->setText( Settings().SecondWarningText() );
    m_secondWarningDialog->show();

    if( m_firstWarningDialog->isVisible() )
    {
        m_firstWarningDialog->hide();
        m_firstWarningDialog->show();
    }
}

void MainWindow::ShowFirstWarning()
{
    DebugAssert( IsEmpty() == false );
    if( IsEmpty() ) return;

    if( Settings().FirstWarningShow() == false ) return;
    if( m_user.GetStatus() == User::VIP ) return;

    if(m_user.IpIsAllowed()) return;

    DebugAssert( m_firstWarningDialog != NULL );
    if( m_firstWarningDialog == NULL ) return;

    m_firstWarningDialog->setText( Settings().FirstWarningText() );
    m_firstWarningDialog->show();
}

void MainWindow::ShowPendingRequestMessage()
{
    DebugAssert( IsEmpty() == false );
    if( IsEmpty() ) return;

    DebugAssert( m_pendingRequestMessage != NULL );
    if( m_pendingRequestMessage == NULL ) return;

    m_pendingRequestMessage->exec();
}

void MainWindow::ShowFeatureNotAllowedMessage()
{
    DebugAssert( IsEmpty() == false );
    if( IsEmpty() ) return;

    DebugAssert( m_featureNotAllowedMessage != NULL );
    if( m_featureNotAllowedMessage == NULL ) return;

    m_featureNotAllowedMessage->exec();
}

void MainWindow::ShowPageIsUnavailableMessage()
{
    DebugAssert( IsEmpty() == false );
    if( IsEmpty() ) return;

    DebugAssert( m_pageIsUnavailableMessage != NULL );
    if( m_pageIsUnavailableMessage == NULL ) return;

    m_pageIsUnavailableMessage->exec();
}

Logic* MainWindow::Owner() const
{
    DebugAssert( m_owner != NULL );
    return m_owner;
}

ViewerSettings MainWindow::Settings() const
{
    return Owner()->GetSettings();
}

int MainWindow::PageIndex() const
{
    return m_renderer->PageIndex();
}

void MainWindow::EnableGui()
{
    if(m_documentType == Document::PDF)
    {
#ifdef PRINT_SAVE
        m_groupDocument->setEnabled( true );
#endif
        m_groupNavigation->setEnabled( true );
        m_groupScaling->setEnabled( true );
        m_groupView->setEnabled( true );
        m_groupPrivate->setEnabled( true );
        m_pagesControl->setEnabled( true );
        m_zoomPanel->setEnabled( true );
        m_searchPanel->setEnabled( true );
        m_actionInvert->setEnabled( true );
        m_actionScaleWidth->setEnabled(true);
        m_actionScaleFull->setEnabled(true);
#ifndef DISABLE_THUMBNAILS
        m_thumbnails->setEnabled( true );
#endif
        m_actionInfo->setEnabled( true );
        m_guiTabWidget->setTabEnabled(m_guiTabWidget->indexOf(m_commentsControl), true);
        m_guiTabWidget->setTabEnabled(m_guiTabWidget->indexOf(m_searchPanel), true);

        m_actionComment->setEnabled(true);
    } else if(m_documentType == Document::EPUB)
    {
        m_groupNavigation->setEnabled( true );
        m_pagesControl->setEnabled(true);
        m_zoomPanel->setEnabled( true );
        m_groupPrivate->setEnabled( true );
        m_actionInvert->setEnabled( true );
        m_groupScaling->setEnabled( true );
        m_actionInfo->setEnabled( true );

        m_guiTabWidget->setTabEnabled(m_guiTabWidget->indexOf(m_commentsControl), false);
        m_guiTabWidget->setTabEnabled(m_guiTabWidget->indexOf(m_searchPanel), false);
        m_actionComment->setEnabled(false);

    }
}

void MainWindow::DisableGui()
{
#ifdef PRINT_SAVE
    m_groupDocument->setEnabled( false );
#endif
    m_groupNavigation->setEnabled( false );
    m_groupScaling->setEnabled( false );
    m_groupView->setEnabled( false );
    m_groupPrivate->setEnabled( false );
    m_pagesControl->setEnabled( false );
    m_zoomPanel->setEnabled( false );
    m_searchPanel->setEnabled( false );
    m_actionInvert->setEnabled( false );
    m_actionScaleWidth->setEnabled(false);
    m_actionScaleFull->setEnabled(false);

#ifndef DISABLE_THUMBNAILS
    m_thumbnails->setEnabled( false );
#endif
    m_actionInfo->setEnabled( false );
    m_actionComment->setEnabled(false);
}

void MainWindow::SetBookmarksTooltip(bool checked)
{
    QString text = checked ? tr( "RemoveBookmark" ) : tr( "AddBookmark" );
    m_actionBookmark->setToolTip( text );
    ToolTip( m_actionBookmark );
}

void MainWindow::SetFavoritesTooltip(bool checked)
{
    QString text = checked ? tr( "Removefavorites" ) : tr( "AddFavorites" );
    m_actionFavorite->setToolTip( text );
    ToolTip( m_actionFavorite );
}

void MainWindow::SetCommentsTooltip(bool checked)
{
    QString toolTip = checked ? tr( "EditComment" ) : tr( "AddComment" );
    m_actionComment->setToolTip( toolTip );
    ToolTip( m_actionComment );
}


bool MainWindow::eventFilter(QObject *object, QEvent *ev)
{
    Q_UNUSED(object)
 if (ev->type() == QEvent::KeyPress)
  {
       QKeyEvent *keyEvent = static_cast<QKeyEvent *>(ev);
       switch(keyEvent->key())
       {
       case Qt::Key_Up:
           m_actionPrevPage->trigger();
           return true;
       case Qt::Key_Down:
           m_actionNextPage->trigger();
           return true;
       }
      return false;
  }

 return false;
}


ContentItemPtr traverserEpubTOC(QVariantMap data ){
    ContentItemPtr topLevel(new ContentDisplay::Item);
    topLevel->label = data.value("label", "n/a").toString();
    topLevel->link = data.value("cfi", "").toString();
    for(auto entry: data.value("subitems", QVariantList()).toList())
    {
        topLevel->subitems << traverserEpubTOC(entry.toMap());
    }
    return topLevel;

};

void MainWindow::parseEpubToc(QVariantList data)
{
    QList<ContentItemPtr> items;
    for(auto entry: data) {
        items << traverserEpubTOC(entry.toMap());
    }
    m_contentDisplay->setData(items);
}
void MainWindow::parsePdfToc(QVariantList data)
{
    QList<ContentItemPtr> items;
    for(auto entry: data) {
        auto d = entry.toMap();
        ContentItemPtr topLevel(new ContentDisplay::Item);
        topLevel->label = d.value("title", "n/a").toString();
        topLevel->link = d.value("startPage", 1).toInt();
        items << topLevel;
    }
    m_contentDisplay->setData(items);
}

void MainWindow::gotoContentLink(QVariant link)
{
    if(m_documentType == Document::PDF)
    {
        GotoSearch( link.toInt(), WordCoordinates() );
    }
    else if(m_documentType == Document::EPUB)
    {
        m_EpubRenderer->gotoCFI(link.toString());
    }
}
