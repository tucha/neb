#ifndef PAGESCONTROL_H
#define PAGESCONTROL_H

#include <QWidget>

class QHBoxLayout;
class QLineEdit;
class QLabel;
class QIntValidator;

#include <defines.h>

class PagesControl : public QWidget
{
	Q_OBJECT
private:
	DISABLE_COPY( PagesControl )

public:
	explicit PagesControl(QWidget *parent = 0);
	void Translate(void);
	void Clear(void);

signals:
	void GotoPage( int pageIndex );

public slots:
	void SetPagesCount( int pagesCount );
	void SetPageIndex( int pageIndex );

private slots:
	void ReturnPressed();

private:
	QHBoxLayout* m_layout;
	QLineEdit* m_pageIndex;
	QLineEdit* m_pagesCount;
	QLabel* m_labelHeader;
	QLabel* m_labelSeparator;
	QIntValidator* m_validator;
};

#endif // PAGESCONTROL_H
