#include "printingwindow.h"

#include <QPrinter>
#include <QPainter>
#include <QVBoxLayout>
#include <QLabel>
#include <QProgressBar>
#include <QPushButton>
#include <QPrintDialog>
#include <QTransform>
#include <QMessageBox>

#include <algorithm>

PrintingWindow::PrintingWindow(QWidget *parent) :
	QMainWindow(parent),

	m_baseRequest( NULL ),
	m_requestImage( NULL ),
	m_printer( NULL ),
	m_painter( NULL ),

	m_layout( new QVBoxLayout() ),
	m_label( new QLabel( this ) ),
	m_progressBar( new QProgressBar( this ) ),
	m_button( new QPushButton( this ) ),

	m_pages(),
	m_current( -1 ),

	m_forbiddenPages()
{
	CreateGui();
	Translate();

	connect(m_button,SIGNAL(clicked()),this,SLOT(Cancel()));
	setFixedSize( 400, sizeHint().height() );
	setWindowModality( Qt::ApplicationModal );
}

PrintingWindow::~PrintingWindow()
{
	Cancel();
}

void PrintingWindow::StartPrinting(Request* baseRequest, PageSet pageSet)
{
	DebugAssert( pageSet.IsEmpty() == false );
	if( pageSet.IsEmpty() ) return;

	if( this->isVisible() ) return;
	this->show();

	m_pages = pageSet.ToList();
	m_current = 0;
	m_baseRequest = baseRequest;

	m_label->setText( tr( "Downloading..." ) );

	DeleteAndNull( m_painter );
	DeleteAndNull( m_printer );

	m_printer = new QPrinter( QPrinter::HighResolution );
	m_painter = new QPainter();

	QPrintDialog dialog( m_printer, this );
	dialog.setWindowTitle( tr( "PrintDocument" ) );
	dialog.setOptions( QAbstractPrintDialog::None );
	if( dialog.exec() != QDialog::Accepted )
	{
		Cancel();
		return;
	}

	if( m_printer->isValid() == false )
	{
		Cancel();
		return;
	}

	DownloadPage( m_pages.at( m_current ) );
}

void PrintingWindow::closeEvent(QCloseEvent* event)
{
	UNUSED( event );
	Cancel();
}

void PrintingWindow::SlotImageFinished(ErrorCode error, QImage image)
{
	if( m_requestImage == NULL ) return;
	if( sender() != m_requestImage ) return;

	if( error == QNetworkReply::ContentOperationNotPermittedError )
	{
		m_forbiddenPages.Add( SENDER_PAGE->PageIndex() );
		DownloadNextPage();
		return;
	}

	if( error != API_NO_ERROR )
	{
		m_requestImage->Send();
		return;
	}

	if( PrepareNewPage() == false )
	{
		Cancel();
		return;
	}

	DrawImage( image );
	DownloadNextPage();
}

void PrintingWindow::SlotDownloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
	m_progressBar->setMaximum( bytesTotal );
	m_progressBar->setValue( bytesReceived );
}

void PrintingWindow::Cancel()
{
	m_baseRequest  = NULL;

	DeleteLaterAndNull( m_requestImage );
	DeleteAndNull( m_painter );
	DeleteAndNull( m_printer );

	m_pages.clear();
	m_current = -1;

	m_progressBar->setValue( 0 );
	m_label->clear();

	m_forbiddenPages = PageSet();

	close();
}

void PrintingWindow::CreateGui()
{
	m_layout->addWidget( m_label );
	m_layout->addWidget( m_progressBar );
	m_layout->addWidget( m_button, 0, Qt::AlignCenter );

	this->setWindowFlags( Qt::Window | Qt::WindowCloseButtonHint );

	QWidget* tmp = new QWidget( this );
	tmp->setLayout( m_layout );
	this->setCentralWidget( tmp );
}

void PrintingWindow::Translate()
{
	m_button->setText( tr( "Cancel" ) );
	this->setWindowTitle( tr( "Printing Document" ) );
}

void PrintingWindow::DownloadPage(int pageIndex)
{
	DebugAssert( pageIndex > 0 );
	if( pageIndex <= 0 )
	{
		Cancel();
		return;
	}

	DeleteLaterAndNull( m_requestImage );

	RequestPage* tmp = m_baseRequest->NewPage( pageIndex );

	m_requestImage = tmp->NewImage( TYPE_IMAGES, 300, FORMAT_JPEG );
	connect(m_requestImage,SIGNAL(SignalFinished(ErrorCode,QImage)),this,SLOT(SlotImageFinished(ErrorCode,QImage)));
	connect(m_requestImage,SIGNAL(SignalDownloadProgress(qint64,qint64)),this,SLOT(SlotDownloadProgress(qint64,qint64)));
	m_requestImage->Send();

	DeleteLaterAndNull( tmp );

	m_label->setText( tr( "Downloading page %1 ( %2 of %3 ) ..." )
					  .arg( pageIndex )
					  .arg( m_current + 1 )
					  .arg( m_pages.count() ) );
	m_progressBar->setValue( 0 );
}

QImage PrintingWindow::Transform(QImage image, QRect page)
{
	QTransform transform;

	bool imageIsLandscape = image.width() > image.height();
	bool pageIsLandscape = page.width() > page.height();

	int rotation = 0;
	if( imageIsLandscape != pageIsLandscape ) rotation = 90;
	transform = transform.rotate( rotation );

	qreal imageSize = std::max( image.width(), image.height() );
	qreal pageSize = std::max( page.width(), page.height() );

	qreal scale = pageSize / imageSize;
	transform = transform.scale( scale, scale );

	return image.transformed( transform );
}

void PrintingWindow::DrawImage(QImage image)
{
	QRect page = m_printer->pageRect();
	QImage transformed = Transform( image, page );

	QSize translate = ( page.size() - transformed.size() ) / 2;
	m_painter->drawImage( translate.width(), translate.height(), transformed );
}

bool PrintingWindow::PrepareNewPage()
{
	DebugAssert( m_current >= 0 );
	if( m_current < 0 ) return false;

	DebugAssert( m_current < m_pages.count() );
	if( m_current >= m_pages.count() ) return false;

	if( m_current == 0 ) return m_painter->begin( m_printer );
	else return m_printer->newPage();
}

void PrintingWindow::DownloadNextPage()
{
	m_current += 1;
	if( m_current == m_pages.count() )
	{
		ShowForbiddenPages();
		Cancel();
		return;
	}

	DownloadPage( m_pages.at( m_current ) );
}

void PrintingWindow::ShowForbiddenPages()
{
	if( m_forbiddenPages.IsEmpty() ) return;

	QString pages = m_forbiddenPages.ToString();

	QString title = tr( "AccessError" );
	QString message = tr( "Pages where not printed: %1" ).arg( pages );
	QMessageBox::information( this, title, message );
}
