#include "searchpanel.h"
#include "searchresults.h"

#include <QBoxLayout>
#include <QLineEdit>
#include <QListView>
#include <QLabel>
#include <QGroupBox>
#include <QPushButton>
#include <QMessageBox>
#include <QListWidgetItem>
#include <QPointer>
#include "mainwindow.h"
SearchPanel::SearchPanel(QWidget *parent) :
    QWidget(parent),
    m_baseRequest( NULL ),
    m_pagesCount( 0 )
{
    CreateGui();
    ConnectSlots();
    Translate();

    m_lastPageIndex = -1;
    m_coordinatesReceivedFor = -1;
}

SearchPanel::~SearchPanel()
{
    Clear();
}

void SearchPanel::Translate()
{
    m_label->setText( tr( "Text for search:" ) );
    m_groupBox->setTitle( tr( "" ) );
    m_searchButton->setText( tr( "Search" ) );
    m_clearButton->setText( tr( "Clear" ) );
}

void SearchPanel::Load(int pagesCount, Request* baseRequest)
{
    DebugAssert( pagesCount > 0 );
    if( pagesCount <= 0 ) return;

    m_baseRequest = baseRequest;
    m_pagesCount = pagesCount;
}

void SearchPanel::Clear()
{
    m_baseRequest = NULL;
    m_pagesCount = 0;
    m_textEdit->setText( "" );
    m_status->setText( tr( "Search results:" ) );
    m_lastSearchText = "";
    m_lastPageIndex = -1;
    m_coordinatesReceivedFor = -1;
}

void SearchPanel::Select(int pageIndex)
{
    m_lastPageIndex = pageIndex;

    int index = -1;
    for(int i = 0; i < m_searchResultsList->count(); i ++)
    {
        auto pi = m_searchResultsList->item(i)->data(Qt::UserRole).toInt() ;
        m_searchResultsList->item(i)->setSelected(
                    pi  - 1== m_lastPageIndex
                    );
        if(pi  - 1== m_lastPageIndex)
            index = pi;
    }
    if(index != -1 && m_coordinatesReceivedFor != m_lastPageIndex)
    {

        if(m_isClosed && pageIndex >= float(m_mainWindow->Settings().SecondWarningBookPercent())/100.0 * float(m_pagesCount) )
        {
            qDebug() << "Search is not performed for closed pages";
            return;
        }
        RequestPage* tmp = m_baseRequest->NewPage( m_lastPageIndex + 1 );

        QStringList words = QStringList() << m_lastSearchText;
        auto requestCoordinates = tmp->NewWordCoordinates( words, PRIORITY_HIGHT );
        connect(requestCoordinates,&RequestWordCoordinates::SignalFinished,this,&SearchPanel::SlotFinishedCoordinates);
        requestCoordinates->Send();

        tmp->deleteLater();
    }
}

void SearchPanel::PerformSearch(QString text)
{
    if(text.isEmpty())
    {
        return;
    }
    m_textEdit->setText(text);
    Search();
}

void SearchPanel::SetMainWindow(MainWindow *w)
{
    m_mainWindow = w;
}

void SearchPanel::Search()
{
    QString text = m_textEdit->text();
    if( text.isEmpty() ) return;

    auto l = text.split("|");
    QStringList searchList;
    for(auto s: l)
    {
        if(!s.isEmpty())
        {
            searchList.append(s.trimmed());
        }
    }
    auto searchRequest = QPointer<RequestDocumentSearch>(m_baseRequest->NewSearch(searchList));

    auto typeRequest = QPointer<RequestDocumentType>(m_baseRequest->NewDocumentType());

    connect(typeRequest, &RequestDocumentType::SignalFinished, [=] (ErrorCode error, QString type){
        if(type == "close")
            m_isClosed = true;
        else
            m_isClosed = false;
        typeRequest->deleteLater();
        searchRequest->Send();
    });


    connect(searchRequest, &RequestDocumentSearch::SignalFinished, [=](ErrorCode error, QVariantMap results, bool js_error){
        if(error !=  QNetworkReply::NoError)
        {
            qDebug() << "Search request network error";
            if(error == QNetworkReply::ContentOperationNotPermittedError)
            {

            }
            NothingWasFound();
        }

        if(js_error == true)
        {
            qDebug() << "Search request response is malformed";
            NothingWasFound();
            return;
        }

        if(searchRequest)
            searchRequest->deleteLater();
        auto pagesList = results["pages"].toList();
        m_searchResultsList->clear();
        if(pagesList.isEmpty())
        {
            NothingWasFound();
        }
        else
        {
            QList<int> sortedPages;
            for(auto varPN: pagesList)
            {
                bool ok;
                auto pageIndex = varPN.toInt(&ok);
                if(ok)
                    sortedPages << pageIndex;
            }
            std::sort(sortedPages.begin(), sortedPages.end());
            int minPage = -1;
            for(auto pageIndex: sortedPages)
            {
                if(minPage == -1) minPage = pageIndex;
                if(pageIndex < minPage) minPage = pageIndex;

                auto newItem = new QListWidgetItem(QString(tr("Page %1 ")).arg(pageIndex), m_searchResultsList);
                newItem->setData(Qt::UserRole, pageIndex);

            }
            m_lastSearchText = text;


            m_coordinatesReceivedFor = -1;
            emit GotoSearch(minPage, WordCoordinates());


            Select(m_lastPageIndex);
        }

    });
    typeRequest->Send();
    m_searchResultsList->clear();
    m_status->setText( tr( "Search results:" ) );
}

void SearchPanel::TextChanged(QString text)
{
    m_searchButton->setEnabled( text.isEmpty() == false );
    m_clearButton->setEnabled( text.isEmpty() == false );
    if( text.isEmpty() ) m_searchResultsList->clear();
}

void SearchPanel::SlotGotoSearch(int pageIndex, WordCoordinates coordinates)
{
    emit GotoSearch( pageIndex, coordinates );
}

void SearchPanel::SearchFinished()
{
    m_status->setText( tr( "Searching was finished" ) );
}

void SearchPanel::NothingWasFound()
{
    m_status->setText( tr( "Nothing was found" ) );
}

void SearchPanel::SearchProgress(int received, int sent)
{
    m_status->setText( tr( "Searching %1 percents where finished" ).arg( 100 * received / sent  ) );
}

void SearchPanel::CreateGui()
{
    m_label = new QLabel();
    m_textEdit = new QLineEdit();
    m_searchButton = new QPushButton();
    m_groupBox = new QGroupBox();

    m_status = new QLabel();
    m_clearButton = new QPushButton();
    m_searchResultsList = new QListWidget(this);
    m_searchResultsList->setSelectionMode(QAbstractItemView::ExtendedSelection);

    m_label->setMargin( 5 );
    m_searchButton->setEnabled( false );
    m_clearButton->setEnabled( false );

#ifdef OS_MACOSX
    m_textEdit->setMinimumWidth( 60 );
#else
    m_searchButton->setMinimumWidth( 40 );
#endif

    QBoxLayout* tmp = new QBoxLayout( QBoxLayout::LeftToRight );
    tmp->setMargin( 0 );
    tmp->setContentsMargins( 0, 0, 0, 0 );
    tmp->setSpacing( 0 );

    tmp->addWidget( m_label );
    tmp->addWidget( m_textEdit );
    tmp->addWidget( m_searchButton );
    //tmp->addWidget( m_clearButton );
    m_groupBox->setLayout( tmp );

    m_verticalLayout = new QBoxLayout( QBoxLayout::TopToBottom );
    m_verticalLayout->addWidget( m_groupBox );
    m_verticalLayout->addWidget( m_status );
    m_verticalLayout->addWidget( m_searchResultsList );

    this->setLayout( m_verticalLayout );
}

void SearchPanel::ConnectSlots()
{
    connect(m_searchButton,SIGNAL(clicked()),this,SLOT(Search()));
    connect(m_textEdit,SIGNAL(textChanged(QString)),this,SLOT(TextChanged(QString)));

    connect(m_clearButton,SIGNAL(clicked()),m_textEdit,SLOT(clear()));
    connect(m_textEdit,SIGNAL(returnPressed()),this,SLOT(Search()));

    connect(m_searchResultsList, &QListWidget::itemClicked, [=](QListWidgetItem * item){
        m_searchResultsList->clearSelection();
        auto pageIndex = item->data(Qt::UserRole).toInt();


        m_coordinatesReceivedFor = -1;
        emit GotoSearch(pageIndex, WordCoordinates());


    });
}


void SearchPanel::SlotFinishedCoordinates(ErrorCode error, WordCoordinates wordCoordinates)
{
    m_coordinatesReceivedFor = m_lastPageIndex;
    auto r = qobject_cast<RequestWordCoordinates*>(sender());
    if(r == nullptr )
        return;
    //    qDebug() << r->PageIndex() << m_lastPageIndex;
    if(r->PageIndex() != m_lastPageIndex + 1)
        return;
    emit GotoSearch(m_lastPageIndex + 1, wordCoordinates);
    r->deleteLater();
}
