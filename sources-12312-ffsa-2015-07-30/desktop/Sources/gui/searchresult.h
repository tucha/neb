#ifndef SEARCHRESULT_H
#define SEARCHRESULT_H

#include <QFrame>
#include <common.h>

class QVBoxLayout;
class QLabel;
class QProgressBar;

class SearchResult : public QFrame
{
	Q_OBJECT
private:
	DISABLE_COPY( SearchResult )

public:
	explicit SearchResult(QWidget *parent = 0);
	virtual ~SearchResult();

	bool IsEmpty(void)const;
	void Load(int pageIndex, Request* baseRequest, QString text);
	void Clear(void);

	virtual void mousePressEvent(QMouseEvent* event);

signals:
	void GotoSearch( int pageIndex, WordCoordinates coordinates );
	void NothingWasFount();
	void Finished();

private slots:
	void SlotFinishedImage( ErrorCode error, QImage image );
	void SlotDownloadProgress( qint64 bytesReceived, qint64 bytesTotal );
	void SlotFinishedCoordinates( ErrorCode error, WordCoordinates wordCoordinates );

private:
	RequestWordCoordinates* m_requestCoordinates;
	RequestImageFixed* m_requestImage;
	QVBoxLayout* m_layout;
	QLabel* m_label;
	QLabel* m_pageIndex;
	QProgressBar* m_progressBar;
	WordCoordinates m_wordCoordinates;
};

#endif // SEARCHRESULT_H
