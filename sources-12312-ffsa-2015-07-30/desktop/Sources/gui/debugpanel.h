#ifndef DEBUGPANEL_H
#define DEBUGPANEL_H

#include <QDockWidget>
#include <QString>
#include <QList>

class QTextEdit;
class QLineEdit;
class QVBoxLayout;

class DebugPanel : public QDockWidget
{
	Q_OBJECT
public:
	explicit DebugPanel(QWidget *parent = 0);

private slots:
	void Output( QString text );
	void FilterChanged( QString newFilter );

private:
	void AppendLine( QString line );

	QTextEdit* m_textEdit;
	QLineEdit* m_filter;
	QVBoxLayout* m_layout;
	QWidget* m_widget;

	QList<QString> m_lines;
};

#endif // DEBUGPANEL_H
