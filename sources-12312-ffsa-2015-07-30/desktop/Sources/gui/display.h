#ifndef DISPLAY_H
#define DISPLAY_H

#include <QWidget>
#include <QPixmap>

class QScrollArea;
class QLabel;
class QBoxLayout;
class QProgressBar;

#include <defines.h>

class Display : public QWidget
{
	Q_OBJECT
private:
	DISABLE_COPY( Display )

public:
	explicit Display(QWidget *parent = 0);
	~Display();

	void ShowText( QString text );
	void ShowTextForbidden(void);
	void ShowPage(void);
	void HidePage(void);

	virtual void resizeEvent(QResizeEvent* event);

signals:
	void Resized( QSize newSize );
public slots:
	void SlotDownloadProgress( qint64 bytesReceived, qint64 bytesTotal );
	void SetPixmap( QPixmap pixmap );
	void Clear(void);

private:
	// Private methods
	void SetForbiddenMessage( QString message );
	void SetForbidden( bool value );
	void CreateGuiElements(void);

	// Gui elements
	QScrollArea*	m_guiScrollArea;
	QLabel*			m_guiLabel;
	QBoxLayout*		m_guiLayout;
	QProgressBar*	m_guiProgressBar;
	QLabel*			m_guiMessage;

	bool m_forbidden;
};

#endif // DISPLAY_H
