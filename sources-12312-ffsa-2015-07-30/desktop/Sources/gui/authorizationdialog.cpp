#include "authorizationdialog.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QAuthenticator>
#include <QMessageBox>
#include <QWidget>

#include "../miscellaneous/config.h"
#include "../logic.h"

#include <authorizationrequest.h>
#include <authorizationbase.h>
#include <AnonymToken.h>
#include <defines.h>

AuthorizationDialog::AuthorizationDialog(QWidget *parent) :
	QMainWindow(parent),

	m_verticalLayout( new QVBoxLayout() ),
	m_horizontalLayout( new QHBoxLayout() ),
	m_labelMessage( new QLabel() ),
	m_labelUser( new QLabel() ),
	m_lineEditUser( new QLineEdit() ),
	m_labelPassword( new QLabel() ),
	m_lineEditPassword( new QLineEdit() ),
	m_okButton( new QPushButton() ),
	m_cancelButton( new QPushButton() ),
    m_anonymButton(new QPushButton()),
    m_centralWidget( new QWidget() ),
	m_base( NULL ),
	m_request( NULL )
{
	CreateGui();
	ConnectSlots();
	Translate();

#ifdef BUILD_DEBUG
//	SetEmail( "reader1@elar.ru" );
//	SetPassword( "Qwe123" );
	SetEmail( "reader@elar.ru" );
	SetPassword( "111111" );
#endif

	SetMessage( QString() );
}

void AuthorizationDialog::SetEmailEnabled(bool enabled)
{
	m_lineEditUser->setEnabled( enabled );
}

void AuthorizationDialog::SetMessage(QString message)
{
	m_labelMessage->setText( message );
	m_labelMessage->setVisible( message.isEmpty() == false );
}

void AuthorizationDialog::SetBase(AuthorizationBase* base)
{
	m_base = base;
}

void AuthorizationDialog::SetEmail(QString email)
{
	m_lineEditUser->setText( email );
	if( email.isEmpty() == false ) m_lineEditPassword->setFocus();
}

void AuthorizationDialog::SetPassword(QString password)
{
	m_lineEditPassword->setText( password );
}

void AuthorizationDialog::OkButtonClicked()
{
	DebugAssert( m_base != NULL );
	if( m_base == NULL ) return;

	m_okButton->setEnabled( false );

	DeleteLaterAndNull( m_request );
	m_request = m_base->NewRequest( m_lineEditUser->text(), m_lineEditPassword->text() );
	connect(m_request,SIGNAL(SignalFinished(ErrorCode,AuthorizationResult)),this,SLOT(SlotFinished(ErrorCode,AuthorizationResult)));
	connect(m_request,SIGNAL(SignalAuthenticationRequired(QAuthenticator*)),this,SLOT(SlotAuthenticationRequired(QAuthenticator*)));
	m_request->Send();
}

void AuthorizationDialog::closeEvent(QCloseEvent* event)
{
	UNUSED( event );
	m_okButton->setEnabled( true );
	emit AuthorizationCancelled();
}

void AuthorizationDialog::SlotFinished(ErrorCode error, AuthorizationResult result)
{
	if( sender() != m_request ) return;
	if( isVisible() == false ) return;

	m_okButton->setEnabled( true );

	if( error != API_NO_ERROR )
	{
		QMessageBox::information( this, tr( "title" ), tr( "error" ) );
		return;
    }

	DebugAssert( result.IsEmpty() == false );
	if( result.IsEmpty() ) return;

	if( result.GetError().isEmpty() == false )
	{
		QMessageBox::information( this, tr( "title" ), result.GetError() );
		return;
	}

	User user = result.GetUser();

	DebugAssert( user.IsEmpty() == false );
	if( user.IsEmpty() ) return;

	emit UserAuthorized( user );
}

void AuthorizationDialog::SlotAuthenticationRequired(QAuthenticator* authenticator)
{
	authenticator->setUser( AUTHENTICATION_USER );
    authenticator->setPassword( AUTHENTICATION_PASSWORD );
}

void AuthorizationDialog::anonymAccessSlot()
{
    auto req = m_base->NewAnonym();
    connect(req, &AnonymToken::SignalFinished, [=](ErrorCode error, QString token){
        Q_UNUSED(error)
       req->deleteLater();

       if(token != "")
           emit AnonymTokenReady(token);
       else
       {
            QMessageBox msgBox;
           msgBox.setText(tr("Cannot get anonym access"));
           msgBox.exec();
       }
    });
    req->Send();
}

void AuthorizationDialog::CreateGui()
{
	m_labelMessage->setWordWrap( true );
	m_lineEditPassword->setEchoMode( QLineEdit::Password );

	m_verticalLayout->addWidget( m_labelMessage );
	m_verticalLayout->addWidget( m_labelUser );
	m_verticalLayout->addWidget( m_lineEditUser );
	m_verticalLayout->addWidget( m_labelPassword );
	m_verticalLayout->addWidget( m_lineEditPassword );
	m_horizontalLayout->addWidget( m_okButton );
	m_horizontalLayout->addWidget( m_cancelButton );
    m_horizontalLayout->addWidget(m_anonymButton);
	m_verticalLayout->addLayout( m_horizontalLayout );
	m_centralWidget->setLayout( m_verticalLayout );
	m_centralWidget->setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed );

	this->setCentralWidget( m_centralWidget );
	this->setWindowFlags( Qt::Window | Qt::WindowCloseButtonHint );
	this->setWindowModality( Qt::ApplicationModal );

	m_labelUser->setMinimumWidth( 300 );
}

void AuthorizationDialog::Translate()
{
	m_labelUser->setText( tr( "UserName" ) );
	m_labelPassword->setText( tr( "Password" ) );
	m_okButton->setText( tr( "Ok" ) );
	m_cancelButton->setText( tr( "Cancel" ) );
    m_anonymButton->setText(tr( "Anonymous access" ) );

	this->setWindowTitle( tr( "Authentication" ) );
}

void AuthorizationDialog::ConnectSlots()
{
	connect(m_cancelButton,SIGNAL(clicked()),this,SIGNAL(AuthorizationCancelled()));
	connect(m_okButton,SIGNAL(clicked()),this,SLOT(OkButtonClicked()));
	connect(m_lineEditPassword,SIGNAL(returnPressed()),m_okButton,SLOT(click()));
	connect(m_lineEditUser,SIGNAL(returnPressed()),m_okButton,SLOT(click()));
    connect(m_anonymButton, &QPushButton::clicked, this, &AuthorizationDialog::anonymAccessSlot);
}
