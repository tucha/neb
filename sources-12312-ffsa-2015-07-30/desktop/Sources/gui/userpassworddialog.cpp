#include "userpassworddialog.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QAuthenticator>
#include <QMessageBox>
#include <QWidget>

#include <defines.h>

UserPasswordDialog::UserPasswordDialog(QWidget *parent) :
	QDialog(parent),

	m_verticalLayout( new QVBoxLayout() ),
	m_horizontalLayout( new QHBoxLayout() ),
	m_labelUser( new QLabel() ),
	m_lineEditUser( new QLineEdit() ),
	m_labelPassword( new QLabel() ),
	m_lineEditPassword( new QLineEdit() ),
	m_okButton( new QPushButton() ),
	m_cancelButton( new QPushButton() )
{
	CreateGui();
	ConnectSlots();
	Translate();
}

QDialog::DialogCode UserPasswordDialog::Show()
{
	return static_cast<QDialog::DialogCode>( exec() );
}

QString UserPasswordDialog::User() const
{
	return m_lineEditUser->text();
}

QString UserPasswordDialog::Password() const
{
	return m_lineEditPassword->text();
}

void UserPasswordDialog::CreateGui()
{
	m_lineEditPassword->setEchoMode( QLineEdit::Password );

	m_verticalLayout->addWidget( m_labelUser );
	m_verticalLayout->addWidget( m_lineEditUser );
	m_verticalLayout->addWidget( m_labelPassword );
	m_verticalLayout->addWidget( m_lineEditPassword );
	m_horizontalLayout->addWidget( m_okButton );
	m_horizontalLayout->addWidget( m_cancelButton );
	m_verticalLayout->addLayout( m_horizontalLayout );

	this->setLayout( m_verticalLayout );
	this->setWindowFlags( Qt::Window | Qt::WindowCloseButtonHint );
	this->setFixedSize( 300, sizeHint().height() );
}

void UserPasswordDialog::Translate()
{
	m_labelUser->setText( tr( "UserName" ) );
	m_labelPassword->setText( tr( "Password" ) );
	m_okButton->setText( tr( "Ok" ) );
	m_cancelButton->setText( tr( "Cancel" ) );

	this->setWindowTitle( tr( "Authentication" ) );
}

void UserPasswordDialog::ConnectSlots()
{
	connect(m_cancelButton,SIGNAL(clicked()),this,SLOT(reject()));
	connect(m_okButton,SIGNAL(clicked()),this,SLOT(accept()));
	connect(m_lineEditPassword,SIGNAL(returnPressed()),m_okButton,SLOT(click()));
	connect(m_lineEditUser,SIGNAL(returnPressed()),m_okButton,SLOT(click()));
}
