#ifndef VIEWERSCONTAINER_H
#define VIEWERSCONTAINER_H

#include <QWidget>

class ViewersContainer : public QWidget
{
    Q_OBJECT
    void resizeEvent(QResizeEvent* event);
public:
    explicit ViewersContainer(QWidget *parent = 0);

signals:
    void resized(QSize);
public slots:
};

#endif // VIEWERSCONTAINER_H
