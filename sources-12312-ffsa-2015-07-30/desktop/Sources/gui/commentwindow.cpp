#include "commentwindow.h"

#include <QKeyEvent>
#include <QTextCursor>

CommentWindow::CommentWindow(QWidget *parent) :
	QMainWindow(parent)
{
	m_centralWidget = new QWidget();
	m_verticalLayout = new QVBoxLayout();
	m_horizontalLayout = new QHBoxLayout();
	m_label = new QLabel();
	m_textEdit = new QTextEdit();
	m_saveButton = new QPushButton();
	m_deleteButton = new QPushButton();
	m_cancelButton = new QPushButton();

	connect(m_saveButton,SIGNAL(clicked()),this,SLOT(Save()));
	connect(m_deleteButton,SIGNAL(clicked()),this,SLOT(Delete()));
	connect(m_cancelButton,SIGNAL(clicked()),this,SLOT(Cancel()));
	connect(m_textEdit,SIGNAL(textChanged()),this,SLOT(TextChanged()));

	m_verticalLayout->addWidget( m_label );
	m_verticalLayout->addWidget( m_textEdit );
	m_horizontalLayout->addWidget( m_saveButton );
	m_horizontalLayout->addWidget( m_deleteButton );
	m_horizontalLayout->addWidget( m_cancelButton );
	m_verticalLayout->addLayout( m_horizontalLayout );
	m_centralWidget->setLayout( m_verticalLayout );

	this->setCentralWidget( m_centralWidget );
	this->setWindowModality( Qt::ApplicationModal );

	m_textEdit->setWordWrapMode( QTextOption::NoWrap );

	Translate();
}

void CommentWindow::SetText(QString text)
{
	m_originalText = text;
	m_textEdit->setText( text );
	m_deleteButton->setVisible( text.isEmpty() == false );
	Translate();

	QTextCursor cursor = m_textEdit->textCursor();
	cursor.movePosition( QTextCursor::End );
	m_textEdit->setTextCursor( cursor );
	m_textEdit->setFocus();
}

void CommentWindow::keyPressEvent(QKeyEvent* event)
{
	if( event->key() == Qt::Key_Escape ) m_cancelButton->click();
	if( event->key() == Qt::Key_Return && event->modifiers() == Qt::ControlModifier ) m_saveButton->click();
}

void CommentWindow::Save()
{
	hide();
	emit SetComment( m_textEdit->toPlainText() );
}

void CommentWindow::Delete()
{
	hide();
	emit SetComment( QString () );
}

void CommentWindow::Cancel()
{
	emit SetComment( m_originalText );
	hide();
}

void CommentWindow::TextChanged()
{
	m_saveButton->setEnabled( m_textEdit->toPlainText().trimmed().isEmpty() == false );
}

void CommentWindow::Translate()
{
	bool add = m_textEdit->toPlainText().isEmpty();
	QString saveText = add ? tr( "AddComment" ) : tr( "SaveComment" );
	m_saveButton->setText( saveText );
	m_deleteButton->setText( tr( "DeleteComment" ) );
	m_cancelButton->setText( tr( "Cancel" ) );
	m_label->setText( tr( "Comment" ) );
	this->setWindowTitle( tr( "CommentWindow" ) );
}
