#ifndef BOOKMARKSCONTROL_H
#define BOOKMARKSCONTROL_H

#include <QScrollArea>
#include <QSet>
#include <QPoint>

class FlowLayout;
class Request;
class Thumbnail;
class BookmarksManager;
class CommentsManager;

#include <defines.h>
#include <pageset.h>
#include <QMenu>

class BookmarksControl : public QScrollArea
{
	Q_OBJECT
private:
	DISABLE_COPY( BookmarksControl )

public:
	explicit BookmarksControl(QWidget *parent = 0);
	~BookmarksControl();

	void Load( Request* baseRequest );
	void Clear(void);

	PageSet SelectedPages(void)const;

public slots:
	void UpdateBookmarks( BookmarksManager* bookmarks );
	void UpdateComments( CommentsManager* comments );

protected:
	virtual void resizeEvent(QResizeEvent* event);
	virtual void showEvent(QShowEvent* event);

public slots:
	void Select( int pageIndex );
    void UpdateCfiToPage(QVariantMap cfiToPage);
    void RequestCFi();
signals:
    void GotoPage( int pageIndex , QString);
    void DeleteBookmark(int, QString);
    void CfiUpdated(QStringList list);
private slots:
	void AddToSelection( int pageIndex );
	void AddRangeToSelection( int pageIndex );
	void Loaded( int pageIndex );
    void RightClickSlot(Thumbnail *src, QPoint pos);
    void DeleteMe();

private:
	// Private methods
	void CreateGui(void);
	Thumbnail* NewThumbnail(void);
	void HighlightSelected(void);
	bool IsSelected( int index )const;
	void EnsureSelectedIsVisible(void);
	Thumbnail* FindByPageIndex( int pageIndex )const;
	QHash<int,int> CollectPageHash(void)const;
	void Swap( int index1, int index2 );
	void UpdatePageHash(QHash<int, int>& pageHash, int index1, int index2)const;
	bool TestPageHash(const QHash<int, int>& pageHash )const;
    void OptimizeThumbnails(QList<QPair<int, QString> > needPages);

	int Count(void)const;
	Thumbnail* Get( int index )const;
	void Add( Thumbnail* thumbnail );

	// Gui items
	FlowLayout*		m_flowLayout;
	QWidget*		m_widget;

	int m_selectionRangeStart;
	int m_selectionRangeEnd;
	QSet<int> m_selection;
	QPoint m_selectedPosition;

	Request* m_baseRequest;

    QMenu *m_contextMenu;
    QPair<int, QString>  m_lastRightClickedIndex;
};

#endif // BOOKMARKSCONTROL_H
