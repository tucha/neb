#include "record.h"

#include <QLabel>

Record::Record(QObject *parent) :
	QObject(parent),
	m_label( new QLabel( NULL ) ),
	m_value( new QLabel( NULL ) ),
	m_request( NULL )
{
	m_label->setAlignment( Qt::AlignTop );
	m_value->setAlignment( Qt::AlignTop );
	m_value->setWordWrap( true );
	m_label->setStyleSheet( "QLabel { color : gray }" );

	QFont font = m_label->font();
	font.setFamily( "Calibri" );
	font.setItalic( true );
	font.setPointSize( 12 );
	m_label->setFont( font );

	font.setItalic( false );
	m_value->setFont( font );

	m_label->setFixedHeight( 25 );
}

Record::~Record()
{
	Clear();
	m_label->deleteLater();
	m_value->deleteLater();
}

bool Record::IsEmpty() const
{
	return m_request == NULL;
}

void Record::Load(Request* baseRequest, QString field, QString subField, RequestPriority priority)
{
	if( IsActual( baseRequest, field, subField ) ) return;

	Clear();

	m_request = baseRequest->NewDescription( field, subField, priority );
	connect(m_request,SIGNAL(SignalFinished(ErrorCode,QString)),this,SLOT(Finished(ErrorCode,QString)));
	m_request->Send();
}

void Record::Clear()
{
	if( m_request != NULL )
	{
		m_request->deleteLater();
		m_request = NULL;
	}
	m_value->clear();

	m_label->setVisible( false );
	m_value->setVisible( false );
}

QLabel*Record::Label() const
{
	return m_label;
}

QLabel*Record::Value() const
{
	return m_value;
}

void Record::SetLabel(QString label)
{
	m_label->setText( QString( "<i>%1</i>" ).arg( label ) );
}

void Record::Finished(ErrorCode error, QString description)
{
	if( IsEmpty() ) return;
	if( sender() != m_request ) return;

	if( error != API_NO_ERROR ) return;
	m_value->setText( description );

	bool visible = description.isEmpty() == false;
	m_label->setVisible( visible );
	m_value->setVisible( visible );

	emit Ready();
}

bool Record::IsActual(Request* baseRequest, QString field, QString subField) const
{
	DebugAssert( baseRequest != NULL );

	if( IsEmpty() ) return false;
	if( m_request->GetDocument() != baseRequest->GetDocument() ) return false;
	if( m_request->Field() != field ) return false;
	if( m_request->SubField() != subField ) return false;
	return true;
}
