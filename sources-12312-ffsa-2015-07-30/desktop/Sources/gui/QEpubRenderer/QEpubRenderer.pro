#-------------------------------------------------
#
# Project created by QtCreator 2016-09-23T20:46:15
#
#-------------------------------------------------

QT       += gui widgets network xmlpatterns webkitwidgets
CONFIG += c++11 c++14

QMAKE_CXXFLAGS += -std=c++0x -std=c++11
QMAKE_LFLAGS +=  -std=c++11 -std=c++0x

TARGET = QEpubRenderer
TEMPLATE = lib

DEFINES += QEPUBRENDERER_LIBRARY

SOURCES += QEpubRenderer.cpp \
    ZipHTTPServer.cpp \
    WebView.cpp


SOURCES += thirdparty/libzippp/src/libzippp.cpp
HEADERS += thirdparty/libzippp/src/libzippp.h \
    ZipHTTPServer.h \
    WebView.h

HEADERS += QEpubRenderer.h\
        qepubrenderer_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

macx {
    INCLUDEPATH += /usr/local/include /usr/local/Cellar/libzip/1.1.2/lib/libzip/include
    LIBS += -L/usr/local/lib -lzip
    LIBS += -L$$PWD/thirdparty/qhttp/xbin -lqhttp
}

RESOURCES += \
    html_res.qrc
