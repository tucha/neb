
QT       += gui widgets network webkitwidgets

CONFIG += c++11 c++14

QMAKE_CXXFLAGS += -std=c++0x -std=c++11
QMAKE_LFLAGS +=  -std=c++11 -std=c++0x

DEFINES += QEPUBRENDERER_LIBRARY

SOURCES += $$PWD/QEpubRenderer.cpp \
    $$PWD/ZipHTTPServer.cpp \
    $$PWD/WebView.cpp


SOURCES += $$PWD/thirdparty/libzippp/src/libzippp.cpp
HEADERS += $$PWD/thirdparty/libzippp/src/libzippp.h \
    $$PWD/ZipHTTPServer.h \
    $$PWD/WebView.h

HEADERS += $$PWD/QEpubRenderer.h\
        $$PWD/qepubrenderer_global.h

macx {
#    INCLUDEPATH += /usr/local/include /usr/local/Cellar/libzip/1.1.2/lib/libzip/include
#    LIBS += -L/usr/local/lib -lzip

    INCLUDEPATH += /Users/Tucher/libzip-macos10.10/include/ /Users/Tucher/libzip-macos10.10/lib/libzip/include
    LIBS += -L/Users/Tucher/libzip-macos10.10/lib -lzip
}
RESOURCES += \
    $$PWD/html_res.qrc
linux {
    LIBS += -L/usr/local/lib -lzip
}
win32{
    INCLUDEPATH += lib/libzip/include
    LIBS += -lzip
}
include($$PWD/thirdparty/qhttp/src/src.pri)
