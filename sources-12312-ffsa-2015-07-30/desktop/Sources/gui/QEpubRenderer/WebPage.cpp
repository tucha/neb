#include "WebPage.h"
#include <QJsonDocument>

void WebPage::javaScriptConsoleMessage(JavaScriptConsoleMessageLevel level, const QString &message, int lineNumber, const QString &sourceID) {
    QJsonParseError err;
    auto d = QJsonDocument::fromJson(message.toUtf8(), &err);
    if(QJsonParseError::NoError == err.error)
    {
        auto ob = d.toVariant();
        if(ob.type() == QVariant::Map){
            emit jsOutput(ob.toMap());
            return;
        }
    }
    qDebug() << sourceID << lineNumber << message;
}

WebPage::WebPage(QWebEngineProfile *profile, QObject *parent):QWebEnginePage(profile, parent){

}
