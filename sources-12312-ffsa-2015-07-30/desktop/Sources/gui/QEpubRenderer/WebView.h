#ifndef WEBENGINEVIEW_H
#define WEBENGINEVIEW_H

#include <QEvent>
#include <QChildEvent>
#include <QPointer>
#include <QWebView>

class WebView : public QWebView
{
    Q_OBJECT
private:

protected:
    void paintEvent(QPaintEvent *e);
public:
    WebView(QWidget *parent = nullptr);

signals:
};

#endif // WEBENGINEVIEW_H
