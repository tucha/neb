#ifndef QEPUBRENDERER_H
#define QEPUBRENDERER_H

#include "qepubrenderer_global.h"
#include <QWidget>
#include <QThread>
#include <QMap>
#include <QVariantMap>
#include <QTimer>
#include <QLabel>

class ZipHTTPServer;
class WebEngineView;
class QResizeEvent;
class QWebEngineView;
class WebView;

class WebPage;
class QEPUBRENDERERSHARED_EXPORT QEpubRenderer: public QWidget
{
    Q_OBJECT
    Q_PROPERTY(int pageCount READ pageCount NOTIFY pageCountChanged)
    Q_PROPERTY(int currentPageIndex READ currentPageIndex WRITE setCurrentPageIndex NOTIFY currentPageIndexChanged)
    Q_PROPERTY(QPixmap currentPageImage READ currentPageImage NOTIFY currentPageImageChanged)
    Q_PROPERTY(float zoomMultiplier READ zoomMultiplier WRITE setZoomMultiplier NOTIFY zoomMultiplierChanged)
    Q_PROPERTY(bool invertedColors READ invertedColors WRITE setInvertedColors NOTIFY invertedColorsChanged)
    Q_PROPERTY(QVariantMap meta READ meta NOTIFY metaChanged)
    Q_PROPERTY(QVariantList tableOfContents READ tableOfContents NOTIFY tableOfContentsChanged)
    Q_PROPERTY(bool rendering READ rendering NOTIFY renderingChanged)
    Q_PROPERTY(QString currentCfi READ currentCfi WRITE setCurrentCfi NOTIFY currentCfiChanged)

    QThread * m_thread;
    ZipHTTPServer *m_fileServer;

    WebView *m_displayView;
    WebView *m_renderView;

    void resizeEvent(QResizeEvent *event);

    QTimer *m_resizeTimer;
    int m_pageCount;
    int m_currentPageIndex;

    QPixmap m_currentPageImage;
    QLabel *m_imgLabel;

    bool m_rendering;
    float m_zoomMultiplier;
    bool m_invertedColors;
    int m_port;

    QString url(QString p);
    void makeShot();
    QString m_currentCfi;
    QVariantMap m_meta;
    QVariantList m_tableOfContents;
    QString m_restoreTo;
public:
    explicit QEpubRenderer(QWidget *parent = 0);
    virtual ~QEpubRenderer();
    void SetEpubData(QByteArray data);
    enum Error
    {
        BAD_EPUB_CONTENT
    };
    int pageCount() const;
    int currentPageIndex() const;
    QPixmap currentPageImage() const;
    bool rendering() const;
    float zoomMultiplier() const;
    bool invertedColors() const;
    QVariantMap meta() const;
    QVariantList tableOfContents() const;

    QString currentCfi() const;
    void RestoreWhenReady(QString cfi);

    QVariantMap GetCFIToPages(QStringList cfiList);
signals:
    void setFileserverData(QByteArray data, QString prefix);
    void setFileserverFile(QByteArray data, QString name);
    void error(Error);
    void pageCountChanged(int pageCount);
    void currentPageIndexChanged(int currentPageIndex);
    void currentPageImageChanged(QPixmap currentPageImage);
    void renderingChanged(bool rendering);
    void zoomMultiplierChanged(float zoomMultiplier);
    void invertedColorsChanged(bool invertedColors);
    void metaChanged(QVariantMap meta);
    void tableOfContentsChanged(QVariantList tableOfContents);
    void currentCfiChanged(QString currentCfi);
    void readyToView();
    void pageRestored();
private slots:
    void onServerReadyLoaded();
    void resizeEnd();
    void listeningSlot(int port);


public slots:
    void pageDown();
    void pageUp();
    void toBegin();
    void toEnd();
    void setCurrentPageIndex(int currentPageIndex);
    void gotoCFI(QString cfi);
    void setZoomMultiplier(float zoomMultiplier);
    void setInvertedColors(bool invertedColors);
    void clear();

    void logTest();
    void bookLoadedToViewer();
    void paginationStarted();
    void paginationFinished(int firstPage, int lastPage);
    void locationChanged(QString location);
    void metaReady(QVariantMap meta);
    void tocReady(QVariantList toc);
    void setCurrentCfi(QString currentCfi);


};



#endif // QEPUBRENDERER_H
