#include "QEpubRenderer.h"
#include "ZipHTTPServer.h"
#include <QDebug>
#include <QBuffer>
#include <QTimer>
#include <QResizeEvent>
#include <QLabel>
#include <QWebView>
#include <QWebFrame>
#include <QWebSettings>
#include <QGuiApplication>
#include "WebView.h"
QEpubRenderer::QEpubRenderer(QWidget * parent):QWidget(parent)
{
    m_invertedColors = false;
    m_currentPageIndex = 1;
    m_pageCount = 0;
    m_zoomMultiplier = 1.0;
    m_rendering = false;

    m_thread = new QThread(this);
    m_thread->start();
    m_fileServer = new ZipHTTPServer(nullptr);
    m_fileServer->moveToThread(m_thread);

    connect(this, &QEpubRenderer::setFileserverData, m_fileServer,
            &ZipHTTPServer::setBinaryZipSource, Qt::QueuedConnection);
    connect(this, &QEpubRenderer::setFileserverFile, m_fileServer,
            &ZipHTTPServer::setBinaryFile, Qt::QueuedConnection);

    connect(m_fileServer, &ZipHTTPServer::error, [](ZipHTTPServer::Error err){qDebug() << "m_fileServer error" << err;});
    connect(m_fileServer, &ZipHTTPServer::readyToServe, this, &QEpubRenderer::onServerReadyLoaded);
    connect(m_fileServer, &ZipHTTPServer::listening, this, &QEpubRenderer::listeningSlot);

    m_fileServer->StartServer();
    QThread::currentThread()->setObjectName("epub_renderer");

    m_resizeTimer = new QTimer(this);
    m_resizeTimer->setSingleShot(true);
    connect(m_resizeTimer, &QTimer::timeout, this,   &QEpubRenderer::resizeEnd);

//    QWebSettings::globalSettings()->setAttribute(QWebSettings::DeveloperExtrasEnabled, true);

    m_renderView = 0;
    m_renderView = new WebView(this);
    m_renderView->show();
    m_renderView->setWindowTitle("renderer");
    m_renderView->resize(size());
    connect(m_renderView->page()->mainFrame(), &QWebFrame::javaScriptWindowObjectCleared, [=](){m_renderView->page()->mainFrame()->addToJavaScriptWindowObject("app", this);});

    m_displayView = new WebView(this);
    m_displayView->setWindowTitle("viewer");
    m_displayView->show();
    m_displayView->resize(size());
    connect(m_displayView->page()->mainFrame(), &QWebFrame::javaScriptWindowObjectCleared, [=](){m_displayView->page()->mainFrame()->addToJavaScriptWindowObject("app", this);});

    m_imgLabel = new QLabel(this);
    m_imgLabel->show();
}

void QEpubRenderer::resizeEvent(QResizeEvent *event)
{
    m_imgLabel->resize(event->size());
    m_imgLabel->setPixmap(m_currentPageImage.scaled(event->size()*m_currentPageImage.devicePixelRatio(), Qt::IgnoreAspectRatio,Qt::SmoothTransformation));

    m_resizeTimer->start(500);
}


QEpubRenderer::~QEpubRenderer()
{
    m_fileServer->Stop();

    m_thread->quit();
    if(m_thread->wait(1000))
        qDebug() << "Epub thread stopped";
    else
        qDebug() << "Epub thread failed to stop";
    delete m_fileServer;
}

void QEpubRenderer::SetEpubData(QByteArray data)
{
    emit setFileserverData(data, "/html/book");
//    emit setFileserverFile(data, "/html/book.epub");
}

int QEpubRenderer::pageCount() const
{
    return m_pageCount;
}

int QEpubRenderer::currentPageIndex() const
{
    return m_currentPageIndex;
}

QPixmap QEpubRenderer::currentPageImage() const
{
    return m_currentPageImage;
}

void QEpubRenderer::onServerReadyLoaded()
{
    m_rendering =true;
    emit renderingChanged(true);
    m_displayView->setUrl(url("html/viewer.html"));
    m_displayView->setZoomFactor(m_zoomMultiplier);

    m_renderView->setUrl(url("html/renderer.html"));
    m_renderView->setZoomFactor(m_zoomMultiplier);
}

void QEpubRenderer::resizeEnd()
{
    m_resizeTimer->stop();
    m_displayView->resize(size());
    m_renderView->resize(size());

    m_renderView->setUrl(url("html/renderer.html"));
    m_renderView->setZoomFactor(m_zoomMultiplier);
    makeShot();
}


void QEpubRenderer::bookLoadedToViewer()
{
     QTimer::singleShot(100, this, [=](){    emit readyToView();});
}

void QEpubRenderer::paginationStarted()
{
    m_rendering = true;
    emit renderingChanged(true);
//    qDebug() << "paginationStarted";
}

void QEpubRenderer::paginationFinished(int firstPage, int lastPage)
{
    Q_UNUSED(firstPage)
    m_rendering = false;
    emit renderingChanged(false);

    if(m_restoreTo != "")
    {
        emit pageRestored();
        gotoCFI(m_restoreTo);
        m_restoreTo = "";
        m_currentCfi = m_restoreTo;
    }

    m_pageCount = lastPage;
    emit pageCountChanged(m_pageCount);
//    qDebug() << "paginationFinished" << firstPage << lastPage;

    QVariant r = m_renderView->page()->mainFrame()->evaluateJavaScript(QString("pageFromCfi('%1')").arg(m_currentCfi));
    auto index = r.toInt();
    if(index != -1)
    {
        m_currentPageIndex = index;
        emit currentPageIndexChanged(index);
    }
}


bool  QEpubRenderer::rendering() const
{
    return m_rendering;
}

void QEpubRenderer::locationChanged(QString location)
{
    if(!m_renderView) return;
    m_currentCfi = location;
    emit currentCfiChanged(m_currentCfi);
    QVariant r = m_renderView->page()->mainFrame()->evaluateJavaScript(QString("pageFromCfi('%1')").arg(m_currentCfi));
    auto index = r.toInt();
    if(index != -1)
    {
        m_currentPageIndex = index;
        emit currentPageIndexChanged(index);
    }
    makeShot();
}

void QEpubRenderer::metaReady(QVariantMap meta)
{
    m_meta =  meta;
    emit metaChanged(m_meta);
//    qDebug() << "meta ready";
}

void QEpubRenderer::tocReady(QVariantList toc)
{
    m_tableOfContents =  toc;
    emit tableOfContentsChanged(m_tableOfContents);
//     qDebug() << "toc ready";
}

void QEpubRenderer::setCurrentCfi(QString currentCfi)
{
    if (m_currentCfi == currentCfi)
        return;
    gotoCFI(currentCfi);
    m_currentCfi = currentCfi;
    emit currentCfiChanged(currentCfi);
}

QString QEpubRenderer::url(QString p)
{
    auto r =  QString("http://localhost:%1/%2").arg(m_port).arg(p);
    //    qDebug() << r;
    return r;
}

void QEpubRenderer::makeShot()
{
    //    return;
    QTimer::singleShot(100, this, [=](){
        QPixmap pixmap(m_displayView->size()*qApp->devicePixelRatio());
        pixmap.setDevicePixelRatio(qApp->devicePixelRatio());
        m_displayView->render(&pixmap);
//        pixmap.save("/Users/Tucher/1.png");
        m_currentPageImage = pixmap;
        if(m_invertedColors)
        {
            auto i = m_currentPageImage.toImage();
            i.invertPixels(QImage::InvertRgb);
            m_currentPageImage = QPixmap::fromImage(i);
        }
        emit currentPageImageChanged(m_currentPageImage);
        m_imgLabel->setScaledContents(false);
        m_imgLabel->setPixmap(m_currentPageImage);

    });
}


void QEpubRenderer::listeningSlot(int port)
{
    m_port = port;
}

float QEpubRenderer::zoomMultiplier() const
{
    return m_zoomMultiplier;
}

bool QEpubRenderer::invertedColors() const
{
    return m_invertedColors;
}

QVariantMap QEpubRenderer::meta() const
{
    return m_meta;
}

QVariantList QEpubRenderer::tableOfContents() const
{
    return m_tableOfContents;
}

QString QEpubRenderer::currentCfi() const
{
    return m_currentCfi;
}

void QEpubRenderer::RestoreWhenReady(QString cfi)
{
    m_restoreTo = cfi;
    if(!m_rendering && m_restoreTo != "")
    {
        emit pageRestored();
        gotoCFI(m_restoreTo);
        m_restoreTo = "";
    }
}

QVariantMap QEpubRenderer::GetCFIToPages(QStringList cfiList)
{
    QVariantMap res;
    if (m_rendering) return res;

    foreach(auto s, cfiList)
        res[s] = m_renderView->page()->mainFrame()->evaluateJavaScript(QString("pageFromCfi('%1')").arg(s));
    return res;

}

void QEpubRenderer::pageDown()
{

    m_displayView->page()->mainFrame()->evaluateJavaScript("nextPage();");
    makeShot();
}

void QEpubRenderer::pageUp()
{
    m_displayView->page()->mainFrame()->evaluateJavaScript("prevPage();");
    makeShot();
}

void QEpubRenderer::toBegin()
{
    m_displayView->page()->mainFrame()->evaluateJavaScript("firstPage();");
    makeShot();
}

void QEpubRenderer::toEnd()
{
    m_displayView->page()->mainFrame()->evaluateJavaScript("lastPage();");
    makeShot();
}

void QEpubRenderer::setCurrentPageIndex(int currentPageIndex)
{
    if(currentPageIndex == m_currentPageIndex) return;
    if(!m_renderView) return;
    QVariant r = m_renderView->page()->mainFrame()->evaluateJavaScript(QString("cfiFromPage('%1')").arg(currentPageIndex));
    auto cfi = r.toString();
    if(cfi != "")
    {
        m_displayView->page()->mainFrame()->evaluateJavaScript(QString("goToCfi('%1')").arg(cfi));
    }
    makeShot();
}

void QEpubRenderer::gotoCFI(QString cfi)
{
    if(cfi != "")
    {
         QTimer::singleShot(100, this, [=](){
             m_displayView->page()->mainFrame()->evaluateJavaScript(QString("goToCfi('%1')").arg(cfi));
         });
    }
}

void QEpubRenderer::setZoomMultiplier(float zoomMultiplier)
{
    if (m_zoomMultiplier == zoomMultiplier) return;
    m_displayView->setZoomFactor(zoomMultiplier);
    m_zoomMultiplier = m_displayView->zoomFactor();

    if(m_renderView)m_renderView->setZoomFactor(m_zoomMultiplier);

    emit zoomMultiplierChanged(m_zoomMultiplier);

    m_renderView->setUrl(url("html/renderer.html"));
    m_renderView->setZoomFactor(m_zoomMultiplier);
    makeShot();
}

void QEpubRenderer::setInvertedColors(bool invertedColors)
{
    makeShot();
    if (m_invertedColors == invertedColors)
        return;

    if(invertedColors)
    {
        auto i = m_currentPageImage.toImage();
        i.invertPixels(QImage::InvertRgb);
        m_currentPageImage = QPixmap::fromImage(i);
    }

    m_invertedColors = invertedColors;
    emit invertedColorsChanged(invertedColors);


}

void QEpubRenderer::clear()
{
    m_currentPageImage = QPixmap(size());
    m_currentPageImage.fill(Qt::transparent);
    m_imgLabel->setPixmap(m_currentPageImage);
    emit currentPageImageChanged(m_currentPageImage);
    emit pageCountChanged(0);
    emit currentPageIndexChanged(0);

    emit setFileserverData("", "");
//    emit setFileserverFile("", "");

    m_displayView->setUrl(url("html/viewer.html"));
    m_displayView->setZoomFactor(m_zoomMultiplier);

    m_renderView->setUrl(url("html/renderer.html"));
    m_renderView->setZoomFactor(m_zoomMultiplier);
    makeShot();
}

void QEpubRenderer::logTest()
{
    //qDebug() << "Yo";

}
