#ifndef WEBPAGE_H
#define WEBPAGE_H
#include <QWebEnginePage>
#include <QWebEngineProfile>

#include <QVariantMap>

class WebPage : public QWebEnginePage {
    Q_OBJECT
    void javaScriptConsoleMessage(JavaScriptConsoleMessageLevel level, const QString &message, int lineNumber, const QString &sourceID);

public:
    WebPage(QWebEngineProfile *profile, QObject*parent=0);

signals:
    void jsOutput(QVariantMap data);
};


#endif // WEBPAGE_H
