#ifndef ZIPHTTPSERVER_H
#define ZIPHTTPSERVER_H
#include <QObject>
#include <QMap>

#include "thirdparty/qhttp/src/qhttpserver.hpp"


class ZipHTTPServer: public QObject
{
    Q_OBJECT
    QMap<QString, QByteArray> m_files;

    QList<char*> m_pointers;

    void clear();

    qhttp::server::QHttpServer * m_server;
    void initMimes();
    int m_port;
    QString m_prefix;
public:
    ZipHTTPServer(QObject * parent=0);
    virtual ~ZipHTTPServer();
    void StartServer();
    enum Error {
        NO,
        ZIP_READING,
        LISTENING,
    };
    void Stop();
signals:
    void readyToServe();
    void initSignal();
    void error(Error);
    void listening(int);
    void stopped();
    void deinitSignal();
public slots:
    void setBinaryZipSource(QByteArray data, QString prefix);
    void setBinaryFile(QByteArray data, QString name);
    QByteArray File(QString);
    QStringList FileNames();
private slots:
    void init();
    void deinit();
};

#endif // ZIPHTTPSERVER_H
