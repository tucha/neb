#include "ZipHTTPServer.h"
#include "thirdparty/libzippp/src/libzippp.h"
//#include <QDebug>
#include <QTimer>
#include "thirdparty/qhttp/src/qhttpserverconnection.hpp"
#include "thirdparty/qhttp/src/qhttpserverrequest.hpp"
#include "thirdparty/qhttp/src/qhttpserverresponse.hpp"
#include <QDateTime>
#include <QDir>
#include <QEventLoop>
namespace {
using namespace qhttp::server;
}

void ZipHTTPServer::clear()
{
    for (int i = 0; i < m_pointers.size(); i ++)
        delete []m_pointers[i];

    m_files.clear();
    m_pointers.clear();
}

void ZipHTTPServer::init()
{
    m_server = new QHttpServer (0);
    while(!m_server->listen(QString("%1").arg(++m_port), [&](QHttpRequest* req, QHttpResponse* res){
        req->onEnd([this, req, res]() {
            if(req->method() == qhttp::EHTTP_GET)
            {
                auto url = req->url().toString();
//                   qDebug() << "requested: " << url;
                   auto ext = url.split(".").last().toLower();
                   if(ext == "css")
                       res->addHeader("Content-Type", "text/css");
                   if(ext == "js")
                       res->addHeader("Content-Type", "text/javascript");
                   if(ext == "html" || ext == "xhtml")
                       res->addHeader("Content-Type", "text/html");
                if(m_files.contains(url))
                {
                    auto message = m_files[url];
                    res->setStatusCode(qhttp::ESTATUS_OK);
                    res->addHeaderValue("content-length", message.size());

                    res->end(message);
                    return;
                } else if(QFile(":"+url).exists()){
                   QFile f(":"+url);
                   f.open(QIODevice::ReadOnly);
                   auto message = f.readAll();
                   res->setStatusCode(qhttp::ESTATUS_OK);
                   res->addHeaderValue("content-length", message.size());
                   res->end(message);
                   return;
                }
                res->setStatusCode(qhttp::ESTATUS_NOT_FOUND);
            }

            res->end();
        });
    }));
    if ( !m_server->isListening() ) {
        emit error(LISTENING);
    } else {
//        qDebug() << m_port;
        emit listening(m_port);
    }
}

void ZipHTTPServer::deinit()
{
    m_server->stopListening();
    delete m_server;
    qDebug() << "epub server stoppped";
    emit stopped();
}

ZipHTTPServer::ZipHTTPServer(QObject *parent):QObject(parent)
{
    m_port = 10021;
}

ZipHTTPServer::~ZipHTTPServer()
{
    clear();    
}

void ZipHTTPServer::StartServer()
{
    connect(this, &ZipHTTPServer::initSignal, this, &ZipHTTPServer::init, Qt::QueuedConnection);
    connect(this, &ZipHTTPServer::deinitSignal, this, &ZipHTTPServer::deinit, Qt::QueuedConnection);
    emit initSignal();
}

void ZipHTTPServer::Stop()
{
    QEventLoop l;
    connect(this, &ZipHTTPServer::stopped, &l, &QEventLoop::quit, Qt::QueuedConnection);
    emit deinitSignal();
    l.exec();
}

void ZipHTTPServer::setBinaryZipSource(QByteArray data, QString prefix)
{
    m_prefix = prefix;
    clear();

    using namespace libzippp;

    ZipArchive zf;
    if (zf.open_memory(data.data(), (quint64)data.size()))
    {
        std::vector<ZipEntry> entries = zf.getEntries();
        std::vector<ZipEntry>::iterator it;
        for(it=entries.begin() ; it!=entries.end(); ++it) {
            ZipEntry entry = *it;
            QString name = m_prefix + "/" + QString::fromStdString(entry.getName());
            qint32 size = (qint32)entry.getSize();

            char* binaryData = (char*)entry.readAsBinary();
            m_files[name] = QByteArray::fromRawData(binaryData, size);
            m_pointers.append(binaryData);
        }

        zf.close();
        emit readyToServe();
    }
    else
    {
        emit error(ZIP_READING);
    }
}

void ZipHTTPServer::setBinaryFile(QByteArray data, QString name)
{
    m_files[name] = data;
    emit readyToServe();
}

QByteArray ZipHTTPServer::File(QString name)
{
    if(name.isEmpty())
        return "";
    if (name[0] != '/')
        name.prepend("/");
    if (m_files.contains(name))
        return m_files[name];
    return "";
}

QStringList ZipHTTPServer::FileNames()
{
    return m_files.keys();
}

