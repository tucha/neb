#include "historyrecord.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QAction>
#include <QMouseEvent>
#include <QApplication>
#define ICON_SIZE 70
#define ICON_MARGIN 10
#define RECORD_MARGIN 3

HistoryRecord::HistoryRecord(QWidget *parent) :
    QWidget(parent),

    m_horizontalLayout( new QHBoxLayout() ),
    m_verticalLayout( new QVBoxLayout() ),
    m_icon( new QLabel( this ) ),
    m_author( new QLabel( this ) ),
    m_title( new QLabel( this ) ),

    m_requestIcon( NULL ),
    m_requestAuthor( NULL ),
    m_requestTitle( NULL )
{
    CreateGui();
    ConnectSlots();
    Translate();
    SetSelected( false );
}

HistoryRecord::~HistoryRecord()
{
    Clear();
}

void HistoryRecord::Load(RequestPage* baseRequest)
{
    if( IsActual( baseRequest ) ) return;

    Clear();
    //    baseRequest->GetDocument().



    m_requestIcon = baseRequest->NewImageFixed( TYPE_THUMBNAILS, GEOMETRY_MAX, ICON_SIZE, FORMAT_JPEG );
    m_requestAuthor = baseRequest->NewDescription( "100", "a" );
    m_requestTitle = baseRequest->NewDescription( "245", "a", PRIORITY_HIGHT );

    connect(m_requestIcon,SIGNAL(SignalFinished(ErrorCode,QImage)),this,SLOT(IconFinished(ErrorCode,QImage)));
    connect(m_requestAuthor,SIGNAL(SignalFinished(ErrorCode,QString)),this,SLOT(AuthorFinished(ErrorCode,QString)));
    connect(m_requestTitle,SIGNAL(SignalFinished(ErrorCode,QString)),this,SLOT(TitleFinished(ErrorCode,QString)));


    m_requestAuthor->Send();
    m_requestTitle->Send();

    auto typeRequest = baseRequest->NewDocumentFormat(PRIORITY_HIGHT);
    connect(typeRequest, &RequestDocumentFormat::SignalFinished, this, &HistoryRecord::TypeRequestFinished);
    typeRequest->Send();
}


void HistoryRecord::TypeRequestFinished(ErrorCode err, Document::Type type)
{
    if(sender())sender()->deleteLater();
    if(err != ErrorCode::NoError) {
//        qDebug() << "Cannot get document type";
        type = Document::UNKNOWN;
    }
    switch(type)
    {
    case Document::EPUB:
    {
        auto p = QPixmap(":/icons/epub.png").scaled(QSize(ICON_SIZE, ICON_SIZE)*qApp->devicePixelRatio(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
        p.setDevicePixelRatio(qApp->devicePixelRatio());
        m_icon->setPixmap( p);
    }
        break;
    case Document::PDF:
        m_requestIcon->Send();
        break;
    case Document::UNKNOWN:
//        qDebug() << "Unknown document type";
        ;
    }
}

void HistoryRecord::Clear()
{
    DeleteLaterAndNull( m_requestIcon );
    DeleteLaterAndNull( m_requestAuthor );
    DeleteLaterAndNull( m_requestTitle );

    m_icon->clear();
    m_author->clear();
    m_title->clear();
}

bool HistoryRecord::IsEmpty() const
{
    bool result = m_requestIcon == NULL;

    if( result )
    {
        DebugAssert( m_requestIcon == NULL );
        DebugAssert( m_requestAuthor == NULL );
        DebugAssert( m_requestTitle == NULL );
    }
    else
    {
        DebugAssert( m_requestIcon != NULL );
        DebugAssert( m_requestAuthor != NULL );
        DebugAssert( m_requestTitle != NULL );
    }

    return result;
}

void HistoryRecord::SetSelected(bool value)
{
    if( value ) this->setBackgroundRole( QPalette::Highlight );
    else this->setBackgroundRole( QPalette::Light );
}

bool HistoryRecord::GetSelected() const
{
    return this->backgroundRole() == QPalette::Highlight;
}

Document HistoryRecord::GetDocument() const
{
    if( IsEmpty() ) return Document();
    return m_requestIcon->GetDocument();
}

void HistoryRecord::mousePressEvent(QMouseEvent* event)
{
    UNUSED( event );
    if( IsEmpty() ) return;
    emit MouseClicked( this );
}

void HistoryRecord::mouseDoubleClickEvent(QMouseEvent* event)
{
    UNUSED( event );
    if( IsEmpty() ) return;
    emit OpenDocument( GetDocument() );
}

void HistoryRecord::IconFinished(ErrorCode error, QImage image)
{
    if( sender() == NULL ) return;
    if( m_requestIcon != sender() ) return;

    if( ApiBase::IsDownloadingError( error ) )
    {
        return;
    }

    if( ApiBase::IsContentForbiddenError( error ) )
    {
        m_icon->setText( tr( "Page %1\nForbidden" ).arg( SENDER_PAGE->PageIndex() ) );
        return;
    }

    if( error != API_NO_ERROR )
    {
        m_requestIcon->Send();
        return;
    }

    m_icon->setPixmap( QPixmap::fromImage( image ) );
}

void HistoryRecord::AuthorFinished(ErrorCode error, QString author)
{
    if( sender() == NULL ) return;
    if( m_requestAuthor != sender() ) return;

    if( ApiBase::IsDownloadingError( error ) )
    {
        return;
    }

    if( error != API_NO_ERROR )
    {
        m_requestAuthor->Send();
        return;
    }

    m_author->setText( tr( "Author: %1" ).arg( author ) );
}

void HistoryRecord::TitleFinished(ErrorCode error, QString title)
{
    if( sender() == NULL ) return;
    if( m_requestTitle != sender() ) return;

    if( ApiBase::IsDownloadingError( error ) )
    {
        m_title->setText( tr( "DownloadingError" ) );
        emit DownloadingError( Document( SENDER->GetDocument() ) );
        return;
    }

    if( error != API_NO_ERROR )
    {
        m_requestTitle->Send();
        return;
    }

    m_title->setText( tr( "Title: %1" ).arg( title ) );
}

void HistoryRecord::CreateGui()
{
    this->setLayout( m_horizontalLayout );
    this->setAutoFillBackground( true );

    m_horizontalLayout->addWidget( m_icon );
    m_horizontalLayout->addLayout( m_verticalLayout );
    m_verticalLayout->addWidget( m_title );
    m_verticalLayout->addWidget( m_author );

    m_horizontalLayout->setStretch( 0, 0 );
    m_horizontalLayout->setStretch( 1, 1 );
    m_horizontalLayout->setContentsMargins( RECORD_MARGIN,RECORD_MARGIN,RECORD_MARGIN,RECORD_MARGIN );

    m_verticalLayout->setStretch( 0, 0 );
    m_verticalLayout->setStretch( 1, 1 );

    m_author->setAlignment( Qt::AlignTop );
    m_title->setAlignment( Qt::AlignTop );

    m_title->setWordWrap( true );
    m_author->setWordWrap( true );

    m_icon->setFrameStyle( QFrame::Box );
    m_icon->setFixedSize( ICON_SIZE + ICON_MARGIN, ICON_SIZE + ICON_MARGIN );
    m_icon->setAlignment( Qt::AlignCenter );
    m_icon->setAutoFillBackground( true );
    m_icon->setBackgroundRole( QPalette::Light );
    m_icon->setWordWrap( true );
}

void HistoryRecord::ConnectSlots()
{

}

void HistoryRecord::Translate()
{

}

bool HistoryRecord::IsActual(RequestPage* baseRequest) const
{
    DebugAssert( baseRequest != NULL );

    if( IsEmpty() ) return false;
    if( m_requestIcon->GetDocument() != baseRequest->GetDocument() ) return false;
    if( m_requestIcon->PageIndex() != baseRequest->PageIndex() ) return false;
    return true;
}
