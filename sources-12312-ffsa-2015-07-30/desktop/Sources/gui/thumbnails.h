#ifndef THUMBNAILS_H
#define THUMBNAILS_H

#include <QWidget>
#include <QSet>
#include <QPoint>

class FlowLayout;
class Request;
class Thumbnail;
class BookmarksManager;
class CommentsManager;
class QScrollArea;
class QVBoxLayout;
class QLabel;

#include <defines.h>
#include <pageset.h>

class Thumbnails : public QWidget
{
	Q_OBJECT
private:
	DISABLE_COPY( Thumbnails )

public:
	explicit Thumbnails(QWidget *parent = 0);
	~Thumbnails();

	void Load( int pagesCount, Request* baseRequest );
	void Clear(void);

	PageSet SelectedPages(void)const;

public slots:
	void UpdateBookmarks( BookmarksManager* bookmarks );
	void UpdateComments( CommentsManager* comments );

protected:
	virtual void resizeEvent(QResizeEvent* event);
	virtual void showEvent(QShowEvent* event);

public slots:
	void Select( int pageIndex );

signals:
	void GotoPage( int pageIndex );

private slots:
	void SlotGotoPage( int pageIndex );
	void AddToSelection( int pageIndex );
	void AddRangeToSelection( int pageIndex );
	void Loaded( int pageIndex );

private:
	// Private methods
	void CreateGui(void);
	Thumbnail* NewThumbnail(void);
	void HighlightSelected(void);
	bool IsSelected( int index )const;
	void EnsureSelectedIsVisible(void);
	void LoadSomePages(void);

	int Count(void)const;
	Thumbnail* Get( int index )const;
	void Add( Thumbnail* thumbnail );

	// Gui items
	FlowLayout*		m_flowLayout;
	QWidget*		m_widget;
	QScrollArea*	m_scrollArea;
	QVBoxLayout*	m_boxLayout;
	QLabel*			m_label;

	int m_selectionRangeStart;
	int m_selectionRangeEnd;
	QSet<int> m_selection;
	QPoint m_selectedPosition;

	int m_pagesLoaded;
	int m_pagesCount;
	Request* m_baseRequest;
};

#endif // THUMBNAILS_H
