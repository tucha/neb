#include "bookmarkscontrol.h"
#include "flowlayout.h"
#include "thumbnail.h"

#include <QObjectList>
#include <QList>
#include <QScrollBar>

#include <algorithm>

#include <bookmarksmanager.h>
#include <commentsmanager.h>
#include <QContextMenuEvent>
BookmarksControl::BookmarksControl(QWidget *parent) :
	QScrollArea(parent),
	m_flowLayout( NULL ),
	m_widget( NULL ),
	m_selectionRangeStart( -1 ),
	m_selectionRangeEnd( -1 ),
	m_selection(),
	m_selectedPosition(),
	m_baseRequest( NULL )
{
	CreateGui();
}

BookmarksControl::~BookmarksControl()
{
	Clear();
}

void BookmarksControl::Load( Request* baseRequest )
{
	Clear();
	m_baseRequest = baseRequest;
}

void BookmarksControl::Clear()
{
	m_selectionRangeStart = -1;
	m_selectionRangeEnd = -1;
	m_selection.clear();
	m_selectedPosition = QPoint();
	for( int i = 0; i < Count(); ++i )
	{
		Thumbnail* thumbnail = Get( i );
		thumbnail->Clear();
	}
	m_baseRequest = NULL;
}

PageSet BookmarksControl::SelectedPages() const
{
	PageSet result;

	for( int i = 0; i < Count(); ++i )
	{
		if( IsSelected( i ) ) result.Add( i + 1 );
	}

	return result;
}

void BookmarksControl::UpdateBookmarks(BookmarksManager* bookmarks)
{

    while(m_flowLayout->count()){
        auto w = Get(0);
        m_flowLayout->removeWidget(w);
        w->deleteLater();
    }
	if( m_baseRequest == NULL ) return;

    QList<QPair<int, QString> > pages = bookmarks->ActualPages();
//    qDebug() << "pages count" << pages.count();
	while( Count() < pages.count() ) Add( NewThumbnail() );

    OptimizeThumbnails( pages );
    QStringList cfiList;
	for( int i = 0; i < Count(); ++i )
	{

		Thumbnail* thumbnail = Get( i );

        if( i >= pages.count() )
        {
            thumbnail->Clear();
        }
        else
        {
            if (pages[ i ].second != "")cfiList << pages[ i ].second;
			thumbnail->Load( pages[ i ], m_baseRequest );
			thumbnail->SetBookmark( true );
        }
	}

	if( m_selectionRangeStart != -1 ) Select( m_selectionRangeStart );
    emit CfiUpdated(cfiList);
}

void BookmarksControl::UpdateComments(CommentsManager* comments)
{
	if( m_baseRequest == NULL ) return;

    QList<QPair<int, QString> > pages = comments->ActualPages();
	while( Count() < pages.count() ) Add( NewThumbnail() );

	OptimizeThumbnails( pages );
    QStringList cfiList;
	for( int i = 0; i < Count(); ++i )
	{

		Thumbnail* thumbnail = Get( i );
		if( i >= pages.count() )
		{
			thumbnail->Clear();
		}
		else
		{
            if (pages[ i ].second != "")cfiList << pages[ i ].second;
			thumbnail->Load( pages[ i ], m_baseRequest );
            thumbnail->SetComment( comments->Get( pages[ i ].first,  pages[ i ].second) );
            //TODO
		}
	}

	if( m_selectionRangeStart != -1 ) Select( m_selectionRangeStart );

    emit CfiUpdated(cfiList);
}

void BookmarksControl::resizeEvent(QResizeEvent* event)
{
	QScrollArea::resizeEvent( event );
	EnsureSelectedIsVisible();
}

void BookmarksControl::showEvent(QShowEvent* event)
{
	UNUSED( event );
    m_flowLayout->DoLayout();
}



void BookmarksControl::Select(int pageIndex)
{
	DebugAssert( pageIndex >=0 );
	if( pageIndex < 0 ) return;

	m_selection.clear();
	m_selection.insert( pageIndex );

	m_selectionRangeStart = pageIndex;
	m_selectionRangeEnd = pageIndex;

	m_selectedPosition = QPoint();

	HighlightSelected();
    EnsureSelectedIsVisible();
}

void BookmarksControl::UpdateCfiToPage(QVariantMap cfiToPage)
{
    for(int i = 0; i < Count(); i ++)
    {
        auto t = Get(i);
        if(cfiToPage.contains(t->ResPath()))
            t->SetPageIndex(cfiToPage[t->ResPath()].toInt());
    }
}

void BookmarksControl::RequestCFi()
{
    QStringList cfiList;
    for( int i = 0; i < Count(); ++i )
    {
        if (Get( i )->ResPath() != "")cfiList << Get( i )->ResPath();
    }
    emit CfiUpdated(cfiList);
}

void BookmarksControl::AddToSelection(int pageIndex)
{
	if( IsSelected( pageIndex ) ) m_selection.remove( pageIndex );
	else m_selection.insert( pageIndex );

	int start = m_selectionRangeStart;
	int end = m_selectionRangeEnd;
	if( start > end ) std::swap( start, end );
	if( start != end )
	{
		for( int i = start; i <= end; ++i )
		{
			if( i == pageIndex ) continue;
			m_selection.insert( i );
		}
	}

	m_selectionRangeStart = pageIndex;
	m_selectionRangeEnd = pageIndex;

	HighlightSelected();
}

void BookmarksControl::AddRangeToSelection(int pageIndex)
{
	m_selectionRangeEnd = pageIndex;
	HighlightSelected();
}

void BookmarksControl::Loaded(int pageIndex)
{
	UNUSED( pageIndex );
    EnsureSelectedIsVisible();
}

void BookmarksControl::RightClickSlot(Thumbnail *src, QPoint pos)
{
    m_lastRightClickedIndex.first = src->PageIndex();
    m_lastRightClickedIndex.second = src->ResPath();
   m_contextMenu->exec(pos);
}

void BookmarksControl::DeleteMe()
{
    emit DeleteBookmark(m_lastRightClickedIndex.first,m_lastRightClickedIndex.second );
}


void BookmarksControl::CreateGui()
{
	m_flowLayout = new FlowLayout();
	m_widget = new QWidget();

	m_widget->setLayout( m_flowLayout );
	this->setWidgetResizable( true );
	this->setWidget( m_widget );
	this->setFrameStyle( QFrame::NoFrame );
	this->setAutoFillBackground( true );
	this->setBackgroundRole( QPalette::Light );
	this->verticalScrollBar()->setContextMenuPolicy( Qt::NoContextMenu );
	this->horizontalScrollBar()->setContextMenuPolicy( Qt::NoContextMenu );


    m_contextMenu = new QMenu(this);
    m_contextMenu->addAction(tr("Delete"),this, SLOT(DeleteMe()));
}

Thumbnail*BookmarksControl::NewThumbnail()
{
	Thumbnail* result = new Thumbnail();

    connect(result,SIGNAL(GotoPage(int, QString)),this,SIGNAL(GotoPage(int, QString)));
	connect(result,SIGNAL(AddRangeToSelection(int)),this,SLOT(AddRangeToSelection(int)));
	connect(result,SIGNAL(AddToSelection(int)),this,SLOT(AddToSelection(int)));
	connect(result,SIGNAL(Loaded(int)),this,SLOT(Loaded(int)));
    connect(result, &Thumbnail::RightClicked,this,&BookmarksControl::RightClickSlot);
	result->setAutoFillBackground( true );

	return result;
}

void BookmarksControl::HighlightSelected()
{
	for( int i = 0; i < Count(); ++i )
	{
		Thumbnail* thumbnail = Get( i );
		if( thumbnail->IsEmpty() ) break;

		if( IsSelected( thumbnail->PageIndex() - 1 ) )
		{
			thumbnail->setBackgroundRole( QPalette::Highlight );
		}
		else
		{
			thumbnail->setBackgroundRole( QPalette::Light );
		}
	}
}

bool BookmarksControl::IsSelected(int index) const
{
    if(index < 0) return false;
	if( m_selection.contains( index ) ) return true;

	int start = m_selectionRangeStart;
	int end = m_selectionRangeEnd;
	if( start > end ) std::swap( start, end );

	if( start != end )
	{
		if( index >= start && index <= end ) return true;
	}

	return false;
}

void BookmarksControl::EnsureSelectedIsVisible()
{
	if( m_selectionRangeStart == -1 ) return;

	DebugAssert( m_selectionRangeStart >= 0 );
	if( m_selectionRangeStart < 0 ) return;

	Thumbnail* selected = FindByPageIndex( m_selectionRangeStart );
	if( selected == NULL ) return;

	if( selected->IsLoaded() == false ) return;
	if( m_selectedPosition == selected->pos() ) return;

	m_selectedPosition = selected->pos();
	ensureWidgetVisible( selected );
}

Thumbnail*BookmarksControl::FindByPageIndex(int pageIndex) const
{
	for( int i = 0; i < Count(); ++i )
	{
		Thumbnail* thumbnail = Get( i );
		if( thumbnail->IsEmpty() ) break;

		if( thumbnail->PageIndex() == pageIndex ) return thumbnail;
	}
	return NULL;
}

QHash<int, int> BookmarksControl::CollectPageHash() const
{
	QHash<int,int> result;

	for( int i = 0; i < Count(); ++i )
	{
		Thumbnail* thumbnail = Get( i );
		if( thumbnail->IsEmpty() ) break;

		result.insert( thumbnail->PageIndex(), i );
	}
	return result;
}

void BookmarksControl::Swap(int index1, int index2)
{
	QLayoutItem* item1 = m_flowLayout->Swap( index1, NULL );
	QLayoutItem* item2 = m_flowLayout->Swap( index2, NULL );

	m_flowLayout->Swap( index1, item2 );
	m_flowLayout->Swap( index2, item1 );
}

void BookmarksControl::UpdatePageHash(QHash<int, int>& pageHash, int index1, int index2 ) const
{
	Thumbnail* thumbnail1 = Get( index1 );
	Thumbnail* thumbnail2 = Get( index2 );

	if( thumbnail1->IsEmpty() == false )
	{
		int pageIndex1 = thumbnail1->PageIndex();
		if( thumbnail2->IsEmpty() == false )
		{
			int pageIndex2 = thumbnail2->PageIndex();
			DebugAssert( pageHash.value( pageIndex1 ) == index2 );
			DebugAssert( pageHash.value( pageIndex2 ) == index1 );
			pageHash[ pageIndex1 ] = index1;
			pageHash[ pageIndex2 ] = index2;
		}
		else
		{
			DebugAssert( pageHash.value( pageIndex1 ) == index2 );
			pageHash[ pageIndex1 ] = index1;
		}
	}
	else
	{
		if( thumbnail2->IsEmpty() == false )
		{
			int pageIndex2 = thumbnail2->PageIndex();
			DebugAssert( pageHash.value( pageIndex2 ) == index1 );
			pageHash[ pageIndex2 ] = index2;
		}
		else
		{
			NotImplemented();
		}
	}

	DebugAssert( TestPageHash( pageHash ) );
}

bool BookmarksControl::TestPageHash(const QHash<int, int>& pageHash) const
{
	int counter = 0;
	for( int i = 0; i < Count(); ++i )
	{
		Thumbnail* thumbnail = Get( i );
		if( thumbnail->IsEmpty() ) continue;

		counter += 1;
		if( pageHash.value( thumbnail->PageIndex() ) != i ) return false;
	}
	if( counter != pageHash.count() ) return false;
	return true;
}

void BookmarksControl::OptimizeThumbnails( QList<QPair<int, QString> > needPages)
{
	QHash<int,int> pageHash = CollectPageHash();
	for( int i = 0; i < needPages.count(); ++i )
	{
        if(needPages[i].second != "") continue;
        int pageIndex = needPages[ i ].first;
		if( pageHash.contains( pageIndex ) == false ) continue;

		int thumbnailIndexHave = pageHash.value( pageIndex );
		if( thumbnailIndexHave == i ) continue;

		Swap( thumbnailIndexHave, i );
		UpdatePageHash( pageHash, thumbnailIndexHave, i );
	}
}

int BookmarksControl::Count() const
{
	return m_flowLayout->count();
}

Thumbnail*BookmarksControl::Get(int index) const
{
	return static_cast<Thumbnail*>( m_flowLayout->itemAt( index )->widget() );
}

void BookmarksControl::Add(Thumbnail* thumbnail)
{
	m_flowLayout->addWidget( thumbnail );

	thumbnail->move( -thumbnail->width(), 0 );
}
