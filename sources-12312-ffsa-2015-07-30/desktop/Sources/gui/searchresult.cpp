#include "searchresult.h"
#include "../miscellaneous/painter.h"
#include "thumbnail.h"

#include <QVBoxLayout>
#include <QLabel>
#include <QPixmap>
#include <QProgressBar>
#include <QStringList>

SearchResult::SearchResult(QWidget *parent) :
	QFrame(parent),
	m_requestCoordinates( NULL ),
	m_requestImage( NULL ),
	m_layout( new QVBoxLayout() ),
	m_label( new QLabel() ),
	m_pageIndex( new QLabel() ),
	m_progressBar( new QProgressBar() ),
	m_wordCoordinates()
{
	m_progressBar->hide();
	m_layout->addWidget( m_progressBar );
	m_layout->addWidget( m_label );
	m_layout->addWidget( m_pageIndex );

	m_layout->setAlignment( m_pageIndex, Qt::AlignCenter );

	this->setLayout( m_layout );
	this->setFrameStyle( QFrame::NoFrame );
	this->setLineWidth( 1 );
}

SearchResult::~SearchResult()
{
	Clear();
}

bool SearchResult::IsEmpty() const
{
	return m_requestCoordinates == NULL;
}

void SearchResult::Load(int pageIndex, Request* baseRequest, QString text)
{
	Clear();

	RequestPage* tmp = baseRequest->NewPage( pageIndex );

	QStringList words = QStringList() << text;
	m_requestCoordinates = tmp->NewWordCoordinates( words, PRIORITY_HIGHT );
	connect(m_requestCoordinates,SIGNAL(SignalFinished(ErrorCode,WordCoordinates)),this,SLOT(SlotFinishedCoordinates(ErrorCode,WordCoordinates)));
	m_requestCoordinates->Send();

	tmp->deleteLater();
}

void SearchResult::Clear()
{
	if( m_requestCoordinates != NULL )
	{
		m_requestCoordinates->deleteLater();
		m_requestCoordinates = NULL;
	}
	if( m_requestImage != NULL )
	{
		m_requestImage->deleteLater();
		m_requestImage = NULL;
	}
	m_wordCoordinates.clear();

	m_pageIndex->setText( "" );
	m_label->setFrameStyle( QFrame::NoFrame );
	m_label->setPixmap( QPixmap() );
	m_progressBar->hide();

	this->hide();
	this->resize( QSize() );
}

void SearchResult::mousePressEvent(QMouseEvent* event)
{
	UNUSED( event );

	if( m_requestImage == NULL ) return;

	emit GotoSearch( m_requestImage->PageIndex(), m_wordCoordinates );
}

void SearchResult::SlotFinishedImage(ErrorCode error, QImage image)
{
	if( IsEmpty() ) return;
	if( sender() != m_requestImage ) return;

	if( error != API_NO_ERROR )
	{
		m_requestImage->Send();
		return;
	}

	m_progressBar->hide();
	m_label->setFrameStyle( FRAME_STYLE );
	m_label->setPixmap( QPixmap::fromImage( Painter::Highlight( image, m_wordCoordinates ) ) );
	m_pageIndex->setText( QString::number( SENDER_PAGE->PageIndex() ) );

	this->show();
	this->resize( sizeHint() );

	emit Finished();
}

void SearchResult::SlotDownloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
	if( IsEmpty() ) return;
	if( sender() != m_requestImage ) return;

	m_progressBar->show();
	m_progressBar->setMaximum( bytesTotal );
	m_progressBar->setValue( bytesReceived );
	m_pageIndex->setText( QString::number( SENDER_PAGE->PageIndex() ) );
	this->show();
}

void SearchResult::SlotFinishedCoordinates(ErrorCode error, WordCoordinates wordCoordinates)
{
	if( IsEmpty() ) return;
	if( sender() != m_requestCoordinates ) return;

	if( error == QNetworkReply::ContentOperationNotPermittedError )
	{
		emit NothingWasFount();
		Clear();
		return;
	}

	if( error != API_NO_ERROR )
	{
		m_requestCoordinates->Send();
		return;
	}

	DebugAssert( wordCoordinates.count() == 1 );
	if( wordCoordinates.count() != 1 )
	{
		Debug::Instance()->Output( tr( "Unexpected service respose" ) );
		emit NothingWasFount();
		Clear();
		return;
	}

	if( wordCoordinates.values().first().count() == 0 )
	{
		emit NothingWasFount();
		Clear();
		return;
	}

	m_wordCoordinates = wordCoordinates;

	m_requestImage = m_requestCoordinates->NewImageFixed( TYPE_THUMBNAILS, GEOMETRY_MAX, THUMBNAIL_SIZE, FORMAT_JPEG );
	connect(m_requestImage,SIGNAL(SignalFinished(ErrorCode,QImage)),this,SLOT(SlotFinishedImage(ErrorCode,QImage)));
	connect(m_requestImage,SIGNAL(SignalDownloadProgress(qint64,qint64)),this,SLOT(SlotDownloadProgress(qint64,qint64)));
	m_requestImage->Send();
}
