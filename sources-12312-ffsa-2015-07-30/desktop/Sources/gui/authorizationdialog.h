#ifndef AUTHORIZATIONDIALOG_H
#define AUTHORIZATIONDIALOG_H

#include <QMainWindow>

class QVBoxLayout;
class QHBoxLayout;
class QLabel;
class QLineEdit;
class QPushButton;
class QAuthenticator;

class Config;

#include <common.h>

class AuthorizationDialog : public QMainWindow
{
	Q_OBJECT
public:
	explicit AuthorizationDialog(QWidget *parent = 0);

	void SetEmailEnabled( bool enabled );
	void SetMessage( QString message );
	void SetBase( AuthorizationBase* base );
	void SetEmail( QString email );
	void SetPassword( QString password );

signals:
	void AuthorizationCancelled();
	void UserAuthorized( User user );
    void AnonymTokenReady(QString str);
public slots:
	void OkButtonClicked();

protected:
	virtual void closeEvent( QCloseEvent* event );

private slots:
	void SlotFinished( ErrorCode error, AuthorizationResult result );
	void SlotAuthenticationRequired( QAuthenticator* authenticator );
    void anonymAccessSlot();
private:
	void CreateGui();
	void Translate();
	void ConnectSlots();

	QVBoxLayout*	m_verticalLayout;
	QHBoxLayout*	m_horizontalLayout;
	QLabel*			m_labelMessage;
	QLabel*			m_labelUser;
	QLineEdit*		m_lineEditUser;
	QLabel*			m_labelPassword;
	QLineEdit*		m_lineEditPassword;
	QPushButton*	m_okButton;
	QPushButton*	m_cancelButton;
    QPushButton*    m_anonymButton;
	QWidget*		m_centralWidget;

	AuthorizationBase*		m_base;
	AuthorizationRequest*	m_request;
};

#endif // AUTHORIZATIONDIALOG_H
