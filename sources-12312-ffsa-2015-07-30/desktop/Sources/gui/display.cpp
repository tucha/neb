#include "display.h"
#include "../miscellaneous/painter.h"

#include <QLabel>
#include <QScrollArea>
#include <QBoxLayout>
#include <QProgressBar>
#include <QScrollBar>
#include <QResizeEvent>


Display::Display(QWidget *parent) :
	QWidget(parent),

	m_guiScrollArea( NULL ),
	m_guiLabel( NULL ),
	m_guiLayout( NULL ),
	m_guiProgressBar( NULL ),
	m_guiMessage( NULL ),
	m_forbidden( false )
{
	CreateGuiElements();
	Clear();
}

Display::~Display()
{
	Clear();
}

void Display::SetForbiddenMessage(QString message)
{
	m_guiMessage->setText( message );
}

void Display::SetForbidden( bool value )
{
	m_forbidden = value;

	if( m_forbidden )
	{
		m_guiScrollArea->hide();
		m_guiMessage->show();
	}
	else
	{
		m_guiScrollArea->show();
		m_guiScrollArea->setFocus();
		m_guiMessage->hide();
	}
}

void Display::ShowText(QString text)
{
	SetForbiddenMessage( text );
	SetForbidden( true );
}

void Display::ShowPage()
{
	SetForbidden( false );
}

void Display::HidePage()
{
	ShowText( QString() );
}

void Display::ShowTextForbidden()
{
	ShowText( tr( "AccessForbidden" ) );
}

void Display::SetPixmap(QPixmap pixmap)
{
	m_guiMessage->hide();
	m_guiProgressBar->hide();
	m_guiScrollArea->show();
	m_guiScrollArea->setFocus();
    m_guiLabel->resize( pixmap.size()/pixmap.devicePixelRatio() );
	m_guiLabel->setPixmap( pixmap );

	if( pixmap.isNull() || m_forbidden )
	{
		m_guiScrollArea->hide();
		m_guiMessage->show();
	}
}

void Display::Clear()
{
	m_forbidden = false;
	m_guiMessage->hide();
	m_guiProgressBar->hide();
	m_guiScrollArea->show();
	m_guiScrollArea->setFocus();
    m_guiScrollArea->ensureVisible( 0, 0 );
    m_guiLabel->setPixmap( QPixmap() );
    m_guiLabel->resize( QSize() );
}

void Display::resizeEvent(QResizeEvent* event)
{
	emit Resized( event->size() );
}

void Display::SlotDownloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
	if( m_forbidden ) return;

	m_guiProgressBar->show();
	m_guiScrollArea->hide();
	m_guiMessage->hide();

	m_guiProgressBar->setMaximum( bytesTotal );
	m_guiProgressBar->setValue( bytesReceived );
}

void Display::CreateGuiElements()
{
	m_guiLabel = new QLabel();
	m_guiScrollArea = new QScrollArea();
	m_guiLayout = new QBoxLayout( QBoxLayout::LeftToRight );
	m_guiProgressBar = new QProgressBar();
	m_guiMessage = new QLabel();

	m_guiMessage->setAlignment( Qt::AlignCenter );
	m_guiMessage->setStyleSheet( "font: 18pt" );
	m_guiMessage->setWordWrap( true );

	m_guiScrollArea->verticalScrollBar()->setContextMenuPolicy( Qt::NoContextMenu );
	m_guiScrollArea->horizontalScrollBar()->setContextMenuPolicy( Qt::NoContextMenu );

	m_guiScrollArea->setAlignment( Qt::AlignHCenter | Qt::AlignVCenter );

	// NOTE: ELARII-19 defect
	this->setFocusProxy( m_guiScrollArea );

	m_guiScrollArea->setWidget( m_guiLabel );
	m_guiLayout->addWidget( m_guiScrollArea );
	m_guiLayout->addWidget( m_guiProgressBar );
	m_guiLayout->addWidget( m_guiMessage );

	m_guiProgressBar->setMaximumWidth( 400 );
	m_guiProgressBar->hide();

	m_guiLabel->setMargin( 0 );
	m_guiLayout->setMargin( 0 );
	m_guiScrollArea->setFrameShape(QFrame::NoFrame);

    this->setLayout( m_guiLayout );
}
