#ifndef CONTENTDISPLAY_H
#define CONTENTDISPLAY_H

#include <QObject>
#include <QWidget>
#include <QTreeWidget>
class ContentDisplay : public QTreeWidget
{
    Q_OBJECT
public:
    explicit ContentDisplay(QWidget *parent = 0);
    struct Item
    {
        QString label;
        QVariant link;
        QList<QSharedPointer<Item>> subitems;
    };
signals:
    void linkActivated(QVariant link);
public slots:
    void setData(QList<QSharedPointer<Item>> data);
    void Clear();
};

typedef QSharedPointer<ContentDisplay::Item> ContentItemPtr;

#endif // CONTENTDISPLAY_H
