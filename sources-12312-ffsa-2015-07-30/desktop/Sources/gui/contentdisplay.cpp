#include "contentdisplay.h"
#include <QLabel>
ContentDisplay::ContentDisplay(QWidget *parent) : QTreeWidget(parent)
{
    setColumnCount(1);
    setHeaderLabel(tr("Content"));
    setWordWrap(true);
    connect(this, &ContentDisplay::itemClicked, [=](QTreeWidgetItem *item, int ){
        if(item)
        {
            emit linkActivated(item->data(0, Qt::UserRole));
        }
    });
}

QTreeWidgetItem * traverser(QSharedPointer<ContentDisplay::Item> data ){
    auto topLevel = new QTreeWidgetItem((QTreeWidget*)nullptr, QStringList(data->label));

    topLevel->setData(0, Qt::UserRole, data->link);
    QList<QTreeWidgetItem *> subitems;
    for(auto entry: data->subitems) {
        subitems << traverser(entry);
    }
    topLevel->addChildren(subitems);
    return topLevel;

};

void ContentDisplay::setData(QList<QSharedPointer<ContentDisplay::Item> > data)
{
    Clear();
    QList<QTreeWidgetItem *> items;
    for(auto entry: data) {
        items << traverser(entry);
    }
    insertTopLevelItems(0, items);
    for(auto item: items)
    {
        auto label = new QLabel("• " + item->text(0));
        label->setWordWrap(true);
        setItemWidget(item, 0, label);
        item->setText(0, "");
    }
    expandAll();
}

void ContentDisplay::Clear()
{
    clear();
}
