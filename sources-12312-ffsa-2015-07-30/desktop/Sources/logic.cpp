#include "logic.h"

#include "gui/mainwindow.h"
#include "gui/history.h"
#include "gui/downloadwindow.h"
#include "gui/printingwindow.h"
#include "gui/authorizationdialog.h"
#include "gui/userpassworddialog.h"
#include "miscellaneous/config.h"
#include "protection/blacklistedprocessesfinder.h"
#include "protection/printscreenprotection.h"

#include <QNetworkAccessManager>
#include <QStringList>
#include <QMessageBox>
#include <QInputDialog>
#include <QNetworkProxy>
#include <QAuthenticator>
#include <QDesktopServices>
#include <QApplication>
#include <QSettings>
#include <QFile>
#include <QDir>

#ifdef OS_MACOSX
#include <QFileOpenEvent>
#include <miscellaneous/macx/maceventshandler.h>
#endif

#ifdef OS_LINUX
#include <QDebug>
#include <QProcess>

using namespace LinuxUtils;
#endif

#include <request.h>
#include <authorizationbase.h>
#include <statusrequest.h>

#ifdef BASE_URL_DEMO
    #define BASE_URL	"http://demo.neb.elar.ru"
#else
    #define BASE_URL	"https://нэб.рф"
    QString Logic::g_baseUrlForDocuments = "https://выдача.нэб.рф";
#endif

#define BLACK_LISTED_PROCESS_SEARCH_INTERVAL	5
#define SETTINGS_REQUEST_TIMEOUT				10000

#define DOCUMENT_BASE_URL_KEY		"DocumentBaseUrl"

Logic::Logic(QObject *parent, QApplication* application) :
    QObject(parent),
    m_mainWindow( NULL ),
    m_history( NULL ),
    m_favorites( NULL ),
    m_baseRequest( NULL ),
    m_config( NULL ),
    m_manager( NULL ),
    m_downloadWindow( NULL ),
    m_printingWindow( NULL ),
    m_authorizationDialog( NULL ),
    m_authorizationBase( NULL ),
    m_startDocuments(),
    m_user(),
    m_accountBase( NULL ),
    m_favoritesManager( NULL, PRIORITY_HIGHT ),
    m_authorizationCheck( NULL ),
    m_authorizationRequest( NULL ),
    m_statusUserRequest( NULL ),
    m_statusIpRequest( NULL ),
    m_blacklistedProcessSearch(),
    m_session( NULL ),
    m_startParameters( QStringList() ),
    m_settingsRequest( NULL ),
    m_settingsRequestTimer(),
    m_stopped( false ),
    m_application( application )
#ifdef OS_LINUX
    ,m_distro( DetectLinuxDistro() )
#endif
{
    if( IsEmpty() ) return;

    m_application->setQuitOnLastWindowClosed( false );
    CreateNewConfig();
    CreateMembers();
    ConnectSlots();
    Initialize();
}

Logic::~Logic()
{
    if( IsEmpty() ) return;

    Stop();
    DeleteMembers();
    Deinitialize();

    QSettings( Config::Organization(), Config::Application() )
    .setValue( DOCUMENT_BASE_URL_KEY, g_baseUrlForDocuments );
}

bool Logic::Start(Parameters parameters )
{
    if( IsEmpty() ) return false;

    if( ApiBase::IsConfigPresented() )
    {
        QString title = tr( "ConfigPresentedTitle" );
        QString message = tr( "ConfigPresentedMessage" );
        QMessageBox::warning( NULL, title, message );
    }

    if( m_stopped ) return false;

    DebugAssert( m_settingsRequest == NULL );
    if( m_settingsRequest != NULL ) return false;

    m_startParameters = parameters;

    m_settingsRequest = new ViewerSettingsRequest( NULL, m_manager, BASE_URL );
    connect(m_settingsRequest,SIGNAL(SignalFinished(ErrorCode,ViewerSettings)),this,SLOT(SettingsRequestFinished(ErrorCode,ViewerSettings)));
    connect(m_settingsRequest,SIGNAL(SignalAuthenticationRequired(QAuthenticator*)),this,SLOT(SlotAuthenticationRequired(QAuthenticator*)));
    m_settingsRequest->Send();

    m_settingsRequestTimer.start( SETTINGS_REQUEST_TIMEOUT );
    return true;
}

FavoritesManager*Logic::GetFavoritesManager()
{
    if( IsEmpty() ) return NULL;
    return &m_favoritesManager;
}

ViewerSettings Logic::GetSettings() const
{
    if( IsEmpty() ) return ViewerSettings();

    DebugAssert( m_settings.IsCorrect() );
    if( m_settings.IsCorrect() == false ) return ViewerSettings();

    return m_settings;
}

bool Logic::IsEmpty() const
{
    return m_application == NULL;
}

QString Logic::BaseUrlForDocuments()
{
    return g_baseUrlForDocuments;
}

void Logic::SlotReceiveParameters(Parameters parameters)
{
    if( IsEmpty() ) return;

    DocumentList documents = parameters.Documents();
    if( m_user.IsEmpty() )
    {
        foreach( Document document, documents )
        {
            m_startDocuments.append( document );
        }
        BringToFront( m_authorizationDialog );
    }
    else
    {
        foreach( Document document, documents )
        {
            m_config->AddToHistory( document );
        }
        OpenDocument( m_config->LastDocument() );
        BringToFront( m_mainWindow );
    }
}

void Logic::ShowAuthorization( QString message )
{
    DeleteLaterAndNull( m_authorizationBase );
    m_authorizationBase = new AuthorizationBase( NULL, m_manager, BASE_URL );

    m_authorizationDialog->SetMessage( message );
    m_authorizationDialog->SetBase( m_authorizationBase );
    m_authorizationDialog->SetEmail( m_config->LastEmail() );
    m_authorizationDialog->SetEmailEnabled( true );
    m_authorizationDialog->show();
}

void Logic::AuthorizeByToken(QString token)
{
    DeleteLaterAndNull( m_authorizationBase );
    m_authorizationBase = new AuthorizationBase( NULL, m_manager, BASE_URL );

    DeleteLaterAndNull( m_authorizationCheck );
    m_authorizationCheck = m_authorizationBase->NewCheck( token );

    connect(m_authorizationCheck,SIGNAL(SignalFinished(ErrorCode,AuthorizationResult)),this,SLOT(AuthorizationByTokenFinished(ErrorCode,AuthorizationResult)));
    connect(m_authorizationCheck,SIGNAL(SignalAuthenticationRequired(QAuthenticator*)),this,SLOT(SlotAuthenticationRequired(QAuthenticator*)));
    m_authorizationCheck->Send();
}

void Logic::AuthorizeByPassword(QString email, QString password)
{
    DeleteLaterAndNull( m_authorizationBase );
    m_authorizationBase = new AuthorizationBase( NULL, m_manager, BASE_URL );

    DeleteLaterAndNull( m_authorizationRequest );
    m_authorizationRequest = m_authorizationBase->NewRequest( email, password );

    connect(m_authorizationRequest,SIGNAL(SignalFinished(ErrorCode,AuthorizationResult)),this,SLOT(AuthorizationByPasswordFinished(ErrorCode,AuthorizationResult)));
    connect(m_authorizationRequest,SIGNAL(SignalAuthenticationRequired(QAuthenticator*)),this,SLOT(SlotAuthenticationRequired(QAuthenticator*)));
    m_authorizationRequest->Send();
}

void Logic::OpenDocument(Document document)
{
    m_mainWindow->Clear();
    m_mainWindow->show();

    if( document.IsEmpty() ) return;
    m_config->AddToHistory( document  );

    DeleteLaterAndNull( m_baseRequest );
    m_baseRequest = new Request( NULL, m_manager, CreateId(), document );

    m_mainWindow->Load( m_baseRequest, m_accountBase, m_config->GetState( document ),  &m_favoritesManager);

    m_history->Load( m_baseRequest, m_config->GetHistory(), m_config );
    m_favorites->Load( m_baseRequest, m_favoritesManager.GetFavoriteDocuments(), m_config );
}

void Logic::SaveDocument(Document document, PageSet pageSet)
{
    DebugAssert( document.IsEmpty() == false );
    if( document.IsEmpty() ) return;

    DeleteLaterAndNull( m_baseRequest );

    m_baseRequest = new Request( NULL, m_manager, CreateId(), document );
    m_downloadWindow->StartDownloading( m_baseRequest, pageSet );
}

void Logic::PrintDocument(Document document, PageSet pageSet)
{
    DebugAssert( document.IsEmpty() == false );
    if( document.IsEmpty() ) return;

    DeleteLaterAndNull( m_baseRequest );

    m_baseRequest = new Request( NULL, m_manager, CreateId(), document );
    m_printingWindow->StartPrinting( m_baseRequest, pageSet );
}

void Logic::DownloadingError(Document document)
{
//	m_config->RemoveFromHistory( document );
//	OpenDocument( m_config->LastDocument() );
    OpenDocument( Document() );
    QMessageBox::information(
                NULL,
                tr( "DownloadingError" ),
                tr( "Downloading %1 was unsuccessful." ).arg( document.DocumentId() ) );
}

void Logic::DownloadingSuccess(Document document)
{
    DebugAssert( document.IsEmpty() == false );
    if( document.IsEmpty() ) return;

    g_baseUrlForDocuments = document.BaseUrl();
}

void Logic::UserAuthorized(User user)
{
    DebugAssert( user.IsEmpty() == false );
    if( user.IsEmpty() ) return;

    DebugAssert( m_user.IsEmpty() );
    if( m_user.IsEmpty() == false ) return;

    m_authorizationDialog->hide();

    m_user = user;

    m_config->SetLastEmail( user.GetEmail() );

    DeleteLaterAndNull( m_statusUserRequest );
    m_statusUserRequest = new StatusRequest( NULL, m_manager, BASE_URL, m_user, StatusRequest::CheckUser );
    connect(m_statusUserRequest,SIGNAL(SignalFinishedUserStatus(ErrorCode,User::UserStatus)),this,SLOT(UserStatusCheckFinished(ErrorCode,User::UserStatus)));
    m_statusUserRequest->Send();
}

void Logic::AuthorizationFinished()
{
    m_session = Session::New( m_authorizationBase, m_user );
    connect(m_session.data(),SIGNAL(SignalTokenExpired()),this,SLOT(UserTokenExpired()));
    m_user.SetSession( m_session );

    m_config->Load( m_user );
    m_mainWindow->restoreState( m_config->GetWindowState() );
    foreach( Document document, m_startDocuments ) m_config->AddToHistory( document );
    m_startDocuments.clear();

    DebugAssert( m_accountBase == NULL );
    if( m_accountBase != NULL ) return;
    m_accountBase = new AccountBaseRequest( NULL, m_manager, BASE_URL, m_user );
    HandleUserRole(m_user);
    m_favoritesManager.Load( m_accountBase );

    m_authorizationDialog->SetPassword( QString() );
    OpenDocument( m_config->LastDocument() );
}

void Logic::UserStatusCheckFinished(ErrorCode error, User::UserStatus userStatus)
{
    if( m_statusUserRequest == NULL ) return;
    if( m_statusUserRequest != sender() ) return;

    if( error != API_NO_ERROR )
    {
        ShowAuthorization();
        return;
    }

    m_user.SetStatus( userStatus );

    DeleteLaterAndNull( m_statusIpRequest );
    m_statusIpRequest = new StatusRequest( NULL, m_manager, BASE_URL, m_user, StatusRequest::CheckIp );
    connect(m_statusIpRequest,SIGNAL(SignalFinished(ErrorCode,bool)),this,SLOT(IpStatusCheckFinished(ErrorCode,bool)));
    m_statusIpRequest->Send();
}

void Logic::IpStatusCheckFinished(ErrorCode error, bool ipIsAllowed)
{
    if( m_statusIpRequest == NULL ) return;
    if( m_statusIpRequest != sender() ) return;

    if( error != API_NO_ERROR )
    {
        ShowAuthorization();
        return;
    }

    m_user.SetIpIsAllowed( ipIsAllowed );

    AuthorizationFinished();
}

void Logic::RequestRemovalFromHistory(Document document)
{
    m_config->RemoveFromHistory( document );
    m_history->RemoveDocument( document );
}

void Logic::RequestRemovalFromFavorites(Document document)
{
    m_favoritesManager.SendRequest( document, false );
}

void Logic::ProxyAuthenticationRequired(const QNetworkProxy& proxy, QAuthenticator* authenticator)
{
    bool timerIsActive = m_settingsRequestTimer.isActive();
    m_settingsRequestTimer.stop();

    UserPasswordDialog d;
    d.setWindowTitle( proxy.hostName() );
    QDialog::DialogCode result = d.Show();

    if( timerIsActive ) m_settingsRequestTimer.start();

    if( result == QDialog::Accepted )
    {
        authenticator->setUser( d.User() );
        authenticator->setPassword( d.Password() );
    }
    else if( result == QDialog::Rejected )
    {
        Stop();
    }
    else
    {
        NotImplemented();
    }
}

void Logic::UpdateFavorites(FavoritesManager* manager)
{
    if( m_favorites == NULL ) return;

    DebugAssert( manager == &m_favoritesManager );
    if( manager != &m_favoritesManager ) return;

    DocumentList documents = manager->GetFavoriteDocuments();
    if( documents.isEmpty() == false && m_baseRequest == NULL )
    {
        Document document = documents.last();
        m_baseRequest = new Request( NULL, m_manager, CreateId(), document );
    }
    m_favorites->Load( m_baseRequest, documents, m_config );
}

void Logic::SaveDocumentState(DocumentState state)
{
    if( state.IsCorrect() == false ) return;

    m_config->SetState( state.GetDocument(), state );
    m_config->SetWindowState( m_mainWindow->saveState() );
}

void Logic::AuthorizationByTokenFinished(ErrorCode error, AuthorizationResult result)
{
    if( m_authorizationCheck == NULL ) return;
    if( m_authorizationCheck != sender() ) return;

    if( error != API_NO_ERROR )
    {
        ShowAuthorization();
        return;
    }

    DebugAssert( result.IsEmpty() == false );
    if( result.IsEmpty() )
    {
        ShowAuthorization();
        return;
    }

    if( result.GetError().isEmpty() == false )
    {
        ShowAuthorization();
        return;
    }

    User user = result.GetUser();

    DebugAssert( user.IsEmpty() == false );
    if( user.IsEmpty() )
    {
        ShowAuthorization();
        return;
    }

    UserAuthorized( user );
}

void Logic::AuthorizationByPasswordFinished(ErrorCode error, AuthorizationResult result)
{
    if( m_authorizationRequest == NULL ) return;
    if( m_authorizationRequest != sender() ) return;

    if( error != API_NO_ERROR )
    {
        ShowAuthorization();
        return;
    }

    DebugAssert( result.IsEmpty() == false );
    if( result.IsEmpty() )
    {
        ShowAuthorization();
        return;
    }

    if( result.GetError().isEmpty() == false )
    {
        ShowAuthorization();
        return;
    }

    User user = result.GetUser();

    DebugAssert( user.IsEmpty() == false );
    if( user.IsEmpty() )
    {
        ShowAuthorization();
        return;
    }

    UserAuthorized( user );
}

void Logic::SlotAuthenticationRequired(QAuthenticator* authenticator)
{
    authenticator->setUser( AUTHENTICATION_USER );
    authenticator->setPassword( AUTHENTICATION_PASSWORD );
}

void Logic::SearchBlackListedProcesses()
{
#if defined(OS_WINDOWS)
    QString processName;
    QString processDescription;
    uint reportNumber;
    bool found = BlacklistedProcessesFinder::GetInstance()->SearchForBlacklistedProcess( processName, processDescription, reportNumber );
    if( found == false ) return;

    Stop();
    QString title = tr( "title" ).arg( processName ).arg( processDescription );
    QString message = tr( "message" ).arg( processName ).arg( processDescription );
    QMessageBox::warning( NULL, title, message  );
#elif defined(OS_LINUX)
    QFile file( QString( "/proc/loadavg" ) );
    if( file.exists() == false )
    {
        Stop();

        QString title = tr( "title" );
        QString message = tr( "loadavg_message" );
        QMessageBox::warning( NULL, title, message );
        return;
    }
    if( file.open( QIODevice::ReadOnly ) == false )
    {
        Stop();

        QString title = tr( "title" );
        QString message = tr( "loadavg_message" );
        QMessageBox::warning( NULL, title, message );
        return;
    }

    QString loadAvgStr = QString::fromUtf8( file.readAll() );
    if( loadAvgStr.isEmpty() )
    {
        Stop();

        QString title = tr( "title" );
        QString message = tr( "loadavg_message" );
        QMessageBox::warning( NULL, title, message );
        return;
    }

    QStringList avgFields = loadAvgStr.split( ' ', QString::SkipEmptyParts );
    if( avgFields.size() != 5 )
    {
        Stop();

        QString title = tr( "title" );
        QString message = tr( "loadavg_message" );
        QMessageBox::warning( NULL, title, message );
        return;
    }

    QString lastPidStr = avgFields.at(4).trimmed();

    QString processName;
    QString processDescription;
    if( ProcessIsGrabber( lastPidStr, processName, processDescription ) )
    {
        Stop();

        QString title = tr( "title" );
        QString message = tr( "message %1 %2." ).arg( processName ).arg( processDescription );
        QMessageBox::warning( NULL, title, message );
    }
#endif
}

void Logic::UserTokenExpired()
{
    m_mainWindow->Clear();
    m_history->Clear();
    m_favoritesManager.Clear();

    DeleteAndNull( m_baseRequest );
    DeleteAndNull( m_accountBase );
    DeleteAndNull( m_config );
    DeleteAndNull( m_authorizationBase );
    DeleteAndNull( m_authorizationCheck );
    DeleteAndNull( m_authorizationRequest );
    DeleteAndNull( m_statusUserRequest );
    DeleteAndNull( m_statusIpRequest );

    CreateNewConfig();
    m_config->SetLastEmail( m_user.GetEmail() );
    m_user = User();
    ShowAuthorization( tr( "UserTokenExpired" ) );
    m_authorizationDialog->SetEmailEnabled( false );
}

void Logic::SettingsRequestFinished(ErrorCode error, ViewerSettings settings)
{
    UNUSED( error );
//    qDebug() << error;
    if( m_settingsRequest == NULL ) return;
    if( m_settingsRequest != sender() ) return;

    m_settingsRequestTimer.stop();

    if( error != API_NO_ERROR )
    {
        SettingsRequestTimeout();
        return;
    }

    if( settings.IsCorrect() == false )
    {
        Stop();
        QString title = tr( "SettingsErrorTitle" );
        QString message = tr( "SettingsErrorMessage" );
        QMessageBox::warning( NULL, title, message );
        return;
    }
    qDebug() << "Supported versions: " << settings.ViewerVersions();
    qDebug() << "Current version: " << VERSION;
    if( settings.ViewerVersions().contains( VERSION ) == false )
    {
        Stop();
        QString title = tr( "NeedUpdateTitle" );
        QString message = tr( "NeedUpdateMessage" );
        QMessageBox::warning( NULL, title, message );
        QDesktopServices::openUrl( QUrl( settings.UpdateLink() ) );
        return;
    }

    m_settings = settings;
    StartInternal();
}

void Logic::SettingsRequestTimeout()
{
    Stop();
    QString title = tr( "SettingsRequestTimeoutTitle" );
    QString message = tr( "SettingsRequestTimeoutMessage" );
    QMessageBox::warning( NULL, title, message );
}

void Logic::HandleUserRole(User &user)
{
    m_mainWindow->EnableAdminInterface(user.IsAdmin(), m_accountBase);
}

void Logic::DeleteMembers()
{
    DeleteAndNull( m_mainWindow );
    DeleteAndNull( m_baseRequest );
    DeleteAndNull( m_accountBase );
    DeleteAndNull( m_config );
    DeleteAndNull( m_manager );
    DeleteAndNull( m_downloadWindow );
    DeleteAndNull( m_printingWindow );
    DeleteAndNull( m_authorizationDialog );
    DeleteAndNull( m_authorizationBase );
    DeleteAndNull( m_authorizationCheck );
    DeleteAndNull( m_authorizationRequest );
    DeleteAndNull( m_statusUserRequest );
    DeleteAndNull( m_statusIpRequest );
    DeleteAndNull( m_settingsRequest );
    m_favorites = NULL;
    m_history = NULL;
}

void Logic::CreateMembers()
{
    m_mainWindow = new MainWindow( NULL, this );
    m_history = m_mainWindow->GetHistory();
    m_favorites = m_mainWindow->GetFavorite();
    m_manager = new QNetworkAccessManager();
    m_downloadWindow = new DownloadWindow();
    m_printingWindow = new PrintingWindow();
    m_authorizationDialog = new AuthorizationDialog();
}

void Logic::ConnectSlots()
{
    connect(m_mainWindow,SIGNAL(SaveDocument(Document,PageSet)),this,SLOT(SaveDocument(Document,PageSet)));
    connect(m_mainWindow,SIGNAL(PrintDocument(Document,PageSet)),this,SLOT(PrintDocument(Document,PageSet)));
    connect(m_history,SIGNAL(OpenDocument(Document)),this,SLOT(OpenDocument(Document)));
//	connect(m_history,SIGNAL(DownloadingError(Document)),this,SLOT(DownloadingError(Document)));
    connect(m_history,SIGNAL(RequestRemoval(Document)),this,SLOT(RequestRemovalFromHistory(Document)));
    connect(m_favorites,SIGNAL(RequestRemoval(Document)),this,SLOT(RequestRemovalFromFavorites(Document)));
    connect(m_favorites,SIGNAL(OpenDocument(Document)),this,SLOT(OpenDocument(Document)));
    connect(m_authorizationDialog,SIGNAL(UserAuthorized(User)),this,SLOT(UserAuthorized(User)));
    connect(m_authorizationDialog,SIGNAL(AuthorizationCancelled()),this,SLOT(Stop()));
    connect(m_authorizationDialog, &AuthorizationDialog::AnonymTokenReady, [=](QString token){
        this->AuthorizeByToken(token);
        m_authorizationDialog->hide();
    });
    connect(m_manager,SIGNAL(proxyAuthenticationRequired(QNetworkProxy,QAuthenticator*)),this,SLOT(ProxyAuthenticationRequired(QNetworkProxy,QAuthenticator*)));
    connect(&m_favoritesManager,SIGNAL(SignalUpdate(FavoritesManager*)),m_mainWindow,SLOT(UpdateFavorites(FavoritesManager*)));
    connect(&m_favoritesManager,SIGNAL(SignalUpdate(FavoritesManager*)),this,SLOT(UpdateFavorites(FavoritesManager*)));
    connect(m_mainWindow,SIGNAL(SendFavoritesRequest(Document,bool)),&m_favoritesManager,SLOT(SendRequest(Document,bool)));
    connect(m_mainWindow,SIGNAL(SaveState(DocumentState)),this,SLOT(SaveDocumentState(DocumentState)));
    connect(m_mainWindow,SIGNAL(OpenDocument(Document)),this,SLOT(OpenDocument(Document)));
    connect(m_mainWindow,SIGNAL(DownloadingError(Document)),this,SLOT(DownloadingError(Document)));
    connect(m_mainWindow,SIGNAL(DownloadingSuccess(Document)),this,SLOT(DownloadingSuccess(Document)));
    connect(&m_blacklistedProcessSearch,SIGNAL(timeout()),this,SLOT(SearchBlackListedProcesses()));
    connect(&m_settingsRequestTimer,SIGNAL(timeout()),this,SLOT(SettingsRequestTimeout()));
}

void Logic::Initialize()
{
    QNetworkProxyFactory::setUseSystemConfiguration( true );

    QSettings settings( Config::Organization(), Config::Application() );
    g_baseUrlForDocuments = settings.value( DOCUMENT_BASE_URL_KEY, g_baseUrlForDocuments ).toString();

    #if defined(OS_WINDOWS)
    m_blacklistedProcessSearch.start( BLACK_LISTED_PROCESS_SEARCH_INTERVAL );
    PrintScreenProtection::Install();
    #elif defined(OS_LINUX)
    QMap<int,int> procs;
    QDir dir( "/proc/" );
    QStringList procList = dir.entryList( QDir::Dirs, QDir::Name );
    for( QStringList::iterator iter = procList.begin(); iter != procList.end(); ++iter )
    {
        procs.insert( (*iter).toInt(), 0 );

        QString processName, processDescription;
        if( ProcessIsGrabber( (*iter), processName, processDescription ) )
        {
            Stop();

            QString title = tr( "title" ).arg( processName ).arg( processDescription );
            QString message = tr( "message" ).arg( processName ).arg( processDescription );
            QMessageBox::warning( NULL, title, message );
        }
    }

    // GNOME 3 specific Print Screen protection install
    if( m_distro == DISTRO_FEDORA )
    {
        // Get current shortcut values
        m_gnomeShortcuts.insert( "window-screenshot",		GetGnomeShortcut( "window-screenshot" ) );
        m_gnomeShortcuts.insert( "screenshot-clip",			GetGnomeShortcut( "screenshot-clip" ) );
        m_gnomeShortcuts.insert( "window-screenshot-clip",	GetGnomeShortcut( "window-screenshot-clip" ) );
        m_gnomeShortcuts.insert( "area-screenshot-clip",	GetGnomeShortcut( "area-screenshot-clip" ) );
        m_gnomeShortcuts.insert( "screenshot",				GetGnomeShortcut( "screenshot" ) );
        m_gnomeShortcuts.insert( "area-screenshot",			GetGnomeShortcut( "area-screenshot" ) );

        // Clear these shortcuts values (i.e. replace them to empty ones)
        if( ClearGnomeShortcut( "window-screenshot" )		== false ) qDebug() << "ERROR: GNOME window-screenshot shortcut capture failed";
        if( ClearGnomeShortcut( "screenshot-clip" )			== false ) qDebug() << "ERROR: GNOME screenshot-clip shortcut capture failed";
        if( ClearGnomeShortcut( "window-screenshot-clip" )	== false ) qDebug() << "ERROR: GNOME window-screenshot-clip shortcut capture failed";
        if( ClearGnomeShortcut( "area-screenshot-clip" )	== false ) qDebug() << "ERROR: GNOME area-screenshot-clip shortcut capture failed";
        if( ClearGnomeShortcut( "screenshot" )				== false ) qDebug() << "ERROR: GNOME screenshot shortcut capture failed";
        if( ClearGnomeShortcut( "area-screenshot" )			== false ) qDebug() << "ERROR: GNOME area-screenshot shortcut capture failed";
    }

    m_blacklistedProcessSearch.start( BLACK_LISTED_PROCESS_SEARCH_INTERVAL );
    #endif // OS_WINDOWS
}

void Logic::Deinitialize()
{
    #if defined(OS_WINDOWS)
    PrintScreenProtection::Remove();
    BlacklistedProcessesFinder::FreeInstance();
    #elif defined(OS_LINUX)
    // GNOME 3 specific Print Screen protection remove
    if( m_distro == DISTRO_FEDORA )
    {
        // Restore initial shortcut values
        for( QMap<QString,QString>::iterator iter = m_gnomeShortcuts.begin(); iter != m_gnomeShortcuts.end(); ++iter )
        {
//			qDebug() << "Restore: " + iter.key() + " : " + iter.value();

            SetGnomeShortcut( iter.key(), iter.value() );
        }
    }
    #endif // OS_WINDOWS
}

void Logic::Stop()
{
    if( IsEmpty() ) return;
    if( m_stopped ) return;

    m_stopped = true;
    m_application->setQuitOnLastWindowClosed( true );

    DeleteLaterAndNull( m_authorizationCheck );
    DeleteLaterAndNull( m_authorizationRequest );
    DeleteLaterAndNull( m_statusUserRequest );
    DeleteLaterAndNull( m_statusIpRequest );
    DeleteLaterAndNull( m_settingsRequest );
    m_favoritesManager.Clear();
    m_mainWindow->Clear();

    m_mainWindow->close();
    m_authorizationDialog->close();
    m_settingsRequestTimer.stop();
    m_blacklistedProcessSearch.stop();
}

QString Logic::GetAppToken() const
{
    if( m_user.GetStatus() == User::Anonymous )		return "2uS8PyK3jTeCjMRqVZTs7Hwd";
    if( m_user.GetStatus() == User::Verified )		return "nXphmdrl1wxKJ2sTlPAGLDqs";
    if( m_user.GetStatus() == User::Registered )	return "v9zVtzsH4PVBkC7A2yGBBFH4";
    if( m_user.GetStatus() == User::VIP )			return "m4RsaJUM93gHYZarM3vQwdZW";
    NotImplemented();
    return QString();
}

Identifier Logic::CreateId() const
{
    Identifier result( m_session );
    result.SetClientId( m_config->ClientId() );
    result.SetUserToken( m_user.GetToken() );
    result.SetAppToken( GetAppToken() );
    return result;
}

void Logic::BringToFront(QMainWindow* window)
{
    DebugAssert( window->isVisible() );
    if( window->isVisible() == false ) return;

    window->setWindowState( window->windowState() & ~Qt::WindowMinimized );
    window->raise();
    window->activateWindow();
}

void Logic::CreateNewConfig()
{
    DebugAssert( m_config == NULL );
    if( m_config != NULL ) return;

    m_config = new Config();
    connect(m_config,SIGNAL(WasChanged()),m_config,SLOT(Save()));
}

void Logic::StartInternal()
{
    m_startDocuments = m_startParameters.Documents();

    if( m_startParameters.ContainsToken() )
    {
        AuthorizeByToken( m_startParameters.Token() );
    }
    else
    {
#ifdef PLAN_B
        AuthorizeByPassword( "reader@elar.ru", "111111" );
#else
        ShowAuthorization();
#endif
    }
}

#ifdef OS_LINUX
bool Logic::ProcessIsGrabber( const QString& pid, QString& processName, QString& processDescription )
{
    QFile file( "/proc/" + pid + "/cmdline" );
    if( file.exists() == false ) return false;

    if( file.open( QIODevice::ReadOnly ) == false ) return false;

    QString cmdLine = QString::fromUtf8( file.readAll() );
    if( cmdLine.isEmpty() ) return false;

    bool grabberDetected = false;
    if( cmdLine.contains( "gnome-screenshot", Qt::CaseInsensitive ) )
    {
        grabberDetected = true;

        processName = cmdLine;
        processDescription = tr( "Screenshot application for GNOME" );
    }
    else if( cmdLine.contains( "ksnapshot", Qt::CaseInsensitive )
          || cmdLine.contains( "kbackgroundsnapshot", Qt::CaseInsensitive ) )
    {
        grabberDetected = true;

        processName = cmdLine;
        processDescription = tr( "KDE screen capture tool" );
    }

    return grabberDetected;
}

QString Logic::GetGnomeShortcut( QString name )
{
    QString gsettingsProgram = "gsettings";
    QStringList gsettingsArguments;
    gsettingsArguments << "get" << "org.gnome.settings-daemon.plugins.media-keys" << name;

    QProcess gsettingsProcess;
    gsettingsProcess.start( gsettingsProgram, gsettingsArguments );
    if( gsettingsProcess.waitForStarted( 1000 ) == false )
    {
//		qDebug() << ( "ERROR: (GetGnomeScreenshot, " + name + " ) unable to start gsettings process" );
        return QString();
    }
    if( gsettingsProcess.waitForFinished( 1000 ) == false )
    {
//		qDebug() << "ERROR: (GetGnomeScreenshot, " + name + " ) unable to wait while gsettings is finished";
        return QString();
    }
    if( gsettingsProcess.exitCode() != 0 )
    {
//		qDebug() << "ERROR: (GetGnomeScreenshot, " + name + " ) gsettings returns non-zero";
        return QString();
    }

    QString value = gsettingsProcess.readAll();
//	qDebug() << "NOTE: (GetGnomeScreenshot, " + name + " ) GNOME shortcut value was successfully read: " + value;
    return value;
}

bool Logic::SetGnomeShortcut( QString name, QString value )
{
    QString gsettingsProgram = "gsettings";
    QStringList gsettingsArguments;
    gsettingsArguments << "set" << "org.gnome.settings-daemon.plugins.media-keys" << name << value;

    QProcess gsettingsProcess;
    gsettingsProcess.start( gsettingsProgram, gsettingsArguments );
    if( gsettingsProcess.waitForStarted( 10000 ) == false )
    {
//		qDebug() << ( "ERROR: (SetGnomeScreenshot, " + name + " ) unable to start gsettings process" );
        return false;
    }
    if( gsettingsProcess.waitForFinished( 10000 ) == false )
    {
//		qDebug() << "ERROR: (SetGnomeScreenshot, " + name + " ) unable to wait while gsettings is finished";
        return false;
    }
    if( gsettingsProcess.exitCode() != 0 )
    {
//		qDebug() << "ERROR: (SetGnomeScreenshot, " + name + " ) gsettings returns non-zero";
        return false;
    }

    return true;
}

bool Logic::ClearGnomeShortcut( QString name )
{
    return SetGnomeShortcut( name, "''" );
}
#endif

#ifdef OS_MACOSX
bool Logic::WasStarted() const
{
    return m_settingsRequestTimer.isActive() || m_settings.IsCorrect();
}

bool Logic::eventFilter( QObject* obj, QEvent* event )
{
    if( event->type() == QEvent::FileOpen )
    {
        QFileOpenEvent* fileEvent = static_cast< QFileOpenEvent* >(event);
        DebugAssert( fileEvent != NULL );

        QString uri;
        if( fileEvent->file().isEmpty() == false )
        {
            uri = fileEvent->file();
        }
        else if( fileEvent->url().isEmpty() == false )
        {
            uri = fileEvent->url().toString();
        }

        if( uri.isEmpty() == false )
        {
            Parameters p( QStringList() << uri );

            if( WasStarted() ) SlotReceiveParameters( p );
            else Start( p );
        }

        return false;
    }
    else if( event->type() == ViewerDryRunEvent::eventType )
    {
        ViewerDryRunEvent* myEvent = static_cast< ViewerDryRunEvent* >( event );
        DebugAssert( myEvent != NULL );
        Parameters cmdLineParams = myEvent->GetCmdLineParams();

        DebugAssert( WasStarted() == false );
        if( WasStarted() ) return false;
        if( Start( cmdLineParams ) == false ) m_application->exit( 0 );
        return false;
    }

    // Standard event processing
    return QObject::eventFilter( obj, event );
}
#endif
