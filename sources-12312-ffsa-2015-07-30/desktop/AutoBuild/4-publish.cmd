@echo off


if "%1" equ "" (
	echo Usage: 4-publish.cmd ^<build-id^> [^<project-id^>]
	exit /b 1
) else (
	set BUILD_ID=%1
	set PROJECT_ID=%2
)


echo.
echo ########################################
echo #                                      #
echo # Publishing...                        #
echo #                                      #
echo ########################################


set STORE_DIR=C:\Builds
if "%PROJECT_ID%" equ "" (
	set TARGET_DIR=%STORE_DIR%\ProtectionKernel\5.70
) else (
	set TARGET_DIR=%STORE_DIR%\Projects\%PROJECT_ID%\ProtectionKernel
)

robocopy "Output\%BUILD_ID%" "%TARGET_DIR%\%BUILD_ID%" /e
if errorlevel 2 goto failure
robocopy "Output\Support\%BUILD_ID%" "%TARGET_DIR%\Support\%BUILD_ID%" /e
if errorlevel 2 goto failure


goto success


:success
echo.
echo ########################################
echo #                                      #
echo # Publishing done                      #
echo #                                      #
echo ########################################
exit /b 0
:failure
echo.
echo ########################################
echo #                                      #
echo # Publishing FAILED                    #
echo #                                      #
echo ########################################
exit /b 1
