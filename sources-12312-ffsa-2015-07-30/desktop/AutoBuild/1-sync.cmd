@echo off


echo.
echo ########################################
echo #                                      #
echo # Syncing...                           #
echo #                                      #
echo ########################################


del /s /q "..\*"
if errorlevel 1 goto failure
p4 sync -f "..\..."


goto success


:success
echo.
echo ########################################
echo #                                      #
echo # Syncing done                         #
echo #                                      #
echo ########################################
exit /b 0
:failure
echo.
echo ########################################
echo #                                      #
echo # Syncing FAILED                       #
echo #                                      #
echo ########################################
exit /b 1
