

START_PATH=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
OUTPUT_PATH=${START_PATH}/Output-linux-amd64/

rm -rf ${OUTPUT_PATH}
mkdir ${OUTPUT_PATH}

VERSION="3.0.0"
ROOT="/media/psf/Home/NEB/sources-12312-ffsa-2015-07-30/desktop/AutoBuild"

QT_DIR="/home/tucher/Qt5.5.1/5.5"
ssh tucher@ubuntu-16.local bash -c "'
cd ${ROOT}
./3-compile-linux.sh -v ${VERSION} -q ${QT_DIR}
'"



BUNDLE_SRC="./Output/elar-nelrfviewer-deployed"
BUNDLE_DST="${OUTPUT_PATH}/nelrfviewer-amd64.run"
BUNDLE_DESC="Elar Corporation NEL RF viewer"
BUNDLE_EXEC="./bin/integrate-to-DE.sh"

makeself --notemp --lsm "../Sources/linux/lsmfile.lsm" "${BUNDLE_SRC}" "${BUNDLE_DST}" "${BUNDLE_DESC}" "${BUNDLE_EXEC}" -p "./share"

