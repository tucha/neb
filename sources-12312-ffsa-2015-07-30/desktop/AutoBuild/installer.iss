﻿[Files]
Source: "Result\*"; DestDir: "{app}"; Flags: ignoreversion createallsubdirs recursesubdirs; Excludes: "*.qm"
Source: "..\Sources\icons\icon.ico"; DestDir: "{app}"; Flags: ignoreversion

[Dirs]
Name: "{app}\accessible"
Name: "{app}\bearer"
Name: "{app}\iconengines"
Name: "{app}\imageformats"
Name: "{app}\platforms"
Name: "{app}\printsupport"

[Setup]
AppName=НЭБ РФ
AppVersion=1.0.0
AppPublisher=ЭЛАР
AppPublisherURL=http://www.elar.ru
UninstallDisplayIcon={app}\icon.ico
MinVersion=0,5.01sp3
DefaultDirName={pf}\НЭБ РФ
DefaultGroupName=ЭЛАР

[Registry]
Root: HKCR; SubKey: ".spd"; ValueType: string; ValueData: "SPD"; Flags: uninsdeletekey
Root: HKCR; SubKey: "SPD"; ValueType: string; ValueData: "Документ"; Flags: uninsdeletekey
Root: HKCR; SubKey: "SPD\Shell\Open\Command"; ValueType: string; ValueData: """{app}\viewer.exe"" ""%1"""; Flags: uninsdeletekey
Root: HKCR; SubKey: "SPD"; ValueType: string; ValueName: "URL Protocol"; Flags: uninsdeletekey
Root: HKCU; SubKey: "Software\ELAR"; Flags: uninsdeletekey

[Icons]
Name: "{group}\НЭБ РФ"; Filename: "{app}\viewer.exe"; WorkingDir: "{app}"; IconFilename: "{app}\icon.ico"; IconIndex: 0
Name: "{group}\Удалить"; Filename: "{uninstallexe}"
Name: "{userdesktop}\НЭБ РФ"; Filename: "{app}\viewer.exe"; WorkingDir: "{app}"; IconFilename: "{app}\icon.ico"; IconIndex: 0

[Languages]
Name: "Russian"; MessagesFile: "Russian.isl"

[Code]
function InitializeSetup(): Boolean;
var
ResultCode: Integer;
begin
Exec( 'msiexec.exe', '/q /x {5F22B4F5-BA89-4C6B-9781-EAF9A0DC5435}', '', SW_SHOW, ewWaitUntilTerminated, ResultCode);
Result := true; // продолжить выполнение
end;
