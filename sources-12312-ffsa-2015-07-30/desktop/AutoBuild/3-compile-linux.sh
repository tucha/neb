#!/bin/bash
#
# Compile and deploy the NEL RF Viewer
# Requirements: Qt5, GNU make and GCC must be installed first
# Output: deployed viewer directory, optionally RUN, DEB, RPM packages

clear

# Setup product information
COMPANY_NAME=elar
TARGET_NAME=nelrfviewer
PROJECT_NAME="${COMPANY_NAME}-${TARGET_NAME}"

# Get the absolute version of the startup path
START_PATH=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Setup various build directories
SOURCES_PATH=${START_PATH}/../Sources
LINUX_STUFF_PATH=${START_PATH}/../Sources/linux
PROJECT=${START_PATH}/../Sources/viewer.pro
BUILD_PATH=${START_PATH}/Build
RESULT_PATH=${START_PATH}/Result
OUTPUT_PATH=${START_PATH}/Output

absPath()
{
    if [[ -d "$1" ]]
    then
        pushd "$1" >/dev/null
        pwd
        popd >/dev/null
    elif [[ -e $1 ]]
    then
        pushd "$(dirname "$1")" >/dev/null
        echo "$(pwd)/$(basename "$1")"
        popd >/dev/null
    else
        echo "$1" does not exist! >&2
        return 127
    fi
}

# Convert relative paths to absolute ones
SOURCES_PATH=$(absPath ${SOURCES_PATH})
PROJECT=$(absPath ${PROJECT})

# TODO: check system openssl version and ask user to update it if nessecary!
#       Alternatively, we can use own copy of openssl libraries.
#SSL_PATH=${TOOLS_PATH}/OpenSSL/

if [ ! -d "${SOURCES_PATH}" ]; then
    echo "${RED}ERROR${NORMAL}: Sources directory was not found. Aborting"
    exit 1
fi

if [ ! -f ${PROJECT} ]; then
    echo "${RED}ERROR${NORMAL}: qmake project file was not found. Aborting"
    exit 1
fi

# Reset the getopts index
# NOTE: this is required to call build script more than once time
OPTIND=0

################################################################################
# Text formatting variables

RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
BOLD=$(tput bold)
UNDERLINE=$(tput smul)
NOUNDERLINE=$(tput rmul)
NORMAL=$(tput sgr0)

################################################################################

VERSION=""
MULTIARCH_SUPPORT="no"
MULTIARCH_DIR=""
BUILD_RUN="no"
BUILD_RUN_MODE=""
BUILD_DEB="no"
BUILD_RPM="no"
QT5_ROOT_DIR_SPECIFIED="no"
QT5_ROOT_DIR=""
QT5_ARCH_SPECIFIED="no"
QT5_ARCH=""
GCC_DIR_SPECIFIED="no"
GCC_DIR=""
GNU_MAKE_DIR_SPECIFIED="no"
GNU_MAKE_DIR=""

# Default directories
DEFAULT_TOOL_DIR="/usr/bin"
STRIP_DIR="${DEFAULT_TOOL_DIR}"
CHRPATH_DIR="${DEFAULT_TOOL_DIR}"
MAKESELF_DIR="${DEFAULT_TOOL_DIR}"
DPKG_DEB_DIR="${DEFAULT_TOOL_DIR}"
FAKEROOT_DIR="${DEFAULT_TOOL_DIR}"
LINTIAN_DIR="${DEFAULT_TOOL_DIR}"
RPMBUILD_DIR="${DEFAULT_TOOL_DIR}"

# General build utilities
TRANSLATE=lrelease
GCC=gcc
QMAKE=qmake
MAKE=make
STRIP=strip
CHRPATH=chrpath

# Target-specific build utilities
FAKEROOT=fakeroot
MAKESELF=makeself
DPKG_DEB=dpkg-deb
LINTIAN=lintian
RPMBUILD=rpmbuild
RPMLINT=rpmlint

################################################################################

usage()
{
    echo "---------------------------------------------------------------"
    echo "${UNDERLINE}USAGE${NOUNDERLINE}:"
    echo "    ./3-compile-linux.sh ${GREEN}-v${NORMAL} <version> [${GREEN}-s${NORMAL}] [${GREEN}-d${NORMAL}] [${GREEN}-r${NORMAL}]"
    echo "                        [${GREEN}-q${NORMAL} <Qt5 dir>] [${GREEN}-a${NORMAL} <Qt5 architecture>]"
    echo "                        [${GREEN}-g${NORMAL} <GCC dir>] [${GREEN}-m${NORMAL} <GNU make dir>]"
    echo "    ${GREEN}-v${NORMAL} (${BOLD}required${NORMAL}): application version"
    echo "        ${BOLD}version${NORMAL}: you should set exact value (1.2.3)"
    echo "    ${GREEN}-d${NORMAL} (${BOLD}optional${NORMAL}): build Debian .deb package"
    echo "    ${GREEN}-r${NORMAL} (${BOLD}optional${NORMAL}): build RedHat .rpm package"
    echo "    ${GREEN}-s${NORMAL} (${BOLD}optional${NORMAL}): build self-extractable .run package (for other Linux distributions)"
    echo "    ${GREEN}-q${NORMAL} (${BOLD}optional${NORMAL}): override default Qt5 directory path (~/Qt5.4.1/5.4)"
    echo "    ${GREEN}-a${NORMAL} (${BOLD}optional${NORMAL}): override default Qt5 architecture (gcc_64)"
    echo "    ${GREEN}-g${NORMAL} (${BOLD}optional${NORMAL}): override default GCC directory (/usr/bin)"
    echo "    ${GREEN}-m${NORMAL} (${BOLD}optional${NORMAL}): override default GNU make directory (/usr/bin)"
    echo
    echo "${UNDERLINE}DEFAULTS${NOUNDERLINE}:"
    echo "    -s disabled"
    echo "    -d disabled"
    echo "    -r disabled"
    echo "    i.e. just deployment to the directory will be done;"
    echo "    Qt5 dir: points to the Qt 5.4.1 installed using default path (~/Qt5.4.1/5.4)"
    echo "    Qt5 architecture: gcc on x32 OS and gcc_64 on 64-bit one"
    echo "    GCC dir: points to the standart one /usr/bin"
    echo "    GNU make dir: points to the standart one /usr/bin"
    echo "---------------------------------------------------------------"
}

processFailure()
{
    echo ${RED}
    echo
    echo "########################################"
    echo "#                                      #"
    echo "# Compilation FAILED                   #"
    echo "#                                      #"
    echo "########################################"
    echo ${NORMAL}
    cd "${START_PATH}"
    exit $1
}

processSuccess()
{
    echo ${GREEN}
    echo
    echo "########################################"
    echo "#                                      #"
    echo "# Compilation done                     #"
    echo "#                                      #"
    echo "########################################"
    echo ${NORMAL}
    cd "${START_PATH}"
    exit 0
}

parseOptions()
{
    echo "---------------------------------------------------------------"

    while getopts ":v:sdrq:a:g:m:h" opt; do
        case $opt in
        v)
            echo "Aplication version: ${OPTARG}"
            VERSION=${OPTARG}
            ;;
        s)
            BUILD_RUN=yes
            ;;
        d)
            BUILD_DEB=yes
            ;;
        r)
            BUILD_RPM=yes
            ;;
        q)
            echo "Qt5 directory: ${OPTARG}"
            QT5_ROOT_DIR_SPECIFIED=yes
            QT5_ROOT_DIR=${OPTARG}
            ;;
        a)
            echo "Qt5 architecture: ${OPTARG}"
            QT5_ARCH_SPECIFIED=yes
            QT5_ARCH=${OPTARG}
            ;;
        g)
            echo "GCC directory: $OPTARG"
            GCC_DIR_SPECIFIED=yes
            GCC_DIR=${OPTARG}
            ;;
        m)
            echo "GNU make directory: $OPTARG"
            GNU_MAKE_DIR_SPECIFIED=yes
            GNU_MAKE_DIR=${OPTARG}
            ;;
        h)
            usage
            exit 0
            ;;
        \?)
            echo "Invalid option: -$OPTARG"
            exit 1
            ;;
        :)
            echo "Option -$OPTARG requires an argument."
            exit 1
            ;;
        esac
    done

    echo "---------------------------------------------------------------"
}

printOptions()
{
    if [ "${BUILD_RUN}" = "yes" ]; then
        echo "${GREEN}NOTE${NORMAL}: self-extracted and self-contained .run archive will be built"
    fi

    if [ "${BUILD_DEB}" = "yes" ]; then
        echo "${GREEN}NOTE${NORMAL}: Debian/Ubuntu .deb package will be built"
    fi

    if [ "${BUILD_RPM}" = "yes" ]; then
        echo "${GREEN}NOTE${NORMAL}: Fedora/OpenSuse .rpm package will be built"
    fi
}

validateOptions()
{
    if [ -n "${VERSION}" ]; then
        echo "${GREEN}NOTE${NORMAL}: using version ${VERSION}"
    else
        usage
        exit 0
    fi

    if [ "${QT5_ROOT_DIR_SPECIFIED}" = "yes" ]; then
        if [ ! -d "${QT5_ROOT_DIR}" ]; then
            echo "${RED}ERROR${NORMAL}: user-specified Qt5 directory ${QT5_ROOT_DIR} not found. Aborting"
            exit 1
        fi
    fi

    if [ "${QT5_ARCH_SPECIFIED}" = "yes" ]; then
        if [ ! -d "${QT5_ROOT_DIR}/${QT5_ARCH}" ]; then
            echo "${RED}ERROR${NORMAL}: user-specified Qt5 architecture directory ${QT5_ROOT_DIR}/${QT5_ARCH} not found. Aborting"
            exit 1
        fi
    fi

    if [ "${GCC_DIR_SPECIFIED}" = "yes" ]; then
        if [ ! -d "${GCC_DIR}" ]; then
            echo "${RED}ERROR${NORMAL}: user-specified GCC directory ${GCC_DIR} not found. Aborting"
            exit 1
        fi
    fi

    if [ "${GNU_MAKE_DIR_SPECIFIED}" = "yes" ]; then
        if [ ! -d "${GNU_MAKE_DIR}" ]; then
            echo "${RED}ERROR${NORMAL}: user-specified GNU make directory ${GNU_MAKE_DIR} not found. Aborting"
            exit 1
        fi
    fi
}

detectEnvironment()
{
    # Linux distribution name
    echo "${GREEN}NOTE${NORMAL}: Linux distribution used:"
	lsb_release -a
    echo

    DISTRO_CODENAME=$( lsb_release -c -s )
    if [ "${DISTRO_CODENAME}" != "lucid" ]; then
        echo "${YELLOW}WARNING${NORMAL}: untested Linux distribution ${DISTRO_CODENAME};"
        echo "${YELLOW}WARNING${NORMAL}: only Ubuntu 10.04 Lucix Lynx (codename lucid) was tested"
	fi

    # Linux distribution architecture
    SYSTEM_ARCH=$( arch )
    echo "${GREEN}NOTE${NORMAL}: Linux distribution architecture: ${SYSTEM_ARCH}"
    # NOTE: deb/rpm packages use only i386 variant of 32-bit architectures
    if [ "${SYSTEM_ARCH}" != "x86_64" ]; then
        SYSTEM_ARCH="i386"
    fi

    # Linux kernel
    echo "${GREEN}NOTE${NORMAL}: Linux kernel used:"
    uname -a
    echo

    # Multiarch support
    MULTIARCH_DIR=$( gcc -print-multiarch )
    if [ -n "${MULTIARCH_DIR}" ]; then
        if [ -d "/usr/lib/${MULTIARCH_DIR}" ]; then
            MULTIARCH_SUPPORT="yes"
            echo "${YELLOW}WARNING${NORMAL}: your system supports multiarch, so multiarch-enabled Debian/Ubuntu package will be generated;"
            echo "${YELLOW}WARNING${NORMAL}: this package can be installed only on modern Debian/Ubuntu with multiarch support."
            echo "${YELLOW}WARNING${NORMAL}: to save compability with old distros, just launch this script under one of them."
        fi
    fi

    if [ "${MULTIARCH_SUPPORT}" = "no" ]; then
        echo "${GREEN}NOTE${NORMAL}: multiarch not supported"
    fi

	MAKE_THREAD_COUNT_OPTION=""
	THREAD_COUNT=$( grep -c ^processor /proc/cpuinfo )
	if [ "${THREAD_COUNT}" -gt "1" ]; then
		echo "${GREEN}NOTE: multi-core CPU detected, make -j${THREAD_COUNT} will be used${NORMAL}"
		MAKE_THREAD_COUNT_OPTION="-j${THREAD_COUNT}"
	fi
}

detectQt5Dirs()
{
    if [ "${QT5_ROOT_DIR_SPECIFIED}" = "yes" ]; then
        if [ ! -d "${QT5_ROOT_DIR}" ]; then
            echo "${RED}ERROR${NORMAL}: user-specified Qt5 directory ${QT5_ROOT_DIR} not found. Aborting"
            exit 1
        else
            echo "${GREEN}NOTE${NORMAL}: overriding default Qt5 directory with ${QT5_ROOT_DIR}"
        fi
    else
        QT5_ROOT_DIR="${HOME}/Qt5.4.1/5.4"
        echo "${GREEN}NOTE${NORMAL}: using default Qt5 directory ${QT5_ROOT_DIR}"
    fi

    if [ "${QT5_ARCH_SPECIFIED}" = "yes" ]; then
        echo "${GREEN}NOTE${NORMAL}: overriding default Qt5 architecture directory with ${QT5_ROOT_DIR}/${QT5_ARCH}"
    else
        if [ "${SYSTEM_ARCH}" = "x86_64" ]; then
            QT5_ARCH="gcc_64"
            echo "${GREEN}NOTE${NORMAL}: 64-bit Qt will be used"
        else
            QT5_ARCH="gcc"
            echo "${GREEN}NOTE${NORMAL}: 32-bit Qt will be used"
        fi
        echo "${GREEN}NOTE${NORMAL}: using default Qt5 architecture directory ${QT5_ROOT_DIR}/${QT5_ARCH}"
    fi

    # Qt5 root directory
    QT5_DIR="${QT5_ROOT_DIR}/${QT5_ARCH}"

    # Build-related tools: qmake, lrelease
    QT5_TOOLS_DIR="${QT5_DIR}/bin"

    # Deployment-related directories: Qt5 libraries and plugins
    QT5_LIBS_DIR="${QT5_DIR}/lib"
    QT5_PLUGINS_DIR="${QT5_DIR}/plugins"

    echo "${GREEN}NOTE${NORMAL}: Qt5 tools binaries will be searched in the ${QT5_TOOLS_DIR}"
}

detectBuildToolsDirs()
{
    if [ "${GCC_DIR_SPECIFIED}" = "yes" ]; then
        echo "${GREEN}NOTE${NORMAL}: overriding default GCC directory with ${GCC_DIR}"
    else
        GCC_DIR="/usr/bin"
        echo "${GREEN}NOTE${NORMAL}: using default GCC directory ${GCC_DIR}"
    fi

    if [ "${GNU_MAKE_DIR_SPECIFIED}" = "yes" ]; then
        echo "${GREEN}NOTE${NORMAL}: overriding default GNU make directory with ${GNU_MAKE_DIR}"
    else
        GNU_MAKE_DIR="/usr/bin"
        echo "${GREEN}NOTE${NORMAL}: using default GNU make directory ${GNU_MAKE_DIR}"
    fi
}

detectQt5()
{
    # Try to detect Qt5 using qmake existance check
    if [ ! -f "${QT5_TOOLS_DIR}/${QMAKE}" ]; then
        echo "${RED}ERROR${NORMAL}: Qt5 framework not found."
        echo "Please specify the path directly in the second parameter (~/Qt/5.4). Aborting"
        exit 1
    else
        echo "${GREEN}NOTE${NORMAL}: Qt5 framework was successfully detected:"
        "${QT5_TOOLS_DIR}/${TRANSLATE}" -version
        "${QT5_TOOLS_DIR}/${QMAKE}" -version
        echo
    fi

    # Check other Qt5 directories existance
    if [ ! -d "${QT5_LIBS_DIR}" ]; then
        echo "${RED}ERROR${NORMAL}: Qt5 libraries binaries ${QT5_LIBS_DIR} not found. Aborting"
        exit 1
    fi

    if [ ! -d "${QT5_PLUGINS_DIR}" ]; then
        echo "${RED}ERROR${NORMAL}: Qt5 plugins binaries ${QT5_PLUGINS_DIR} not found. Aborting"
        exit 1
    fi
}

# Check whether the specified program with path ($2) named $1 exists
# If not, displays the error message and aborts script execution
# Otherwise displays program version information and continue execution
detectProgram()
{
    if [ ! -f "$2" ]; then
        echo "${RED}ERROR${NORMAL}: $1 not found. Aborting"
        exit 1
    else
        echo "${GREEN}NOTE${NORMAL}: $1 was successfully detected:"
        "$2" --version
        echo
    fi
}

################################################################################

# Read command line arguments using Bash getopts function, and validate them
parseOptions "$@"
printOptions
validateOptions

detectEnvironment
detectQt5Dirs
detectBuildToolsDirs

echo "---------------------------------------------------------------"

################################################################################
# General build utilities detection

detectQt5

detectProgram "GNU GCC"   "${GCC_DIR}/${GCC}"
detectProgram "GNU make"  "${GNU_MAKE_DIR}/${MAKE}"
detectProgram "GNU strip" "${STRIP_DIR}/${STRIP}"
# detectProgram "chrpath"   "${CHRPATH_DIR}/${CHRPATH}"

################################################################################
# Target-specific build utilities detection (custom tools for RUN, RPM, DEB)

if [ "${BUILD_RUN}" = "yes" ]; then
    detectProgram "makeself" "${MAKESELF_DIR}/${MAKESELF}"
fi

if [ "${BUILD_DEB}" = "yes" ]; then
    detectProgram "dpkg-deb" "${DPKG_DEB_DIR}/${DPKG_DEB}"
    detectProgram "fakeroot" "${FAKEROOT_DIR}/${FAKEROOT}"
    detectProgram "lintian"  "${LINTIAN_DIR}/${LINTIAN}"
fi

if [ "${BUILD_RPM}" = "yes" ]; then
    detectProgram "rpmbuild" "${RPMBUILD_DIR}/${RPMBUILD}"
fi

################################################################################

# Add gcc, make and Qt5 tools to the PATH environment variable
export PATH="${QT5_TOOLS_DIR}:${GCC_DIR}:${GNU_MAKE_DIR}:${PATH}"

echo "Clear build directories..."
rm -Rfv ${OUTPUT_PATH}
rm -Rfv ${BUILD_PATH}
rm -Rfv ${RESULT_PATH}
mkdir ${OUTPUT_PATH}
mkdir ${BUILD_PATH}
mkdir ${RESULT_PATH}

cd ${BUILD_PATH}

echo ${GREEN}
echo
echo "########################################"
echo "#                                      #"
echo "# Compiling...                         #"
echo "#                                      #"
echo "########################################"
echo ${NORMAL}

${TRANSLATE} ${PROJECT}
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

if [ "${SYSTEM_ARCH}" = "x86_64" ]; then
    ${QMAKE} ${PROJECT} CONFIG+=release CONFIG+=x86_64 VERSION=${VERSION} CONFIG+=silent
else
    ${QMAKE} ${PROJECT} CONFIG+=release VERSION=${VERSION} CONFIG+=silent
fi
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

${MAKE} ${MAKE_THREAD_COUNT_OPTION}
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

strip "${BUILD_PATH}/${TARGET_NAME}"
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

# chrpath -d "${BUILD_PATH}/${TARGET_NAME}"
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

echo ${GREEN}
echo
echo "########################################"
echo "#                                      #"
echo "# Deployment...                        #"
echo "#                                      #"
echo "########################################"
echo
echo ${NORMAL}

# Application deployment
# 1) Self-contained bundle
# NOTE: there is no linuxdeployqt tool, sigh

APP_TARGET_DIR="${OUTPUT_PATH}/${COMPANY_NAME}-${TARGET_NAME}-deployed"
APP_TARGET_DIR_RUN="${OUTPUT_PATH}/${COMPANY_NAME}-${TARGET_NAME}"
APP_BIN_TARGET_DIR=${APP_TARGET_DIR}/bin
APP_LIB_TARGET_DIR=${APP_TARGET_DIR}/lib
APP_PLUGINS_TARGET_DIR=${APP_TARGET_DIR}/bin
APP_SHARE_TARGET_DIR=${APP_TARGET_DIR}/share

mkdir "${APP_TARGET_DIR}"
mkdir "${APP_BIN_TARGET_DIR}"
mkdir "${APP_LIB_TARGET_DIR}"
mkdir "${APP_PLUGINS_TARGET_DIR}"
mkdir "${APP_PLUGINS_TARGET_DIR}/bearer"
mkdir "${APP_PLUGINS_TARGET_DIR}/imageformats"
mkdir "${APP_PLUGINS_TARGET_DIR}/platforms"
mkdir "${APP_PLUGINS_TARGET_DIR}/platformthemes"
mkdir "${APP_PLUGINS_TARGET_DIR}/printsupport"
mkdir "${APP_SHARE_TARGET_DIR}"

# Copy viewer executable, launch script and DE integration script
cp "${BUILD_PATH}/${TARGET_NAME}"                               "${APP_BIN_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${LINUX_STUFF_PATH}/${COMPANY_NAME}-${TARGET_NAME}.sh"      "${APP_BIN_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${LINUX_STUFF_PATH}/integrate-to-DE.sh"                     "${APP_BIN_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

# Copy DE integration data
cp "${LINUX_STUFF_PATH}/${COMPANY_NAME}-${TARGET_NAME}.desktop" "${APP_SHARE_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${LINUX_STUFF_PATH}/${COMPANY_NAME}-${TARGET_NAME}-128.png"     "${APP_SHARE_TARGET_DIR}/${COMPANY_NAME}-${TARGET_NAME}.png"
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${LINUX_STUFF_PATH}/${COMPANY_NAME}-${TARGET_NAME}-128.png"     "${APP_SHARE_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${LINUX_STUFF_PATH}/${COMPANY_NAME}-${TARGET_NAME}-64.png"     "${APP_SHARE_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${LINUX_STUFF_PATH}/${COMPANY_NAME}-${TARGET_NAME}-48.png"     "${APP_SHARE_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${LINUX_STUFF_PATH}/${COMPANY_NAME}-${TARGET_NAME}-32.png"     "${APP_SHARE_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${LINUX_STUFF_PATH}/${COMPANY_NAME}-${TARGET_NAME}.xml"     "${APP_SHARE_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
# TODO: appdata.xml

# TODO: determine dependencies (libs and plugins) automatically

# Copy Qt5 shared libraries
cp "${QT5_LIBS_DIR}/libicudata.so.54"           "${APP_LIB_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_LIBS_DIR}/libicui18n.so.54"           "${APP_LIB_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_LIBS_DIR}/libicuuc.so.54"             "${APP_LIB_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_LIBS_DIR}/libQt5Core.so.5"            "${APP_LIB_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_LIBS_DIR}/libQt5DBus.so.5"            "${APP_LIB_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_LIBS_DIR}/libQt5Gui.so.5"             "${APP_LIB_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_LIBS_DIR}/libQt5Network.so.5"         "${APP_LIB_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_LIBS_DIR}/libQt5PrintSupport.so.5"    "${APP_LIB_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_LIBS_DIR}/libQt5Widgets.so.5"         "${APP_LIB_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_LIBS_DIR}/libQt5Svg.so.5"             "${APP_LIB_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_LIBS_DIR}/libQt5WebKitWidgets.so.5"             "${APP_LIB_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_LIBS_DIR}/libQt5MultimediaWidgets.so.5"             "${APP_LIB_TARGET_DIR}"/

cp "${QT5_LIBS_DIR}/libQt5Multimedia.so.5"             "${APP_LIB_TARGET_DIR}"/

cp "${QT5_LIBS_DIR}/libQt5WebKit.so.5"             "${APP_LIB_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_LIBS_DIR}/libQt5Sensors.so.5"             "${APP_LIB_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_LIBS_DIR}/libQt5Positioning.so.5"             "${APP_LIB_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_LIBS_DIR}/libQt5OpenGL.so.5"             "${APP_LIB_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_LIBS_DIR}/libQt5Sql.so.5"             "${APP_LIB_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_LIBS_DIR}/libQt5Quick.so.5"             "${APP_LIB_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_LIBS_DIR}/libQt5Qml.so.5"             "${APP_LIB_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_LIBS_DIR}/libQt5WebChannel.so.5"             "${APP_LIB_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_LIBS_DIR}/libQt5XcbQpa.so.5"             "${APP_LIB_TARGET_DIR}"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

cp "/usr/lib/x86_64-linux-gnu/libzip.so.4"             "${APP_LIB_TARGET_DIR}"/
cp "/usr/lib/i386-linux-gnu/libzip.so.4"             "${APP_LIB_TARGET_DIR}"/
    
cp "/usr/lib/x86_64-linux-gnu/libgstapp-0.10.so.0"             "${APP_LIB_TARGET_DIR}"/
cp "/usr/lib/i386-linux-gnu/libgstapp-1.0.so.0"             "${APP_LIB_TARGET_DIR}"/

cp "/usr/lib/x86_64-linux-gnu/libgstinterfaces-0.10.so.0"             "${APP_LIB_TARGET_DIR}"/
cp "/usr/lib/i386-linux-gnu/libgstinterfaces-0.10.so.0"             "${APP_LIB_TARGET_DIR}"/

cp "/usr/lib/x86_64-linux-gnu/libgstpbutils-0.10.so.0"             "${APP_LIB_TARGET_DIR}"/
cp "/usr/lib/i386-linux-gnu/libgstpbutils-0.10.so.0"             "${APP_LIB_TARGET_DIR}"/

cp "/usr/lib/x86_64-linux-gnu/libgstvideo-0.10.so.0"             "${APP_LIB_TARGET_DIR}"/
cp "/usr/lib/i386-linux-gnu/libgstvideo-0.10.so.0"             "${APP_LIB_TARGET_DIR}"/

cp "/usr/lib/x86_64-linux-gnu/libgstbase-0.10.so.0"             "${APP_LIB_TARGET_DIR}"/
cp "/usr/lib/i386-linux-gnu/libgstbase-0.10.so.0"             "${APP_LIB_TARGET_DIR}"/

cp "/usr/lib/x86_64-linux-gnu/libgstreamer-0.10.so.0"             "${APP_LIB_TARGET_DIR}"/
cp "/usr/lib/i386-linux-gnu/libgstreamer-0.10.so.0"             "${APP_LIB_TARGET_DIR}"/

cp "/usr/lib/x86_64-linux-gnu/libgstreamer-0.10.so.0"             "${APP_LIB_TARGET_DIR}"/
cp "/usr/lib/i386-linux-gnu/libgstreamer-0.10.so.0"             "${APP_LIB_TARGET_DIR}"/

cp "/usr/lib/x86_64-linux-gnu/libgstapp-0.10.so.0"             "${APP_LIB_TARGET_DIR}"/
cp "/usr/lib/i386-linux-gnu/libgstapp-0.10.so.0"             "${APP_LIB_TARGET_DIR}"/

cp "/usr/lib/x86_64-linux-gnu/libgstbase-0.10.so.0"             "${APP_LIB_TARGET_DIR}"/
cp "/usr/lib/i386-linux-gnu/libgstbase-0.10.so.0"             "${APP_LIB_TARGET_DIR}"/

cp "/usr/lib/x86_64-linux-gnu/libgstinterfaces-0.10.so.0"             "${APP_LIB_TARGET_DIR}"/
cp "/usr/lib/i386-linux-gnu/libgstinterfaces-0.10.so.0"             "${APP_LIB_TARGET_DIR}"/

cp "/usr/lib/x86_64-linux-gnu/libgstpbutils-0.10.so.0"             "${APP_LIB_TARGET_DIR}"/
cp "/usr/lib/i386-linux-gnu/libgstpbutils-0.10.so.0"             "${APP_LIB_TARGET_DIR}"/

cp "/usr/lib/x86_64-linux-gnu/libgstvideo-0.10.so.0"             "${APP_LIB_TARGET_DIR}"/
cp "/usr/lib/i386-linux-gnu/libgstvideo-0.10.so.0"             "${APP_LIB_TARGET_DIR}"/

cp "/usr/lib/x86_64-linux-gnu/libstdc++.so.6"             "${APP_LIB_TARGET_DIR}"/
cp "/usr/lib/i386-linux-gnu/libstdc++.so.6"             "${APP_LIB_TARGET_DIR}"/

# Copy Qt5 plugins ################################################################################

# Bearer
cp "${QT5_PLUGINS_DIR}/bearer/libqconnmanbearer.so" "${APP_PLUGINS_TARGET_DIR}/bearer"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_PLUGINS_DIR}/bearer/libqgenericbearer.so" "${APP_PLUGINS_TARGET_DIR}/bearer"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_PLUGINS_DIR}/bearer/libqnmbearer.so"      "${APP_PLUGINS_TARGET_DIR}/bearer"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

# Various image formats support
cp "${QT5_PLUGINS_DIR}/imageformats/libqwebp.so"    "${APP_PLUGINS_TARGET_DIR}/imageformats"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_PLUGINS_DIR}/imageformats/libqdds.so"     "${APP_PLUGINS_TARGET_DIR}/imageformats"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_PLUGINS_DIR}/imageformats/libqgif.so"     "${APP_PLUGINS_TARGET_DIR}/imageformats"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_PLUGINS_DIR}/imageformats/libqicns.so"    "${APP_PLUGINS_TARGET_DIR}/imageformats"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_PLUGINS_DIR}/imageformats/libqico.so"     "${APP_PLUGINS_TARGET_DIR}/imageformats"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_PLUGINS_DIR}/imageformats/libqjp2.so"     "${APP_PLUGINS_TARGET_DIR}/imageformats"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_PLUGINS_DIR}/imageformats/libqjpeg.so"    "${APP_PLUGINS_TARGET_DIR}/imageformats"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_PLUGINS_DIR}/imageformats/libqmng.so"     "${APP_PLUGINS_TARGET_DIR}/imageformats"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_PLUGINS_DIR}/imageformats/libqsvg.so"     "${APP_PLUGINS_TARGET_DIR}/imageformats"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_PLUGINS_DIR}/imageformats/libqtga.so"     "${APP_PLUGINS_TARGET_DIR}/imageformats"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_PLUGINS_DIR}/imageformats/libqtiff.so"    "${APP_PLUGINS_TARGET_DIR}/imageformats"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
cp "${QT5_PLUGINS_DIR}/imageformats/libqwbmp.so"    "${APP_PLUGINS_TARGET_DIR}/imageformats"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

# Platform plugin (application will not launch without it)
cp "${QT5_PLUGINS_DIR}/platforms/libqxcb.so"        "${APP_PLUGINS_TARGET_DIR}/platforms"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

# Qt5 GTK+ compability plugin (enable GTK+ theme support)
cp "${QT5_PLUGINS_DIR}/platformthemes/libqgtk2.so"  "${APP_PLUGINS_TARGET_DIR}/platformthemes"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

# Printing support using CUPS
cp "${QT5_PLUGINS_DIR}/printsupport/libcupsprintersupport.so" "${APP_PLUGINS_TARGET_DIR}/printsupport"/
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
###################################################################################################

# Validate the desktop file
desktop-file-validate "${APP_SHARE_TARGET_DIR}/${PROJECT_NAME}.desktop"
rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

# 1) Self-contained "bundle" which can be executed on any Linux distribution;
#    however, system glibc and libstdc++ versions must be equal or higher then the viewer ones
if [ "${BUILD_RUN}" = "yes" ]; then
    echo "${GREEN}NOTE${NORMAL}: deploying self-contained application bundle (run-file)..."

    cp -a "${APP_TARGET_DIR}"/ "${APP_TARGET_DIR_RUN}"/
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    if [ "${SYSTEM_ARCH}" = "x86_64" ]; then
        BUNDLE_ARCH="amd64"
    else
        BUNDLE_ARCH="i386"
    fi

    BUNDLE_SRC="${APP_TARGET_DIR_RUN}"
    BUNDLE_DST="${OUTPUT_PATH}/${COMPANY_NAME}-${TARGET_NAME}-${VERSION}_${BUNDLE_ARCH}.run"
    BUNDLE_DESC="Elar Corporation NEL RF viewer"
    BUNDLE_EXEC="bin/${COMPANY_NAME}-${TARGET_NAME}.sh"

    ${MAKESELF} --notemp --lsm "${LINUX_STUFF_PATH}/lsmfile.lsm" "${BUNDLE_SRC}" "${BUNDLE_DST}" "${BUNDLE_DESC}" "${BUNDLE_EXEC}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    rm -rf "${APP_TARGET_DIR_RUN}"/
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
fi

# 2) DEB package
if [ "${BUILD_DEB}" = "yes" ]; then
    echo "${GREEN}NOTE${NORMAL}: building Debian/Ubuntu DEB package..."

    # Deb auxiliary dirs
    DEB_ROOT_DIR="${OUTPUT_PATH}/debian"
    DEB_CONTROL_DIR="${DEB_ROOT_DIR}/DEBIAN"

    # Deb auxiliary variables
    DEB_RELEASE=1

    # Application dirs
    BIN_DEB_TARGET_DIR="${DEB_ROOT_DIR}/usr/bin"

    USER_LIB_DIR="${DEB_ROOT_DIR}/usr/lib"
    if [ "${SYSTEM_ARCH}" = "x86_64" ]; then
        if [ "${MULTIARCH_SUPPORT}" = "yes" ]; then
            USER_LIB_DIR="${DEB_ROOT_DIR}/usr/lib/${MULTIARCH_DIR}"
        else
            USER_LIB_DIR="${DEB_ROOT_DIR}/usr/lib64"
        fi
    fi
    LIB_DEB_TARGET_DIR="${USER_LIB_DIR}/${PROJECT_NAME}"
    PLUGINS_DEB_TARGET_DIR="${LIB_DEB_TARGET_DIR}/plugins"

    SHARE_DEB_TARGET_DIR="${DEB_ROOT_DIR}/usr/share"
    # Documentation dirs
    MAN_DEB_TARGET_DIR="${SHARE_DEB_TARGET_DIR}/man/man1"
    DOC_DEB_TARGET_DIR="${SHARE_DEB_TARGET_DIR}/doc/${PROJECT_NAME}"

    mkdir "${DEB_ROOT_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    mkdir "${DEB_CONTROL_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    mkdir -p "${BIN_DEB_TARGET_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    mkdir -p "${LIB_DEB_TARGET_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    mkdir -p "${PLUGINS_DEB_TARGET_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    mkdir -p "${SHARE_DEB_TARGET_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    mkdir -p "${MAN_DEB_TARGET_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    mkdir -p "${DOC_DEB_TARGET_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    # control file: describe dependencies, version and much more application properties
    cp "${LINUX_STUFF_PATH}/debian-control" "${DEB_CONTROL_DIR}/control"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    # Pre- and post-install actions
    cp "${LINUX_STUFF_PATH}/debian-prerm" "${DEB_CONTROL_DIR}/prerm"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    cp "${LINUX_STUFF_PATH}/debian-postrm" "${DEB_CONTROL_DIR}/postrm"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    cp "${LINUX_STUFF_PATH}/debian-postinst" "${DEB_CONTROL_DIR}/postinst"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    # Documentation
    cp "${LINUX_STUFF_PATH}/debian-${TARGET_NAME}.1" "${MAN_DEB_TARGET_DIR}/${PROJECT_NAME}.1"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    cp "${LINUX_STUFF_PATH}/debian-copyright" "${DOC_DEB_TARGET_DIR}/copyright"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    cp "${LINUX_STUFF_PATH}/debian-changelog" "${DOC_DEB_TARGET_DIR}/changelog"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    # Copy application files
    echo "${GREEN}INFO${NORMAL}: copy binaries..."
    cp -a "${APP_BIN_TARGET_DIR}"/.       "${BIN_DEB_TARGET_DIR}"/
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    # Rename some binaries to match Linux requirements
    rm -f "${BIN_DEB_TARGET_DIR}/integrate-to-DE.sh"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    rm -f "${BIN_DEB_TARGET_DIR}/${PROJECT_NAME}.sh"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    cp "${LINUX_STUFF_PATH}/debian-${PROJECT_NAME}.sh" "${BIN_DEB_TARGET_DIR}/${PROJECT_NAME}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    mv "${BIN_DEB_TARGET_DIR}/${TARGET_NAME}" "${BIN_DEB_TARGET_DIR}/${PROJECT_NAME}-binary"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    
    echo "${GREEN}INFO${NORMAL}: copy libraries..."
    cp -a "${APP_LIB_TARGET_DIR}"/.       "${LIB_DEB_TARGET_DIR}"/
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    
    echo "${GREEN}INFO${NORMAL}: copy plugins..."
    cp -a "${APP_PLUGINS_TARGET_DIR}"/.   "${PLUGINS_DEB_TARGET_DIR}"/
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    
    echo "${GREEN}INFO${NORMAL}: copy shared data..."
    cp -a "${APP_SHARE_TARGET_DIR}"/.     "${SHARE_DEB_TARGET_DIR}/${PROJECT_NAME}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    # Ajust rights
    echo
    echo "${GREEN}NOTE${NORMAL}: correcting rights..."
    ${FAKEROOT} chmod -R 755 "${DEB_ROOT_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    # NOTE: lintian shlib-with-executable-bit error fix
    find "${LIB_DEB_TARGET_DIR}" -type f -print0 | xargs -0 chmod 644
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    # executable-in-usr-share-doc
    # executable-manpage
    find "${SHARE_DEB_TARGET_DIR}" -type f -print0 | xargs -0 chmod 644
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    # Strip
    echo "${GREEN}NOTE${NORMAL}: stripping shared libraries..."
    ${FAKEROOT} ${STRIP} "${LIB_DEB_TARGET_DIR}/libicudata.so.54"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    ${FAKEROOT} ${STRIP} "${LIB_DEB_TARGET_DIR}/libicui18n.so.54"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    ${FAKEROOT} ${STRIP} "${LIB_DEB_TARGET_DIR}/libicuuc.so.54"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    echo "${GREEN}NOTE${NORMAL}: updating version number field in spec..."
    sed -i "s/VERSION_STUB/${VERSION}/g" "${DEB_CONTROL_DIR}/control"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    echo "${GREEN}NOTE${NORMAL}: updating version number field in changelog..."
    sed -i "s/VERSION_STUB/${VERSION}/g" "${DOC_DEB_TARGET_DIR}/changelog"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    echo "${GREEN}NOTE${NORMAL}: updating installed-size field in spec..."
    # NOTE: du shows size AND specified directory path - we need only the first one
    INSTALLED_SIZE_VALUE=$( du -sx --exclude "DEBIAN" "${DEB_ROOT_DIR}" | cut -f1 )
    sed -i "s/INSTALLED_SIZE_STUB/${INSTALLED_SIZE_VALUE}/g" "${DEB_CONTROL_DIR}/control"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    echo "${GREEN}NOTE${NORMAL}: updating package architecture field in spec..."
    if [ "${SYSTEM_ARCH}" = "x86_64" ]; then
        DEB_ARCH="amd64"

        sed -i "s/: i386/: amd64/g" "${DEB_CONTROL_DIR}/control"
        rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
        
        if [ "${MULTIARCH_SUPPORT}" = "yes" ]; then
            sed -i "s:lib/:lib/${MULTIARCH_DIR}/:g" "${BIN_DEB_TARGET_DIR}/${PROJECT_NAME}"
            rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
        else
            sed -i "s:lib/:lib64/:g" "${BIN_DEB_TARGET_DIR}/${PROJECT_NAME}"
            rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
        fi
    else
        DEB_ARCH="i386"
    fi

    echo "${GREEN}NOTE${NORMAL}: packing man pages..."
    gzip --best "${MAN_DEB_TARGET_DIR}/${PROJECT_NAME}.1"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    cp "${MAN_DEB_TARGET_DIR}/${PROJECT_NAME}.1.gz" "${MAN_DEB_TARGET_DIR}/${PROJECT_NAME}-binary.1.gz"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    echo "${GREEN}NOTE${NORMAL}: packing changelog..."
    gzip --best "${DOC_DEB_TARGET_DIR}/changelog"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
#    cp "${DOC_DEB_TARGET_DIR}/changelog.gz" "${DOC_DEB_TARGET_DIR}/changelog.Debian.gz"

    # Build DEB package
    ${FAKEROOT} ${DPKG_DEB} --build "${DEB_ROOT_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    DEB_FILE_NAME="${OUTPUT_PATH}/${PROJECT_NAME}_${VERSION}-${DEB_RELEASE}_${DEB_ARCH}.deb"
    mv "${OUTPUT_PATH}/debian.deb" "${DEB_FILE_NAME}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    
    # Validate DEB package
    ${LINTIAN} --color auto "${DEB_FILE_NAME}"
#    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
fi

# 3) RPM package
if [ "${BUILD_RPM}" = "yes" ]; then
    # RPM auxiliary dirs
    RPM_ROOT_DIR="${OUTPUT_PATH}/rpmbuild"
    RPM_SOURCES_DIR="${RPM_ROOT_DIR}/SOURCES"
    RPM_SPECS_DIR="${RPM_ROOT_DIR}/SPECS"
    RPM_RPMS_DIR="${RPM_ROOT_DIR}/RPMS"
    RPM_SRPMS_DIR="${RPM_ROOT_DIR}/SRPMS"
    RPM_BUILD_DIR="${RPM_ROOT_DIR}/BUILD"
    RPM_TMP_DIR="${RPM_ROOT_DIR}/tmp"
    # RPM aux variables
    RELEASE_NUMBER=1
    # Application dirs
    RPM_TARBALL_DIR="${RPM_ROOT_DIR}/${PROJECT_NAME}-${VERSION}"
    BIN_RPM_TARGET_DIR="${RPM_TARBALL_DIR}/usr/bin"
    LIB_RPM_TARGET_DIR="${RPM_TARBALL_DIR}/usr/lib/${PROJECT_NAME}"
    PLUGINS_RPM_TARGET_DIR="${LIB_RPM_TARGET_DIR}/plugins"
    ROOT_SHARE_RPM_TARGET_DIR="${RPM_TARBALL_DIR}/usr/share"
    SHARE_RPM_TARGET_DIR="${ROOT_SHARE_RPM_TARGET_DIR}/${PROJECT_NAME}"
    # Documentation dirs
    MAN_RPM_TARGET_DIR="${ROOT_SHARE_RPM_TARGET_DIR}/man/man1"
    DOC_RPM_TARGET_DIR="${ROOT_SHARE_RPM_TARGET_DIR}/doc/${PROJECT_NAME}"

    # 1) Setup RPM build environment
    mkdir "${RPM_ROOT_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    mkdir "${RPM_SOURCES_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    mkdir "${RPM_SPECS_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    mkdir "${RPM_RPMS_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    mkdir "${RPM_SRPMS_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    mkdir "${RPM_BUILD_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    mkdir "${RPM_TMP_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

rm ~/.rpmmacros
cat <<EOF >~/.rpmmacros
%_topdir   $RPM_ROOT_DIR
%_tmppath  %{_topdir}/tmp
EOF

    # Move to RPM build directory
    cd "${RPM_ROOT_DIR}"

    # 2) Create the tarball of project
    mkdir -p "${RPM_TARBALL_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    mkdir -p "${BIN_RPM_TARGET_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    mkdir -p "${LIB_RPM_TARGET_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    mkdir -p "${PLUGINS_RPM_TARGET_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    mkdir -p "${SHARE_RPM_TARGET_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    mkdir -p "${MAN_RPM_TARGET_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    mkdir -p "${DOC_RPM_TARGET_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    # Binaries
    echo "${GREEN}INFO${NORMAL}: copy binaries..."
    install -m 755 "${APP_BIN_TARGET_DIR}/${TARGET_NAME}"  "${BIN_RPM_TARGET_DIR}"/
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

#    install -m 755 "${APP_BIN_TARGET_DIR}/${PROJECT_NAME}.sh"  "${BIN_RPM_TARGET_DIR}"/
    install -m 755 "${LINUX_STUFF_PATH}/debian-${COMPANY_NAME}-${TARGET_NAME}.sh" "${BIN_RPM_TARGET_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    mv "${BIN_RPM_TARGET_DIR}/${TARGET_NAME}" "${BIN_RPM_TARGET_DIR}/${PROJECT_NAME}-binary"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    mv "${BIN_RPM_TARGET_DIR}/debian-${COMPANY_NAME}-${TARGET_NAME}.sh" "${BIN_RPM_TARGET_DIR}/${PROJECT_NAME}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    # Libraries
    echo "${GREEN}INFO${NORMAL}: copy libraries..."
    cp -a "${APP_LIB_TARGET_DIR}"/.       "${LIB_RPM_TARGET_DIR}"/
    # Plugins
    echo "${GREEN}INFO${NORMAL}: copy plugins..."
    cp -a "${APP_PLUGINS_TARGET_DIR}"/.   "${PLUGINS_RPM_TARGET_DIR}"/
    # Shared data
    echo "${GREEN}INFO${NORMAL}: copy shared data..."
    cp -a "${APP_SHARE_TARGET_DIR}"/.     "${SHARE_RPM_TARGET_DIR}"
    
    echo "${GREEN}INFO${NORMAL}: create binary tarball..."
    RPM_TARBALL_PATH="${RPM_ROOT_DIR}/${PROJECT_NAME}-${VERSION}.tar.gz"
    tar -zcvf "${RPM_TARBALL_PATH}" "${PROJECT_NAME}-${VERSION}"/
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi
    cp "${RPM_TARBALL_PATH}" "${RPM_SOURCES_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    echo "${GREEN}INFO${NORMAL}: copy specification file..."
    cp "${LINUX_STUFF_PATH}/${PROJECT_NAME}.spec" "${RPM_SPECS_DIR}"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    # Replace version in spec to the user-specified one
    sed -i "s/VERSION_STUB/${VERSION}/g" "${RPM_SPECS_DIR}/${PROJECT_NAME}.spec"
    sed -i "s/RELEASE_STUB/${RELEASE_NUMBER}/g" "${RPM_SPECS_DIR}/${PROJECT_NAME}.spec"

    echo "${GREEN}INFO${NORMAL}: calling rpmbuild utility..."
    echo "${GREEN}${RPM_ROOT_DIR}${NORMAL}"
    cd "${RPM_ROOT_DIR}"

    ${RPMBUILD} -ba "${RPM_SPECS_DIR}/${PROJECT_NAME}.spec"
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    echo "${GREEN}INFO${NORMAL}: copy final RPM package file to the output directory..."
    # RPM package name: e.g. elar-nelrfviewer-1.0.7-1.x86_64.rpm
    RPM_FILE_NAME="${PROJECT_NAME}-${VERSION}-${RELEASE_NUMBER}.${SYSTEM_ARCH}.rpm"
    cp "${RPM_RPMS_DIR}/${SYSTEM_ARCH}/${RPM_FILE_NAME}" "${OUTPUT_PATH}"/
    rc=$?; if [ $rc != 0 ]; then processFailure $rc; fi

    # TODO: implement RPM package validation - like DEB one does
#    rpmlint

    # Back to home, sweet home
    cd "${SOURCE_PATH}"

    # Cleanup
    rm -rf "${RPM_ROOT_DIR}"/
    rm -f ~/.rpmmacros    
fi

cd $START_PATH

# TODO: implement custom OpenSSL libraries
#cp "${SSL_PATH}"/* "${RESULT_PATH}"/*
#rc=$?; if [[ $rc != 0 ]]; then processFailure $rc; fi

# Remove the temponary directories
rm -Rf "${BUILD_PATH}"
rm -Rf "${RESULT_PATH}"

# Remove the 'read-only' attribute (Perforce arteface)
chmod -R +w "${APP_TARGET_DIR}"
rc=$?; if [[ $rc != 0 ]]; then processFailure $rc; fi

# Ensure viewer binaries and scripts have executable bit
chmod -R +x "${APP_BIN_TARGET_DIR}"

processSuccess

