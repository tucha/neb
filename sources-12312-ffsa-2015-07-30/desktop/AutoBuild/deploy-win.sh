QMAKE="/Users/Tucher/mxe/usr/i686-w64-mingw32.shared/qt5/bin/qmake"

START_PATH=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

BUILD_PATH=${START_PATH}/Build-win/
RESULT_PATH=${START_PATH}/Result-win/
OUTPUT_PATH=${START_PATH}/Output-win/

rm -rf ${BUILD_PATH}
rm -rf ${RESULT_PATH}
rm -rf ${OUTPUT_PATH}
mkdir ${BUILD_PATH}
mkdir ${RESULT_PATH}
cd ${BUILD_PATH}

PROJECT=${START_PATH}/../Sources/viewer.pro
VERSION="3.0.0"
${QMAKE} ${PROJECT} -spec win32-g++ CONFIG+=silent "VERSION=${VERSION}"
make -j8
cp ${BUILD_PATH}/release/viewer.exe ${RESULT_PATH}/viewer.exe
cp -r ${START_PATH}/win-deps/* ${RESULT_PATH}

cd ${START_PATH}
wine ../Tools/InnoSetup5/ISCC.exe installerCopy.iss
