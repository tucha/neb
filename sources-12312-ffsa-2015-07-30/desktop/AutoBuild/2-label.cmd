@echo off


if "%1" equ "" (
	echo Usage: 2-label.cmd ^<build-id^> [^<project-id^>]
	exit /b 1
) else (
	set BUILD_ID=%1
	set PROJECT_ID=%2
)


echo.
echo ########################################
echo #                                      #
echo # Labeling...                          #
echo #                                      #
echo ########################################


set LABEL=ElarViewerDesktop.%BUILD_ID%
if "%PROJECT_ID%" neq "" (
	set LABEL=%PROJECT_ID%.%LABEL%
)

p4 tag -l %LABEL% "..\..."


echo.
echo ########################################
echo #                                      #
echo # Labeling done                        #
echo #                                      #
echo ########################################
exit /b 0
