@echo off

if "%1" equ "" (
	echo "Please specify version: you can set exact value (1.2.3)"
	exit /b 1
)

echo.
echo ########################################
echo #                                      #
echo # Compiling...                         #
echo #                                      #
echo ########################################

set VERSION=%1
set START_PATH=%~dp0
set TOOLS_PATH=%START_PATH%..\Tools\
set BIN_1=%TOOLS_PATH%Qt\5.3\mingw482_32\bin\
set BIN_3=%TOOLS_PATH%Qt\Tools\mingw482_32\bin\
set BIN_2=%TOOLS_PATH%Qt\Tools\QtCreator\bin\
set BIN_4=%TOOLS_PATH%
set BIN_5=%TOOLS_PATH%InnoSetup5\
set PATH=%BIN_1%;%BIN_2%;%BIN_3%;%BIN_4%;%BIN_5%;%PATH%

set TRANSLATE=lrelease.exe
set QMAKE=qmake.exe
set MINGW_MAKE=mingw32-make.exe
set DEPLOY=windeployqt.exe
set INNO_SETUP=iscc.exe
set FIND_AND_REPLACE=fnr.exe

set PROJECT=%START_PATH%..\Sources\viewer.pro
set BUILD_PATH=%START_PATH%Build\
set RESULT_PATH=%START_PATH%Result\
set OUTPUT_PATH=%START_PATH%Output\
set SSL_PATH=%TOOLS_PATH%OpenSSL\
rem SIGN=signtool.exe sign /n "Protection Technology, Ltd." -d "StarForce Viewer" -t http://timestamp.verisign.com/scripts/timstamp.dll
set SIGN=signtool.exe sign /n "ELAR.TEST" -d "StarForce Viewer" -t http://timestamp.verisign.com/scripts/timstamp.dll
set INSTALLER=installer.iss
set INSTALLER_COPY=installerCopy.iss

rmdir /S /Q %OUTPUT_PATH%
rmdir /S /Q %BUILD_PATH%
rmdir /S /Q %RESULT_PATH%
mkdir %OUTPUT_PATH%
mkdir %BUILD_PATH%
mkdir %RESULT_PATH%
cd %BUILD_PATH%

%TRANSLATE% %PROJECT%
if errorlevel 1 goto failure
%QMAKE% %PROJECT% VERSION=%VERSION%
if errorlevel 1 goto failure
%MINGW_MAKE%
if errorlevel 1 goto failure
copy %BUILD_PATH%release\viewer.exe %RESULT_PATH%viewer.exe
if errorlevel 1 goto failure

cd %START_PATH%

%DEPLOY% %RESULT_PATH%
if errorlevel 1 goto failure
copy %SSL_PATH%* %RESULT_PATH%*
if errorlevel 1 goto failure

copy /y %INSTALLER% %INSTALLER_COPY%
%FIND_AND_REPLACE% --cl --dir %START_PATH% --fileMask %INSTALLER_COPY% --find "AppVersion=1.0.0" --replace "AppVersion=%VERSION%"
if errorlevel 1 goto failure

%SIGN% %RESULT_PATH%viewer.exe
if errorlevel 1 goto failure
%INNO_SETUP% %INSTALLER_COPY%
if errorlevel 1 goto failure
%SIGN% %OUTPUT_PATH%setup.exe
if errorlevel 1 goto failure

del %INSTALLER_COPY%
rmdir /S /Q %BUILD_PATH%
rmdir /S /Q %RESULT_PATH%

attrib -r "Output\*" /s /d
if errorlevel 1 goto failure

goto success


:success
echo.
echo ########################################
echo #                                      #
echo # Compilation done                     #
echo #                                      #
echo ########################################
cd %START_PATH%
exit /b 0
:failure
echo.
echo ########################################
echo #                                      #
echo # Compilation FAILED                   #
echo #                                      #
echo ########################################
cd %START_PATH%
exit /b 1
