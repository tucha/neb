#!/bin/bash
# Viewer build script for Mac OS X 10.7+

clear

# if [[ $EUID -ne 0 ]]; then
    # echo "This script must be run as root!"
    # echo "E.g.: sudo ./3-compile-mac.sh -v 1.0.7"
    # exit 1
# fi
NON_ROOT_USER=$( who am i | awk '{print $1}' )

VERSION=""
SIGNING_ID_NAME=""
VM_DETECT="yes"
QT5_DIR_SPECIFIED="no"
QT5_DIR=""
XCODE_PATH_SPECIFIED="no"
XCODE_PATH=""

# Reset the getopts index
# NOTE: this is required to call build script more than once time
OPTIND=0

# Read command line arguments using Bash getopts function
echo "---------------------------------------------------------------"
while getopts ":v:i:p:q:x:" opt; do
  case $opt in
    v)
      echo "Aplication bundle version: $OPTARG"
      VERSION=$OPTARG
      ;;
    i)
      echo "Application bundle signing identity: $OPTARG"
      SIGNING_ID_NAME=$OPTARG
      ;;
    p)
      echo "PC emulator detection (VMWare, VirtualBox): $OPTARG"
      VM_DETECT=$OPTARG
      ;;
    q)
      echo "Qt5 directory: $OPTARG"
      QT5_DIR_SPECIFIED=yes
      QT5_DIR=$OPTARG
      ;;
    x)
      echo "XCode IDE application bundle path: $OPTARG"
      XCODE_PATH_SPECIFIED=yes
      XCODE_PATH=$OPTARG
      ;;
    \?)
      echo "Invalid option: -$OPTARG"
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument."
      exit 1
      ;;
  esac
done
echo "---------------------------------------------------------------"

function usage {
    echo "---------------------------------------------------------------"
    echo "USAGE: ./3-compile-mac.sh -v=<version> [-i=<identity>] [-p=vm detection flag] [-q=<Qt5 dir>] [-x=<XCode path>]"
    echo "       version (required): application bundle version; you should set exact value (1.2.3)"
    echo "       identity (optional): signing identity name to sign the application bundle with ('Elar OS X Developer')"
    echo "       vm detection flag (optional): whether to enable PC emulators (VMWare, VirtualBox) detection code (true or false)"
    echo "       Qt5 dir (optional): path to the Qt5 directory (Users/alex/Qt5.4.1/5.4)"
    echo "       XCode path (optional): path to the XCode IDE (/Applications/XCode.app)"
    echo
    echo "DEFAULTS:"
    echo "       identity is empty: application bundle will not be signed;"
    echo "       vm detection flag is not set: PC emulator detection code will not be compiled;"
    echo "       Qt5 dir: points to the Qt 5.4.1 installed using default path (~/Qt5.4.1/5.4)"
    echo "       XCode path: points to standard Appp"
    echo "---------------------------------------------------------------"
    exit 1
}

if [ -n "$VERSION" ]; then
    echo "NOTE: using version $VERSION"
else
    usage
    exit 0
fi

CUSTOM_QMAKE_VARS=""
if [ "$VM_DETECT" == "yes" ]; then
    CUSTOM_QMAKE_VARS="CONFIG+=PCEMULATORDETECTION"
    echo "NOTE: pc emulator detection was enabled"
else
    echo "WARNING: PC emulator detection was disabled!"
    echo "         Use this build for the testing purposes only"
fi

if [ "${QT5_DIR_SPECIFIED}" == "yes" ]; then
    if [ ! -d "${QT5_DIR}" ]; then
        echo "ERROR: user-specified Qt5 directory ${QT5_DIR} does not exists"
        exit 1
    else
        echo "NOTE: overriding default Qt5 directory with ${QT5_DIR}"
    fi
else
    # QT5_DIR="/Users/Tucher/Qt5.7/5.5"
     QT5_DIR="/Users/Tucher/qt5.7-mac-build/"
    echo "NOTE: using default Qt5 directory $QT5_DIR"
fi

# QT5_TOOLS_DIR="${QT5_DIR}/clang_64/bin"
QT5_TOOLS_DIR="${QT5_DIR}/bin"
echo "NOTE: Qt5 tools will be searched in the ${QT5_TOOLS_DIR}"

if [ "${XCODE_PATH_SPECIFIED}" == "yes" ]; then
    if [ ! -d "${XCODE_PATH}" ]; then
        echo "ERROR: user-specified XCode IDE path ${XCODE_PATH} does not exists"
        exit 1
    else
        echo "NOTE: overriding default XCode IDE path with ${XCODE_PATH}"
    fi
else
    XCODE_PATH="/Applications/Xcode.app"
    echo "NOTE: using default XCode IDE path ${XCODE_PATH}"
fi

echo "---------------------------------------------------------------"

START_PATH=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

function processFailure {
    echo
    echo "########################################"
    echo "#                                      #"
    echo "# Compilation FAILED                   #"
    echo "#                                      #"
    echo "########################################"
    cd ${START_PATH}
    exit 1
}

function processSuccess {
    echo
    echo "########################################"
    echo "#                                      #"
    echo "# Compilation done                     #"
    echo "#                                      #"
    echo "########################################"
    cd ${START_PATH}    
    exit 0
}

TOOLS_PATH="${START_PATH}/../Tools"
PLIST_BUDDY="/usr/libexec/PlistBuddy"

# Try to detect Qt5
if [ ! -d "${QT5_TOOLS_DIR}" ]; then
    echo "ERROR: Qt5 framework not found! Please specify its path directly in second parameter, like ~/Qt5.4.1/5.4"
    exit 1
fi

# Try to detect XCode
if [ ! -d "${XCODE_PATH}" ]; then
    echo "ERROR: XCode IDE not found!"
    exit 1
fi

# Try to detect PlistBuddy
if [ ! -f "${PLIST_BUDDY}" ]; then
    echo "PlistBuddy tool was not found!"
    exit 1
fi

# Setup path: make utility
MAKE_TOOLS_DIR="${XCODE_PATH}/Contents/Developer/usr/bin"
    if [ ! -d "${MAKE_TOOLS_DIR}" ]; then
    echo "ERROR: make utililies directory not found!"
    exit 1
fi

# Setup path: clang compiler
COMPILER_TOOLS_DIR="${XCODE_PATH}/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin"
if [ ! -d "${COMPILER_TOOLS_DIR}" ]; then
    echo "ERROR: clang compiler directory not found!"
    exit 1
fi

# Add make, clang, and Qt5 tools to the PATH environment variable
export PATH=${QT5_TOOLS_DIR}:${MAKE_TOOLS_DIR}:${COMPILER_TOOLS_DIR}:${PATH}

TRANSLATE=lrelease
QMAKE=qmake
MAKE=make

TARGET_NAME="НЭБ РФ"

SOURCES_PATH=${START_PATH}/../Sources
PROJECT=${START_PATH}/../Sources/viewer.pro
BUILD_PATH=${START_PATH}/Build-mac/
RESULT_PATH=${START_PATH}/Result-mac/
OUTPUT_PATH=${START_PATH}/Output-mac/

if [ ! -f ${PROJECT} ]; then
    echo "ERROR: qmake project file was not found"
    exit 1
fi

echo "Clear build directories..."
rm -Rfv ${OUTPUT_PATH}
rm -Rfv ${BUILD_PATH}
rm -Rfv ${RESULT_PATH}
mkdir ${OUTPUT_PATH}
mkdir ${BUILD_PATH}
mkdir ${RESULT_PATH}

# FIXME: qmake bug workaround, disable shadow build
# BUILD_PATH=${SOURCES_PATH}

cd ${BUILD_PATH}

echo
echo "########################################"
echo "#                                      #"
echo "# Compiling...                         #"
echo "#                                      #"
echo "########################################"

${MAKE} distclean
# rc=$?; if [[ $rc != 0 ]]; then processFailure $rc; fi
rm -Rfv "${BUILD_PATH}/${TARGET_NAME}.app"
rc=$?; if [[ $rc != 0 ]]; then processFailure $rc; fi
rm -fv "${BUILD_PATH}/${TARGET_NAME}.dmg"
rc=$?; if [[ $rc != 0 ]]; then processFailure $rc; fi

# Replace version string in the Info.plist file
# NOTE: the original one will be saved as copy
cp "${SOURCES_PATH}/ViewerInfo.plist" "${SOURCES_PATH}/ViewerInfo.plist.backup"
rc=$?; if [[ $rc != 0 ]]; then processFailure $rc; fi
chmod -v +w "${SOURCES_PATH}/ViewerInfo.plist"
rc=$?; if [[ $rc != 0 ]]; then processFailure $rc; fi
${PLIST_BUDDY} -c "Set :CFBundleShortVersionString ${VERSION}" "${SOURCES_PATH}/ViewerInfo.plist"
rc=$?; if [[ $rc != 0 ]]; then processFailure $rc; fi

${TRANSLATE} ${PROJECT}
rc=$?; if [[ $rc != 0 ]]; then processFailure $rc; fi

${QMAKE} ${PROJECT} -r -spec macx-clang CONFIG+=x86_64 CONFIG+=release CONFIG+=silent "VERSION=${VERSION}" APPCERT="${SIGNING_ID_NAME}" ${CUSTOM_QMAKE_VARS}
rc=$?; if [[ $rc != 0 ]]; then processFailure $rc; fi

${MAKE} -j8
rc=$?; if [[ $rc != 0 ]]; then processFailure $rc; fi


macdeployqt 'НЭБ РФ.app' -codesign="${SIGNING_ID_NAME}" -verbose=2;

echo "Copying application bundle to the output directory..."
cp -R "${BUILD_PATH}/${TARGET_NAME}.app" "${OUTPUT_PATH}/"
rc=$?; if [[ $rc != 0 ]]; then processFailure $rc; fi



echo
echo "Check application bundle signature..."
codesign --verify --verbose "${OUTPUT_PATH}/${TARGET_NAME}.app"
rc=$?; if [[ $rc != 0 ]]; then processFailure $rc; fi
codesign -vvv -d "${OUTPUT_PATH}/${TARGET_NAME}.app"
rc=$?; if [[ $rc != 0 ]]; then processFailure $rc; fi
spctl -a -v "${OUTPUT_PATH}/${TARGET_NAME}.app"
rc=$?; if [[ $rc != 0 ]]; then processFailure $rc; fi  
echo
echo

cd "${START_PATH}/dmg/yoursway-create-dmg"

echo "Creating disk image installer (DMG)..."
ICON_PATH="${SOURCES_PATH}/icons/ViewerMacIcon.icns"
BCND_PATH="${SOURCES_PATH}/icons/viewerDmgBackground.png"
sudo "${START_PATH}/dmg/yoursway-create-dmg/create-dmg" --volname "${TARGET_NAME}" \
                  --volicon "${ICON_PATH}" \
                  --background "${BCND_PATH}" \
                  --window-size 400 220 \
                  --icon-size 96 \
                  --icon "${TARGET_NAME}.app" 96 96 \
                  --hide-extension "${TARGET_NAME}.app" \
                  --app-drop-link 300 90 \
                  "TEST.dmg" \
                  "${OUTPUT_PATH}"/
rc=$?; if [[ $rc != 0 ]]; then processFailure $rc; fi

echo "Copying disk image installer (DMG) to the output directory..."
mv "TEST.dmg" "${OUTPUT_PATH}/${TARGET_NAME}.dmg"
rc=$?; if [[ $rc != 0 ]]; then processFailure $rc; fi
sudo chown "${NON_ROOT_USER}" "${OUTPUT_PATH}/${TARGET_NAME}.dmg"
rc=$?; if [[ $rc != 0 ]]; then processFailure $rc; fi
sudo chown -R "${NON_ROOT_USER}" "${OUTPUT_PATH}/${TARGET_NAME}.app"
rc=$?; if [[ $rc != 0 ]]; then processFailure $rc; fi

echo
echo "Sign application DMG..."

codesign --force --sign "${SIGNING_ID_NAME}" --verbose "${OUTPUT_PATH}/${TARGET_NAME}.dmg"

rc=$?; if [[ $rc != 0 ]]; then processFailure $rc; fi
echo

echo
echo "Check application DMG signature..."
# codesign --verify --verbose "${OUTPUT_PATH}/${TARGET_NAME}.dmg"
spctl -a -t open --context context:primary-signature -v "${OUTPUT_PATH}/${TARGET_NAME}.dmg"
rc=$?; if [[ $rc != 0 ]]; then processFailure $rc; fi
# codesign -vvv -d "${OUTPUT_PATH}/${TARGET_NAME}.dmg"
# rc=$?; if [[ $rc != 0 ]]; then processFailure $rc; fi
# spctl -a -v "${OUTPUT_PATH}/${TARGET_NAME}.dmg"
# rc=$?; if [[ $rc != 0 ]]; then processFailure $rc; fi  
echo
echo

# Cleanup
# NOTE: for shadow build this will not be needed
${MAKE} distclean
rm -fv "${BUILD_PATH}/${TARGET_NAME}.dmg"

cd ${START_PATH}

# Remove the temponary directories
# rm -Rfv "${BUILD_PATH}"
rm -Rfv "${RESULT_PATH}"

# Remove the 'read-only' attribute
chmod -Rv +w "${OUTPUT_PATH}"
rc=$?; if [[ $rc != 0 ]]; then processFailure $rc; fi

# Restore original Apple manifest file
rm -fv "${SOURCES_PATH}/ViewerInfo.plist"
cp "${SOURCES_PATH}/ViewerInfo.plist.backup" "${SOURCES_PATH}/ViewerInfo.plist"
chmod -v -w "${SOURCES_PATH}/ViewerInfo.plist"

processSuccess
